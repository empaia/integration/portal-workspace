import vFile from '../../version.json';
export const environment = {
  production: true,
  appVersion: vFile.version,
};
