import { ChangeDetectionStrategy, Component, ElementRef, inject, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, ValidationErrors, Validators } from '@angular/forms';
import { CustomValidators, SecurePasswordValidation } from '@services/form-validator/formValidators';
import { KcContextProviderService } from '@services/kc-context-provider/kcContextProvider.service';
import { FORM_LENGTH } from '@services/form-validator/formValidators';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UpdatePasswordComponent {

  @ViewChild('updatePasswordFormHTMLElement', {
    read: ElementRef,
    static: true,
  }) updatePasswordFormHTMLElement!: ElementRef;

  protected readonly MAX_PASSWORD_LENGTH = FORM_LENGTH.MAX_PASSWORD_LENGTH;

  private kcContext = inject(KcContextProviderService).getUpdatePasswordContext();
  private fb = inject(FormBuilder);
  updatePasswordForm = this.fb.nonNullable.group({
    // email field is hidden from user and just used for validation
    email: [this.email],
    password: ['',
      [
        Validators.required,
        Validators.maxLength(this.MAX_PASSWORD_LENGTH),
        CustomValidators.PasswordStrengthValidator
      ],
    ],
    passwordConfirm: ['',
      [
        Validators.required,
        Validators.maxLength(this.MAX_PASSWORD_LENGTH)
      ]
    ],
  }, {
    validators: [
      CustomValidators.MatchValidator('password', 'passwordConfirm'),
      CustomValidators.NoMatchValidator('email', 'password')
    ]
  });

  get updateAction(): string {
    // keycloak stores the update action url inside login action...
    return this.kcContext?.url.loginAction;
  }

  // returns the email address in our case
  get email(): string {
    // keycloak stores the update action url inside login action...
    return this.kcContext?.username;
  }

  get password(): FormControl<string> {
    return this.updatePasswordForm.controls.password;
  }

  get passwordConfirm(): FormControl<string> {
    return this.updatePasswordForm.controls.passwordConfirm;
  }
  get passwordMatchError(): boolean {
    // we need to merge the parent form (updatePasswordForm) error into this form (passwordConfirm) error to display the mat-error element

    const passwordMismatchError: boolean = this.updatePasswordForm.getError('formFieldsNoMatch');
    // console.log('error', passwordMismatchError);

    const errors = this.updatePasswordForm.controls.passwordConfirm.errors;

    let merged: ValidationErrors | null = errors;

    if (passwordMismatchError) {
      merged = { ...errors, mismatch: passwordMismatchError };
    }

    if (merged && Object.keys(merged).length === 0) {
      merged = null;
    }

    this.updatePasswordForm.controls.passwordConfirm.setErrors(merged);

    if (!passwordMismatchError) {
      this.updatePasswordForm.controls.passwordConfirm.updateValueAndValidity();
    }

    return passwordMismatchError;
  }

  get passwordMatchesEmailError(): boolean {
    // we need to merge the parent form (registerForm) error into this form (passwordConfirm) error to display the mat-error element

    const passwordMismatchError: boolean = this.updatePasswordForm.getError('formFieldsMatch');
    // console.log('error', passwordMismatchError);

    const errors = this.updatePasswordForm.controls.password.errors;

    let merged: ValidationErrors | null = errors;

    if (passwordMismatchError) {
      merged = { ...errors, matchesEmail: passwordMismatchError };
    }

    if (merged && Object.keys(merged).length === 0) {
      merged = null;
    }

    this.updatePasswordForm.controls.password.setErrors(merged);

    if (!passwordMismatchError) {
      this.updatePasswordForm.controls.password.updateValueAndValidity();
    }

    return passwordMismatchError;
  }

  get passwordStrengthErrors(): SecurePasswordValidation | null {
    return this.updatePasswordForm.controls.password.errors?.['strength'];
  }

  onPasteValidate() {
    // console.log('update validation');
    this.updatePasswordForm.updateValueAndValidity();
  }
}
