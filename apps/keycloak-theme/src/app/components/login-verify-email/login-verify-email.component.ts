import { ChangeDetectionStrategy, Component } from '@angular/core';
import { KcContextBase } from '@empaia/keycloakify';
import { KcContextProviderService } from '@services/kc-context-provider/kcContextProvider.service';

@Component({
  selector: 'app-login-verify-email',
  templateUrl: './login-verify-email.component.html',
  styleUrls: ['./login-verify-email.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginVerifyEmailComponent {

  protected ctx: KcContextBase.LoginVerifyEmail;

  protected userEmail: string | undefined;
  protected loginAction: string;
  constructor(private ctxService: KcContextProviderService
  ) {
    this.ctx = this.ctxService.getVerifyEmailContext();
    console.log('ctx', this.ctx);


    this.userEmail = this.ctx?.user?.email;
    this.loginAction = this.ctx.url.loginAction;
  }

}
