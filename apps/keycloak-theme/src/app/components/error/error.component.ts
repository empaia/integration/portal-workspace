import { Component, Input } from '@angular/core';
import { KcContextBase } from '@empaia/keycloakify';
import { environment } from '@env/environment';
import { DebugService } from '@services/debug.service';
import {  KcContextProviderService } from '@services/kc-context-provider/kcContextProvider.service';
import { LanguageService } from '@transloco/language.service';

type KcContextCommonMessage = KcContextBase.Common['message'];

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss'],
})
export class ErrorComponent {

  protected readonly production = environment.production;

  private _message: KcContextCommonMessage;
  @Input() public set message(val: KcContextCommonMessage) {
    this._message = val;
    this.setStyle(this.message);
  }
  public get message() {
    return this._message;
  }
  private kcContext: KcContextBase.Error;
 
  protected style = "";

  constructor(
    private lang: LanguageService,
    private kcContextService: KcContextProviderService,
    protected debug: DebugService,
  ) {
    this.kcContext = this.kcContextService.getErrorContext();
  }

 

  setStyle(msg: KcContextCommonMessage) {
    if (this.message) {
      this.style = msg ? msg?.type.toString() : '';
    }
  }

  get loginUrl() {
    return this.kcContext.url.loginUrl + this.lang.appendLocaleParam;
  }

  get baseUrl() {
    return this.kcContext.client.baseUrl;
  }


  get icon() {
    if(this.message?.type === 'success') return 'check_circle';
    if(this.message?.type === 'warning') return 'warning';
    if(this.message?.type === 'error') return 'error';
    return 'help';
  }
}
