import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { environment } from '@env/environment';
import { ConsentDialogComponent, ConsentDialogData } from '../consent-dialog/consent-dialog.component';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FooterComponent {
  protected appVersion = environment.appVersion;
  protected year: number = new Date().getFullYear();

  constructor(public dialog: MatDialog) { }

  openDialog(d: ConsentDialogData) {
    this.dialog.open(ConsentDialogComponent, {
      width: '600px',
      data: { ...d, confirmType: 'ok' }
    });

    return false;

  }
}
