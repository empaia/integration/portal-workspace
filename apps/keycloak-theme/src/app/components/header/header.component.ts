import { Component } from '@angular/core';
import { PageId } from '@empaia/keycloakify';
import { LangDefinition } from '@ngneat/transloco';
import { KcContextProviderService } from '../../services/kc-context-provider/kcContextProvider.service';
import { LanguageService } from '../../transloco/language.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  protected pageId: PageId | undefined;

  protected switchToLang: LangDefinition | undefined;
  protected currentLang: string;

  constructor(
    private kcContext: KcContextProviderService,
    private languageService: LanguageService,
  ) {
    this.pageId = this.kcContext.getContext()?.pageId;
    this.switchToLang = this.languageService.getSwitchToLang();
    this.currentLang = this.languageService.getLanguage();
  }


  setLanguage($event: LangDefinition) {
    this.languageService.setActiveLanguage($event);
    this.switchToLang = this.languageService.getSwitchToLang();
    this.currentLang = this.languageService.getLanguage();
  }

  public get baseUrl(): string | undefined {
    const url = this.kcContext.getErrorContext().client.baseUrl;
    return url;
  }
}
