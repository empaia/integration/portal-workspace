import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { KcContextProviderService } from '@services/kc-context-provider/kcContextProvider.service';

@Component({
  selector: 'app-page-expired',
  templateUrl: './page-expired.component.html',
  styleUrls: ['./page-expired.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageExpiredComponent {
  /* page expired is shown when user clicks on an expired reset password link*/
  private kcContext = inject(KcContextProviderService).getVerifyPageExpired();

  protected restartLink = this.kcContext.url.loginRestartFlowUrl;
  protected continueLink = this.kcContext.url.loginUrl;

}
