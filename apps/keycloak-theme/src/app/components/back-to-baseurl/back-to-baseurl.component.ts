import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { KcContextProviderService } from '@services/kc-context-provider/kcContextProvider.service';

@Component({
  selector: 'app-back-to-baseurl',
  templateUrl: './back-to-baseurl.component.html',
  styleUrls: ['./back-to-baseurl.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BackToBaseurlComponent {

  protected kcContext = inject(KcContextProviderService).getErrorContext();

  get baseUrl() {
    return this.kcContext.client?.baseUrl;
  }
}
