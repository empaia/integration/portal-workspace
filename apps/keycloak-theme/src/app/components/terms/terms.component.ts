import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { KcContextProviderService } from '@services/kc-context-provider/kcContextProvider.service';
import { LanguageService } from '@transloco/language.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TermsComponent {
  protected kcContext = inject(KcContextProviderService).getTermsContext();

  protected lang = inject(LanguageService);

  // accept - action="http://localhost:39000/auth/realms/empaia/login-actions/required-action?session_code=aTV2fkrvm8BYToYHZcpsNaO1_hUIism8o2vQAnJ26CI&execution=TERMS_AND_CONDITIONS&client_id=portal_client&tab_id=KGWqzRb_nj8"

  get loginFormAction(): string {
    // we need to append the language here because change password 
    // -> redirects to login
    // -> redirects back to change password page 
    //    if kc_locale is  missing the wrong language set on change pass page
    return this.kcContext.url.loginAction + this.lang.appendLocaleParam;
  }

  get lang$(): Observable<string> {
    return this.lang.getLanguage$();
  }

  reject() {
    window.location.href = this.kcContext.url.loginUrl;
  }
}
