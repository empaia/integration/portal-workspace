import { ChangeDetectionStrategy, Component, inject, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LanguageService } from '@transloco/language.service';

export interface ConsentDialogData {
  type: 'tos' | 'privacy' | 'imprint',
  confirmType?: 'accept' | 'ok'
}


@Component({
  selector: 'app-consent-dialog',
  templateUrl: './consent-dialog.component.html',
  styleUrls: ['./consent-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConsentDialogComponent {
  protected lang = inject(LanguageService).getLanguage$();
  constructor(
    public dialogRef: MatDialogRef<ConsentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ConsentDialogData) { }


}
