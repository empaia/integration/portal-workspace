import { Spectator, createComponentFactory } from '@ngneat/spectator';

import { SecurePasswordHintComponent } from './secure-password-hint.component';

describe('SecurePasswordHintComponent', () => {
  let spectator: Spectator<SecurePasswordHintComponent>;
  const createComponent = createComponentFactory(SecurePasswordHintComponent);

  it('should create', () => {
    spectator = createComponent();

    expect(spectator.component).toBeTruthy();
  });
});
