import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { SecurePasswordValidation } from '@services/form-validator/formValidators';


@Component({
  selector: 'app-secure-password-hint',
  templateUrl: './secure-password-hint.component.html',
  styleUrls: ['./secure-password-hint.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SecurePasswordHintComponent {

  @Input() public errors: null | SecurePasswordValidation = {
    noUppercase: false,
    noLowercase: false,
    noNumber: false,
    noSpecial: false,
    length: false,
  };

}
