import { Component, inject } from '@angular/core';
import { FormBuilder, FormControl, ValidationErrors, Validators } from '@angular/forms';
import { CustomValidators, SecurePasswordValidation } from '@services/form-validator/formValidators';
import { KcContextProviderService } from '@services/kc-context-provider/kcContextProvider.service';
import { LanguageService } from '@transloco/language.service';
import { MatDialog } from '@angular/material/dialog';
import { ConsentDialogComponent, ConsentDialogData } from '../consent-dialog/consent-dialog.component';
import { FORM_LENGTH } from '@services/form-validator/formValidators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent {

  private fb = inject(FormBuilder);
  private lang = inject(LanguageService);
  // private ctxService = inject(KcContextProviderService);
  private ctx = inject(KcContextProviderService).getRegisterContext();
  public dialog = inject(MatDialog);

  protected loginUrl = this.ctx.url.loginUrl;

  protected readonly MAX_FORM_LENGTH = FORM_LENGTH.MAX_FORM_LENGTH;
  protected readonly MAX_PASSWORD_LENGTH = FORM_LENGTH.MAX_PASSWORD_LENGTH;


  registerForm = this.fb.nonNullable.group({
    firstName: ['',
      [
        Validators.required,
        Validators.maxLength(FORM_LENGTH.MAX_FORM_LENGTH)
      ],
    ],
    lastName: ['',
      [
        Validators.required,
        Validators.maxLength(FORM_LENGTH.MAX_FORM_LENGTH)
      ]
    ],
    email: ['',
      [
        Validators.required,
        Validators.email,
        Validators.maxLength(FORM_LENGTH.MAX_FORM_LENGTH)
      ]
    ],
    password: ['',
      [
        Validators.required,
        Validators.maxLength(FORM_LENGTH.MAX_PASSWORD_LENGTH),
        CustomValidators.PasswordStrengthValidator
      ],
    ],
    passwordConfirm: ['',
      [
        Validators.required,
        Validators.maxLength(FORM_LENGTH.MAX_PASSWORD_LENGTH)
      ]
    ],
    tos: [false, [Validators.requiredTrue]],
    privacy: [false, [Validators.requiredTrue]]
  }, {
    validators: [
      CustomValidators.MatchValidator('password', 'passwordConfirm'),
      CustomValidators.NoMatchValidator('email', 'password')
    ],
  });

  get registrationAction(): string {
    return this.ctx?.url.registrationAction + this.lang.appendLocaleParam;
  }

  get firstName(): FormControl<string> {
    return this.registerForm.controls.firstName;
  }

  get lastName(): FormControl<string> {
    return this.registerForm.controls.lastName;
  }

  get email(): FormControl<string> {
    return this.registerForm.controls.email;
  }

  get password(): FormControl<string> {
    return this.registerForm.controls.password;
  }

  get passwordConfirm(): FormControl<string> {
    return this.registerForm.controls.passwordConfirm;
  }

  get passwordMatchError(): boolean {
    // we need to merge the parent form (registerForm) error into this form (passwordConfirm) error to display the mat-error element

    const passwordMismatchError: boolean = this.registerForm.getError('formFieldsNoMatch');
    // console.log('error', passwordMismatchError);

    const errors = this.registerForm.controls.passwordConfirm.errors;

    let merged: ValidationErrors | null = errors;

    if (passwordMismatchError) {
      merged = { ...errors, mismatch: passwordMismatchError };
    }

    if (merged && Object.keys(merged).length === 0) {
      merged = null;
    }

    this.registerForm.controls.passwordConfirm.setErrors(merged);

    if (!passwordMismatchError) {
      this.registerForm.controls.passwordConfirm.updateValueAndValidity();
    }

    return passwordMismatchError;
  }

  get passwordMatchesEmailError(): boolean {
    // we need to merge the parent form (registerForm) error into this form (passwordConfirm) error to display the mat-error element

    const passwordMismatchError: boolean = this.registerForm.getError('formFieldsMatch');
    // console.log('error', passwordMismatchError);

    const errors = this.registerForm.controls.password.errors;

    let merged: ValidationErrors | null = errors;

    if (passwordMismatchError) {
      merged = { ...errors, matchesEmail: passwordMismatchError };
    }

    if (merged && Object.keys(merged).length === 0) {
      merged = null;
    }

    this.registerForm.controls.password.setErrors(merged);

    if (!passwordMismatchError) {
      this.registerForm.controls.password.updateValueAndValidity();
    }

    return passwordMismatchError;
  }

  get passwordStrengthErrors(): SecurePasswordValidation | null {
    return this.registerForm.controls.password.errors?.['strength'];
  }

  openDialog(d: ConsentDialogData) {
    const dialogRef = this.dialog.open(ConsentDialogComponent, {
      width: '600px',
      data: { ...d, confirmType: 'accept' }
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      switch (d.type) {
        case 'privacy': {
          this.registerForm.controls.privacy.setValue(result);
          break;
        }
        case 'tos': {
          this.registerForm.controls.tos.setValue(result);
          break;
        }
      }
    });
    return false;

  }

  onPasteValidate() {
    // console.log('update validation');
    this.registerForm.updateValueAndValidity();
  }

}
