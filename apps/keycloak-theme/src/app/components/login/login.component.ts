import { Component, ElementRef, inject, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { LanguageService } from '@transloco/language.service';
import { FORM_LENGTH } from '@services/form-validator/formValidators';
import { KcContextProviderService } from '@services//kc-context-provider/kcContextProvider.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  private kcContext = inject(KcContextProviderService).getLoginContext();
  private fb = inject(FormBuilder);
  private lang = inject(LanguageService);

  @ViewChild('loginFormHTMLElement', {
    read: ElementRef,
    static: true,
  }) loginFormHTMLElement!: ElementRef;

  MAX_FORM_LENGTH = FORM_LENGTH.MAX_FORM_LENGTH;
  MAX_PASSWORD_FORM_LENGTH = FORM_LENGTH.MAX_PASSWORD_LENGTH;

  protected loginForm = this.fb.nonNullable.group({
    email: ['',
      [
        Validators.required,
        Validators.maxLength(FORM_LENGTH.MAX_FORM_LENGTH),
        Validators.email,
      ]
    ],
    password: ['',
      [
        Validators.required,
        Validators.maxLength(FORM_LENGTH.MAX_PASSWORD_LENGTH)
      ]
    ],
  });

  get loginFormAction(): string {
    // we need to append the language here because change password
    // -> redirects to login
    // -> redirects back to change password page
    //    if kc_locale is  missing the wrong language set on change pass page
    return this.kcContext.url.loginAction + this.lang.appendLocaleParam;
  }

  get email(): FormControl<string> {
    return this.loginForm.controls.email;
  }

  get password(): FormControl<string> {
    return this.loginForm.controls.password;
  }

  get resetPasswordAllowed() {
    return this.kcContext.realm.resetPasswordAllowed;
  }

  get showRegistrationLink() {
    return this.kcContext.realm.registrationAllowed;
  }

  get registrationLink() {
    return this.kcContext.url.registrationUrl + this.lang.appendLocaleParam;
  }

  get resetPasswordLink() {
    return this.kcContext.url.loginResetCredentialsUrl + this.lang.appendLocaleParam;
  }

  onSubmit() {
    if (this.loginForm.valid) {
      const htmlFormElement = this.loginFormHTMLElement.nativeElement; htmlFormElement.submit();
    }
  }

}
