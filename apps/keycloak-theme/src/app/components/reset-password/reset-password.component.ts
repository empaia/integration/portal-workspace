import { ChangeDetectionStrategy, Component, ElementRef, inject, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { KcContextProviderService } from '@services/kc-context-provider/kcContextProvider.service';
import { LanguageService } from '@transloco/language.service';
import { FORM_LENGTH } from '@services/form-validator/formValidators';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResetPasswordComponent {
  @ViewChild('resetPasswordFormHTMLElement', {
    read: ElementRef,
    static: true,
  }) resetPasswordFormHTMLElement!: ElementRef;

  protected readonly MAX_FORM_LENGTH = FORM_LENGTH.MAX_FORM_LENGTH;
  private kcContext = inject(KcContextProviderService).getResetPasswordContext();
  private lang = inject(LanguageService);
  private fb = inject(FormBuilder);

  protected resetPasswordForm = this.fb.nonNullable.group({
    username: ['',
      [
        Validators.required,
        Validators.maxLength(FORM_LENGTH.MAX_FORM_LENGTH),
        Validators.email
      ]
    ],
  });

  get loginLink(): string {
    return this.kcContext.url.loginUrl + this.lang.appendLocaleParam;
  }

  get resetPasswordFormAction(): string {
    // keycloak stores the password reset action url inside login action...
    return this.kcContext.url.loginAction;
  }


  get username(): FormControl<string> {
    return this.resetPasswordForm.controls.username;
  }

  onSubmit() {
    if (this.resetPasswordForm.valid) {
      const htmlFormElement = this.resetPasswordFormHTMLElement.nativeElement; htmlFormElement.submit();
    }
  }


}
