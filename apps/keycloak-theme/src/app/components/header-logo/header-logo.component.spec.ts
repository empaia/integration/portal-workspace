import { HeaderLogoComponent } from './header-logo.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MockDirectives } from 'ng-mocks';
import { LetDirective } from '@ngrx/component';

describe('HeaderLogoComponent', () => {
  let spectator: Spectator<HeaderLogoComponent>;
  const createComponent = createComponentFactory({
    component: HeaderLogoComponent,
    declarations: [
      MockDirectives(
        LetDirective
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
