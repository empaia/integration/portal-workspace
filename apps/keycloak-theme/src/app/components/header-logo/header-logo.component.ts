import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { ThemeService } from '../../services/theme/theme.service';

@Component({
  selector: 'app-header-logo',
  templateUrl: './header-logo.component.html',
  styleUrls: ['./header-logo.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderLogoComponent {

  public theme$: Observable<string>;

  darkLogoPath = './assets/logos/empaia-logo-portal.png';
  lightLogoPath = './assets/logos/empaia-logo-portal-light.png';

  constructor(
    public theme: ThemeService,
  ) {
    this.theme$ = this.theme.getTheme$();
  }

}
