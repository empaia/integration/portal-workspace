import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { getKcContext, KcContextBase } from '@empaia/keycloakify';
import { TRANSLOCO_SCOPE } from '@ngneat/transloco';
import { LetDirective, PushPipe } from '@ngrx/component';
import { MaterialModule } from './material/material.module';
import { CommonUiModule } from '@empaia/common-ui';

import { TranslocoRootModule } from './transloco/transloco-root.module';
import { AppComponent } from './app.component';
import { KC_CONTEXT } from './app.config';

import { ErrorComponent } from './components/error/error.component';
import { MainComponent } from './container/main/main.component';
import { HeaderLogoComponent } from './components/header-logo/header-logo.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { KcContextProviderService } from './services/kc-context-provider/kcContextProvider.service';
import { HeaderComponent } from './components/header/header.component';
import { LanguageButtonComponent } from './components/language-button/language-button.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginVerifyEmailComponent } from './components/login-verify-email/login-verify-email.component';
import { SecurePasswordHintComponent } from './components/secure-password-hint/secure-password-hint.component';
import { ConsentDialogComponent } from './components/consent-dialog/consent-dialog.component';
import { BackToBaseurlComponent } from './components/back-to-baseurl/back-to-baseurl.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { UpdatePasswordComponent } from './components/update-password/update-password.component';
import { PageExpiredComponent } from './components/page-expired/page-expired.component';
import { TermsComponent } from './components/terms/terms.component';
import { FormControlTrimDirective } from './directives/form-control-trim/form-control-trim.directive';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    RegisterComponent,
    LoginComponent,
    ErrorComponent,
    HeaderComponent,
    HeaderLogoComponent,
    LanguageButtonComponent,
    FooterComponent,
    LoginVerifyEmailComponent,
    SecurePasswordHintComponent,
    ConsentDialogComponent,
    BackToBaseurlComponent,
    ResetPasswordComponent,
    UpdatePasswordComponent,
    PageExpiredComponent,
    TermsComponent,
    FormControlTrimDirective,
  ],
  imports: [
    BrowserModule,
    CommonUiModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    LetDirective,
    PushPipe,
    HttpClientModule,
    TranslocoRootModule,
  ],
  providers: [
    KcContextProviderService, { provide: KC_CONTEXT, useFactory: () => getKcContext<KcContextBase>().kcContext?.pageId },
    { provide: TRANSLOCO_SCOPE, useValue: 'login' },
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
