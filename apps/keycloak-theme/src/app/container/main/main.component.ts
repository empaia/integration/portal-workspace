import { Component } from '@angular/core';
import { KcContextBase, PageId } from '@empaia/keycloakify';
import { environment } from '@env/environment';
import { DebugService } from '@services/debug.service';
import {  KcContextProviderService } from '@services/kc-context-provider/kcContextProvider.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent {

  protected readonly production = environment.production;
  protected pageId: PageId | undefined;
  protected errorMessage: KcContextBase.Common['message'];

  // for debugging
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  protected kcContext: any;
  constructor(
    private kcContextService: KcContextProviderService,
    protected debug: DebugService,
  ) {
    this.pageId = this.kcContextService.getContext()?.pageId;
    this.errorMessage = this.kcContextService.getContext()?.['message'];
    this.kcContext = this.kcContextService.getContext();
  }
}
