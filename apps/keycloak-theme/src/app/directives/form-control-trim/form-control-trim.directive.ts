import { Directive, Input, OnDestroy, OnInit } from '@angular/core';
import { NgControl } from '@angular/forms';
import { Subject, debounceTime, takeUntil } from 'rxjs';

@Directive({
  selector: 'input[app-trim][formControlName], input[app-trim][formControl]'
})
export class FormControlTrimDirective implements OnInit, OnDestroy {
  private destroy = new Subject();

  @Input() public debounceTime = 300;

  constructor(
    private ngControl: NgControl,
  ) { }

  public ngOnInit(): void {
    if (this.ngControl.control) {
      this.ngControl.control.valueChanges
        .pipe(
          takeUntil(this.destroy),
          debounceTime(this.debounceTime)
        )
        .subscribe((changes: string) => {
          if (!changes) {
            return;
          }

          const trimmed = changes.trim();
          if (trimmed !== changes) {
            this.ngControl.control?.setValue(trimmed);
          }
        });
    }
  }

  public ngOnDestroy(): void {
    this.destroy.next(null);
  }

}
