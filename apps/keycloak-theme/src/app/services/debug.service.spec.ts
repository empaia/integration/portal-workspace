import { createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { DebugService } from './debug.service';

describe('DebugService', () => {
  let spectator: SpectatorService<DebugService>;
  const createService = createServiceFactory(DebugService);

  beforeEach(() => spectator = createService());

  it('should...', () => {
    expect(spectator.service).toBeTruthy();
  });
});