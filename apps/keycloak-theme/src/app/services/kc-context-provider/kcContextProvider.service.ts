import { Inject, Injectable } from '@angular/core';
import { KcContextBase, getKcContext } from '@empaia/keycloakify';
import { PageId } from '@empaia/keycloakify';
import { KC_CONTEXT } from '../../app.config';

interface PageIdType {
  pageId: PageId;
}

export type KcContext =
  | KcContextBase
  | KcContextBase.Login
  | KcContextBase.LoginVerifyEmail
  | KcContextBase.Error
  | KcContextBase.Register
  | KcContextBase.Info
  | KcContextBase.LoginResetPassword
  | KcContextBase.LoginUpdatePassword
  | KcContextBase.LoginPageExpired
  ;

// const pageIdLoginTypeObj: PageIdType = { pageId: 'login.ftl' };
// const pageIdErrorTypeObj: PageIdType = { pageId: 'error.ftl' };
// const pageIdInfoTypeObj: PageIdType = { pageId: 'info.ftl' };
// const pageIdRegisterTypeObj: PageIdType = { pageId: 'register.ftl' };

@Injectable({
  providedIn: 'root'
})
export class KcContextProviderService {


  constructor(
    @Inject(KC_CONTEXT) private readonly kcPageId: PageId
  ) {

  }
  getLoginContext(): KcContextBase.Login {
    return this.getContext() as KcContextBase.Login;
  }

  getErrorContext(): KcContextBase.Error {
    return (this.getContext() as KcContextBase.Error);
  }

  getResetPasswordContext(): KcContextBase.LoginResetPassword {
    return (this.getContext() as KcContextBase.LoginResetPassword);
  }

  getUpdatePasswordContext(): KcContextBase.LoginUpdatePassword {
    return (this.getContext() as KcContextBase.LoginUpdatePassword);
  }

  getVerifyPageExpired(): KcContextBase.LoginPageExpired {
    return (this.getContext() as KcContextBase.LoginPageExpired);
  }

  getRegisterContext(): KcContextBase.Register {
    return (this.getContext() as KcContextBase.Register);
  }

  getVerifyEmailContext(): KcContextBase.LoginVerifyEmail {
    return (this.getContext() as KcContextBase.LoginVerifyEmail);
  }

  getTermsContext(): KcContextBase.Terms {
    return (this.getContext() as KcContextBase.Terms);
  }

  getContextId(pageID: PageId) {
    const pageId: PageId = pageID;
    const pageIdTypeObj: PageIdType = { pageId: pageId };
    type pageIdType = typeof pageIdTypeObj;

    const ctx = getKcContext<pageIdType>().kcContext;

    return ctx;
  }

  // getContext(): KcContextBase.LoginVerifyEmail;
  // getContext(): KcContextBase.Register;
  // getContext(): KcContextBase.Login;
  // getContext(): KcContextBase {
  getContext(): KcContext {
    switch (this.kcPageId) {
      case 'login.ftl':
        return this.getContextId(this.kcPageId) as KcContextBase.Login;
      case 'register.ftl':
        return this.getContextId(this.kcPageId) as KcContextBase.Register;
      case 'login-verify-email.ftl':
        return this.getContextId(this.kcPageId) as KcContextBase.LoginVerifyEmail;
      case 'error.ftl':
        return this.getContextId(this.kcPageId) as KcContextBase.Error;
      case 'info.ftl':
        return this.getContextId(this.kcPageId) as KcContextBase.Info;
      case 'login-reset-password.ftl':
        return this.getContextId(this.kcPageId) as KcContextBase.LoginResetPassword;
      case 'login-update-password.ftl':
        return this.getContextId(this.kcPageId) as KcContextBase.LoginUpdatePassword;
      case 'login-page-expired.ftl':
        return this.getContextId(this.kcPageId) as KcContextBase.LoginPageExpired;
      case 'terms.ftl':
        return this.getContextId(this.kcPageId) as KcContextBase.Terms;
      default:
        throw new Error('unknown Keycloak Context');
    }
  }
}
