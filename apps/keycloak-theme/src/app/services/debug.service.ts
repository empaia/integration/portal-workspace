import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DebugService {

  private debugModeSubject$ = new BehaviorSubject<boolean>(false);
  private debugMode$ = this.debugModeSubject$.asObservable();

  set(on: boolean) {
    this.debugModeSubject$.next(on);
  }

  toggle() {
    this.debugModeSubject$.next(!this.debugModeSubject$.value);
  }

  get debugMode() {
    return this.debugMode$;
  }



}
