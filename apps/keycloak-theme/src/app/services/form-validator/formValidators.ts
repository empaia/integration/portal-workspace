import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export class FORM_LENGTH {
  static readonly MAX_FORM_LENGTH = 255;
  static readonly MAX_PASSWORD_LENGTH = 1024;
}


export interface SecurePasswordValidation extends ValidationErrors {
  noUppercase: boolean;
  noLowercase: boolean;
  noNumber: boolean;
  noSpecial: boolean;
  length: boolean;
}

export class CustomValidators {
  static MatchValidator(source: string, target: string): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const sourceCtrl = control.get(source);
      const targetCtrl = control.get(target);

      let noMatch = false;
      if (sourceCtrl && targetCtrl && sourceCtrl.value !== targetCtrl.value) {
        noMatch = true;
      }

      // console.log('mismatch', noMatch);

      return noMatch ? { formFieldsNoMatch: true } : null;
    };
  }

  static NoMatchValidator(source: string, target: string): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const sourceCtrl = control.get(source);
      const targetCtrl = control.get(target);

      let match = false;
      if (sourceCtrl && targetCtrl && sourceCtrl.value === targetCtrl.value) {
        match = true;
      }

      // console.log('mismatch', noMatch);

      return match ? { formFieldsMatch: true } : null;
    };
  }

  static PasswordStrengthValidator(control: AbstractControl): ValidationErrors | null {
    const value: string = control.value || '';

    // console.log(value);

    let errors: SecurePasswordValidation = {
      noUppercase: false,
      noLowercase: false,
      noNumber: false,
      noSpecial: false,
      length: false,
    };

    if (!value) {
      const allErrors: SecurePasswordValidation = {
        noUppercase: true,
        noLowercase: true,
        noNumber: true,
        noSpecial: true,
        length: true,
      };
      return {
        strength: allErrors,
      };
    }

    if (value.length < 12) {
      errors = { ...errors, length: true };
    }


    const upperCaseCharacters = /[A-Z]+/g;
    if (upperCaseCharacters.test(value) === false) {
      errors = { ...errors, noUppercase: true };
    }

    const lowerCaseCharacters = /[a-z]+/g;
    if (lowerCaseCharacters.test(value) === false) {
      errors = { ...errors, noLowercase: true };
    }


    const numberCharacters = /[0-9]+/g;
    if (numberCharacters.test(value) === false) {
      errors = { ...errors, noNumber: true };
    }

    const specialCharacters = /[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?°^§`~]+/;
    if (specialCharacters.test(value) === false) {
      errors = { ...errors, noSpecial: true };
    }

    let allValid = true;

    // console.log('errors', errors);
    if (errors.length || errors.noLowercase || errors.noNumber || errors.noSpecial || errors.noUppercase) {
      allValid = false;
    }
    return allValid ? null : { strength: errors };
  }

}