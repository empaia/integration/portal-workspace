import { Component, HostListener, OnInit } from '@angular/core';
import { TranslocoService } from '@ngneat/transloco';
import { PageId } from '@empaia/keycloakify';
import { languages, themes } from './app.models';
import { KcContextProviderService } from './services/kc-context-provider/kcContextProvider.service';
import { OverlayContainer } from '@angular/cdk/overlay';
import { ThemeService } from './services/theme/theme.service';
import { environment } from '@env/environment';
import { DebugService } from '@services/debug.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

  private currentLang = 0;
  public currentTheme = themes[0];

  protected pageId: PageId | undefined;


  @HostListener('document:keyup.control.alt.s')
  switchTheme() {
    if (!environment.production) {
      this.theme.nextTheme();
    }
  }

  @HostListener('document:keyup.control.alt.d')
  debugLog() {
    if (!environment.production) {
      console.log(this.kcContext.getContext(),null,4);
      this.debugService.toggle();
    }
  }

  @HostListener('document:keyup.control.alt.l')
  switchLang() {
    this.currentLang = this.currentLang + 1 < languages.length ? this.currentLang + 1 : 0;
    this.lang.setActiveLang(languages[this.currentLang]);
  }

  constructor(
    private lang: TranslocoService,
    private overlay: OverlayContainer,
    private theme: ThemeService,
    private kcContext: KcContextProviderService,
    private debugService: DebugService,
  ) {
    theme.setTheme(this.currentTheme);
    this.pageId = this.kcContext.getContext()?.pageId;
  }

  ngOnInit(): void {
    this.theme.getPrevTheme$().subscribe((name) => {
      if (name !== '') {
        this.overlay.getContainerElement().classList.remove(name);
      }
    });
    this.theme.getTheme$().subscribe((name) => {
      this.currentTheme = name;
      if (name !== '') {
        this.overlay.getContainerElement().classList.add(name);
      }
    });

  }
}
