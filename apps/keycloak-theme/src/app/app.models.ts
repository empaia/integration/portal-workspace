import { LangDefinition } from '@ngneat/transloco';

// theme names must mirror css class names in styles.scss
export const themes = [
  'light-theme',
  'dark-theme'
];

export const languages = ["en", "de"] as const;
export type Language = typeof languages[number];

export const availableLanguages: LangDefinition[] = [
  { id: 'en', label: 'English' },
  { id: 'de', label: 'Deutsch' },
];



export const LOCALE_PARAM = '&kc_locale=';