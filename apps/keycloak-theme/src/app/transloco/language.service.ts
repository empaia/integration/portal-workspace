import { Injectable } from '@angular/core';
import { LangDefinition, TranslocoService } from '@ngneat/transloco';
import { KcContextProviderService } from '@services/kc-context-provider/kcContextProvider.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LOCALE_PARAM } from '../app.models';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  private readonly COOKIE_ID = 'KEYCLOAK_LOCALE';

  private realmName = '';

  constructor(
    private langService: TranslocoService,
    private kcContext: KcContextProviderService,
  ) {
    this.realmName = this.kcContext.getContext().realm.name;

    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const lang = urlParams.get('kc_locale');
    if (lang) {
      this.setActiveLanguageId(lang);
    }


    const contextLang = this.kcContext.getContext().locale?.currentLanguageTag;
    if (contextLang && !lang) {
      this.setActiveLanguageId(contextLang);
    }
  }

  getLanguage$(): Observable<string> {
    return this.langService.langChanges$.pipe(map(s => s));
  }

  getLangFromCookie(): string | undefined {
    const cookieValue = document.cookie
      .split('; ')  
      .find((row) => row.startsWith(this.COOKIE_ID + '='))
      ?.split('=')[1];
    return cookieValue;
  }

  getLanguage(): string {
    return this.langService.getActiveLang();
  }

  getLanguageDef(): LangDefinition | undefined {
    return (this.langService.getAvailableLangs() as LangDefinition[]).find((l: LangDefinition) => l.id === this.langService.getActiveLang());
  }

  setActiveLanguage(lang: LangDefinition) {
    this.setActiveLanguageId(lang.id);
  }

  setActiveLanguageId(langId: string) {
    this.langService.setActiveLang(langId);
    this.setCookie(langId);
  }


  setCookie(id: string) {
    // persist lang selection for keycloak in cookie
    // see: https://github.com/keycloak/keycloak/issues/10981
    document.cookie = `${this.COOKIE_ID}=${id}; SameSite=Lax; Path=/auth/realms/${this.realmName}/`;
  }

  getSwitchToLang(): LangDefinition | undefined {
    const l = this.langService.getActiveLang();
    switch (l) {
      case 'de':
        return (this.langService.getAvailableLangs() as LangDefinition[]).find((l: LangDefinition) => l.id === 'en');
      case 'en':
        return (this.langService.getAvailableLangs() as LangDefinition[]).find((l: LangDefinition) => l.id === 'de');
      default:
        return undefined;
    }
  }

  switchLang() {
    const l = this.getSwitchToLang()?.id;
    if (l) {
      this.langService.setActiveLang(l);
    }
  }

  get appendLocaleParam(): string {
    return LOCALE_PARAM + this.getLanguage();
  }
}
