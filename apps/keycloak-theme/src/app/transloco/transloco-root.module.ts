import { Injectable, NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {
  TRANSLOCO_LOADER,
  Translation,
  TranslocoLoader,
  TRANSLOCO_CONFIG,
  translocoConfig,
  TranslocoModule
} from '@ngneat/transloco';
import { TranslocoMessageFormatModule } from '@ngneat/transloco-messageformat';
import { TranslocoLocaleModule } from '@ngneat/transloco-locale';

import { environment } from '../../environments/environment';
import { getKcContext, KcContextBase } from '@empaia/keycloakify';
import { availableLanguages } from '../app.models';

@Injectable({ providedIn: 'root' })
export class TranslocoHttpLoader implements TranslocoLoader {

  private resPath: string | undefined;

  constructor(private http: HttpClient) {
    const kcContext = getKcContext<KcContextBase>().kcContext;
    this.resPath = kcContext?.url.resourcesPath;
  }

  getTranslation(lang: string) {
    return this.http.get<Translation>(this.resPath + `/assets/i18n/${lang}.json`);
  }
}


@NgModule({
  imports: [
    // TranslocoPreloadLangsModule.forRoot(['lazy-page/es']),
    TranslocoMessageFormatModule.forRoot(),
    TranslocoLocaleModule.forRoot({
      // langToLocaleMapping: {
      //   en: 'en',
      //   de: 'de',
      // },
    }),
  ],
  exports: [TranslocoModule],
  providers: [
    {
      provide: TRANSLOCO_CONFIG,
      useValue: translocoConfig({
        prodMode: environment.production,
        availableLangs: availableLanguages,
        reRenderOnLangChange: true,
        fallbackLang: 'en',
        defaultLang: 'de',
        // missingHandler: {
        //   useFallbackTranslation: false,
        // },
        // interpolation: ['<<<', '>>>']
      }),
    },
    { provide: TRANSLOCO_LOADER, useClass: TranslocoHttpLoader },
  ],
})
export class TranslocoRootModule { }