import {
  CustomWebpackBrowserSchema,
  TargetOptions,
} from '@angular-builders/custom-webpack';
import * as webpack from 'webpack';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const FileManagerPlugin = require('filemanager-webpack-plugin');
// TODO typed module import
// import FileManagerPlugin from 'filemanager-webpack-plugin';

export default (
  config: webpack.Configuration,
  options: CustomWebpackBrowserSchema,
  _targetOptions: TargetOptions
) => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let onStart: any;
  if (!options.watch) {
    onStart = {
      // cleanup previous build - if not in watch mode
      delete: ['./dist/keycloak-theme'],
    };
  }

  config?.plugins?.push(
    new FileManagerPlugin({
      events: {
        onStart,
        onEnd: {
          // create separate build dir for theming file structure
          mkdir: ['./dist/keycloak-theme/themes/empaia-material/login/'],
          copy: [
            // copy bundled angular output (move does not support globs)
            {
              source:
                './dist/apps/keycloak-theme/**/*.{ftl,js,map,css,json,png,jpg,txt,ico,ttf,woff2}',
              destination:
                './dist/keycloak-theme/themes/empaia-material/login/resources/',
            },
            // copy images from app assets
            {
              source: './apps/keycloak-theme/src/assets/logos/**',
              destination:
                './dist/keycloak-theme/themes/empaia-material/login/resources/assets/logos/',
            },
            // copy translations folder from app assets (transloco)
            {
              source: './apps/keycloak-theme/src/assets/i18n/**',
              destination:
                './dist/keycloak-theme/themes/empaia-material/login/resources/assets/i18n/',
            },
            // copy keycloak translation files
            {
              source: './apps/keycloak-theme/src/assets/messages/**',
              destination:
                './dist/keycloak-theme/themes/empaia-material/login/messages/',
            },
            // copy properties file from app assets
            {
              source: './apps/keycloak-theme/src/assets/theme.properties',
              destination:
                './dist/keycloak-theme/themes/empaia-material/login/theme.properties',
            },
            // copy favicon
            {
              source: './apps/keycloak-theme/src/favicon.ico',
              destination:
                './dist/keycloak-theme/themes/empaia-material/login/resources/favicon.ico',
            },
            // copy ftl script from lib
            {
              source:
                './libs/keycloakify/src/bin/keycloakify/generateFtl/ftl_object_to_js_code_declaring_an_object.ftl',
              destination:
                './dist/keycloak-theme/themes/empaia-material/login/resources/',
            },
            // copy email templates
            {
              source:
                './dist/apps/email-template-generator/out/keycloak/email/**',
              destination: './dist/keycloak-theme/themes/empaia-material/email/',
            },
          ],
          delete: ['./dist/apps/keycloak-theme/'],
        },
      },
    })
  );
  return config;
};
