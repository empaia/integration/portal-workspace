import * as fs from 'fs';
import * as  semver from 'semver';
import { TargetOptions } from '@angular-builders/custom-webpack';
import type { PageId } from '@empaia/keycloakify';
import { EOL } from 'os';

const replacementMarker = '@PAGE_ID@';

const pagesToBuild: PageId[] = [
  'login.ftl',
  'register.ftl',
  'login-verify-email.ftl',
  'info.ftl',
  'error.ftl',
  'login-reset-password.ftl',
  'login-update-password.ftl',
  'login-page-expired.ftl',
  'terms.ftl'
];

const themeFilesLocation = '/dist/keycloak-theme/themes/empaia-material/login/';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (targetOptions: TargetOptions, indexHtml: string) => {

  ensureNodeVersion();

  ftlFromTemplate(indexHtml).then(() =>
    // eslint-disable-next-line no-restricted-syntax
    console.info(`transformed index.html to: ${EOL} ${pagesToBuild.join(EOL)}`)
  );

  return 'intentionally EMPTY index.html';
};

function ensureNodeVersion() {
  const minNodeVersion = '16.13.1';
  if (semver.gte(minNodeVersion, process.versions.node)) {
    console.error(`detected node version ${process.versions.node} - ${EOL}
    minimum required version is ${minNodeVersion}${EOL}`);
    // exit with error
    process.exit(1);
  }
}

async function ftlFromTemplate(indexHtml: string) {

  pagesToBuild.forEach(async pageId => {
    let outFile = indexHtml.replace(replacementMarker, `${pageId}`);

    // production build replace <> with html entities
    // we replace entities back with proper chars...
    outFile = outFile.replaceAll('&lt;', '<');
    outFile = outFile.replaceAll('&gt;', '>');

    const outFilePath = `${process.cwd()}${themeFilesLocation}`;
    // eslint-disable-next-line no-restricted-syntax
    // console.info(`${ EOL } ftl file: ${ outFilePath }${ pageId } ${ EOL }`);

    // create out files path
    fs.mkdirSync(outFilePath, { recursive: true });

    try {
      await fs.promises.writeFile(outFilePath + pageId, outFile, 'utf8');
    } catch (error) {
      console.error(`ERROR: writing file ${outFilePath}`);
      console.error(`${error}`);
    }
  });
}
