# Keycloak Theme

## 0.5.21

* upgraded to angular 17

## 0.5.18

### Fixes

* trims leading and tailing white spaces from email field in reset password

## 0.5.16

* initialize public project
