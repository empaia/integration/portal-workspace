/* eslint-disable no-undef */

/**
 * Name: Group Audience Mapper
 *
 * Available variables:
 * user - the current user
 * realm - the current realm
 * token - the current token
 * userSession - the current userSession
 * keycloakSession - the current keycloakSession
 *
 * see https://www.keycloak.org/docs/latest/server_development/#_script_providers
 *
 */

var AccessToken = Java.type("org.keycloak.representations.AccessToken");

if (token !== null && token instanceof AccessToken) {
    // Get user groups
    var groups = user.getGroupsStream();

    // Iterate groups
    groups.forEach(function (group) {
        var attributes = group.getAttributes();

        // Look for 'allowed_audience_count' attribute ( V2 API )
        if (attributes.allowed_audience_count !== null &&  typeof attributes.allowed_audience_count !== "undefined") {
            var allowed_count = attributes.allowed_audience_count[0];
            for (var i=0; i<allowed_count; i++) {
                var key = "allowed_audience_" + String(i);
                var value = attributes[key];
                if ( value !== null && value.length > 0 ) {
                    token.addAudience(value[0]);
                }
            }
        }
    });

    // add "marketplace_service_client" audience for users with "moderator" or "admin" role
    // add "medical_device_companion_client" audience for users with "empaia-international-associate" role
    var access = token.getResourceAccess("auth_service_client");
    if (access !== null) {
        if (access.isUserInRole("moderator") || access.isUserInRole("admin")) {
            token.addAudience("marketplace_service_client");
        }
        if (access.isUserInRole("empaia-international-associate")) {
            token.addAudience("medical_device_companion_client");
        }
    }
}
