/* eslint-disable no-undef */
/**
 * Name: Organizations Mapper
 *
 * Available variables:
 * user - the current user
 * realm - the current realm
 * token - the current token
 * userSession - the current userSession
 * keycloakSession - the current userSession
 *
 * see https://www.keycloak.org/docs/latest/server_development/#_script_providers
 *
 */

var AccessToken = Java.type("org.keycloak.representations.AccessToken");
var ArrayList = Java.type("java.util.ArrayList");
var HashMap = Java.type("java.util.HashMap");

if (token !== null && token instanceof AccessToken) {
    var organizations = new HashMap();

    // Get user groups
    var groups = user.getGroupsStream();

    // Iterate groups
    groups.forEach(function (group) {
        var isOrganizationUserRoleGroup = group.getParent() !== null;
        var organizationId = isOrganizationUserRoleGroup ? group.getParent().getId() : group.getId();
        var organization;
        if (!organizations.containsKey(organizationId)) {
            // add the roles list also if no role is defined for this user to indicate the the user is a member of
            // this organization (only members can get a role assigned)
            organization = new HashMap();
            organization.put("roles", new ArrayList());
            organizations.put(organizationId, organization);
        }
        if (isOrganizationUserRoleGroup) {
            // "Member" is implied by adding the organization id to this claim, so it
            // should not be added to the roles list:
            if (group.getName() !== "Member") {
                organization = organizations[organizationId];
                organization["roles"].add(group.getName());
            }
        }
    });

    token.setOtherClaims("organizations", organizations);
}
