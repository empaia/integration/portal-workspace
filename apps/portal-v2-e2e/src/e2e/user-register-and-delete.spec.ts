import { test, expect } from '@playwright/test';

test.describe('new user', () => {

  test('register, login & delete', async ({ browser }) => {
    const page = await browser.newPage();
    await page.goto('/');
    await page.getByRole('button', { name: 'Register' }).click();
    await expect(page.getByRole('heading', { name: 'Register' })).toBeVisible();

    await page.getByPlaceholder('First name').click();
    await page.getByPlaceholder('First name').fill('playwright');

    await page.getByPlaceholder('Last name').click();
    await page.getByPlaceholder('Last name').fill('e2e');
    await page.getByPlaceholder('Last name').press('Tab');
    await page.getByPlaceholder('Email address').fill('e2e-test-user@example.com');
    await page.getByPlaceholder('Email address').press('Tab');
    await page.getByPlaceholder('Password', { exact: true }).fill('Password123!');
    await page.getByPlaceholder('Password', { exact: true }).press('Tab');

    // fill wrong pw
    await page.getByPlaceholder('Repeat password').fill('WrongPassword123!');
    await page.getByPlaceholder('Repeat password').press('Tab');
    await expect(page.getByText('The provided passwords don\'t match.')).toBeVisible();
    await page.getByPlaceholder('Repeat password').click();
    await page.getByPlaceholder('Repeat password').fill('Password123!');
    await page.getByRole('link', { name: 'Terms of use' }).click();
    await page.getByRole('button', { name: 'Accept' }).click();
    await page.getByRole('link', { name: 'Data privacy policy' }).click();
    await page.getByRole('button', { name: 'Accept' }).click();
    await page.getByRole('button', { name: 'Register' }).click();
    await expect(page.getByText('You need to verify your email address to activate your account.')).toBeVisible();
    await expect(page.getByRole('heading', { name: 'Email verification' })).toBeVisible();

    // confirm email in MailDev
    const pageMailDev = await browser.newPage();
    await pageMailDev.goto(process.env.MAILDEV_URL ?? '');

    await pageMailDev.getByRole('link', { name: 'Verify email To: e2e-test-user@example.com' }).first().click();
    const page2Promise = pageMailDev.waitForEvent('popup');
    
    await pageMailDev.frameLocator('iframe >> nth=0').getByRole('link', { name: 'Confirm email' }).click();
    
    // 3rd tab
    const page2Portal = await page2Promise;
    await page2Portal.getByRole('link', { name: 'Back to Homepage' }).click();
    await page2Portal.getByRole('button', { name: 'Login' }).click();
    await page2Portal.getByRole('button', { name: 'Profile dropdown menu' }).click();
    await page2Portal.getByRole('menuitem', { name: 'My Profile' }).click();
    await page2Portal.getByRole('paragraph').filter({ hasText: 'playwright e2e' }).click();
    await page2Portal.getByText('e2e-test-user@example.com').click();
    await page2Portal.getByRole('button', { name: 'Delete' }).click();
    await page2Portal.getByRole('button', { name: 'Confirm' }).click();

    // try login again after delete
    await page2Portal.getByRole('button', { name: 'Login' }).click();
    // back on keycloak theme
    await page2Portal.getByPlaceholder('Email').click();
    await page2Portal.getByPlaceholder('Email').fill('e2e-test-user@example.com');
    await page2Portal.getByPlaceholder('Email').press('Tab');
    await page2Portal.getByPlaceholder('Password').fill('Password123!');
    await page2Portal.getByRole('button', { name: 'Login' }).click();
    await expect(page2Portal.getByText('Invalid e-mail address or password.')).toBeVisible();
  });
});

