import { Browser, BrowserContext, Page } from '@playwright/test';
import * as fs from "fs";

export async function saveSessionStorage(page: Page, name: string) {
  // Get session storage and store as env variable
  const sessionStorage = await page.evaluate(() => JSON.stringify(sessionStorage));
  await fs.writeFileSync(`.auth/${name}-ss.json`, JSON.stringify(sessionStorage), 'utf-8');

}

export async function useSessionStorage(context: BrowserContext, name: string) {
  // Set session storage in a new context
  const sessionStorage = JSON.parse(await fs.readFileSync(`.auth/${name}-ss.json`, 'utf-8'));
  await context.addInitScript(storage => {
    if (window.location.hostname === 'portal-v2.localdev') {
      const entries = JSON.parse(storage);
      for (const [key, value] of Object.entries(entries)) {
        window.sessionStorage.setItem(key, value as string);
      }
    }
  }, sessionStorage);
}

export async function useAdminContextPage(browser: Browser): Promise<Page> {
  const adminContext = await browser.newContext({ storageState: '.auth/admin-ls.json' });
  await useSessionStorage(adminContext, 'admin');
  const page = await adminContext.newPage();

  await page.goto('http://portal-v2.localdev:4200/');

  return page;
}