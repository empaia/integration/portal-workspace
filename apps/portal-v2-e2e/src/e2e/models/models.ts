
export const languageLS = {
  "origins": [
    {
      "origin": process.env.E2E_BASE_URL,
      "localStorage": [
        {
          "name": "language",
          "value": "en"
        }
      ]
    }
  ]
};