import { test, expect } from './fixtures/fixtures';

// test.beforeEach(async ({ page }) => {
//   await page.goto('http://portal-v2.localdev:4200/');
//   await page.getByRole('button', { name: 'Sprache' }).click();
//   await page.getByRole('menuitem', { name: 'EN' }).click();
// });
test.describe('user can log in', () => {

  test('test admin login', async ({ adminPage, userPage }) => {
    const page = adminPage.page;
    // adminContext and all pages inside, including adminPage, are signed in as "admin".
    // const adminContext = await browser.newContext({ storageState: '.auth/admin-ls.json' });
    // await useSessionStorage(adminContext, 'admin');
    // const page = await adminContext.newPage();

    await page.goto('/');

    // if no default org is selected there will be a dialog overlay
    if (page.locator('mat-card-content').filter({ hasText: 'hospital_ref_1' }).isVisible()) {
      // select a default org
      await page.locator('mat-card-content').filter({ hasText: 'hospital_ref_1' }).click();
    }

    // await page.getByRole('button', { name: 'Login' }).click();
    await expect(page.getByRole('button', { name: 'Profile dropdown menu' })).toBeVisible();

    await page.getByRole('button', { name: 'Profile dropdown menu' }).click();
    await page.getByRole('menuitem', { name: 'My Profile' }).click();

    await expect(page.getByText('Name empaia admin')).toBeVisible();
    await expect(page.getByText(process.env.USER_ADMIN_EMAIL)).toBeVisible();

    await page.getByRole('button', { name: 'Profile dropdown menu' }).click();
    await expect(page.getByRole('menuitem', { name: 'Administration' })).toBeVisible();
  });

  test('test bob login', async ({ adminPage, userPage }) => {
    const page = userPage.page;
    // adminContext and all pages inside, including adminPage, are signed in as "admin".
    // const adminContext = await browser.newContext({ storageState: '.auth/admin-ls.json' });
    // await useSessionStorage(adminContext, 'admin');
    // const page = await adminContext.newPage();

    await page.goto('/');


    // await page.getByRole('button', { name: 'Login' }).click();
    await expect(page.getByRole('button', { name: 'Profile dropdown menu' })).toBeVisible();

    await page.getByRole('button', { name: 'Profile dropdown menu' }).click();
    await page.getByRole('menuitem', { name: 'My Profile' }).click();

    await expect(page.getByText('Name Bob Doe')).toBeVisible();
    await expect(page.getByText(process.env.USER_BOB_EMAIL)).toBeVisible();

    await page.getByRole('button', { name: 'Profile dropdown menu' }).click();
    await expect(page.getByRole('menuitem', { name: 'Administration' })).not.toBeVisible();
  });
});