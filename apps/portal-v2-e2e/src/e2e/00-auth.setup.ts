// auth.setup.ts
import { Browser, BrowserContext, expect, Page, test as setup } from '@playwright/test';
import { saveSessionStorage } from './helper/helper';

interface User {
  name: string;
  email: string;
  password: string;
}

const userList: User[] = [
  {
    name: 'admin',
    email: process.env.USER_ADMIN_EMAIL,
    password: process.env.USER_ADMIN_PASSWORD
  },
  {
    name: 'bob',
    email: process.env.USER_BOB_EMAIL,
    password: process.env.USER_BOB_PASSWORD
  },

];

setup('authenticate users', async ({ browser }) => {
  // await setLanguage(browser,'EN');
  for (const user of userList) {
    const context = await browser.newContext();
    await loginAs(context, user.name, user.email, user.password);
    await context.close();
  }
});

async function loginAs(context: BrowserContext, name: string, email: string, password: string) {
  // storageState should accept incomplete types but it does not so we cast to any here
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  // const context = await browser.newContext({ storageState: languageLS as any });

  const pagePromise = context.newPage();
  context.waitForEvent('page');
  const page = await pagePromise;
  await page.waitForLoadState();

  await page.goto('http://portal-v2.localdev:4200/');
  // login
  await page.getByText('Login').click();
  // context.waitForEvent('page');
  await page.waitForLoadState();

  await page.getByPlaceholder('Email').fill(email);
  await page.getByPlaceholder('Password').fill(password);

  await page.getByRole('button', { name: 'Login' }).click();

  // Wait until the page receives the cookies.
  await page.waitForLoadState();

  //
  // Sometimes login flow sets cookies in the process of several redirects.
  // Wait for the final URL to ensure that the cookies are actually set.
  // await page.waitForURL('/');
  // await page.waitForLoadState();
  // await page.getByText('Login').click();

  await page.getByRole('button', { name: 'Profile dropdown menu' }).waitFor();
  // Alternatively, you can wait until the page reaches a state where all cookies are set.
  await expect(page.getByRole('button', { name: 'Profile dropdown menu' })).toBeVisible();

  // End of authentication steps.
  await page.context().storageState({ path: `.auth/${name}-ls.json` });
  await saveSessionStorage(page, name);

  // DO NOT LOG OUT! Keycloak revokes access token on logout!
  // await page.getByRole('button', { name: 'Profile dropdown menu' }).click();
  // await page.getByRole('menuitem', { name: 'Logout' }).click();
  await page.close();
}

async function setLanguage(page: Page, lang: string) {
  await page.goto('http://portal-v2.localdev:4200/');
  await page.getByRole('button', { name: 'Sprache' }).click();
  await page.getByRole('menuitem', { name: lang }).click();
}