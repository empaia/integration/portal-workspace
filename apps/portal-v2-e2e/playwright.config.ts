import type { PlaywrightTestConfig } from '@playwright/test';
import { devices } from '@playwright/test';

/**
 * Read environment variables from file.
 * https://github.com/motdotla/dotenv
 */
import * as dotenv from 'dotenv';
dotenv.config();


/**
 * See https://playwright.dev/docs/test-configuration for more information
 */

const baseURL = process.env.E2E_BASE_URL || 'http://portal-v2.localdev:4200/';

export const config: PlaywrightTestConfig = {
  testDir: './src/e2e',
  retries: process.env.CI ? 2 : 0,
  maxFailures: 2,
  timeout: 30 * 1000,
  expect: {
    timeout: 5000,
  },
  use: {
    actionTimeout: 0,
    baseURL,
    trace: 'on-first-retry',
  },
  reporter: [
    [
      'html',
      { outputFolder: '../../dist/apps/portal-v2-e2e/playwright-report' },
    ],
    [
      'json',
      {
        outputFile:
          '../../dist/apps/portal-v2-e2e/playwright-report/test-results.json',
      },
    ],
  ],
  fullyParallel: true,
  forbidOnly: !!process.env.CI,
  workers: process.env.CI ? 1 : undefined,
  projects: [
    {
      name: 'setup',
      testMatch: /auth.setup\.ts/,
    },
    {
      name: 'chromium',
      use: {
        ...devices['Desktop Chrome'],
      },
      dependencies: ['setup'],
    },
    {
      name: 'firefox',
      use: {
        ...devices['Desktop Firefox'],
      },
      dependencies: ['setup'],
    },
    {
      name: 'webkit',
      use: {
        ...devices['Desktop Safari'],
      },
      dependencies: ['setup'],
    },
    /* Test against mobile viewports. */
    // {
    //   name: 'Mobile Chrome',
    //   use: {
    //     ...devices['Pixel 5'],
    //   },
    // },
    // {
    //   name: 'Mobile Safari',
    //   use: {
    //     ...devices['iPhone 12'],
    //   },
    // },
    /* Test against branded browsers. */
    // {
    //   name: 'Microsoft Edge',
    //   use: {
    //     channel: 'msedge',
    //   },
    // },
    // {
    //   name: 'Google Chrome',
    //   use: {
    //     channel: 'chrome',
    //   },
    // },
  ],
  webServer: {
    command: 'nx serve portal-v2 --host portal-v2.localdev',
    port: 4200,
    reuseExistingServer: true,
  },
};

export default config;
