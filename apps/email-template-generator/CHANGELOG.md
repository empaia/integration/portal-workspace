# Email Template Generator

## 0.0.X - 2023.05.16

* encoding of .properties files changed to 'latin1' - fixes display of german umlauts in emails

## 0.0.1

* added version.json, changelog and gitlab-ci

## 0.0.0 (2022-12-20)
* initialize project
