import { spawn } from 'child_process';

const run = () => {
  spawn('lite-server -c apps/email-template-generator/src/bs-config.js', {
    stdio: 'inherit',
    shell: true,
  });
};

run();
