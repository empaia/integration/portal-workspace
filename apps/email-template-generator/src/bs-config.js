const glob = require('glob');
const path = require('path');

const indexTemplate = (files) => /*html*/ `
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Current templates</title>
    <style>
      .app {
        max-width: 1000px;
        margin: 0 auto;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
      }
    </style>
  </head>
  <body>
    <div class="app">
      <h1>Current templates:</h1>
      ${files}
    </div>
  </body>
</html>
`;

module.exports = {
  port: 3000,
  files: [
    'apps/email-template-generator/src/app/templates/**/*.{html,htm,css,js}',
  ],
  server: {
    baseDir: 'apps/email-template-generator/src/app/templates',
    middleware: [
      {
        route: '/',
        handle: function (_req, res) {
          glob(
            'apps/email-template-generator/src/app/templates/*.html',
            (err, files) => {
              if (err) {
                console.error(err);
              }

              const filesAsHtmlList = files
                .map((e) => path.basename(e))
                .map((e) => `<li><a href="${e}">${e}</a></li>`)
                .join('\n');

              res.end(indexTemplate(filesAsHtmlList));
            }
          );
        },
      },
    ],
  },
};
