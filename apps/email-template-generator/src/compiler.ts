import * as fse from 'fs-extra';
import { glob } from 'glob';

import { transformers } from './lib/transformers';
import { writers } from './lib/writers';
import { OUT_DIR, workDir } from './lib/constants';

import { CompilerRules } from './lib/types';

const rules: CompilerRules = {
  default: {
    transform: ['inlineCss', 'addFooter'],
    text: ['addFooter', 'toText'],
    write: ['save'],
  },

  keycloak: {
    transform: ['inlineCss', 'addFooter'],
    text: ['addFooter', 'toText'],
    write: ['keycloak'],
  },

  auth: {
    transform: ['inlineCss', 'addFooter'],
    text: ['addFooter', 'weedOut', 'toText'],
    write: ['saveAuth'],
  },
};


function rulesetForFile(filename: string, rules: CompilerRules) {
  let rule: string | undefined = 'default';

  const validExtensions = Object.keys(rules)
    .filter((r) => r !== 'default')
    .map((e) => `.${e}.html`);

  for (const extension of validExtensions) {
    if (filename.endsWith(extension)) {
      rule = extension.match(/.*\.(.*).html/)?.[1];
      break;
    }
  }
  if (rule) {
    return rules[rule];
  }
};


(async () => {
  await glob.glob(`${workDir}/app/templates/**/*.*`, undefined).then((files) => {
    console.log(`glob files: ${files.length}`);

    // if (err) {
    //   console.error(err);
    // }

    fse.ensureDirSync(`${workDir}/${OUT_DIR}/keycloak/`);
    const copyFrom = `${workDir}/app/bones/keycloak/`;
    const copyTo = `${workDir}/${OUT_DIR}/keycloak/`;
    fse.copySync(copyFrom, copyTo);
    console.log(`copy from ${copyFrom} to ${copyTo}`);

    for (const file of files) {
      if (file.endsWith(".html") || file.endsWith(".htm")) {
        // console.log(`processing file: ${file}`);
        const template = fse.readFileSync(file, 'utf8');
        const set = rulesetForFile(file, rules);

        if (set) {
          const compiledHtml: string = set.transform.reduce((prev, cur) => {
            return transformers[cur as keyof typeof transformers].call(prev);
          }, template);

          const compiledText: string = set.text.reduce((prev, cur) => {
            return transformers[cur as keyof typeof transformers].call(prev);
          }, template);

          for (const fun of set.write) {
            writers[fun as keyof typeof writers].call(null, file, compiledHtml, compiledText);
          }
        }
      }
      if (file.endsWith(".csv")) {
        const pathSegments = file.replaceAll('\\','/').split("/");
        console.log(`pathSegments: ${pathSegments}`);

        const copyTo = `${workDir}/${OUT_DIR}/notifications/${pathSegments[pathSegments.length - 1]}`;
        console.log(`copyTo: ${copyTo}`);
        fse.copySync(file, copyTo);
      }
    }
  });
})();
