import * as path from 'path';
import * as fs from 'fs-extra';
import * as cheerio from 'cheerio';
import * as Case from 'case';

import { OUT_DIR, propertiesEncoding, TEMPLATE_PARAMS, workDir } from './constants';


const outDir = workDir + OUT_DIR;

export const writers = {
  save(filename: string, html: string, text: string): void {
    const outFile = `${outDir}/${path.basename(filename)}`;

    fs.writeFileSync(outFile, html, 'utf8');
    fs.writeFileSync(`${outFile}.txt`, text, 'utf8');
  },

  saveAuth(filename: string, html: string, text: string): void {
    fs.ensureDirSync(`${outDir}/auth/`);

    const outFileHtml = `${outDir}/auth/${path.basename(
      filename,
      '.auth.html'
    )}.html`;

    const outFileText = `${outDir}/auth/${path.basename(
      filename,
      '.auth.html'
    )}.txt`;

    fs.writeFileSync(outFileHtml, html, 'utf8');
    fs.writeFileSync(outFileText, text, 'utf8');
  },

  keycloak(filename: string, html: string, text: string): void {
    /**
     * @param {string} originalTemplate
     * @param {string} propertyKey
     * @returns {array}
     */
    const preparedKeycloakTemplateHtml = (
      originalTemplate: string,
      propertyKey: string
    ) => {
      const $ = cheerio.load(originalTemplate);
      const body = $.root().find('body');
      const parameters = propertyKey in TEMPLATE_PARAMS ? TEMPLATE_PARAMS[propertyKey].join(', ') : '';
      let substitute = '${kcSanitize(msg("PROPERTY_MSG", PARAMETERS))?no_esc}'.replace(
        'PROPERTY_MSG',
        propertyKey
      );
      substitute = substitute.replace(
        'PARAMETERS',
        parameters
      );
      const propertyValue = body?.html()?.trim().replace(/\n/g, '\\n');

      body.html(substitute);

      return [$.root().html(), propertyValue];
    };

    /**
     * @param {string} filename
     * @returns {string}
     */
    const filenameToPropertiesKey = (filename: string) =>
      Case.camel(path.basename(filename, '.keycloak.html').slice(0, -3));

    /**
     * @param {string} textTemplate
     * @param {string} propertyKey
     * @returns {array}
     */
    const preparedKeycloakTemplateText = (textTemplate: string, propertyKey: string) => {
      const parameters = propertyKey in TEMPLATE_PARAMS ? TEMPLATE_PARAMS[propertyKey].join(', ') : '';
      let content =
        '<#ftl output_format="plainText">\n${msg("SUBSTITUTE", PARAMETERS)}'.replace(
          'SUBSTITUTE',
          propertyKey
        );
      content = content.replace(
        'PARAMETERS',
        parameters
      );

      const propertyValue = textTemplate.replace(/\n/g, '\\n');

      return [content, propertyValue];
    };

    /**
     * @param {string} configText
     * @returns {array}
     */
    const propertiesConfToArray = (configText: string) => {
      return configText
        .split('\n')
        .filter((e) => !e.startsWith('#'))
        .map((e) => {
          const key = e.split('=')[0].trim();
          const value = e.split('=').slice(1).join('=').trim();
          return [key, value];
        })
        .filter((e) => e[0] !== '' || e[1] !== '');
    };

    /**
     * @param {array} properties
     * @param {string} key
     * @param {string} value
     */
    const replaceProperty = (properties: any[], key: string, value: string) => {
      return properties.map((e) => {
        if (e[0] === key) {
          e[1] = value;
        }

        return e;
      });
    };

    /**
     * @param {array} config
     * @returns {string}
     */
    const propertiesArrayToConf = (config: any[]) => {
      return config.map((e) => e.join('=')).join('\n');
    };

    //////////////////////

    const propertyKey = filenameToPropertiesKey(filename);

    const [preparedTemplateHtml, preparedPropertyValueHtml] =
      preparedKeycloakTemplateHtml(html, `${propertyKey}BodyHtml`);

    const [preparedTemplateText, preparedPropertyValueText] =
      preparedKeycloakTemplateText(text, `${propertyKey}Body`);

    const outFileHtml = `${outDir}/keycloak/email/html/${path.basename(
      filename,
      '.keycloak.html'
    ).slice(0, -3)}.ftl`;

    const outFileText = `${outDir}/keycloak/email/text/${path.basename(
      filename,
      '.keycloak.html'
    ).slice(0, -3)}.ftl`;

    const langCode = filename.endsWith('DE.keycloak.html') ? 'de' : 'en';
    const propertiesFile = `${outDir}/keycloak/email/messages/messages_${langCode}.properties`;

    let properties: any = fs.readFileSync(propertiesFile, 'utf8');
    properties = propertiesConfToArray(properties) as any;
    properties = replaceProperty(properties as any, `${propertyKey}BodyHtml`, preparedPropertyValueHtml as any);
    properties = replaceProperty(properties as any, `${propertyKey}Body`, preparedPropertyValueText);
    const modified_properties =  propertiesEncoding + propertiesArrayToConf(properties as any);


    fs.writeFileSync(propertiesFile, modified_properties, 'utf8');
    fs.writeFileSync(`${outFileHtml}`, preparedTemplateHtml as any, 'utf8');
    fs.writeFileSync(`${outFileText}`, preparedTemplateText, 'utf8');
  },
};
