// basedir to work from
// glob needs posix paths therefore we place backslash with slash
export const workDir = __dirname.replaceAll('\\', '/');
export const TEMPLATE_DIR = `/app/templates`;
export const OUT_DIR = '/out';

export const propertiesEncoding = '# encoding: UTF-8 \n';

export const TEMPLATE_PARAMS: Record<string, string[]> = {
  "emailVerificationBodyHtml": [
    "link", "linkExpiration", "realmName", "linkExpirationFormatter(linkExpiration)", "user.username"
  ],
  "emailVerificationBody": [
    "link", "linkExpiration", "realmName", "linkExpirationFormatter(linkExpiration)", "user.username"
  ],
  "passwordResetBodyHtml": [
    "link", "linkExpiration", "realmName", "linkExpirationFormatter(linkExpiration)", "user.email"
  ],
  "passwordResetBody": [
    "link", "linkExpiration", "realmName", "linkExpirationFormatter(linkExpiration)", "user.email"
  ],
};
