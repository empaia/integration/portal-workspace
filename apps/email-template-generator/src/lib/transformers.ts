import * as fs from 'fs-extra';
import * as juice from 'juice';
import { convert } from 'html-to-text';
import * as cheerio from 'cheerio';

import { TEMPLATE_DIR, workDir } from './constants';

export const transformers = {
  inlineCss(): string {
    const extraCss = fs.readFileSync(`${workDir}/${TEMPLATE_DIR}/shared.css`, 'utf8');

    return juice(this as unknown as string, { extraCss });
  },

  addFooter(): string {
    const $ = cheerio.load(this as unknown as string, null, true);

    let imprint = 'Imprint';
    let policy = 'Data privacy policy';

    if ($('html').attr('lang') === 'de') {
      imprint = 'Impressum';
      policy = 'Datenschutzerklärung';
    }

    const footer1 = `
      <p>
        E-Mail: <a class="link" href="mailto:support@empaia.org">support@empaia.org</a>
      </p>
    `;
    const footer2 = `
      <p>
        <a class="link" href="https://www.empaia.org">EMPAIA</a> |
        <a class="link" href="https://portal.empaia.org/imprint">${imprint}</a> |
        <a class="link" href="https://portal.empaia.org/data-privacy">${policy}</a>
      </p>
    `;
    $('footer').append(footer1);
    $('footer').append(footer2);

    return $.html();
  },

  toText(): string {
    return convert(this as unknown as string, {
      wordwrap: 80,
      selectors: [{ selector: 'title', format: 'skip' }],
    });
  },

  weedOut(): string {
    const $ = cheerio.load(this as unknown as string, null, false);

    const selectors = ['[th\\:text]', '[th\\:href]'];

    for (const selector of selectors) {
      const found = $(selector);
      const value = found.attr(selectorToAttr(selector));
      if (value) {
        const plainText = value.split('{')?.pop()?.split('}')[0].split('+');

        if (plainText) {
          const constText = plainText
            .filter(e => e.includes('\''))
            .map(e => e.replaceAll(' ', ''))
            .map(e => e.replaceAll('\'', ''));
          let newSelector = plainText
            .filter(e => !e.includes('\''))
            .map(e => e.replaceAll(' ', ''))
            .map(e => `[( $\{${e}} )]`)
            .join(' ');

          if (constText.includes("Hello") || constText.includes("Hallo")) {
            newSelector = constText[0] + " " + newSelector + constText[2];
          }

          found.html(value).html(newSelector);
        }
      }
    }

    return $.html();
  },
};

function selectorToAttr(selector: string) {
  return selector.replace(/[\\|[\]]/g, '');
}
