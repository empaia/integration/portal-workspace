export interface CompilerRules {
  [key: string]: {
    transform: string[];
    text: string[];
    write: string[];
  };
}
