# CHANGELOG Portal

# 2.15.1

* upgraded to angular 17

# 2.15.0

* added statistics to moderator view

# 2.14.1

* changed date format for Pathology AI Register to european standard

# 2.14.0

* added export Pathology AI Register to excel sheet

# 2.13.2

* changed feedback of buttons in Pathology AI Register section

# 2.13.1

# Refactor

* changed translations from Market Survey to Pathology AI Register

# 2.13.0

### Features

* added copy link to clipboard button in market survey
* added send link via email button in market survey

# 2.12.2

### Fixes

* break to long words in product description

# 2.12.1

### Refactor

* clicking on save button in create/edit market survey product resolves in showing of which necessary fields are missing

# 2.12.0

### Features

* uploaded images will be resized to a MB limit
* MB limit for images can be set via env variable

# 2.11.1

* Initial public commit
