(function (window) {
    window['env'] = window['env'] || {};

    // Environment variables
    window['env']['production'] = true;

    // API URLS
    window['env']['mpsApiUrl'] = '${PORTAL_V2_MPS_API_URL}'
    window['env']['aaaApiUrl'] = '${PORTAL_V2_AAA_API_URL}'
    window['env']['evesApiUrl'] = '${PORTAL_V2_EVES_API_URL}'

    // Auth config
    window['env']['authKeycloakUrl'] = '${PORTAL_V2_AUTH_KEYCLOAK_URL}'
    window['env']['authClientId'] = '${PORTAL_V2_KEYCLOAK_CLIENT_ID}'
    window['env']['authRealmName'] = '${PORTAL_V2_AUTH_REALM_NAME}'

    window['env']['authRequireHttps'] = '${PORTAL_V2_AUTH_REQUIRE_HTTPS}'
    window['env']['authShowDebugInformation'] = '${PORTAL_V2_AUTH_SHOW_DEBUG_INFORMATION}'
    window['env']['authSyncSessionCheck'] = '${PORTAL_V2_AUTH_SYNC_SESSION_CHECK}'

    window['env']['showClearances'] = '${PORTAL_V2_SHOW_CLEARANCES}'

    window['env']['maxImageMbSize'] = '${PORTAL_V2_MAX_IMAGE_MB_SIZE}'
})(this);
