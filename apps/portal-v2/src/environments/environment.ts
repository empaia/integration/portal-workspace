// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { baseEnvironment, Environment } from './environment.model';

export const environment: Environment = {
  ...baseEnvironment,
  appVersion: baseEnvironment.appVersion + '-dev',
  production: false,

  mpsApiUrl: "http://localhost:39005",
  aaaApiUrl: "http://localhost:39001",
  evesApiUrl: "http://localhost:39013",

  authKeycloakUrl: "http://localhost:39000",
  authClientId: "portal_client",
  authRealmName: "empaia",

  authRequireHttps: false,
  authShowDebugInformation: false,
  authSyncSessionCheck: false,

  showClearances: true,

  maxImageMbSize: 1,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
