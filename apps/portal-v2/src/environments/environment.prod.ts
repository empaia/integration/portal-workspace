/* eslint-disable @typescript-eslint/no-explicit-any */

import { baseEnvironment, Environment, toBoolean } from './environment.model';

const windowEnv = ((window as any)['env'] as Environment);

export const environment: Environment = {
  ...baseEnvironment,
  production: true,

  mpsApiUrl: windowEnv.mpsApiUrl,
  aaaApiUrl: windowEnv.aaaApiUrl,
  evesApiUrl: windowEnv.evesApiUrl,

  authKeycloakUrl: windowEnv.authKeycloakUrl,
  authRealmName: windowEnv.authRealmName,
  authClientId: windowEnv.authClientId,

  authSyncSessionCheck: toBoolean(windowEnv.authSyncSessionCheck),
  authRequireHttps: toBoolean(windowEnv.authRequireHttps),
  authShowDebugInformation: toBoolean(windowEnv.authShowDebugInformation),

  showClearances: toBoolean(windowEnv.showClearances),

  maxImageMbSize: windowEnv.maxImageMbSize && Number(windowEnv.maxImageMbSize) > 0 ? Number(windowEnv.maxImageMbSize) : baseEnvironment.maxImageMbSize,
};
