// import * as vFile from '../../version.json';
// eslint-disable-next-line @typescript-eslint/no-var-requires
// const appVersion = require('../../version.json').version as string;
import { default as vFile } from '../../version.json';

// convert env to string to boolean
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const toBoolean = (dataStr: any) => {
  return !!(dataStr?.toLowerCase?.() === 'true' || dataStr === true);
};

export interface Environment {
  production: boolean,
  appVersion: string | undefined,


  //api
  aaaApiUrl: string,
  mpsApiUrl: string,
  evesApiUrl: string,

  // auth
  authKeycloakUrl: string,
  authClientId: string,
  authRealmName: string,
  authRequireHttps: boolean,
  authShowDebugInformation: boolean,
  authSyncSessionCheck: boolean,

  // clearances
  showClearances: boolean,

  // image
  maxImageMbSize: number;
}

//default env
export const baseEnvironment: Environment = {
  appVersion: vFile?.version,
  // some sane defaults
  production: false,
  authRequireHttps: true,
  authShowDebugInformation: false,
  authSyncSessionCheck: false,

  authRealmName: "",
  authClientId: "",

  authKeycloakUrl: "",

  aaaApiUrl: "",
  mpsApiUrl: "",
  evesApiUrl:"",

  showClearances: false,

  maxImageMbSize: 1,
};
