/* tslint:disable */
/* eslint-disable */
import { TextTranslation } from './text-translation';
export interface InfoSourceContent {

  /**
   * Info source content label
   */
  label?: (Array<TextTranslation> | null);

  /**
   * Url of the info source content
   */
  reference_url: string;
}
