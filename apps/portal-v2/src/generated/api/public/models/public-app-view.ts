/* tslint:disable */
/* eslint-disable */
import { AppDetails } from './app-details';
import { MediaList } from './media-list';
import { TagList } from './tag-list';
export interface PublicAppView {

  /**
   * UNIX timestamp in seconds - set by server
   */
  created_at: number;
  details?: (AppDetails | null);
  media?: (MediaList | null);

  /**
   * If true, portal app can be listed although technical app is not yet available
   */
  non_functional?: (boolean | null);

  /**
   * If true, app is intended to be used for reasearch only
   */
  research_only?: boolean;

  /**
   * UNIX timestamp in seconds - set by server
   */
  reviewed_at?: (number | null);
  tags?: (TagList | null);

  /**
   * Version of the currently active app
   */
  version?: (string | null);
}
