/* tslint:disable */
/* eslint-disable */
export interface ClearanceQuery {

  /**
   * Filter option for analysis types
   */
  analysis?: (Array<string> | null);

  /**
   * Filter option for clearance/certification types
   */
  clearances?: (Array<string> | null);

  /**
   * Filter option for image types used for analysis
   */
  image_types?: (Array<string> | null);

  /**
   * Filter option for indication types
   */
  indications?: (Array<string> | null);

  /**
   * Filter option for organizations
   */
  organizations?: (Array<string> | null);

  /**
   * Filter option for histopathological preparation procedures
   */
  procedures?: (Array<string> | null);

  /**
   * Filter option for WSI scanner
   */
  scanners?: (Array<string> | null);

  /**
   * Filter option for stain types
   */
  stains?: (Array<string> | null);

  /**
   * Filter option for tissue types
   */
  tissues?: (Array<string> | null);
}
