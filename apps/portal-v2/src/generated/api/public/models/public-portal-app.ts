/* tslint:disable */
/* eslint-disable */
import { ListingStatus } from './listing-status';
import { PublicActiveAppViews } from './public-active-app-views';
export interface PublicPortalApp {

  /**
   * Currently active app views
   */
  active_app_views?: (PublicActiveAppViews | null);

  /**
   * UNIX timestamp in seconds - set by server
   */
  created_at: number;

  /**
   * Creator ID
   */
  creator_id: string;

  /**
   * ID of the portal app
   */
  id: string;

  /**
   * ID of the organization providing the portal app
   */
  organization_id: string;
  status: ListingStatus;

  /**
   * UNIX timestamp in seconds - set by server
   */
  updated_at: number;
}
