/* tslint:disable */
/* eslint-disable */
export enum DatePrecision {
  DATE = 'DATE',
  MONTH_YEAR = 'MONTH_YEAR',
  QUARTER = 'QUARTER',
  YEAR = 'YEAR',
  TBA = 'TBA',
  UNKNOWN = 'UNKNOWN'
}
