/* tslint:disable */
/* eslint-disable */
export enum ListingStatus {
  LISTED = 'LISTED',
  DELISTED = 'DELISTED',
  ADMIN_DELISTED = 'ADMIN_DELISTED',
  DRAFT = 'DRAFT'
}
