/* tslint:disable */
/* eslint-disable */
import { PublicAppView } from './public-app-view';
export interface PublicActiveAppViews {

  /**
   * App view for EAD v1-darft3 without App UI (WBC 1.0)
   */
  v1?: (PublicAppView | null);

  /**
   * App view for EAD v1-darft3 with App UI (WBC 2.0)
   */
  v2?: (PublicAppView | null);

  /**
   * App view for EAD v3
   */
  v3?: (PublicAppView | null);
}
