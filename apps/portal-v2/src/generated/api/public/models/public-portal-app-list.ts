/* tslint:disable */
/* eslint-disable */
import { PublicPortalApp } from './public-portal-app';
export interface PublicPortalAppList {

  /**
   * Count of all available apps
   */
  item_count: number;
  items: Array<PublicPortalApp>;
}
