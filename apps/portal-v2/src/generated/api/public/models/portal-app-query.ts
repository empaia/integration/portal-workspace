/* tslint:disable */
/* eslint-disable */
import { ApiVersion } from './api-version';
export interface PortalAppQuery {

  /**
   * Filter option for active app version
   */
  active_app_version?: (ApiVersion | null);

  /**
   * Filter option for analysis types
   */
  analysis?: (Array<string> | null);

  /**
   * Filter option for clearance/certification types
   */
  clearances?: (Array<string> | null);

  /**
   * Filter option for indication types
   */
  indications?: (Array<string> | null);

  /**
   * Filter option for stain types
   */
  stains?: (Array<string> | null);

  /**
   * Filter option for tissue types
   */
  tissues?: (Array<string> | null);
}
