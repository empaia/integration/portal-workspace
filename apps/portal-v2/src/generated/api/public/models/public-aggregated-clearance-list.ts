/* tslint:disable */
/* eslint-disable */
import { PublicAggregatedClearance } from './public-aggregated-clearance';
export interface PublicAggregatedClearanceList {

  /**
   * Total number of clearances matching the query
   */
  item_count: number;
  items: Array<PublicAggregatedClearance>;
}
