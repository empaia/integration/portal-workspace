/* tslint:disable */
/* eslint-disable */
export enum InfoSourceType {
  EMAIL_CONTACT = 'EMAIL_CONTACT',
  PUBLIC_WEBSITE = 'PUBLIC_WEBSITE',
  UNKNOWN = 'UNKNOWN'
}
