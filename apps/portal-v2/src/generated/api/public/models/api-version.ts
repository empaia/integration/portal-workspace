/* tslint:disable */
/* eslint-disable */
export enum ApiVersion {
  V_1 = 'v1',
  V_2 = 'v2',
  V_3 = 'v3'
}
