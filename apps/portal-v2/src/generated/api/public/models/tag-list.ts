/* tslint:disable */
/* eslint-disable */
import { AppTag } from './app-tag';
export interface TagList {

  /**
   * List of analysis
   */
  analysis?: Array<AppTag>;

  /**
   * List of market clearances / certifications
   */
  clearances?: Array<AppTag>;

  /**
   * List of indications
   */
  indications?: Array<AppTag>;

  /**
   * List of stains
   */
  stains?: Array<AppTag>;

  /**
   * List of tissues
   */
  tissues?: Array<AppTag>;
}
