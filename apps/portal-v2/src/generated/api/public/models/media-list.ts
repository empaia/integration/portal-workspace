/* tslint:disable */
/* eslint-disable */
import { MediaObject } from './media-object';
export interface MediaList {

  /**
   * Banner media
   */
  banner?: Array<MediaObject>;

  /**
   * Manual media
   */
  manual?: Array<MediaObject>;

  /**
   * Peek media
   */
  peek?: Array<MediaObject>;

  /**
   * Workflow media
   */
  workflow?: Array<MediaObject>;
}
