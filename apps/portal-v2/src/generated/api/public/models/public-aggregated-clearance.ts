/* tslint:disable */
/* eslint-disable */
import { DatePrecision } from './date-precision';
import { ExtendedTagList } from './extended-tag-list';
import { InfoProvider } from './info-provider';
import { InfoSourceContent } from './info-source-content';
import { InfoSourceType } from './info-source-type';
import { TextTranslation } from './text-translation';
export interface PublicAggregatedClearance {

  /**
   * UNIX timestamp in seconds - set by server
   */
  created_at: number;

  /**
   * Description
   */
  description?: (Array<TextTranslation> | null);

  /**
   * Clearance ID
   */
  id: string;

  /**
   * Provider of the clearance/product info
   */
  info_provider: InfoProvider;

  /**
   * List of info source contents
   */
  info_source_content?: (Array<InfoSourceContent> | null);

  /**
   * Source type where the product info was obtained
   */
  info_source_type: InfoSourceType;

  /**
   * UNIX timestamp in seconds - set by server
   */
  info_updated_at: number;

  /**
   * Organization ID
   */
  organization_id: string;

  /**
   * Brand and product name
   */
  product_name: string;

  /**
   * UNIX timestamp in seconds - set by server
   */
  product_release_at: number;

  /**
   * Date precision type
   */
  product_release_at_precision: DatePrecision;

  /**
   * Additional information on product tags that can not be expressed through existing tags
   */
  tag_remark?: (string | null);
  tags?: (ExtendedTagList | null);

  /**
   * UNIX timestamp in seconds - set by server
   */
  updated_at: number;
}
