/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { ClearanceQuery } from '../models/clearance-query';
import { PublicAggregatedClearanceList } from '../models/public-aggregated-clearance-list';

@Injectable({
  providedIn: 'root',
})
export class ClearancesService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation clearancesQueryPut
   */
  static readonly ClearancesQueryPutPath = '/clearances/query';

  /**
   * Query all active clearances.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `clearancesQueryPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  clearancesQueryPut$Response(params: {
    skip?: number;
    limit?: number;
    'statistics-id'?: (string | null);
    body: ClearanceQuery
  },
  context?: HttpContext

): Observable<StrictHttpResponse<PublicAggregatedClearanceList>> {

    const rb = new RequestBuilder(this.rootUrl, ClearancesService.ClearancesQueryPutPath, 'put');
    if (params) {
      rb.query('skip', params.skip, {});
      rb.query('limit', params.limit, {});
      rb.header('statistics-id', params['statistics-id'], {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<PublicAggregatedClearanceList>;
      })
    );
  }

  /**
   * Query all active clearances.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `clearancesQueryPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  clearancesQueryPut(params: {
    skip?: number;
    limit?: number;
    'statistics-id'?: (string | null);
    body: ClearanceQuery
  },
  context?: HttpContext

): Observable<PublicAggregatedClearanceList> {

    return this.clearancesQueryPut$Response(params,context).pipe(
      map((r: StrictHttpResponse<PublicAggregatedClearanceList>) => r.body as PublicAggregatedClearanceList)
    );
  }

}
