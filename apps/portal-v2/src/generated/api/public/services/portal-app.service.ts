/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { PortalAppQuery } from '../models/portal-app-query';
import { PublicPortalApp } from '../models/public-portal-app';
import { PublicPortalAppList } from '../models/public-portal-app-list';

@Injectable({
  providedIn: 'root',
})
export class PortalAppService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation portalAppsQueryPut
   */
  static readonly PortalAppsQueryPutPath = '/portal-apps/query';

  /**
   * Query all accepted portal apps.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `portalAppsQueryPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  portalAppsQueryPut$Response(params: {
    skip?: number;
    limit?: number;
    'statistics-id'?: (string | null);
    body: PortalAppQuery
  },
  context?: HttpContext

): Observable<StrictHttpResponse<PublicPortalAppList>> {

    const rb = new RequestBuilder(this.rootUrl, PortalAppService.PortalAppsQueryPutPath, 'put');
    if (params) {
      rb.query('skip', params.skip, {});
      rb.query('limit', params.limit, {});
      rb.header('statistics-id', params['statistics-id'], {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<PublicPortalAppList>;
      })
    );
  }

  /**
   * Query all accepted portal apps.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `portalAppsQueryPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  portalAppsQueryPut(params: {
    skip?: number;
    limit?: number;
    'statistics-id'?: (string | null);
    body: PortalAppQuery
  },
  context?: HttpContext

): Observable<PublicPortalAppList> {

    return this.portalAppsQueryPut$Response(params,context).pipe(
      map((r: StrictHttpResponse<PublicPortalAppList>) => r.body as PublicPortalAppList)
    );
  }

  /**
   * Path part for operation portalAppsPortalAppIdGet
   */
  static readonly PortalAppsPortalAppIdGetPath = '/portal-apps/{portal_app_id}';

  /**
   * Get accepted portal app by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `portalAppsPortalAppIdGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  portalAppsPortalAppIdGet$Response(params: {

    /**
     * Portal App ID
     */
    portal_app_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<PublicPortalApp>> {

    const rb = new RequestBuilder(this.rootUrl, PortalAppService.PortalAppsPortalAppIdGetPath, 'get');
    if (params) {
      rb.path('portal_app_id', params.portal_app_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<PublicPortalApp>;
      })
    );
  }

  /**
   * Get accepted portal app by ID.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `portalAppsPortalAppIdGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  portalAppsPortalAppIdGet(params: {

    /**
     * Portal App ID
     */
    portal_app_id: string;
  },
  context?: HttpContext

): Observable<PublicPortalApp> {

    return this.portalAppsPortalAppIdGet$Response(params,context).pipe(
      map((r: StrictHttpResponse<PublicPortalApp>) => r.body as PublicPortalApp)
    );
  }

  /**
   * Path part for operation portalAppsPortalAppIdRelatedPortalAppsPut
   */
  static readonly PortalAppsPortalAppIdRelatedPortalAppsPutPath = '/portal-apps/{portal_app_id}/related-portal-apps';

  /**
   * Get related portal apps.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `portalAppsPortalAppIdRelatedPortalAppsPut()` instead.
   *
   * This method doesn't expect any request body.
   */
  portalAppsPortalAppIdRelatedPortalAppsPut$Response(params: {

    /**
     * Portal App ID
     */
    portal_app_id: string;
    skip?: number;
    limit?: number;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<PublicPortalAppList>> {

    const rb = new RequestBuilder(this.rootUrl, PortalAppService.PortalAppsPortalAppIdRelatedPortalAppsPutPath, 'put');
    if (params) {
      rb.path('portal_app_id', params.portal_app_id, {});
      rb.query('skip', params.skip, {});
      rb.query('limit', params.limit, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<PublicPortalAppList>;
      })
    );
  }

  /**
   * Get related portal apps.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `portalAppsPortalAppIdRelatedPortalAppsPut$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  portalAppsPortalAppIdRelatedPortalAppsPut(params: {

    /**
     * Portal App ID
     */
    portal_app_id: string;
    skip?: number;
    limit?: number;
  },
  context?: HttpContext

): Observable<PublicPortalAppList> {

    return this.portalAppsPortalAppIdRelatedPortalAppsPut$Response(params,context).pipe(
      map((r: StrictHttpResponse<PublicPortalAppList>) => r.body as PublicPortalAppList)
    );
  }

}
