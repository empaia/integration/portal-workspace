/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { ExtendedTagList } from '../models/extended-tag-list';

@Injectable({
  providedIn: 'root',
})
export class GeneralService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation tagsGet
   */
  static readonly TagsGetPath = '/tags';

  /**
   * Get all available tags.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `tagsGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  tagsGet$Response(params?: {
  },
  context?: HttpContext

): Observable<StrictHttpResponse<ExtendedTagList>> {

    const rb = new RequestBuilder(this.rootUrl, GeneralService.TagsGetPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ExtendedTagList>;
      })
    );
  }

  /**
   * Get all available tags.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `tagsGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  tagsGet(params?: {
  },
  context?: HttpContext

): Observable<ExtendedTagList> {

    return this.tagsGet$Response(params,context).pipe(
      map((r: StrictHttpResponse<ExtendedTagList>) => r.body as ExtendedTagList)
    );
  }

}
