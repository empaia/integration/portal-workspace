/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { StatisticsList } from '../models/statistics-list';

@Injectable({
  providedIn: 'root',
})
export class StatisticsService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation statisticsGet
   */
  static readonly StatisticsGetPath = '/statistics';

  /**
   * Get all pending portal apps for given organization.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `statisticsGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  statisticsGet$Response(params?: {
  },
  context?: HttpContext

): Observable<StrictHttpResponse<StatisticsList>> {

    const rb = new RequestBuilder(this.rootUrl, StatisticsService.StatisticsGetPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<StatisticsList>;
      })
    );
  }

  /**
   * Get all pending portal apps for given organization.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `statisticsGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  statisticsGet(params?: {
  },
  context?: HttpContext

): Observable<StatisticsList> {

    return this.statisticsGet$Response(params,context).pipe(
      map((r: StrictHttpResponse<StatisticsList>) => r.body as StatisticsList)
    );
  }

}
