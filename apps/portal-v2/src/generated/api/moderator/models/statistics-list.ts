/* tslint:disable */
/* eslint-disable */
import { Statistics } from './statistics';
export interface StatisticsList {

  /**
   * Count of all statistics items
   */
  item_count: number;
  items: Array<Statistics>;
}
