/* tslint:disable */
/* eslint-disable */
import { StatisticsPage } from './statistics-page';
export interface Statistics {
  accessed_at: number;
  page: StatisticsPage;
  statistics_id: string;
}
