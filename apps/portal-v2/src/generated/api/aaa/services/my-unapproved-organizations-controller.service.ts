/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { CanRequestApprovalEntity } from '../models/can-request-approval-entity';
import { ConfidentialOrganizationContactDataEntity } from '../models/confidential-organization-contact-data-entity';
import { OrganizationAccountStateQueryEntity } from '../models/organization-account-state-query-entity';
import { PostInitialConfidentialOrganizationDetailsEntity } from '../models/post-initial-confidential-organization-details-entity';
import { PostOrganizationEntity } from '../models/post-organization-entity';
import { PostOrganizationNameEntity } from '../models/post-organization-name-entity';
import { UnapprovedConfidentialOrganizationProfileEntity } from '../models/unapproved-confidential-organization-profile-entity';
import { UnapprovedConfidentialOrganizationProfilesEntity } from '../models/unapproved-confidential-organization-profiles-entity';

@Injectable({
  providedIn: 'root',
})
export class MyUnapprovedOrganizationsControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation revokeUnapprovedOrganizationApprovalRequest
   */
  static readonly RevokeUnapprovedOrganizationApprovalRequestPath = '/api/v2/my/unapproved-organizations/{organization_id}/revoke-approval-request';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `revokeUnapprovedOrganizationApprovalRequest()` instead.
   *
   * This method doesn't expect any request body.
   */
  revokeUnapprovedOrganizationApprovalRequest$Response(params: {
    organization_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<UnapprovedConfidentialOrganizationProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, MyUnapprovedOrganizationsControllerService.RevokeUnapprovedOrganizationApprovalRequestPath, 'put');
    if (params) {
      rb.path('organization_id', params.organization_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UnapprovedConfidentialOrganizationProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `revokeUnapprovedOrganizationApprovalRequest$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  revokeUnapprovedOrganizationApprovalRequest(params: {
    organization_id: string;
  },
  context?: HttpContext

): Observable<UnapprovedConfidentialOrganizationProfileEntity> {

    return this.revokeUnapprovedOrganizationApprovalRequest$Response(params,context).pipe(
      map((r: StrictHttpResponse<UnapprovedConfidentialOrganizationProfileEntity>) => r.body as UnapprovedConfidentialOrganizationProfileEntity)
    );
  }

  /**
   * Path part for operation requestUnapprovedOrganizationApproval
   */
  static readonly RequestUnapprovedOrganizationApprovalPath = '/api/v2/my/unapproved-organizations/{organization_id}/request-approval';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `requestUnapprovedOrganizationApproval()` instead.
   *
   * This method doesn't expect any request body.
   */
  requestUnapprovedOrganizationApproval$Response(params: {
    organization_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<UnapprovedConfidentialOrganizationProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, MyUnapprovedOrganizationsControllerService.RequestUnapprovedOrganizationApprovalPath, 'put');
    if (params) {
      rb.path('organization_id', params.organization_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UnapprovedConfidentialOrganizationProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `requestUnapprovedOrganizationApproval$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  requestUnapprovedOrganizationApproval(params: {
    organization_id: string;
  },
  context?: HttpContext

): Observable<UnapprovedConfidentialOrganizationProfileEntity> {

    return this.requestUnapprovedOrganizationApproval$Response(params,context).pipe(
      map((r: StrictHttpResponse<UnapprovedConfidentialOrganizationProfileEntity>) => r.body as UnapprovedConfidentialOrganizationProfileEntity)
    );
  }

  /**
   * Path part for operation updateUnapprovedOrganizationName
   */
  static readonly UpdateUnapprovedOrganizationNamePath = '/api/v2/my/unapproved-organizations/{organization_id}/name';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `updateUnapprovedOrganizationName()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateUnapprovedOrganizationName$Response(params: {
    organization_id: string;
    body: PostOrganizationNameEntity
  },
  context?: HttpContext

): Observable<StrictHttpResponse<UnapprovedConfidentialOrganizationProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, MyUnapprovedOrganizationsControllerService.UpdateUnapprovedOrganizationNamePath, 'put');
    if (params) {
      rb.path('organization_id', params.organization_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UnapprovedConfidentialOrganizationProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `updateUnapprovedOrganizationName$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateUnapprovedOrganizationName(params: {
    organization_id: string;
    body: PostOrganizationNameEntity
  },
  context?: HttpContext

): Observable<UnapprovedConfidentialOrganizationProfileEntity> {

    return this.updateUnapprovedOrganizationName$Response(params,context).pipe(
      map((r: StrictHttpResponse<UnapprovedConfidentialOrganizationProfileEntity>) => r.body as UnapprovedConfidentialOrganizationProfileEntity)
    );
  }

  /**
   * Path part for operation setUnapprovedOrganizationLogo
   */
  static readonly SetUnapprovedOrganizationLogoPath = '/api/v2/my/unapproved-organizations/{organization_id}/logo';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `setUnapprovedOrganizationLogo()` instead.
   *
   * This method sends `multipart/form-data` and handles request body of type `multipart/form-data`.
   */
  setUnapprovedOrganizationLogo$Response(params: {
    organization_id: string;
    body?: {
'logo': Blob;
}
  },
  context?: HttpContext

): Observable<StrictHttpResponse<{
}>> {

    const rb = new RequestBuilder(this.rootUrl, MyUnapprovedOrganizationsControllerService.SetUnapprovedOrganizationLogoPath, 'put');
    if (params) {
      rb.path('organization_id', params.organization_id, {});
      rb.body(params.body, 'multipart/form-data');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/hal+json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<{
        }>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `setUnapprovedOrganizationLogo$Response()` instead.
   *
   * This method sends `multipart/form-data` and handles request body of type `multipart/form-data`.
   */
  setUnapprovedOrganizationLogo(params: {
    organization_id: string;
    body?: {
'logo': Blob;
}
  },
  context?: HttpContext

): Observable<{
}> {

    return this.setUnapprovedOrganizationLogo$Response(params,context).pipe(
      map((r: StrictHttpResponse<{
}>) => r.body as {
})
    );
  }

  /**
   * Path part for operation updateUnapprovedOrganizationDetails
   */
  static readonly UpdateUnapprovedOrganizationDetailsPath = '/api/v2/my/unapproved-organizations/{organization_id}/details';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `updateUnapprovedOrganizationDetails()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateUnapprovedOrganizationDetails$Response(params: {
    organization_id: string;
    body: PostInitialConfidentialOrganizationDetailsEntity
  },
  context?: HttpContext

): Observable<StrictHttpResponse<UnapprovedConfidentialOrganizationProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, MyUnapprovedOrganizationsControllerService.UpdateUnapprovedOrganizationDetailsPath, 'put');
    if (params) {
      rb.path('organization_id', params.organization_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UnapprovedConfidentialOrganizationProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `updateUnapprovedOrganizationDetails$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateUnapprovedOrganizationDetails(params: {
    organization_id: string;
    body: PostInitialConfidentialOrganizationDetailsEntity
  },
  context?: HttpContext

): Observable<UnapprovedConfidentialOrganizationProfileEntity> {

    return this.updateUnapprovedOrganizationDetails$Response(params,context).pipe(
      map((r: StrictHttpResponse<UnapprovedConfidentialOrganizationProfileEntity>) => r.body as UnapprovedConfidentialOrganizationProfileEntity)
    );
  }

  /**
   * Path part for operation updateUnapprovedOrganizationContactData
   */
  static readonly UpdateUnapprovedOrganizationContactDataPath = '/api/v2/my/unapproved-organizations/{organization_id}/contact';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `updateUnapprovedOrganizationContactData()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateUnapprovedOrganizationContactData$Response(params: {
    organization_id: string;
    body: ConfidentialOrganizationContactDataEntity
  },
  context?: HttpContext

): Observable<StrictHttpResponse<UnapprovedConfidentialOrganizationProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, MyUnapprovedOrganizationsControllerService.UpdateUnapprovedOrganizationContactDataPath, 'put');
    if (params) {
      rb.path('organization_id', params.organization_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UnapprovedConfidentialOrganizationProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `updateUnapprovedOrganizationContactData$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateUnapprovedOrganizationContactData(params: {
    organization_id: string;
    body: ConfidentialOrganizationContactDataEntity
  },
  context?: HttpContext

): Observable<UnapprovedConfidentialOrganizationProfileEntity> {

    return this.updateUnapprovedOrganizationContactData$Response(params,context).pipe(
      map((r: StrictHttpResponse<UnapprovedConfidentialOrganizationProfileEntity>) => r.body as UnapprovedConfidentialOrganizationProfileEntity)
    );
  }

  /**
   * Path part for operation queryUnapprovedOrganizations
   */
  static readonly QueryUnapprovedOrganizationsPath = '/api/v2/my/unapproved-organizations/query';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `queryUnapprovedOrganizations()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  queryUnapprovedOrganizations$Response(params: {
    page: number;
    page_size: number;
    body: OrganizationAccountStateQueryEntity
  },
  context?: HttpContext

): Observable<StrictHttpResponse<UnapprovedConfidentialOrganizationProfilesEntity>> {

    const rb = new RequestBuilder(this.rootUrl, MyUnapprovedOrganizationsControllerService.QueryUnapprovedOrganizationsPath, 'put');
    if (params) {
      rb.query('page', params.page, {});
      rb.query('page_size', params.page_size, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UnapprovedConfidentialOrganizationProfilesEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `queryUnapprovedOrganizations$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  queryUnapprovedOrganizations(params: {
    page: number;
    page_size: number;
    body: OrganizationAccountStateQueryEntity
  },
  context?: HttpContext

): Observable<UnapprovedConfidentialOrganizationProfilesEntity> {

    return this.queryUnapprovedOrganizations$Response(params,context).pipe(
      map((r: StrictHttpResponse<UnapprovedConfidentialOrganizationProfilesEntity>) => r.body as UnapprovedConfidentialOrganizationProfilesEntity)
    );
  }

  /**
   * Path part for operation createUnapprovedOrganization
   */
  static readonly CreateUnapprovedOrganizationPath = '/api/v2/my/unapproved-organizations';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `createUnapprovedOrganization()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createUnapprovedOrganization$Response(params: {
    body: PostOrganizationEntity
  },
  context?: HttpContext

): Observable<StrictHttpResponse<UnapprovedConfidentialOrganizationProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, MyUnapprovedOrganizationsControllerService.CreateUnapprovedOrganizationPath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UnapprovedConfidentialOrganizationProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `createUnapprovedOrganization$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createUnapprovedOrganization(params: {
    body: PostOrganizationEntity
  },
  context?: HttpContext

): Observable<UnapprovedConfidentialOrganizationProfileEntity> {

    return this.createUnapprovedOrganization$Response(params,context).pipe(
      map((r: StrictHttpResponse<UnapprovedConfidentialOrganizationProfileEntity>) => r.body as UnapprovedConfidentialOrganizationProfileEntity)
    );
  }

  /**
   * Path part for operation getUnapprovedOrganization
   */
  static readonly GetUnapprovedOrganizationPath = '/api/v2/my/unapproved-organizations/{organization_id}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getUnapprovedOrganization()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUnapprovedOrganization$Response(params: {
    organization_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<UnapprovedConfidentialOrganizationProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, MyUnapprovedOrganizationsControllerService.GetUnapprovedOrganizationPath, 'get');
    if (params) {
      rb.path('organization_id', params.organization_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UnapprovedConfidentialOrganizationProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getUnapprovedOrganization$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUnapprovedOrganization(params: {
    organization_id: string;
  },
  context?: HttpContext

): Observable<UnapprovedConfidentialOrganizationProfileEntity> {

    return this.getUnapprovedOrganization$Response(params,context).pipe(
      map((r: StrictHttpResponse<UnapprovedConfidentialOrganizationProfileEntity>) => r.body as UnapprovedConfidentialOrganizationProfileEntity)
    );
  }

  /**
   * Path part for operation deleteUnapprovedOrganization
   */
  static readonly DeleteUnapprovedOrganizationPath = '/api/v2/my/unapproved-organizations/{organization_id}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `deleteUnapprovedOrganization()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteUnapprovedOrganization$Response(params: {
    organization_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<{
}>> {

    const rb = new RequestBuilder(this.rootUrl, MyUnapprovedOrganizationsControllerService.DeleteUnapprovedOrganizationPath, 'delete');
    if (params) {
      rb.path('organization_id', params.organization_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/hal+json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<{
        }>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `deleteUnapprovedOrganization$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteUnapprovedOrganization(params: {
    organization_id: string;
  },
  context?: HttpContext

): Observable<{
}> {

    return this.deleteUnapprovedOrganization$Response(params,context).pipe(
      map((r: StrictHttpResponse<{
}>) => r.body as {
})
    );
  }

  /**
   * Path part for operation canRequestApproval
   */
  static readonly CanRequestApprovalPath = '/api/v2/my/unapproved-organizations/{organization_id}/can-request-approval';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `canRequestApproval()` instead.
   *
   * This method doesn't expect any request body.
   */
  canRequestApproval$Response(params: {
    organization_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<CanRequestApprovalEntity>> {

    const rb = new RequestBuilder(this.rootUrl, MyUnapprovedOrganizationsControllerService.CanRequestApprovalPath, 'get');
    if (params) {
      rb.path('organization_id', params.organization_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<CanRequestApprovalEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `canRequestApproval$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  canRequestApproval(params: {
    organization_id: string;
  },
  context?: HttpContext

): Observable<CanRequestApprovalEntity> {

    return this.canRequestApproval$Response(params,context).pipe(
      map((r: StrictHttpResponse<CanRequestApprovalEntity>) => r.body as CanRequestApprovalEntity)
    );
  }

}
