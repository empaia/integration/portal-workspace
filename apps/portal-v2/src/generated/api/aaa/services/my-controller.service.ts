/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { DefaultActiveOrganizationEntity } from '../models/default-active-organization-entity';
import { LanguageEntity } from '../models/language-entity';
import { MembershipRequestEntity } from '../models/membership-request-entity';
import { MembershipRequestsEntity } from '../models/membership-requests-entity';
import { MembershipsEntity } from '../models/memberships-entity';
import { PostMembershipRequestEntity } from '../models/post-membership-request-entity';
import { PostUserContactDataEntity } from '../models/post-user-contact-data-entity';
import { PublicOrganizationProfilesEntity } from '../models/public-organization-profiles-entity';
import { UserDetailsEntity } from '../models/user-details-entity';
import { UserProfileEntity } from '../models/user-profile-entity';

@Injectable({
  providedIn: 'root',
})
export class MyControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation setProfilePicture
   */
  static readonly SetProfilePicturePath = '/api/v2/my/profile/picture';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `setProfilePicture()` instead.
   *
   * This method sends `multipart/form-data` and handles request body of type `multipart/form-data`.
   */
  setProfilePicture$Response(params?: {
    body?: {
'picture': Blob;
}
  },
  context?: HttpContext

): Observable<StrictHttpResponse<{
}>> {

    const rb = new RequestBuilder(this.rootUrl, MyControllerService.SetProfilePicturePath, 'put');
    if (params) {
      rb.body(params.body, 'multipart/form-data');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/hal+json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<{
        }>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `setProfilePicture$Response()` instead.
   *
   * This method sends `multipart/form-data` and handles request body of type `multipart/form-data`.
   */
  setProfilePicture(params?: {
    body?: {
'picture': Blob;
}
  },
  context?: HttpContext

): Observable<{
}> {

    return this.setProfilePicture$Response(params,context).pipe(
      map((r: StrictHttpResponse<{
}>) => r.body as {
})
    );
  }

  /**
   * Path part for operation deleteProfilePictures
   */
  static readonly DeleteProfilePicturesPath = '/api/v2/my/profile/picture';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `deleteProfilePictures()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteProfilePictures$Response(params?: {
  },
  context?: HttpContext

): Observable<StrictHttpResponse<{
}>> {

    const rb = new RequestBuilder(this.rootUrl, MyControllerService.DeleteProfilePicturesPath, 'delete');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/hal+json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<{
        }>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `deleteProfilePictures$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteProfilePictures(params?: {
  },
  context?: HttpContext

): Observable<{
}> {

    return this.deleteProfilePictures$Response(params,context).pipe(
      map((r: StrictHttpResponse<{
}>) => r.body as {
})
    );
  }

  /**
   * Path part for operation updateLanguage
   */
  static readonly UpdateLanguagePath = '/api/v2/my/profile/language';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `updateLanguage()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateLanguage$Response(params: {
    body: LanguageEntity
  },
  context?: HttpContext

): Observable<StrictHttpResponse<UserProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, MyControllerService.UpdateLanguagePath, 'put');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UserProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `updateLanguage$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateLanguage(params: {
    body: LanguageEntity
  },
  context?: HttpContext

): Observable<UserProfileEntity> {

    return this.updateLanguage$Response(params,context).pipe(
      map((r: StrictHttpResponse<UserProfileEntity>) => r.body as UserProfileEntity)
    );
  }

  /**
   * Path part for operation updateDetails
   */
  static readonly UpdateDetailsPath = '/api/v2/my/profile/details';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `updateDetails()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateDetails$Response(params: {
    body: UserDetailsEntity
  },
  context?: HttpContext

): Observable<StrictHttpResponse<UserProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, MyControllerService.UpdateDetailsPath, 'put');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UserProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `updateDetails$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateDetails(params: {
    body: UserDetailsEntity
  },
  context?: HttpContext

): Observable<UserProfileEntity> {

    return this.updateDetails$Response(params,context).pipe(
      map((r: StrictHttpResponse<UserProfileEntity>) => r.body as UserProfileEntity)
    );
  }

  /**
   * Path part for operation updateContactData
   */
  static readonly UpdateContactDataPath = '/api/v2/my/profile/contact';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `updateContactData()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateContactData$Response(params: {
    body: PostUserContactDataEntity
  },
  context?: HttpContext

): Observable<StrictHttpResponse<UserProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, MyControllerService.UpdateContactDataPath, 'put');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UserProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `updateContactData$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateContactData(params: {
    body: PostUserContactDataEntity
  },
  context?: HttpContext

): Observable<UserProfileEntity> {

    return this.updateContactData$Response(params,context).pipe(
      map((r: StrictHttpResponse<UserProfileEntity>) => r.body as UserProfileEntity)
    );
  }

  /**
   * Path part for operation revokeMembershipRequest
   */
  static readonly RevokeMembershipRequestPath = '/api/v2/my/membership-requests/{membership_request_id}/revoke';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `revokeMembershipRequest()` instead.
   *
   * This method doesn't expect any request body.
   */
  revokeMembershipRequest$Response(params: {
    membership_request_id: number;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<MembershipRequestEntity>> {

    const rb = new RequestBuilder(this.rootUrl, MyControllerService.RevokeMembershipRequestPath, 'put');
    if (params) {
      rb.path('membership_request_id', params.membership_request_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<MembershipRequestEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `revokeMembershipRequest$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  revokeMembershipRequest(params: {
    membership_request_id: number;
  },
  context?: HttpContext

): Observable<MembershipRequestEntity> {

    return this.revokeMembershipRequest$Response(params,context).pipe(
      map((r: StrictHttpResponse<MembershipRequestEntity>) => r.body as MembershipRequestEntity)
    );
  }

  /**
   * Path part for operation getDefaultActiveOrganization
   */
  static readonly GetDefaultActiveOrganizationPath = '/api/v2/my/default-active-organization';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getDefaultActiveOrganization()` instead.
   *
   * This method doesn't expect any request body.
   */
  getDefaultActiveOrganization$Response(params?: {
  },
  context?: HttpContext

): Observable<StrictHttpResponse<DefaultActiveOrganizationEntity>> {

    const rb = new RequestBuilder(this.rootUrl, MyControllerService.GetDefaultActiveOrganizationPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<DefaultActiveOrganizationEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getDefaultActiveOrganization$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getDefaultActiveOrganization(params?: {
  },
  context?: HttpContext

): Observable<DefaultActiveOrganizationEntity> {

    return this.getDefaultActiveOrganization$Response(params,context).pipe(
      map((r: StrictHttpResponse<DefaultActiveOrganizationEntity>) => r.body as DefaultActiveOrganizationEntity)
    );
  }

  /**
   * Path part for operation setDefaultActiveOrganization
   */
  static readonly SetDefaultActiveOrganizationPath = '/api/v2/my/default-active-organization';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `setDefaultActiveOrganization()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  setDefaultActiveOrganization$Response(params: {
    body: DefaultActiveOrganizationEntity
  },
  context?: HttpContext

): Observable<StrictHttpResponse<DefaultActiveOrganizationEntity>> {

    const rb = new RequestBuilder(this.rootUrl, MyControllerService.SetDefaultActiveOrganizationPath, 'put');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<DefaultActiveOrganizationEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `setDefaultActiveOrganization$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  setDefaultActiveOrganization(params: {
    body: DefaultActiveOrganizationEntity
  },
  context?: HttpContext

): Observable<DefaultActiveOrganizationEntity> {

    return this.setDefaultActiveOrganization$Response(params,context).pipe(
      map((r: StrictHttpResponse<DefaultActiveOrganizationEntity>) => r.body as DefaultActiveOrganizationEntity)
    );
  }

  /**
   * Path part for operation deleteDefaultActiveOrganization
   */
  static readonly DeleteDefaultActiveOrganizationPath = '/api/v2/my/default-active-organization';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `deleteDefaultActiveOrganization()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteDefaultActiveOrganization$Response(params?: {
  },
  context?: HttpContext

): Observable<StrictHttpResponse<{
}>> {

    const rb = new RequestBuilder(this.rootUrl, MyControllerService.DeleteDefaultActiveOrganizationPath, 'delete');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/hal+json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<{
        }>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `deleteDefaultActiveOrganization$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteDefaultActiveOrganization(params?: {
  },
  context?: HttpContext

): Observable<{
}> {

    return this.deleteDefaultActiveOrganization$Response(params,context).pipe(
      map((r: StrictHttpResponse<{
}>) => r.body as {
})
    );
  }

  /**
   * Path part for operation getMembershipRequests
   */
  static readonly GetMembershipRequestsPath = '/api/v2/my/membership-requests';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getMembershipRequests()` instead.
   *
   * This method doesn't expect any request body.
   */
  getMembershipRequests$Response(params: {
    page: number;
    page_size: number;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<MembershipRequestsEntity>> {

    const rb = new RequestBuilder(this.rootUrl, MyControllerService.GetMembershipRequestsPath, 'get');
    if (params) {
      rb.query('page', params.page, {});
      rb.query('page_size', params.page_size, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<MembershipRequestsEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getMembershipRequests$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getMembershipRequests(params: {
    page: number;
    page_size: number;
  },
  context?: HttpContext

): Observable<MembershipRequestsEntity> {

    return this.getMembershipRequests$Response(params,context).pipe(
      map((r: StrictHttpResponse<MembershipRequestsEntity>) => r.body as MembershipRequestsEntity)
    );
  }

  /**
   * Path part for operation requestMembership
   */
  static readonly RequestMembershipPath = '/api/v2/my/membership-requests';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `requestMembership()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  requestMembership$Response(params: {
    body: PostMembershipRequestEntity
  },
  context?: HttpContext

): Observable<StrictHttpResponse<MembershipRequestEntity>> {

    const rb = new RequestBuilder(this.rootUrl, MyControllerService.RequestMembershipPath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<MembershipRequestEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `requestMembership$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  requestMembership(params: {
    body: PostMembershipRequestEntity
  },
  context?: HttpContext

): Observable<MembershipRequestEntity> {

    return this.requestMembership$Response(params,context).pipe(
      map((r: StrictHttpResponse<MembershipRequestEntity>) => r.body as MembershipRequestEntity)
    );
  }

  /**
   * Path part for operation getProfile
   */
  static readonly GetProfilePath = '/api/v2/my/profile';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getProfile()` instead.
   *
   * This method doesn't expect any request body.
   */
  getProfile$Response(params?: {
  },
  context?: HttpContext

): Observable<StrictHttpResponse<UserProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, MyControllerService.GetProfilePath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UserProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getProfile$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getProfile(params?: {
  },
  context?: HttpContext

): Observable<UserProfileEntity> {

    return this.getProfile$Response(params,context).pipe(
      map((r: StrictHttpResponse<UserProfileEntity>) => r.body as UserProfileEntity)
    );
  }

  /**
   * Path part for operation getOrganizations
   */
  static readonly GetOrganizationsPath = '/api/v2/my/organizations';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getOrganizations()` instead.
   *
   * This method doesn't expect any request body.
   */
  getOrganizations$Response(params?: {
    page?: number;
    page_size?: number;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<PublicOrganizationProfilesEntity>> {

    const rb = new RequestBuilder(this.rootUrl, MyControllerService.GetOrganizationsPath, 'get');
    if (params) {
      rb.query('page', params.page, {});
      rb.query('page_size', params.page_size, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<PublicOrganizationProfilesEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getOrganizations$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getOrganizations(params?: {
    page?: number;
    page_size?: number;
  },
  context?: HttpContext

): Observable<PublicOrganizationProfilesEntity> {

    return this.getOrganizations$Response(params,context).pipe(
      map((r: StrictHttpResponse<PublicOrganizationProfilesEntity>) => r.body as PublicOrganizationProfilesEntity)
    );
  }

  /**
   * Path part for operation getMemberships
   */
  static readonly GetMembershipsPath = '/api/v2/my/memberships';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getMemberships()` instead.
   *
   * This method doesn't expect any request body.
   */
  getMemberships$Response(params?: {
  },
  context?: HttpContext

): Observable<StrictHttpResponse<MembershipsEntity>> {

    const rb = new RequestBuilder(this.rootUrl, MyControllerService.GetMembershipsPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<MembershipsEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getMemberships$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getMemberships(params?: {
  },
  context?: HttpContext

): Observable<MembershipsEntity> {

    return this.getMemberships$Response(params,context).pipe(
      map((r: StrictHttpResponse<MembershipsEntity>) => r.body as MembershipsEntity)
    );
  }

  /**
   * Path part for operation deleteMyUserAccount
   */
  static readonly DeleteMyUserAccountPath = '/api/v2/my/account';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `deleteMyUserAccount()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteMyUserAccount$Response(params?: {
  },
  context?: HttpContext

): Observable<StrictHttpResponse<{
}>> {

    const rb = new RequestBuilder(this.rootUrl, MyControllerService.DeleteMyUserAccountPath, 'delete');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/hal+json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<{
        }>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `deleteMyUserAccount$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteMyUserAccount(params?: {
  },
  context?: HttpContext

): Observable<{
}> {

    return this.deleteMyUserAccount$Response(params,context).pipe(
      map((r: StrictHttpResponse<{
}>) => r.body as {
})
    );
  }

}
