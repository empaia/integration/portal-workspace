/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { ConfidentialMemberProfileEntity } from '../models/confidential-member-profile-entity';
import { ConfidentialMemberProfilesEntity } from '../models/confidential-member-profiles-entity';
import { ConfidentialOrganizationContactDataEntity } from '../models/confidential-organization-contact-data-entity';
import { ConfidentialOrganizationProfileEntity } from '../models/confidential-organization-profile-entity';
import { MembershipRequestEntity } from '../models/membership-request-entity';
import { MembershipRequestsEntity } from '../models/membership-requests-entity';
import { OrganizationCategoryRequestEntity } from '../models/organization-category-request-entity';
import { OrganizationCategoryRequestsEntity } from '../models/organization-category-requests-entity';
import { OrganizationUserRoleRequestEntity } from '../models/organization-user-role-request-entity';
import { OrganizationUserRoleRequestsEntity } from '../models/organization-user-role-requests-entity';
import { OrganizationUserRolesEntity } from '../models/organization-user-roles-entity';
import { PostConfidentialOrganizationDetailsEntity } from '../models/post-confidential-organization-details-entity';
import { PostOrganizationCategoryRequestEntity } from '../models/post-organization-category-request-entity';
import { PostOrganizationNameEntity } from '../models/post-organization-name-entity';
import { PostOrganizationUserRolesEntity } from '../models/post-organization-user-roles-entity';
import { PostReviewerComment } from '../models/post-reviewer-comment';

@Injectable({
  providedIn: 'root',
})
export class ManagerControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation setOrganizationUserRoles
   */
  static readonly SetOrganizationUserRolesPath = '/api/v2/manager/organization/users/{target_user_id}/roles';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `setOrganizationUserRoles()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  setOrganizationUserRoles$Response(params: {
    target_user_id: string;
    body: PostOrganizationUserRolesEntity
  },
  context?: HttpContext

): Observable<StrictHttpResponse<OrganizationUserRolesEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ManagerControllerService.SetOrganizationUserRolesPath, 'put');
    if (params) {
      rb.path('target_user_id', params.target_user_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<OrganizationUserRolesEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `setOrganizationUserRoles$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  setOrganizationUserRoles(params: {
    target_user_id: string;
    body: PostOrganizationUserRolesEntity
  },
  context?: HttpContext

): Observable<OrganizationUserRolesEntity> {

    return this.setOrganizationUserRoles$Response(params,context).pipe(
      map((r: StrictHttpResponse<OrganizationUserRolesEntity>) => r.body as OrganizationUserRolesEntity)
    );
  }

  /**
   * Path part for operation removeUserFromOrganization
   */
  static readonly RemoveUserFromOrganizationPath = '/api/v2/manager/organization/users/{target_user_id}/remove';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `removeUserFromOrganization()` instead.
   *
   * This method doesn't expect any request body.
   */
  removeUserFromOrganization$Response(params: {
    target_user_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<{
}>> {

    const rb = new RequestBuilder(this.rootUrl, ManagerControllerService.RemoveUserFromOrganizationPath, 'put');
    if (params) {
      rb.path('target_user_id', params.target_user_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/hal+json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<{
        }>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `removeUserFromOrganization$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  removeUserFromOrganization(params: {
    target_user_id: string;
  },
  context?: HttpContext

): Observable<{
}> {

    return this.removeUserFromOrganization$Response(params,context).pipe(
      map((r: StrictHttpResponse<{
}>) => r.body as {
})
    );
  }

  /**
   * Path part for operation denyOrganizationUserRoleRequest
   */
  static readonly DenyOrganizationUserRoleRequestPath = '/api/v2/manager/organization/role-requests/{role_request_id}/deny';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `denyOrganizationUserRoleRequest()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  denyOrganizationUserRoleRequest$Response(params: {
    role_request_id: number;
    body: PostReviewerComment
  },
  context?: HttpContext

): Observable<StrictHttpResponse<OrganizationUserRoleRequestEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ManagerControllerService.DenyOrganizationUserRoleRequestPath, 'put');
    if (params) {
      rb.path('role_request_id', params.role_request_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<OrganizationUserRoleRequestEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `denyOrganizationUserRoleRequest$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  denyOrganizationUserRoleRequest(params: {
    role_request_id: number;
    body: PostReviewerComment
  },
  context?: HttpContext

): Observable<OrganizationUserRoleRequestEntity> {

    return this.denyOrganizationUserRoleRequest$Response(params,context).pipe(
      map((r: StrictHttpResponse<OrganizationUserRoleRequestEntity>) => r.body as OrganizationUserRoleRequestEntity)
    );
  }

  /**
   * Path part for operation acceptOrganizationUserRoleRequest
   */
  static readonly AcceptOrganizationUserRoleRequestPath = '/api/v2/manager/organization/role-requests/{role_request_id}/accept';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `acceptOrganizationUserRoleRequest()` instead.
   *
   * This method doesn't expect any request body.
   */
  acceptOrganizationUserRoleRequest$Response(params: {
    role_request_id: number;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<OrganizationUserRoleRequestEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ManagerControllerService.AcceptOrganizationUserRoleRequestPath, 'put');
    if (params) {
      rb.path('role_request_id', params.role_request_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<OrganizationUserRoleRequestEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `acceptOrganizationUserRoleRequest$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  acceptOrganizationUserRoleRequest(params: {
    role_request_id: number;
  },
  context?: HttpContext

): Observable<OrganizationUserRoleRequestEntity> {

    return this.acceptOrganizationUserRoleRequest$Response(params,context).pipe(
      map((r: StrictHttpResponse<OrganizationUserRoleRequestEntity>) => r.body as OrganizationUserRoleRequestEntity)
    );
  }

  /**
   * Path part for operation setOrganizationName
   */
  static readonly SetOrganizationNamePath = '/api/v2/manager/organization/profile/name';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `setOrganizationName()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  setOrganizationName$Response(params: {
    body: PostOrganizationNameEntity
  },
  context?: HttpContext

): Observable<StrictHttpResponse<ConfidentialOrganizationProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ManagerControllerService.SetOrganizationNamePath, 'put');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ConfidentialOrganizationProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `setOrganizationName$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  setOrganizationName(params: {
    body: PostOrganizationNameEntity
  },
  context?: HttpContext

): Observable<ConfidentialOrganizationProfileEntity> {

    return this.setOrganizationName$Response(params,context).pipe(
      map((r: StrictHttpResponse<ConfidentialOrganizationProfileEntity>) => r.body as ConfidentialOrganizationProfileEntity)
    );
  }

  /**
   * Path part for operation setOrganizationLogo
   */
  static readonly SetOrganizationLogoPath = '/api/v2/manager/organization/profile/logo';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `setOrganizationLogo()` instead.
   *
   * This method sends `multipart/form-data` and handles request body of type `multipart/form-data`.
   */
  setOrganizationLogo$Response(params?: {
    body?: {
'logo': Blob;
}
  },
  context?: HttpContext

): Observable<StrictHttpResponse<ConfidentialOrganizationProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ManagerControllerService.SetOrganizationLogoPath, 'put');
    if (params) {
      rb.body(params.body, 'multipart/form-data');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ConfidentialOrganizationProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `setOrganizationLogo$Response()` instead.
   *
   * This method sends `multipart/form-data` and handles request body of type `multipart/form-data`.
   */
  setOrganizationLogo(params?: {
    body?: {
'logo': Blob;
}
  },
  context?: HttpContext

): Observable<ConfidentialOrganizationProfileEntity> {

    return this.setOrganizationLogo$Response(params,context).pipe(
      map((r: StrictHttpResponse<ConfidentialOrganizationProfileEntity>) => r.body as ConfidentialOrganizationProfileEntity)
    );
  }

  /**
   * Path part for operation setOrganizationDetails
   */
  static readonly SetOrganizationDetailsPath = '/api/v2/manager/organization/profile/details';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `setOrganizationDetails()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  setOrganizationDetails$Response(params: {
    body: PostConfidentialOrganizationDetailsEntity
  },
  context?: HttpContext

): Observable<StrictHttpResponse<ConfidentialOrganizationProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ManagerControllerService.SetOrganizationDetailsPath, 'put');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ConfidentialOrganizationProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `setOrganizationDetails$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  setOrganizationDetails(params: {
    body: PostConfidentialOrganizationDetailsEntity
  },
  context?: HttpContext

): Observable<ConfidentialOrganizationProfileEntity> {

    return this.setOrganizationDetails$Response(params,context).pipe(
      map((r: StrictHttpResponse<ConfidentialOrganizationProfileEntity>) => r.body as ConfidentialOrganizationProfileEntity)
    );
  }

  /**
   * Path part for operation setOrganizationContactData
   */
  static readonly SetOrganizationContactDataPath = '/api/v2/manager/organization/profile/contact';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `setOrganizationContactData()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  setOrganizationContactData$Response(params: {
    body: ConfidentialOrganizationContactDataEntity
  },
  context?: HttpContext

): Observable<StrictHttpResponse<ConfidentialOrganizationProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ManagerControllerService.SetOrganizationContactDataPath, 'put');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ConfidentialOrganizationProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `setOrganizationContactData$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  setOrganizationContactData(params: {
    body: ConfidentialOrganizationContactDataEntity
  },
  context?: HttpContext

): Observable<ConfidentialOrganizationProfileEntity> {

    return this.setOrganizationContactData$Response(params,context).pipe(
      map((r: StrictHttpResponse<ConfidentialOrganizationProfileEntity>) => r.body as ConfidentialOrganizationProfileEntity)
    );
  }

  /**
   * Path part for operation denyMembershipRequest
   */
  static readonly DenyMembershipRequestPath = '/api/v2/manager/organization/membership-requests/{membership_request_id}/deny';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `denyMembershipRequest()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  denyMembershipRequest$Response(params: {
    membership_request_id: number;
    body: PostReviewerComment
  },
  context?: HttpContext

): Observable<StrictHttpResponse<MembershipRequestEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ManagerControllerService.DenyMembershipRequestPath, 'put');
    if (params) {
      rb.path('membership_request_id', params.membership_request_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<MembershipRequestEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `denyMembershipRequest$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  denyMembershipRequest(params: {
    membership_request_id: number;
    body: PostReviewerComment
  },
  context?: HttpContext

): Observable<MembershipRequestEntity> {

    return this.denyMembershipRequest$Response(params,context).pipe(
      map((r: StrictHttpResponse<MembershipRequestEntity>) => r.body as MembershipRequestEntity)
    );
  }

  /**
   * Path part for operation acceptMembershipRequest
   */
  static readonly AcceptMembershipRequestPath = '/api/v2/manager/organization/membership-requests/{membership_request_id}/accept';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `acceptMembershipRequest()` instead.
   *
   * This method doesn't expect any request body.
   */
  acceptMembershipRequest$Response(params: {
    membership_request_id: number;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<MembershipRequestEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ManagerControllerService.AcceptMembershipRequestPath, 'put');
    if (params) {
      rb.path('membership_request_id', params.membership_request_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<MembershipRequestEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `acceptMembershipRequest$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  acceptMembershipRequest(params: {
    membership_request_id: number;
  },
  context?: HttpContext

): Observable<MembershipRequestEntity> {

    return this.acceptMembershipRequest$Response(params,context).pipe(
      map((r: StrictHttpResponse<MembershipRequestEntity>) => r.body as MembershipRequestEntity)
    );
  }

  /**
   * Path part for operation getOrganizationCategoryRequests
   */
  static readonly GetOrganizationCategoryRequestsPath = '/api/v2/manager/organization/category-requests';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getOrganizationCategoryRequests()` instead.
   *
   * This method doesn't expect any request body.
   */
  getOrganizationCategoryRequests$Response(params: {
    page: number;
    page_size: number;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<OrganizationCategoryRequestsEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ManagerControllerService.GetOrganizationCategoryRequestsPath, 'get');
    if (params) {
      rb.query('page', params.page, {});
      rb.query('page_size', params.page_size, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<OrganizationCategoryRequestsEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getOrganizationCategoryRequests$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getOrganizationCategoryRequests(params: {
    page: number;
    page_size: number;
  },
  context?: HttpContext

): Observable<OrganizationCategoryRequestsEntity> {

    return this.getOrganizationCategoryRequests$Response(params,context).pipe(
      map((r: StrictHttpResponse<OrganizationCategoryRequestsEntity>) => r.body as OrganizationCategoryRequestsEntity)
    );
  }

  /**
   * Path part for operation requestOrganizationCategory
   */
  static readonly RequestOrganizationCategoryPath = '/api/v2/manager/organization/category-requests';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `requestOrganizationCategory()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  requestOrganizationCategory$Response(params: {
    body: PostOrganizationCategoryRequestEntity
  },
  context?: HttpContext

): Observable<StrictHttpResponse<OrganizationCategoryRequestEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ManagerControllerService.RequestOrganizationCategoryPath, 'put');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<OrganizationCategoryRequestEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `requestOrganizationCategory$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  requestOrganizationCategory(params: {
    body: PostOrganizationCategoryRequestEntity
  },
  context?: HttpContext

): Observable<OrganizationCategoryRequestEntity> {

    return this.requestOrganizationCategory$Response(params,context).pipe(
      map((r: StrictHttpResponse<OrganizationCategoryRequestEntity>) => r.body as OrganizationCategoryRequestEntity)
    );
  }

  /**
   * Path part for operation revokeOrganizationCategoryRequest
   */
  static readonly RevokeOrganizationCategoryRequestPath = '/api/v2/manager/organization/category-requests/{category_request_id}/revoke';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `revokeOrganizationCategoryRequest()` instead.
   *
   * This method doesn't expect any request body.
   */
  revokeOrganizationCategoryRequest$Response(params: {
    category_request_id: number;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<OrganizationCategoryRequestEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ManagerControllerService.RevokeOrganizationCategoryRequestPath, 'put');
    if (params) {
      rb.path('category_request_id', params.category_request_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<OrganizationCategoryRequestEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `revokeOrganizationCategoryRequest$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  revokeOrganizationCategoryRequest(params: {
    category_request_id: number;
  },
  context?: HttpContext

): Observable<OrganizationCategoryRequestEntity> {

    return this.revokeOrganizationCategoryRequest$Response(params,context).pipe(
      map((r: StrictHttpResponse<OrganizationCategoryRequestEntity>) => r.body as OrganizationCategoryRequestEntity)
    );
  }

  /**
   * Path part for operation getConfidentialMemberProfiles
   */
  static readonly GetConfidentialMemberProfilesPath = '/api/v2/manager/organization/users';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getConfidentialMemberProfiles()` instead.
   *
   * This method doesn't expect any request body.
   */
  getConfidentialMemberProfiles$Response(params?: {
  },
  context?: HttpContext

): Observable<StrictHttpResponse<ConfidentialMemberProfilesEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ManagerControllerService.GetConfidentialMemberProfilesPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ConfidentialMemberProfilesEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getConfidentialMemberProfiles$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getConfidentialMemberProfiles(params?: {
  },
  context?: HttpContext

): Observable<ConfidentialMemberProfilesEntity> {

    return this.getConfidentialMemberProfiles$Response(params,context).pipe(
      map((r: StrictHttpResponse<ConfidentialMemberProfilesEntity>) => r.body as ConfidentialMemberProfilesEntity)
    );
  }

  /**
   * Path part for operation getConfidentialMemberProfile
   */
  static readonly GetConfidentialMemberProfilePath = '/api/v2/manager/organization/users/{target_user_id}/profile';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getConfidentialMemberProfile()` instead.
   *
   * This method doesn't expect any request body.
   */
  getConfidentialMemberProfile$Response(params: {
    target_user_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<ConfidentialMemberProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ManagerControllerService.GetConfidentialMemberProfilePath, 'get');
    if (params) {
      rb.path('target_user_id', params.target_user_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ConfidentialMemberProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getConfidentialMemberProfile$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getConfidentialMemberProfile(params: {
    target_user_id: string;
  },
  context?: HttpContext

): Observable<ConfidentialMemberProfileEntity> {

    return this.getConfidentialMemberProfile$Response(params,context).pipe(
      map((r: StrictHttpResponse<ConfidentialMemberProfileEntity>) => r.body as ConfidentialMemberProfileEntity)
    );
  }

  /**
   * Path part for operation getOrganizationUserRoleRequestsOfOrganization
   */
  static readonly GetOrganizationUserRoleRequestsOfOrganizationPath = '/api/v2/manager/organization/role-requests';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getOrganizationUserRoleRequestsOfOrganization()` instead.
   *
   * This method doesn't expect any request body.
   */
  getOrganizationUserRoleRequestsOfOrganization$Response(params: {
    page: number;
    page_size: number;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<OrganizationUserRoleRequestsEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ManagerControllerService.GetOrganizationUserRoleRequestsOfOrganizationPath, 'get');
    if (params) {
      rb.query('page', params.page, {});
      rb.query('page_size', params.page_size, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<OrganizationUserRoleRequestsEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getOrganizationUserRoleRequestsOfOrganization$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getOrganizationUserRoleRequestsOfOrganization(params: {
    page: number;
    page_size: number;
  },
  context?: HttpContext

): Observable<OrganizationUserRoleRequestsEntity> {

    return this.getOrganizationUserRoleRequestsOfOrganization$Response(params,context).pipe(
      map((r: StrictHttpResponse<OrganizationUserRoleRequestsEntity>) => r.body as OrganizationUserRoleRequestsEntity)
    );
  }

  /**
   * Path part for operation getOrganizationProfile
   */
  static readonly GetOrganizationProfilePath = '/api/v2/manager/organization/profile';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getOrganizationProfile()` instead.
   *
   * This method doesn't expect any request body.
   */
  getOrganizationProfile$Response(params?: {
  },
  context?: HttpContext

): Observable<StrictHttpResponse<ConfidentialOrganizationProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ManagerControllerService.GetOrganizationProfilePath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ConfidentialOrganizationProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getOrganizationProfile$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getOrganizationProfile(params?: {
  },
  context?: HttpContext

): Observable<ConfidentialOrganizationProfileEntity> {

    return this.getOrganizationProfile$Response(params,context).pipe(
      map((r: StrictHttpResponse<ConfidentialOrganizationProfileEntity>) => r.body as ConfidentialOrganizationProfileEntity)
    );
  }

  /**
   * Path part for operation getMembershipRequestsOfOrganization
   */
  static readonly GetMembershipRequestsOfOrganizationPath = '/api/v2/manager/organization/membership-requests';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getMembershipRequestsOfOrganization()` instead.
   *
   * This method doesn't expect any request body.
   */
  getMembershipRequestsOfOrganization$Response(params: {
    page: number;
    page_size: number;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<MembershipRequestsEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ManagerControllerService.GetMembershipRequestsOfOrganizationPath, 'get');
    if (params) {
      rb.query('page', params.page, {});
      rb.query('page_size', params.page_size, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<MembershipRequestsEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getMembershipRequestsOfOrganization$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getMembershipRequestsOfOrganization(params: {
    page: number;
    page_size: number;
  },
  context?: HttpContext

): Observable<MembershipRequestsEntity> {

    return this.getMembershipRequestsOfOrganization$Response(params,context).pipe(
      map((r: StrictHttpResponse<MembershipRequestsEntity>) => r.body as MembershipRequestsEntity)
    );
  }

}
