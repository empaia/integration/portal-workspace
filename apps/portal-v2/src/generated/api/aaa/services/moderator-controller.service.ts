/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { ConfidentialOrganizationProfileEntity } from '../models/confidential-organization-profile-entity';
import { ConfidentialOrganizationsEntity } from '../models/confidential-organizations-entity';
import { EmailAddressListEntity } from '../models/email-address-list-entity';
import { OrganizationAccountStateQueryEntity } from '../models/organization-account-state-query-entity';
import { OrganizationCategoriesEntity } from '../models/organization-categories-entity';
import { OrganizationCategoryRequestEntity } from '../models/organization-category-request-entity';
import { OrganizationCategoryRequestsEntity } from '../models/organization-category-requests-entity';
import { PostReviewerComment } from '../models/post-reviewer-comment';
import { RegisteredUsersEntity } from '../models/registered-users-entity';
import { UserProfileEntity } from '../models/user-profile-entity';
import { UserRole } from '../models/user-role';
import { UserRolesEntity } from '../models/user-roles-entity';

@Injectable({
  providedIn: 'root',
})
export class ModeratorControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation setUserRoles
   */
  static readonly SetUserRolesPath = '/api/v2/moderator/users/{target_user_id}/roles';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `setUserRoles()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  setUserRoles$Response(params: {
    target_user_id: string;
    body: UserRolesEntity
  },
  context?: HttpContext

): Observable<StrictHttpResponse<Array<UserRole>>> {

    const rb = new RequestBuilder(this.rootUrl, ModeratorControllerService.SetUserRolesPath, 'put');
    if (params) {
      rb.path('target_user_id', params.target_user_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<UserRole>>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `setUserRoles$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  setUserRoles(params: {
    target_user_id: string;
    body: UserRolesEntity
  },
  context?: HttpContext

): Observable<Array<UserRole>> {

    return this.setUserRoles$Response(params,context).pipe(
      map((r: StrictHttpResponse<Array<UserRole>>) => r.body as Array<UserRole>)
    );
  }

  /**
   * Path part for operation deactivateUser
   */
  static readonly DeactivateUserPath = '/api/v2/moderator/users/{target_user_id}/deactivate';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `deactivateUser()` instead.
   *
   * This method doesn't expect any request body.
   */
  deactivateUser$Response(params: {
    target_user_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<UserProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ModeratorControllerService.DeactivateUserPath, 'put');
    if (params) {
      rb.path('target_user_id', params.target_user_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UserProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `deactivateUser$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  deactivateUser(params: {
    target_user_id: string;
  },
  context?: HttpContext

): Observable<UserProfileEntity> {

    return this.deactivateUser$Response(params,context).pipe(
      map((r: StrictHttpResponse<UserProfileEntity>) => r.body as UserProfileEntity)
    );
  }

  /**
   * Path part for operation activateUser
   */
  static readonly ActivateUserPath = '/api/v2/moderator/users/{target_user_id}/activate';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `activateUser()` instead.
   *
   * This method doesn't expect any request body.
   */
  activateUser$Response(params: {
    target_user_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<UserProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ModeratorControllerService.ActivateUserPath, 'put');
    if (params) {
      rb.path('target_user_id', params.target_user_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UserProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `activateUser$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  activateUser(params: {
    target_user_id: string;
  },
  context?: HttpContext

): Observable<UserProfileEntity> {

    return this.activateUser$Response(params,context).pipe(
      map((r: StrictHttpResponse<UserProfileEntity>) => r.body as UserProfileEntity)
    );
  }

  /**
   * Path part for operation denyOrganization
   */
  static readonly DenyOrganizationPath = '/api/v2/moderator/unapproved-organizations/{organization_id}/deny';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `denyOrganization()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  denyOrganization$Response(params: {
    organization_id: string;
    body: PostReviewerComment
  },
  context?: HttpContext

): Observable<StrictHttpResponse<{
}>> {

    const rb = new RequestBuilder(this.rootUrl, ModeratorControllerService.DenyOrganizationPath, 'put');
    if (params) {
      rb.path('organization_id', params.organization_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/hal+json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<{
        }>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `denyOrganization$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  denyOrganization(params: {
    organization_id: string;
    body: PostReviewerComment
  },
  context?: HttpContext

): Observable<{
}> {

    return this.denyOrganization$Response(params,context).pipe(
      map((r: StrictHttpResponse<{
}>) => r.body as {
})
    );
  }

  /**
   * Path part for operation acceptOrganization
   */
  static readonly AcceptOrganizationPath = '/api/v2/moderator/unapproved-organizations/{organization_id}/accept';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `acceptOrganization()` instead.
   *
   * This method doesn't expect any request body.
   */
  acceptOrganization$Response(params: {
    organization_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<{
}>> {

    const rb = new RequestBuilder(this.rootUrl, ModeratorControllerService.AcceptOrganizationPath, 'put');
    if (params) {
      rb.path('organization_id', params.organization_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/hal+json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<{
        }>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `acceptOrganization$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  acceptOrganization(params: {
    organization_id: string;
  },
  context?: HttpContext

): Observable<{
}> {

    return this.acceptOrganization$Response(params,context).pipe(
      map((r: StrictHttpResponse<{
}>) => r.body as {
})
    );
  }

  /**
   * Path part for operation getUnapprovedOrganizationsEntity
   */
  static readonly GetUnapprovedOrganizationsEntityPath = '/api/v2/moderator/unapproved-organizations/query';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getUnapprovedOrganizationsEntity()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  getUnapprovedOrganizationsEntity$Response(params: {
    page: number;
    page_size: number;
    body: OrganizationAccountStateQueryEntity
  },
  context?: HttpContext

): Observable<StrictHttpResponse<ConfidentialOrganizationsEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ModeratorControllerService.GetUnapprovedOrganizationsEntityPath, 'put');
    if (params) {
      rb.query('page', params.page, {});
      rb.query('page_size', params.page_size, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ConfidentialOrganizationsEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getUnapprovedOrganizationsEntity$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  getUnapprovedOrganizationsEntity(params: {
    page: number;
    page_size: number;
    body: OrganizationAccountStateQueryEntity
  },
  context?: HttpContext

): Observable<ConfidentialOrganizationsEntity> {

    return this.getUnapprovedOrganizationsEntity$Response(params,context).pipe(
      map((r: StrictHttpResponse<ConfidentialOrganizationsEntity>) => r.body as ConfidentialOrganizationsEntity)
    );
  }

  /**
   * Path part for operation sendRegistrationInvitation
   */
  static readonly SendRegistrationInvitationPath = '/api/v2/moderator/send-registration-invitation';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `sendRegistrationInvitation()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  sendRegistrationInvitation$Response(params: {
    body: EmailAddressListEntity
  },
  context?: HttpContext

): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, ModeratorControllerService.SendRegistrationInvitationPath, 'put');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `sendRegistrationInvitation$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  sendRegistrationInvitation(params: {
    body: EmailAddressListEntity
  },
  context?: HttpContext

): Observable<void> {

    return this.sendRegistrationInvitation$Response(params,context).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

  /**
   * Path part for operation deactivateOrganization
   */
  static readonly DeactivateOrganizationPath = '/api/v2/moderator/organizations/{organization_id}/deactivate';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `deactivateOrganization()` instead.
   *
   * This method doesn't expect any request body.
   */
  deactivateOrganization$Response(params: {
    organization_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<{
}>> {

    const rb = new RequestBuilder(this.rootUrl, ModeratorControllerService.DeactivateOrganizationPath, 'put');
    if (params) {
      rb.path('organization_id', params.organization_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/hal+json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<{
        }>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `deactivateOrganization$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  deactivateOrganization(params: {
    organization_id: string;
  },
  context?: HttpContext

): Observable<{
}> {

    return this.deactivateOrganization$Response(params,context).pipe(
      map((r: StrictHttpResponse<{
}>) => r.body as {
})
    );
  }

  /**
   * Path part for operation setOrganizationCategories
   */
  static readonly SetOrganizationCategoriesPath = '/api/v2/moderator/organizations/{organization_id}/categories';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `setOrganizationCategories()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  setOrganizationCategories$Response(params: {
    organization_id: string;
    body: OrganizationCategoriesEntity
  },
  context?: HttpContext

): Observable<StrictHttpResponse<{
}>> {

    const rb = new RequestBuilder(this.rootUrl, ModeratorControllerService.SetOrganizationCategoriesPath, 'put');
    if (params) {
      rb.path('organization_id', params.organization_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/hal+json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<{
        }>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `setOrganizationCategories$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  setOrganizationCategories(params: {
    organization_id: string;
    body: OrganizationCategoriesEntity
  },
  context?: HttpContext

): Observable<{
}> {

    return this.setOrganizationCategories$Response(params,context).pipe(
      map((r: StrictHttpResponse<{
}>) => r.body as {
})
    );
  }

  /**
   * Path part for operation activateOrganization
   */
  static readonly ActivateOrganizationPath = '/api/v2/moderator/organizations/{organization_id}/activate';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `activateOrganization()` instead.
   *
   * This method doesn't expect any request body.
   */
  activateOrganization$Response(params: {
    organization_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<{
}>> {

    const rb = new RequestBuilder(this.rootUrl, ModeratorControllerService.ActivateOrganizationPath, 'put');
    if (params) {
      rb.path('organization_id', params.organization_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/hal+json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<{
        }>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `activateOrganization$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  activateOrganization(params: {
    organization_id: string;
  },
  context?: HttpContext

): Observable<{
}> {

    return this.activateOrganization$Response(params,context).pipe(
      map((r: StrictHttpResponse<{
}>) => r.body as {
})
    );
  }

  /**
   * Path part for operation denyOrganizationCategoryRequest
   */
  static readonly DenyOrganizationCategoryRequestPath = '/api/v2/moderator/category-requests/{category_request_id}/deny';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `denyOrganizationCategoryRequest()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  denyOrganizationCategoryRequest$Response(params: {
    category_request_id: number;
    body: PostReviewerComment
  },
  context?: HttpContext

): Observable<StrictHttpResponse<OrganizationCategoryRequestEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ModeratorControllerService.DenyOrganizationCategoryRequestPath, 'put');
    if (params) {
      rb.path('category_request_id', params.category_request_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<OrganizationCategoryRequestEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `denyOrganizationCategoryRequest$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  denyOrganizationCategoryRequest(params: {
    category_request_id: number;
    body: PostReviewerComment
  },
  context?: HttpContext

): Observable<OrganizationCategoryRequestEntity> {

    return this.denyOrganizationCategoryRequest$Response(params,context).pipe(
      map((r: StrictHttpResponse<OrganizationCategoryRequestEntity>) => r.body as OrganizationCategoryRequestEntity)
    );
  }

  /**
   * Path part for operation acceptOrganizationCategoryRequest
   */
  static readonly AcceptOrganizationCategoryRequestPath = '/api/v2/moderator/category-requests/{category_request_id}/accept';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `acceptOrganizationCategoryRequest()` instead.
   *
   * This method doesn't expect any request body.
   */
  acceptOrganizationCategoryRequest$Response(params: {
    category_request_id: number;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<OrganizationCategoryRequestEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ModeratorControllerService.AcceptOrganizationCategoryRequestPath, 'put');
    if (params) {
      rb.path('category_request_id', params.category_request_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<OrganizationCategoryRequestEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `acceptOrganizationCategoryRequest$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  acceptOrganizationCategoryRequest(params: {
    category_request_id: number;
  },
  context?: HttpContext

): Observable<OrganizationCategoryRequestEntity> {

    return this.acceptOrganizationCategoryRequest$Response(params,context).pipe(
      map((r: StrictHttpResponse<OrganizationCategoryRequestEntity>) => r.body as OrganizationCategoryRequestEntity)
    );
  }

  /**
   * Path part for operation getUsers
   */
  static readonly GetUsersPath = '/api/v2/moderator/users';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getUsers()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUsers$Response(params: {
    page: number;
    page_size: number;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<RegisteredUsersEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ModeratorControllerService.GetUsersPath, 'get');
    if (params) {
      rb.query('page', params.page, {});
      rb.query('page_size', params.page_size, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<RegisteredUsersEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getUsers$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUsers(params: {
    page: number;
    page_size: number;
  },
  context?: HttpContext

): Observable<RegisteredUsersEntity> {

    return this.getUsers$Response(params,context).pipe(
      map((r: StrictHttpResponse<RegisteredUsersEntity>) => r.body as RegisteredUsersEntity)
    );
  }

  /**
   * Path part for operation getUserProfile
   */
  static readonly GetUserProfilePath = '/api/v2/moderator/users/{target_user_id}/profile';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getUserProfile()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUserProfile$Response(params: {
    target_user_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<UserProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ModeratorControllerService.GetUserProfilePath, 'get');
    if (params) {
      rb.path('target_user_id', params.target_user_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UserProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getUserProfile$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUserProfile(params: {
    target_user_id: string;
  },
  context?: HttpContext

): Observable<UserProfileEntity> {

    return this.getUserProfile$Response(params,context).pipe(
      map((r: StrictHttpResponse<UserProfileEntity>) => r.body as UserProfileEntity)
    );
  }

  /**
   * Path part for operation getOrganizationsOfUser
   */
  static readonly GetOrganizationsOfUserPath = '/api/v2/moderator/users/{target_user_id}/organizations';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getOrganizationsOfUser()` instead.
   *
   * This method doesn't expect any request body.
   */
  getOrganizationsOfUser$Response(params: {
    target_user_id: string;
    page: number;
    page_size: number;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<ConfidentialOrganizationsEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ModeratorControllerService.GetOrganizationsOfUserPath, 'get');
    if (params) {
      rb.path('target_user_id', params.target_user_id, {});
      rb.query('page', params.page, {});
      rb.query('page_size', params.page_size, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ConfidentialOrganizationsEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getOrganizationsOfUser$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getOrganizationsOfUser(params: {
    target_user_id: string;
    page: number;
    page_size: number;
  },
  context?: HttpContext

): Observable<ConfidentialOrganizationsEntity> {

    return this.getOrganizationsOfUser$Response(params,context).pipe(
      map((r: StrictHttpResponse<ConfidentialOrganizationsEntity>) => r.body as ConfidentialOrganizationsEntity)
    );
  }

  /**
   * Path part for operation getUnapprovedUsers
   */
  static readonly GetUnapprovedUsersPath = '/api/v2/moderator/unapproved-users';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getUnapprovedUsers()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUnapprovedUsers$Response(params: {
    page: number;
    page_size: number;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<RegisteredUsersEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ModeratorControllerService.GetUnapprovedUsersPath, 'get');
    if (params) {
      rb.query('page', params.page, {});
      rb.query('page_size', params.page_size, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<RegisteredUsersEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getUnapprovedUsers$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUnapprovedUsers(params: {
    page: number;
    page_size: number;
  },
  context?: HttpContext

): Observable<RegisteredUsersEntity> {

    return this.getUnapprovedUsers$Response(params,context).pipe(
      map((r: StrictHttpResponse<RegisteredUsersEntity>) => r.body as RegisteredUsersEntity)
    );
  }

  /**
   * Path part for operation getUserProfileOfUnapprovedUser
   */
  static readonly GetUserProfileOfUnapprovedUserPath = '/api/v2/moderator/unapproved-users/{unapproved_user_id}/profile';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getUserProfileOfUnapprovedUser()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUserProfileOfUnapprovedUser$Response(params: {
    unapproved_user_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<UserProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ModeratorControllerService.GetUserProfileOfUnapprovedUserPath, 'get');
    if (params) {
      rb.path('unapproved_user_id', params.unapproved_user_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UserProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getUserProfileOfUnapprovedUser$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUserProfileOfUnapprovedUser(params: {
    unapproved_user_id: string;
  },
  context?: HttpContext

): Observable<UserProfileEntity> {

    return this.getUserProfileOfUnapprovedUser$Response(params,context).pipe(
      map((r: StrictHttpResponse<UserProfileEntity>) => r.body as UserProfileEntity)
    );
  }

  /**
   * Path part for operation getUnapprovedOrganizationProfile
   */
  static readonly GetUnapprovedOrganizationProfilePath = '/api/v2/moderator/unapproved-organizations/{organization_id}/profile';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getUnapprovedOrganizationProfile()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUnapprovedOrganizationProfile$Response(params: {
    organization_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<ConfidentialOrganizationProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ModeratorControllerService.GetUnapprovedOrganizationProfilePath, 'get');
    if (params) {
      rb.path('organization_id', params.organization_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ConfidentialOrganizationProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getUnapprovedOrganizationProfile$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUnapprovedOrganizationProfile(params: {
    organization_id: string;
  },
  context?: HttpContext

): Observable<ConfidentialOrganizationProfileEntity> {

    return this.getUnapprovedOrganizationProfile$Response(params,context).pipe(
      map((r: StrictHttpResponse<ConfidentialOrganizationProfileEntity>) => r.body as ConfidentialOrganizationProfileEntity)
    );
  }

  /**
   * Path part for operation getApprovedOrganizationsEntity
   */
  static readonly GetApprovedOrganizationsEntityPath = '/api/v2/moderator/organizations';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getApprovedOrganizationsEntity()` instead.
   *
   * This method doesn't expect any request body.
   */
  getApprovedOrganizationsEntity$Response(params: {
    page: number;
    page_size: number;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<ConfidentialOrganizationsEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ModeratorControllerService.GetApprovedOrganizationsEntityPath, 'get');
    if (params) {
      rb.query('page', params.page, {});
      rb.query('page_size', params.page_size, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ConfidentialOrganizationsEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getApprovedOrganizationsEntity$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getApprovedOrganizationsEntity(params: {
    page: number;
    page_size: number;
  },
  context?: HttpContext

): Observable<ConfidentialOrganizationsEntity> {

    return this.getApprovedOrganizationsEntity$Response(params,context).pipe(
      map((r: StrictHttpResponse<ConfidentialOrganizationsEntity>) => r.body as ConfidentialOrganizationsEntity)
    );
  }

  /**
   * Path part for operation getConfidentialOrganizationProfileEntity
   */
  static readonly GetConfidentialOrganizationProfileEntityPath = '/api/v2/moderator/organizations/{organization_id}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getConfidentialOrganizationProfileEntity()` instead.
   *
   * This method doesn't expect any request body.
   */
  getConfidentialOrganizationProfileEntity$Response(params: {
    organization_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<ConfidentialOrganizationProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ModeratorControllerService.GetConfidentialOrganizationProfileEntityPath, 'get');
    if (params) {
      rb.path('organization_id', params.organization_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ConfidentialOrganizationProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getConfidentialOrganizationProfileEntity$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getConfidentialOrganizationProfileEntity(params: {
    organization_id: string;
  },
  context?: HttpContext

): Observable<ConfidentialOrganizationProfileEntity> {

    return this.getConfidentialOrganizationProfileEntity$Response(params,context).pipe(
      map((r: StrictHttpResponse<ConfidentialOrganizationProfileEntity>) => r.body as ConfidentialOrganizationProfileEntity)
    );
  }

  /**
   * Path part for operation getAllOrganizationCategoryRequests
   */
  static readonly GetAllOrganizationCategoryRequestsPath = '/api/v2/moderator/category-requests';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getAllOrganizationCategoryRequests()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllOrganizationCategoryRequests$Response(params: {
    page: number;
    page_size: number;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<OrganizationCategoryRequestsEntity>> {

    const rb = new RequestBuilder(this.rootUrl, ModeratorControllerService.GetAllOrganizationCategoryRequestsPath, 'get');
    if (params) {
      rb.query('page', params.page, {});
      rb.query('page_size', params.page_size, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<OrganizationCategoryRequestsEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getAllOrganizationCategoryRequests$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllOrganizationCategoryRequests(params: {
    page: number;
    page_size: number;
  },
  context?: HttpContext

): Observable<OrganizationCategoryRequestsEntity> {

    return this.getAllOrganizationCategoryRequests$Response(params,context).pipe(
      map((r: StrictHttpResponse<OrganizationCategoryRequestsEntity>) => r.body as OrganizationCategoryRequestsEntity)
    );
  }

  /**
   * Path part for operation deleteUserAccount
   */
  static readonly DeleteUserAccountPath = '/api/v2/moderator/users/{target_user_id}/account';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `deleteUserAccount()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteUserAccount$Response(params: {
    target_user_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<{
}>> {

    const rb = new RequestBuilder(this.rootUrl, ModeratorControllerService.DeleteUserAccountPath, 'delete');
    if (params) {
      rb.path('target_user_id', params.target_user_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/hal+json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<{
        }>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `deleteUserAccount$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteUserAccount(params: {
    target_user_id: string;
  },
  context?: HttpContext

): Observable<{
}> {

    return this.deleteUserAccount$Response(params,context).pipe(
      map((r: StrictHttpResponse<{
}>) => r.body as {
})
    );
  }

  /**
   * Path part for operation deleteUnapprovedUser
   */
  static readonly DeleteUnapprovedUserPath = '/api/v2/moderator/unapproved-users/{unapproved_user_id}/account';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `deleteUnapprovedUser()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteUnapprovedUser$Response(params: {
    unapproved_user_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, ModeratorControllerService.DeleteUnapprovedUserPath, 'delete');
    if (params) {
      rb.path('unapproved_user_id', params.unapproved_user_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `deleteUnapprovedUser$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteUnapprovedUser(params: {
    unapproved_user_id: string;
  },
  context?: HttpContext

): Observable<void> {

    return this.deleteUnapprovedUser$Response(params,context).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

}
