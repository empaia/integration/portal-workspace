/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { CanLeaveEntity } from '../models/can-leave-entity';
import { MemberProfileEntity } from '../models/member-profile-entity';
import { MemberProfilesEntity } from '../models/member-profiles-entity';
import { OrganizationUserRoleRequestEntity } from '../models/organization-user-role-request-entity';
import { OrganizationUserRoleRequestsEntity } from '../models/organization-user-role-requests-entity';
import { PostOrganizationUserRoleRequestEntity } from '../models/post-organization-user-role-request-entity';

@Injectable({
  providedIn: 'root',
})
export class OrganizationControllerV2Service extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation getOrganizationUserRoleRequests
   */
  static readonly GetOrganizationUserRoleRequestsPath = '/api/v2/organization/role-requests';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getOrganizationUserRoleRequests()` instead.
   *
   * This method doesn't expect any request body.
   */
  getOrganizationUserRoleRequests$Response(params: {
    page: number;
    page_size: number;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<OrganizationUserRoleRequestsEntity>> {

    const rb = new RequestBuilder(this.rootUrl, OrganizationControllerV2Service.GetOrganizationUserRoleRequestsPath, 'get');
    if (params) {
      rb.query('page', params.page, {});
      rb.query('page_size', params.page_size, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<OrganizationUserRoleRequestsEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getOrganizationUserRoleRequests$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getOrganizationUserRoleRequests(params: {
    page: number;
    page_size: number;
  },
  context?: HttpContext

): Observable<OrganizationUserRoleRequestsEntity> {

    return this.getOrganizationUserRoleRequests$Response(params,context).pipe(
      map((r: StrictHttpResponse<OrganizationUserRoleRequestsEntity>) => r.body as OrganizationUserRoleRequestsEntity)
    );
  }

  /**
   * Path part for operation requestOrganizationUserRole
   */
  static readonly RequestOrganizationUserRolePath = '/api/v2/organization/role-requests';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `requestOrganizationUserRole()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  requestOrganizationUserRole$Response(params: {
    body: PostOrganizationUserRoleRequestEntity
  },
  context?: HttpContext

): Observable<StrictHttpResponse<OrganizationUserRoleRequestEntity>> {

    const rb = new RequestBuilder(this.rootUrl, OrganizationControllerV2Service.RequestOrganizationUserRolePath, 'put');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<OrganizationUserRoleRequestEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `requestOrganizationUserRole$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  requestOrganizationUserRole(params: {
    body: PostOrganizationUserRoleRequestEntity
  },
  context?: HttpContext

): Observable<OrganizationUserRoleRequestEntity> {

    return this.requestOrganizationUserRole$Response(params,context).pipe(
      map((r: StrictHttpResponse<OrganizationUserRoleRequestEntity>) => r.body as OrganizationUserRoleRequestEntity)
    );
  }

  /**
   * Path part for operation revokeOrganizationUserRoleRequest
   */
  static readonly RevokeOrganizationUserRoleRequestPath = '/api/v2/organization/role-requests/{role_request_id}/revoke';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `revokeOrganizationUserRoleRequest()` instead.
   *
   * This method doesn't expect any request body.
   */
  revokeOrganizationUserRoleRequest$Response(params: {
    role_request_id: number;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<OrganizationUserRoleRequestEntity>> {

    const rb = new RequestBuilder(this.rootUrl, OrganizationControllerV2Service.RevokeOrganizationUserRoleRequestPath, 'put');
    if (params) {
      rb.path('role_request_id', params.role_request_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<OrganizationUserRoleRequestEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `revokeOrganizationUserRoleRequest$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  revokeOrganizationUserRoleRequest(params: {
    role_request_id: number;
  },
  context?: HttpContext

): Observable<OrganizationUserRoleRequestEntity> {

    return this.revokeOrganizationUserRoleRequest$Response(params,context).pipe(
      map((r: StrictHttpResponse<OrganizationUserRoleRequestEntity>) => r.body as OrganizationUserRoleRequestEntity)
    );
  }

  /**
   * Path part for operation leaveOrganization
   */
  static readonly LeaveOrganizationPath = '/api/v2/organization/leave';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `leaveOrganization()` instead.
   *
   * This method doesn't expect any request body.
   */
  leaveOrganization$Response(params?: {
  },
  context?: HttpContext

): Observable<StrictHttpResponse<{
}>> {

    const rb = new RequestBuilder(this.rootUrl, OrganizationControllerV2Service.LeaveOrganizationPath, 'put');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/hal+json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<{
        }>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `leaveOrganization$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  leaveOrganization(params?: {
  },
  context?: HttpContext

): Observable<{
}> {

    return this.leaveOrganization$Response(params,context).pipe(
      map((r: StrictHttpResponse<{
}>) => r.body as {
})
    );
  }

  /**
   * Path part for operation getMemberProfiles
   */
  static readonly GetMemberProfilesPath = '/api/v2/organization/users';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getMemberProfiles()` instead.
   *
   * This method doesn't expect any request body.
   */
  getMemberProfiles$Response(params?: {
  },
  context?: HttpContext

): Observable<StrictHttpResponse<MemberProfilesEntity>> {

    const rb = new RequestBuilder(this.rootUrl, OrganizationControllerV2Service.GetMemberProfilesPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<MemberProfilesEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getMemberProfiles$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getMemberProfiles(params?: {
  },
  context?: HttpContext

): Observable<MemberProfilesEntity> {

    return this.getMemberProfiles$Response(params,context).pipe(
      map((r: StrictHttpResponse<MemberProfilesEntity>) => r.body as MemberProfilesEntity)
    );
  }

  /**
   * Path part for operation getMemberProfile
   */
  static readonly GetMemberProfilePath = '/api/v2/organization/users/{target_user_id}/profile';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getMemberProfile()` instead.
   *
   * This method doesn't expect any request body.
   */
  getMemberProfile$Response(params: {
    target_user_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<MemberProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, OrganizationControllerV2Service.GetMemberProfilePath, 'get');
    if (params) {
      rb.path('target_user_id', params.target_user_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<MemberProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getMemberProfile$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getMemberProfile(params: {
    target_user_id: string;
  },
  context?: HttpContext

): Observable<MemberProfileEntity> {

    return this.getMemberProfile$Response(params,context).pipe(
      map((r: StrictHttpResponse<MemberProfileEntity>) => r.body as MemberProfileEntity)
    );
  }

  /**
   * Path part for operation canLeaveOrganization
   */
  static readonly CanLeaveOrganizationPath = '/api/v2/organization/can-leave';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `canLeaveOrganization()` instead.
   *
   * This method doesn't expect any request body.
   */
  canLeaveOrganization$Response(params?: {
  },
  context?: HttpContext

): Observable<StrictHttpResponse<CanLeaveEntity>> {

    const rb = new RequestBuilder(this.rootUrl, OrganizationControllerV2Service.CanLeaveOrganizationPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<CanLeaveEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `canLeaveOrganization$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  canLeaveOrganization(params?: {
  },
  context?: HttpContext

): Observable<CanLeaveEntity> {

    return this.canLeaveOrganization$Response(params,context).pipe(
      map((r: StrictHttpResponse<CanLeaveEntity>) => r.body as CanLeaveEntity)
    );
  }

}
