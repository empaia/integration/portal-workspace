/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { PostUserEntity } from '../models/post-user-entity';
import { PublicOrganizationProfileEntity } from '../models/public-organization-profile-entity';
import { PublicOrganizationsEntity } from '../models/public-organizations-entity';

@Injectable({
  providedIn: 'root',
})
export class PublicControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation verifyAccount
   */
  static readonly VerifyAccountPath = '/api/v2/public/verify-registration';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `verifyAccount()` instead.
   *
   * This method doesn't expect any request body.
   */
  verifyAccount$Response(params: {
    email_address: string;
    token: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<{
}>> {

    const rb = new RequestBuilder(this.rootUrl, PublicControllerService.VerifyAccountPath, 'post');
    if (params) {
      rb.query('email_address', params.email_address, {});
      rb.query('token', params.token, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/hal+json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<{
        }>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `verifyAccount$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  verifyAccount(params: {
    email_address: string;
    token: string;
  },
  context?: HttpContext

): Observable<{
}> {

    return this.verifyAccount$Response(params,context).pipe(
      map((r: StrictHttpResponse<{
}>) => r.body as {
})
    );
  }

  /**
   * Path part for operation registerNewUser
   */
  static readonly RegisterNewUserPath = '/api/v2/public/register-new-user';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `registerNewUser()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  registerNewUser$Response(params: {
    body: PostUserEntity
  },
  context?: HttpContext

): Observable<StrictHttpResponse<{
}>> {

    const rb = new RequestBuilder(this.rootUrl, PublicControllerService.RegisterNewUserPath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/hal+json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<{
        }>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `registerNewUser$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  registerNewUser(params: {
    body: PostUserEntity
  },
  context?: HttpContext

): Observable<{
}> {

    return this.registerNewUser$Response(params,context).pipe(
      map((r: StrictHttpResponse<{
}>) => r.body as {
})
    );
  }

  /**
   * Path part for operation getActiveOrganizations
   */
  static readonly GetActiveOrganizationsPath = '/api/v2/public/organizations';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getActiveOrganizations()` instead.
   *
   * This method doesn't expect any request body.
   */
  getActiveOrganizations$Response(params?: {
    page?: number;
    page_size?: number;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<PublicOrganizationsEntity>> {

    const rb = new RequestBuilder(this.rootUrl, PublicControllerService.GetActiveOrganizationsPath, 'get');
    if (params) {
      rb.query('page', params.page, {});
      rb.query('page_size', params.page_size, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<PublicOrganizationsEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getActiveOrganizations$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getActiveOrganizations(params?: {
    page?: number;
    page_size?: number;
  },
  context?: HttpContext

): Observable<PublicOrganizationsEntity> {

    return this.getActiveOrganizations$Response(params,context).pipe(
      map((r: StrictHttpResponse<PublicOrganizationsEntity>) => r.body as PublicOrganizationsEntity)
    );
  }

  /**
   * Path part for operation getActiveOrganization
   */
  static readonly GetActiveOrganizationPath = '/api/v2/public/organizations/{organization_id}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getActiveOrganization()` instead.
   *
   * This method doesn't expect any request body.
   */
  getActiveOrganization$Response(params: {
    organization_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<PublicOrganizationProfileEntity>> {

    const rb = new RequestBuilder(this.rootUrl, PublicControllerService.GetActiveOrganizationPath, 'get');
    if (params) {
      rb.path('organization_id', params.organization_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<PublicOrganizationProfileEntity>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getActiveOrganization$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getActiveOrganization(params: {
    organization_id: string;
  },
  context?: HttpContext

): Observable<PublicOrganizationProfileEntity> {

    return this.getActiveOrganization$Response(params,context).pipe(
      map((r: StrictHttpResponse<PublicOrganizationProfileEntity>) => r.body as PublicOrganizationProfileEntity)
    );
  }

}
