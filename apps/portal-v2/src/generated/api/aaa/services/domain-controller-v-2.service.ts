/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { CountryEntityV2 } from '../models/country-entity-v-2';

@Injectable({
  providedIn: 'root',
})
export class DomainControllerV2Service extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation getCountries
   */
  static readonly GetCountriesPath = '/api/v2/domain/countries';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getCountries()` instead.
   *
   * This method doesn't expect any request body.
   */
  getCountries$Response(params?: {
  },
  context?: HttpContext

): Observable<StrictHttpResponse<Array<CountryEntityV2>>> {

    const rb = new RequestBuilder(this.rootUrl, DomainControllerV2Service.GetCountriesPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<CountryEntityV2>>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getCountries$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getCountries(params?: {
  },
  context?: HttpContext

): Observable<Array<CountryEntityV2>> {

    return this.getCountries$Response(params,context).pipe(
      map((r: StrictHttpResponse<Array<CountryEntityV2>>) => r.body as Array<CountryEntityV2>)
    );
  }

  /**
   * Path part for operation getCountry
   */
  static readonly GetCountryPath = '/api/v2/domain/countries/{iso_3166_alpha_2_code}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getCountry()` instead.
   *
   * This method doesn't expect any request body.
   */
  getCountry$Response(params: {
    iso_3166_alpha_2_code: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<CountryEntityV2>> {

    const rb = new RequestBuilder(this.rootUrl, DomainControllerV2Service.GetCountryPath, 'get');
    if (params) {
      rb.path('iso_3166_alpha_2_code', params.iso_3166_alpha_2_code, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<CountryEntityV2>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getCountry$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getCountry(params: {
    iso_3166_alpha_2_code: string;
  },
  context?: HttpContext

): Observable<CountryEntityV2> {

    return this.getCountry$Response(params,context).pipe(
      map((r: StrictHttpResponse<CountryEntityV2>) => r.body as CountryEntityV2)
    );
  }

}
