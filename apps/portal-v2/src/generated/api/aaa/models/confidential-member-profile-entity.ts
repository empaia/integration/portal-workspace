/* tslint:disable */
/* eslint-disable */
import { OrganizationUserRoleV2 } from './organization-user-role-v-2';
export interface ConfidentialMemberProfileEntity {
  email_address: string;
  first_name: string;
  last_name: string;
  organization_user_roles: Array<OrganizationUserRoleV2>;
  title: string;
  user_id: string;
}
