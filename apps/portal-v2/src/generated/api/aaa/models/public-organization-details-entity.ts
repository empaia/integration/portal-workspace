/* tslint:disable */
/* eslint-disable */
import { OrganizationCategory } from './organization-category';
import { TextTranslationEntity } from './text-translation-entity';
export interface PublicOrganizationDetailsEntity {
  categories: Array<OrganizationCategory>;
  description: Array<TextTranslationEntity>;
  number_of_members?: number;
}
