/* tslint:disable */
/* eslint-disable */
import { OrganizationAccountState } from './organization-account-state';
export interface OrganizationAccountStateQueryEntity {
  organization_account_states?: Array<OrganizationAccountState>;
}
