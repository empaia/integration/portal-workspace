/* tslint:disable */
/* eslint-disable */
import { CountryCode } from './country-code';
export interface PostUserContactDataEntity {
  country_code: CountryCode;
  phone_number?: string;
  place_name: string;
  street_name: string;
  street_number: string;
  zip_code: string;
}
