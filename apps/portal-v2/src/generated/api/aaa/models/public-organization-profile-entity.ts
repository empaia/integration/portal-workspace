/* tslint:disable */
/* eslint-disable */
import { PublicOrganizationContactDataEntity } from './public-organization-contact-data-entity';
import { PublicOrganizationDetailsEntity } from './public-organization-details-entity';
import { ResizedPictureUrlsEntity } from './resized-picture-urls-entity';
export interface PublicOrganizationProfileEntity {
  contact_data: PublicOrganizationContactDataEntity;
  details: PublicOrganizationDetailsEntity;
  logo_url?: string;
  organization_id: string;
  organization_name: string;
  resized_logo_urls?: ResizedPictureUrlsEntity;
}
