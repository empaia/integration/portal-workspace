/* tslint:disable */
/* eslint-disable */
import { OrganizationCategory } from './organization-category';
export interface OrganizationCategoriesEntity {
  categories: Array<OrganizationCategory>;
}
