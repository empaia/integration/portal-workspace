/* tslint:disable */
/* eslint-disable */
import { LanguageCode } from './language-code';
import { ResizedPictureUrlsEntity } from './resized-picture-urls-entity';
import { UserAccountState } from './user-account-state';
import { UserContactDataEntity } from './user-contact-data-entity';
import { UserDetailsEntity } from './user-details-entity';
import { UserRole } from './user-role';
export interface UserProfileEntity {
  account_state: UserAccountState;
  contact_data: UserContactDataEntity;
  created_timestamp: number;
  details: UserDetailsEntity;
  language_code: LanguageCode;
  profile_picture_url?: string;
  resized_profile_picture_urls?: ResizedPictureUrlsEntity;
  user_id: string;
  user_roles: Array<UserRole>;
  username: string;
}
