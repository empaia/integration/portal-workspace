/* tslint:disable */
/* eslint-disable */
import { ExtendedUserDetailsEntity } from './extended-user-details-entity';
import { RequestState } from './request-state';
export interface MembershipRequestEntity {
  created_at: number;
  membership_request_id: number;
  organization_id: string;
  organization_name: string;
  request_state: RequestState;
  reviewer_comment?: string;
  reviewer_id?: string;
  updated_at?: number;
  user_details: ExtendedUserDetailsEntity;
}
