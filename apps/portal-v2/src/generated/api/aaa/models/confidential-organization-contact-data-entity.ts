/* tslint:disable */
/* eslint-disable */
import { CountryCode } from './country-code';
export interface ConfidentialOrganizationContactDataEntity {
  country_code: CountryCode;
  department?: string;
  email_address: string;
  fax_number?: string;
  is_email_address_public: boolean;
  is_fax_number_public: boolean;
  is_phone_number_public: boolean;
  phone_number: string;
  place_name: string;
  street_name: string;
  street_number: string;
  website: string;
  zip_code: string;
}
