/* tslint:disable */
/* eslint-disable */
import { LanguageCode } from './language-code';
export interface LanguageEntity {
  language_code: LanguageCode;
}
