/* tslint:disable */
/* eslint-disable */
import { CountryCode } from './country-code';
import { TextTranslationEntity } from './text-translation-entity';
export interface CountryEntityV2 {
  country_code: CountryCode;
  translated_names: Array<TextTranslationEntity>;
}
