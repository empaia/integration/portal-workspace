/* tslint:disable */
/* eslint-disable */
import { MembershipRequestEntity } from './membership-request-entity';
export interface MembershipRequestsEntity {
  membership_requests: Array<MembershipRequestEntity>;
  total_membership_requests_count: number;
}
