/* tslint:disable */
/* eslint-disable */
import { ConfidentialMemberProfileEntity } from './confidential-member-profile-entity';
export interface ConfidentialMemberProfilesEntity {
  users: Array<ConfidentialMemberProfileEntity>;
}
