/* tslint:disable */
/* eslint-disable */
import { ConfidentialOrganizationContactDataEntity } from './confidential-organization-contact-data-entity';
import { ConfidentialOrganizationDetailsEntity } from './confidential-organization-details-entity';
import { OrganizationAccountState } from './organization-account-state';
import { ResizedPictureUrlsEntity } from './resized-picture-urls-entity';
export interface ConfidentialOrganizationProfileEntity {
  account_state: OrganizationAccountState;
  contact_data: ConfidentialOrganizationContactDataEntity;
  created_timestamp: number;
  details: ConfidentialOrganizationDetailsEntity;
  logo_url?: string;
  normalized_name: string;
  organization_id: string;
  organization_name: string;
  resized_logo_urls?: ResizedPictureUrlsEntity;
  updated_timestamp: number;
}
