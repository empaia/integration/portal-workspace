/* tslint:disable */
/* eslint-disable */
import { OrganizationUserRoleRequestEntity } from './organization-user-role-request-entity';
export interface OrganizationUserRoleRequestsEntity {
  organization_user_role_requests: Array<OrganizationUserRoleRequestEntity>;
  total_organization_user_role_requests_count: number;
}
