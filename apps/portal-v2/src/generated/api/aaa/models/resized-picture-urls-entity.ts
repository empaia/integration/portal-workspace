/* tslint:disable */
/* eslint-disable */
export interface ResizedPictureUrlsEntity {
  w1200?: string;
  w400?: string;
  w60?: string;
  w800?: string;
}
