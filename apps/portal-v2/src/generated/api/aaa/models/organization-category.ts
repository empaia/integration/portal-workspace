/* tslint:disable */
/* eslint-disable */
export enum OrganizationCategory {
  APP_CUSTOMER = 'APP_CUSTOMER',
  APP_VENDOR = 'APP_VENDOR',
  COMPUTE_PROVIDER = 'COMPUTE_PROVIDER',
  PRODUCT_PROVIDER = 'PRODUCT_PROVIDER'
}
