/* tslint:disable */
/* eslint-disable */
import { LanguageCode } from './language-code';
import { UserContactDataEntity } from './user-contact-data-entity';
import { UserDetailsEntity } from './user-details-entity';
export interface PostUserEntity {
  contact_data: UserContactDataEntity;
  details: UserDetailsEntity;
  language_code: LanguageCode;
  password?: string;
}
