/* tslint:disable */
/* eslint-disable */
export interface UserDetailsEntity {
  first_name: string;
  last_name: string;
  title?: string;
}
