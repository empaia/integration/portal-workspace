/* tslint:disable */
/* eslint-disable */
import { OrganizationCategory } from './organization-category';
export interface PostInitialConfidentialOrganizationDetailsEntity {
  categories: Array<OrganizationCategory>;
  description_de: string;
  description_en: string;
  is_user_count_public: boolean;
}
