/* tslint:disable */
/* eslint-disable */
import { UserRole } from './user-role';
export interface UserRolesEntity {
  roles: Array<UserRole>;
}
