/* tslint:disable */
/* eslint-disable */
export interface EmailAddressListEntity {
  email_addresses: Array<string>;
}
