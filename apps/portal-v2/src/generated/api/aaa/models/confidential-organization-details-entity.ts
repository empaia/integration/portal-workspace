/* tslint:disable */
/* eslint-disable */
import { OrganizationCategory } from './organization-category';
export interface ConfidentialOrganizationDetailsEntity {
  categories: Array<OrganizationCategory>;
  description_de: string;
  description_en: string;
  is_user_count_public: boolean;
  number_of_members: number;
}
