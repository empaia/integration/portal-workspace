/* tslint:disable */
/* eslint-disable */
import { MemberProfileEntity } from './member-profile-entity';
export interface MemberProfilesEntity {
  users: Array<MemberProfileEntity>;
}
