/* tslint:disable */
/* eslint-disable */
export interface ExtendedUserDetailsEntity {
  email_address: string;
  first_name: string;
  last_name: string;
  title?: string;
  user_id: string;
}
