/* tslint:disable */
/* eslint-disable */
import { UserRole } from './user-role';
export interface RegisteredUserEntity {
  is_active: boolean;
  member_of: Array<string>;
  user_id: string;
  user_roles: Array<UserRole>;
  username: string;
}
