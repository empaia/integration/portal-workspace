/* tslint:disable */
/* eslint-disable */
import { MembershipEntity } from './membership-entity';
export interface MembershipsEntity {
  memberships: Array<MembershipEntity>;
}
