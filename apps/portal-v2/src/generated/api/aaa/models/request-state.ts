/* tslint:disable */
/* eslint-disable */
export enum RequestState {
  REQUESTED = 'REQUESTED',
  REVOKED = 'REVOKED',
  ACCEPTED = 'ACCEPTED',
  REJECTED = 'REJECTED'
}
