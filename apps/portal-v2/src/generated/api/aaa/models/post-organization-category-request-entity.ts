/* tslint:disable */
/* eslint-disable */
import { OrganizationCategory } from './organization-category';
export interface PostOrganizationCategoryRequestEntity {
  requested_organization_categories: Array<OrganizationCategory>;
}
