/* tslint:disable */
/* eslint-disable */
export interface PostConfidentialOrganizationDetailsEntity {
  description_de: string;
  description_en: string;
  is_user_count_public: boolean;
}
