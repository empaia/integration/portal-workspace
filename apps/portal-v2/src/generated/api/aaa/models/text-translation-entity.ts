/* tslint:disable */
/* eslint-disable */
import { LanguageCode } from './language-code';
export interface TextTranslationEntity {
  language_code?: LanguageCode;
  text?: string;
}
