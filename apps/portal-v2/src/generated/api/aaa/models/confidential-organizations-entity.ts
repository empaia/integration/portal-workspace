/* tslint:disable */
/* eslint-disable */
import { ConfidentialOrganizationEntity } from './confidential-organization-entity';
export interface ConfidentialOrganizationsEntity {
  organizations: Array<ConfidentialOrganizationEntity>;
  total_organizations_count: number;
}
