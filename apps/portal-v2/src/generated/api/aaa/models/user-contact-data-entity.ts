/* tslint:disable */
/* eslint-disable */
import { CountryCode } from './country-code';
export interface UserContactDataEntity {
  country_code: CountryCode;
  email_address: string;
  phone_number?: string;
  place_name: string;
  street_name: string;
  street_number: string;
  zip_code: string;
}
