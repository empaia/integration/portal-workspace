/* tslint:disable */
/* eslint-disable */
import { RegisteredUserEntity } from './registered-user-entity';
export interface RegisteredUsersEntity {
  total_users_count: number;
  users: Array<RegisteredUserEntity>;
}
