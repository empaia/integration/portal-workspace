/* tslint:disable */
/* eslint-disable */
import { PublicOrganizationEntity } from './public-organization-entity';
export interface PublicOrganizationsEntity {
  organizations: Array<PublicOrganizationEntity>;
  total_organizations_count: number;
}
