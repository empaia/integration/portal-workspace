/* tslint:disable */
/* eslint-disable */
import { ExtendedUserDetailsEntity } from './extended-user-details-entity';
import { OrganizationCategory } from './organization-category';
import { RequestState } from './request-state';
export interface OrganizationCategoryRequestEntity {
  created_at: number;
  existing_categories: Array<OrganizationCategory>;
  organization_category_request_id: number;
  organization_id: string;
  organization_name: string;
  request_state: RequestState;
  requested_categories: Array<OrganizationCategory>;
  retracted_categories: Array<OrganizationCategory>;
  reviewer_comment?: string;
  reviewer_id?: string;
  updated_at?: number;
  user_details: ExtendedUserDetailsEntity;
}
