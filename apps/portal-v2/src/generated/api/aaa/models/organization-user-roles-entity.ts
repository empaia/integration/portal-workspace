/* tslint:disable */
/* eslint-disable */
import { OrganizationUserRoleV2 } from './organization-user-role-v-2';
export interface OrganizationUserRolesEntity {
  owned_roles: Array<OrganizationUserRoleV2>;
  selectable_roles: Array<OrganizationUserRoleV2>;
}
