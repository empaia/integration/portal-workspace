/* tslint:disable */
/* eslint-disable */
import { PublicOrganizationProfileEntity } from './public-organization-profile-entity';
export interface PublicOrganizationProfilesEntity {
  organizations: Array<PublicOrganizationProfileEntity>;
  total_organizations_count: number;
}
