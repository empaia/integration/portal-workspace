/* tslint:disable */
/* eslint-disable */
import { UnapprovedConfidentialOrganizationProfileEntity } from './unapproved-confidential-organization-profile-entity';
export interface UnapprovedConfidentialOrganizationProfilesEntity {
  organizations: Array<UnapprovedConfidentialOrganizationProfileEntity>;
  total_organizations_count: number;
}
