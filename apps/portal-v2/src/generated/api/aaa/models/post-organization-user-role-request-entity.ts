/* tslint:disable */
/* eslint-disable */
import { OrganizationUserRoleV2 } from './organization-user-role-v-2';
export interface PostOrganizationUserRoleRequestEntity {
  requested_roles: Array<OrganizationUserRoleV2>;
}
