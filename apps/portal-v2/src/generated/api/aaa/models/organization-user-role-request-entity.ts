/* tslint:disable */
/* eslint-disable */
import { ExtendedUserDetailsEntity } from './extended-user-details-entity';
import { OrganizationUserRoleV2 } from './organization-user-role-v-2';
import { RequestState } from './request-state';
export interface OrganizationUserRoleRequestEntity {
  created_at: number;
  existing_roles: Array<OrganizationUserRoleV2>;
  organization_id: string;
  organization_name: string;
  organization_user_role_request_id: number;
  request_state: RequestState;
  requested_roles: Array<OrganizationUserRoleV2>;
  retracted_roles: Array<OrganizationUserRoleV2>;
  reviewer_comment?: string;
  reviewer_id?: string;
  updated_at?: number;
  user_details: ExtendedUserDetailsEntity;
}
