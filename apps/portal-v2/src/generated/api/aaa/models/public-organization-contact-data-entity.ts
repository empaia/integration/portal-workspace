/* tslint:disable */
/* eslint-disable */
import { CountryCode } from './country-code';
export interface PublicOrganizationContactDataEntity {
  country_code: CountryCode;
  department?: string;
  email_address?: string;
  fax_number?: string;
  phone_number?: string;
  place_name: string;
  street_name: string;
  street_number: string;
  website?: string;
  zip_code: string;
}
