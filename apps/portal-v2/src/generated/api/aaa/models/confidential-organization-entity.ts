/* tslint:disable */
/* eslint-disable */
import { OrganizationAccountState } from './organization-account-state';
import { OrganizationCategory } from './organization-category';
import { ResizedPictureUrlsEntity } from './resized-picture-urls-entity';
export interface ConfidentialOrganizationEntity {
  account_state: OrganizationAccountState;
  categories: Array<OrganizationCategory>;
  logo_url?: string;
  organization_id: string;
  organization_name: string;
  resized_logo_urls?: ResizedPictureUrlsEntity;
}
