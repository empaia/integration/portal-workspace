/* tslint:disable */
/* eslint-disable */
import { ConfidentialOrganizationContactDataEntity } from './confidential-organization-contact-data-entity';
import { PostInitialConfidentialOrganizationDetailsEntity } from './post-initial-confidential-organization-details-entity';
export interface PostOrganizationEntity {
  contact_data?: ConfidentialOrganizationContactDataEntity;
  details?: PostInitialConfidentialOrganizationDetailsEntity;
  organization_name?: string;
}
