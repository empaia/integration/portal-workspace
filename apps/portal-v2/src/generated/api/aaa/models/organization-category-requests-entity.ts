/* tslint:disable */
/* eslint-disable */
import { OrganizationCategoryRequestEntity } from './organization-category-request-entity';
export interface OrganizationCategoryRequestsEntity {
  organization_category_requests: Array<OrganizationCategoryRequestEntity>;
  total_organization_category_requests_count: number;
}
