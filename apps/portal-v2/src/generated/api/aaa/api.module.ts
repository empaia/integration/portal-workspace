/* tslint:disable */
/* eslint-disable */
import { NgModule, ModuleWithProviders, SkipSelf, Optional } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConfiguration, ApiConfigurationParams } from './api-configuration';

import { OrganizationControllerV2Service } from './services/organization-controller-v-2.service';
import { MyUnapprovedOrganizationsControllerService } from './services/my-unapproved-organizations-controller.service';
import { MyControllerService } from './services/my-controller.service';
import { ModeratorControllerService } from './services/moderator-controller.service';
import { ManagerControllerService } from './services/manager-controller.service';
import { PublicControllerService } from './services/public-controller.service';
import { DomainControllerV2Service } from './services/domain-controller-v-2.service';

/**
 * Module that provides all services and configuration.
 */
@NgModule({
  imports: [],
  exports: [],
  declarations: [],
  providers: [
    OrganizationControllerV2Service,
    MyUnapprovedOrganizationsControllerService,
    MyControllerService,
    ModeratorControllerService,
    ManagerControllerService,
    PublicControllerService,
    DomainControllerV2Service,
    ApiConfiguration
  ],
})
export class ApiModule {
  static forRoot(params: ApiConfigurationParams): ModuleWithProviders<ApiModule> {
    return {
      ngModule: ApiModule,
      providers: [
        {
          provide: ApiConfiguration,
          useValue: params
        }
      ]
    }
  }

  constructor( 
    @Optional() @SkipSelf() parentModule: ApiModule,
    @Optional() http: HttpClient
  ) {
    if (parentModule) {
      throw new Error('ApiModule is already loaded. Import in your base AppModule only.');
    }
    if (!http) {
      throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
      'See also https://github.com/angular/angular/issues/20575');
    }
  }
}
