/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { NotificationList } from '../models/notification-list';
import { PutConfirm } from '../models/put-confirm';

@Injectable({
  providedIn: 'root',
})
export class NotificationsService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation notificationsGet
   */
  static readonly NotificationsGetPath = '/notifications';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `notificationsGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  notificationsGet$Response(params?: {
  },
  context?: HttpContext

): Observable<StrictHttpResponse<NotificationList>> {

    const rb = new RequestBuilder(this.rootUrl, NotificationsService.NotificationsGetPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<NotificationList>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `notificationsGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  notificationsGet(params?: {
  },
  context?: HttpContext

): Observable<NotificationList> {

    return this.notificationsGet$Response(params,context).pipe(
      map((r: StrictHttpResponse<NotificationList>) => r.body as NotificationList)
    );
  }

  /**
   * Path part for operation notificationsConfirmPut
   */
  static readonly NotificationsConfirmPutPath = '/notifications/confirm';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `notificationsConfirmPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  notificationsConfirmPut$Response(params: {
    body: PutConfirm
  },
  context?: HttpContext

): Observable<StrictHttpResponse<NotificationList>> {

    const rb = new RequestBuilder(this.rootUrl, NotificationsService.NotificationsConfirmPutPath, 'put');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<NotificationList>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `notificationsConfirmPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  notificationsConfirmPut(params: {
    body: PutConfirm
  },
  context?: HttpContext

): Observable<NotificationList> {

    return this.notificationsConfirmPut$Response(params,context).pipe(
      map((r: StrictHttpResponse<NotificationList>) => r.body as NotificationList)
    );
  }

}
