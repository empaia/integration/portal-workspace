/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { PutEmailConfirm } from '../models/put-email-confirm';

@Injectable({
  providedIn: 'root',
})
export class EmailsService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation emailsConfirmPut
   */
  static readonly EmailsConfirmPutPath = '/emails/confirm';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `emailsConfirmPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  emailsConfirmPut$Response(params: {
    body: PutEmailConfirm
  },
  context?: HttpContext

): Observable<StrictHttpResponse<any>> {

    const rb = new RequestBuilder(this.rootUrl, EmailsService.EmailsConfirmPutPath, 'put');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<any>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `emailsConfirmPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  emailsConfirmPut(params: {
    body: PutEmailConfirm
  },
  context?: HttpContext

): Observable<any> {

    return this.emailsConfirmPut$Response(params,context).pipe(
      map((r: StrictHttpResponse<any>) => r.body as any)
    );
  }

}
