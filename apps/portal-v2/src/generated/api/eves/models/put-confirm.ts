/* tslint:disable */
/* eslint-disable */
export interface PutConfirm {
  notification_id: string;
  trigger_event?: boolean;
}
