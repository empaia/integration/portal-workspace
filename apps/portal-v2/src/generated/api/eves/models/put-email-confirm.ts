/* tslint:disable */
/* eslint-disable */
export interface PutEmailConfirm {
  confirm_token: string;
  trigger_event?: boolean;
}
