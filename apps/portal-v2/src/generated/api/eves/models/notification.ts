/* tslint:disable */
/* eslint-disable */
export interface Notification {
  content: string;
  created_at: number;
  id: string;
}
