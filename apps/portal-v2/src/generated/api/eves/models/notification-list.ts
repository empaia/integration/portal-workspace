/* tslint:disable */
/* eslint-disable */
import { Notification } from './notification';
export interface NotificationList {

  /**
   * Number of Notifications.
   */
  item_count: number;

  /**
   * List of Notification items.
   */
  items: Array<Notification>;
}
