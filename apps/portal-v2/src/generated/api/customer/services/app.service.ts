/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { AppConfiguration } from '../models/app-configuration';
import { AppUiConfiguration } from '../models/app-ui-configuration';
import { ClosedApp } from '../models/closed-app';

@Injectable({
  providedIn: 'root',
})
export class AppService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation appsAppIdGet
   */
  static readonly AppsAppIdGetPath = '/apps/{app_id}';

  /**
   * Get app by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `appsAppIdGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  appsAppIdGet$Response(params: {

    /**
     * App ID
     */
    app_id: string;
    'organization-id': string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<ClosedApp>> {

    const rb = new RequestBuilder(this.rootUrl, AppService.AppsAppIdGetPath, 'get');
    if (params) {
      rb.path('app_id', params.app_id, {});
      rb.header('organization-id', params['organization-id'], {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ClosedApp>;
      })
    );
  }

  /**
   * Get app by ID.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `appsAppIdGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  appsAppIdGet(params: {

    /**
     * App ID
     */
    app_id: string;
    'organization-id': string;
  },
  context?: HttpContext

): Observable<ClosedApp> {

    return this.appsAppIdGet$Response(params,context).pipe(
      map((r: StrictHttpResponse<ClosedApp>) => r.body as ClosedApp)
    );
  }

  /**
   * Path part for operation appsAppIdConfigGet
   */
  static readonly AppsAppIdConfigGetPath = '/apps/{app_id}/config';

  /**
   * Get the configuration of an existing app.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `appsAppIdConfigGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  appsAppIdConfigGet$Response(params: {

    /**
     * App ID
     */
    app_id: string;
    'organization-id': string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<AppConfiguration>> {

    const rb = new RequestBuilder(this.rootUrl, AppService.AppsAppIdConfigGetPath, 'get');
    if (params) {
      rb.path('app_id', params.app_id, {});
      rb.header('organization-id', params['organization-id'], {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<AppConfiguration>;
      })
    );
  }

  /**
   * Get the configuration of an existing app.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `appsAppIdConfigGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  appsAppIdConfigGet(params: {

    /**
     * App ID
     */
    app_id: string;
    'organization-id': string;
  },
  context?: HttpContext

): Observable<AppConfiguration> {

    return this.appsAppIdConfigGet$Response(params,context).pipe(
      map((r: StrictHttpResponse<AppConfiguration>) => r.body as AppConfiguration)
    );
  }

  /**
   * Path part for operation appsAppIdAppUiConfigGet
   */
  static readonly AppsAppIdAppUiConfigGetPath = '/apps/{app_id}/app-ui-config';

  /**
   * Get the configuration of an app ui.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `appsAppIdAppUiConfigGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  appsAppIdAppUiConfigGet$Response(params: {

    /**
     * App ID
     */
    app_id: string;
    'organization-id': string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<AppUiConfiguration>> {

    const rb = new RequestBuilder(this.rootUrl, AppService.AppsAppIdAppUiConfigGetPath, 'get');
    if (params) {
      rb.path('app_id', params.app_id, {});
      rb.header('organization-id', params['organization-id'], {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<AppUiConfiguration>;
      })
    );
  }

  /**
   * Get the configuration of an app ui.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `appsAppIdAppUiConfigGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  appsAppIdAppUiConfigGet(params: {

    /**
     * App ID
     */
    app_id: string;
    'organization-id': string;
  },
  context?: HttpContext

): Observable<AppUiConfiguration> {

    return this.appsAppIdAppUiConfigGet$Response(params,context).pipe(
      map((r: StrictHttpResponse<AppUiConfiguration>) => r.body as AppUiConfiguration)
    );
  }

  /**
   * Path part for operation appsAppIdAppUiFilesResourceFilenameGet
   */
  static readonly AppsAppIdAppUiFilesResourceFilenameGetPath = '/apps/{app_id}/app-ui-files/{resource_filename}';

  /**
   * Obtain app UI files.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `appsAppIdAppUiFilesResourceFilenameGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  appsAppIdAppUiFilesResourceFilenameGet$Response(params: {

    /**
     * App ID
     */
    app_id: string;

    /**
     * Filename of the frontend resource
     */
    resource_filename: string;
    'organization-id': string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, AppService.AppsAppIdAppUiFilesResourceFilenameGetPath, 'get');
    if (params) {
      rb.path('app_id', params.app_id, {});
      rb.path('resource_filename', params.resource_filename, {});
      rb.header('organization-id', params['organization-id'], {});
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * Obtain app UI files.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `appsAppIdAppUiFilesResourceFilenameGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  appsAppIdAppUiFilesResourceFilenameGet(params: {

    /**
     * App ID
     */
    app_id: string;

    /**
     * Filename of the frontend resource
     */
    resource_filename: string;
    'organization-id': string;
  },
  context?: HttpContext

): Observable<void> {

    return this.appsAppIdAppUiFilesResourceFilenameGet$Response(params,context).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

}
