/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { ClosedAppView } from '../models/closed-app-view';
import { CustomerAppViewQuery } from '../models/customer-app-view-query';

@Injectable({
  providedIn: 'root',
})
export class AppViewsService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation appViewsQueryPut
   */
  static readonly AppViewsQueryPutPath = '/app-views/query';

  /**
   * Query app views by app IDs and API version.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `appViewsQueryPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  appViewsQueryPut$Response(params: {
    'organization-id': string;
    body: CustomerAppViewQuery
  },
  context?: HttpContext

): Observable<StrictHttpResponse<Array<ClosedAppView>>> {

    const rb = new RequestBuilder(this.rootUrl, AppViewsService.AppViewsQueryPutPath, 'put');
    if (params) {
      rb.header('organization-id', params['organization-id'], {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<ClosedAppView>>;
      })
    );
  }

  /**
   * Query app views by app IDs and API version.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `appViewsQueryPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  appViewsQueryPut(params: {
    'organization-id': string;
    body: CustomerAppViewQuery
  },
  context?: HttpContext

): Observable<Array<ClosedAppView>> {

    return this.appViewsQueryPut$Response(params,context).pipe(
      map((r: StrictHttpResponse<Array<ClosedAppView>>) => r.body as Array<ClosedAppView>)
    );
  }

}
