/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { ClosedApp } from '../models/closed-app';
import { ClosedPortalApp } from '../models/closed-portal-app';
import { ClosedPortalAppList } from '../models/closed-portal-app-list';
import { CustomerPortalAppQuery } from '../models/customer-portal-app-query';

@Injectable({
  providedIn: 'root',
})
export class PortalAppService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation portalAppsQueryPut
   */
  static readonly PortalAppsQueryPutPath = '/portal-apps/query';

  /**
   * Query portal app for the customers organization.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `portalAppsQueryPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  portalAppsQueryPut$Response(params: {
    skip?: number;
    limit?: number;
    'organization-id': string;
    body: CustomerPortalAppQuery
  },
  context?: HttpContext

): Observable<StrictHttpResponse<ClosedPortalAppList>> {

    const rb = new RequestBuilder(this.rootUrl, PortalAppService.PortalAppsQueryPutPath, 'put');
    if (params) {
      rb.query('skip', params.skip, {});
      rb.query('limit', params.limit, {});
      rb.header('organization-id', params['organization-id'], {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ClosedPortalAppList>;
      })
    );
  }

  /**
   * Query portal app for the customers organization.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `portalAppsQueryPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  portalAppsQueryPut(params: {
    skip?: number;
    limit?: number;
    'organization-id': string;
    body: CustomerPortalAppQuery
  },
  context?: HttpContext

): Observable<ClosedPortalAppList> {

    return this.portalAppsQueryPut$Response(params,context).pipe(
      map((r: StrictHttpResponse<ClosedPortalAppList>) => r.body as ClosedPortalAppList)
    );
  }

  /**
   * Path part for operation portalAppsPortalAppIdGet
   */
  static readonly PortalAppsPortalAppIdGetPath = '/portal-apps/{portal_app_id}';

  /**
   * Get portal app for the customers organization.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `portalAppsPortalAppIdGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  portalAppsPortalAppIdGet$Response(params: {

    /**
     * Portal App ID
     */
    portal_app_id: string;
    'organization-id': string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<ClosedPortalApp>> {

    const rb = new RequestBuilder(this.rootUrl, PortalAppService.PortalAppsPortalAppIdGetPath, 'get');
    if (params) {
      rb.path('portal_app_id', params.portal_app_id, {});
      rb.header('organization-id', params['organization-id'], {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ClosedPortalApp>;
      })
    );
  }

  /**
   * Get portal app for the customers organization.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `portalAppsPortalAppIdGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  portalAppsPortalAppIdGet(params: {

    /**
     * Portal App ID
     */
    portal_app_id: string;
    'organization-id': string;
  },
  context?: HttpContext

): Observable<ClosedPortalApp> {

    return this.portalAppsPortalAppIdGet$Response(params,context).pipe(
      map((r: StrictHttpResponse<ClosedPortalApp>) => r.body as ClosedPortalApp)
    );
  }

  /**
   * Path part for operation portalAppsPortalAppIdActiveAppGet
   */
  static readonly PortalAppsPortalAppIdActiveAppGetPath = '/portal-apps/{portal_app_id}/active-app';

  /**
   * Get an app by portal app ID including all technical data.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `portalAppsPortalAppIdActiveAppGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  portalAppsPortalAppIdActiveAppGet$Response(params: {

    /**
     * Portal App ID
     */
    portal_app_id: string;
    'organization-id': string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<ClosedApp>> {

    const rb = new RequestBuilder(this.rootUrl, PortalAppService.PortalAppsPortalAppIdActiveAppGetPath, 'get');
    if (params) {
      rb.path('portal_app_id', params.portal_app_id, {});
      rb.header('organization-id', params['organization-id'], {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ClosedApp>;
      })
    );
  }

  /**
   * Get an app by portal app ID including all technical data.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `portalAppsPortalAppIdActiveAppGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  portalAppsPortalAppIdActiveAppGet(params: {

    /**
     * Portal App ID
     */
    portal_app_id: string;
    'organization-id': string;
  },
  context?: HttpContext

): Observable<ClosedApp> {

    return this.portalAppsPortalAppIdActiveAppGet$Response(params,context).pipe(
      map((r: StrictHttpResponse<ClosedApp>) => r.body as ClosedApp)
    );
  }

  /**
   * Path part for operation portalAppsPortalAppIdAppsGet
   */
  static readonly PortalAppsPortalAppIdAppsGetPath = '/portal-apps/{portal_app_id}/apps';

  /**
   * Get all apps connected to a portal app.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `portalAppsPortalAppIdAppsGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  portalAppsPortalAppIdAppsGet$Response(params: {

    /**
     * Portal App ID
     */
    portal_app_id: string;
    'organization-id': string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<Array<ClosedApp>>> {

    const rb = new RequestBuilder(this.rootUrl, PortalAppService.PortalAppsPortalAppIdAppsGetPath, 'get');
    if (params) {
      rb.path('portal_app_id', params.portal_app_id, {});
      rb.header('organization-id', params['organization-id'], {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<ClosedApp>>;
      })
    );
  }

  /**
   * Get all apps connected to a portal app.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `portalAppsPortalAppIdAppsGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  portalAppsPortalAppIdAppsGet(params: {

    /**
     * Portal App ID
     */
    portal_app_id: string;
    'organization-id': string;
  },
  context?: HttpContext

): Observable<Array<ClosedApp>> {

    return this.portalAppsPortalAppIdAppsGet$Response(params,context).pipe(
      map((r: StrictHttpResponse<Array<ClosedApp>>) => r.body as Array<ClosedApp>)
    );
  }

}
