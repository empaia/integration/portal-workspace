/* tslint:disable */
/* eslint-disable */
import { ClosedActiveAppViews } from './closed-active-app-views';
import { ListingStatus } from './listing-status';
export interface ClosedPortalApp {

  /**
   * Currently active app views
   */
  active_app_views?: (ClosedActiveAppViews | null);

  /**
   * UNIX timestamp in seconds - set by server
   */
  created_at: number;

  /**
   * Creator ID
   */
  creator_id: string;

  /**
   * ID of the portal app
   */
  id: string;

  /**
   * ID of the organization providing the portal app
   */
  organization_id: string;
  status: ListingStatus;

  /**
   * UNIX timestamp in seconds - set by server
   */
  updated_at: number;
}
