/* tslint:disable */
/* eslint-disable */
import { ClosedAppView } from './closed-app-view';
export interface ClosedActiveAppViews {

  /**
   * App view for EAD v1-darft3 without App UI (WBC 1.0)
   */
  v1?: (ClosedAppView | null);

  /**
   * App view for EAD v1-darft3 with App UI (WBC 2.0)
   */
  v2?: (ClosedAppView | null);

  /**
   * App view for EAD v3
   */
  v3?: (ClosedAppView | null);
}
