/* tslint:disable */
/* eslint-disable */
import { AppStatus } from './app-status';
import { AppUiConfiguration } from './app-ui-configuration';
export interface ClosedApp {

  /**
   * App UI configuration
   */
  app_ui_configuration?: (AppUiConfiguration | null);

  /**
   * Url where the app UI is located
   */
  app_ui_url?: (string | null);

  /**
   * UNIX timestamp in seconds - set by server
   */
  created_at: number;

  /**
   * Creator ID
   */
  creator_id: string;

  /**
   * EAD content of the app
   */
  ead?: ({
} | null);

  /**
   * If true, app is shipped with a frontend
   */
  has_frontend: boolean;

  /**
   * ID of the app
   */
  id: string;

  /**
   * ID of the portal app
   */
  portal_app_id: string;

  /**
   * Url to the container image in the registry
   */
  registry_image_url?: (string | null);

  /**
   * Status of the app
   */
  status: AppStatus;

  /**
   * UNIX timestamp in seconds - set by server
   */
  updated_at: number;

  /**
   * Version of the app
   */
  version?: (string | null);
}
