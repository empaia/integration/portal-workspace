/* tslint:disable */
/* eslint-disable */
export enum ListingStatus {
  Listed = 'LISTED',
  Delisted = 'DELISTED',
  AdminDelisted = 'ADMIN_DELISTED',
  Draft = 'DRAFT'
}
