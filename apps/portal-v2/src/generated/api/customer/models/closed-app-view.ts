/* tslint:disable */
/* eslint-disable */
import { ApiVersion } from './api-version';
import { AppDetails } from './app-details';
import { AppStatus } from './app-status';
import { ClosedApp } from './closed-app';
import { MediaList } from './media-list';
import { TagList } from './tag-list';
export interface ClosedAppView {

  /**
   * Supported API version by this app view
   */
  api_version: ApiVersion;
  app?: (ClosedApp | null);

  /**
   * UNIX timestamp in seconds - set by server
   */
  created_at: number;

  /**
   * ID of the app view creator
   */
  creator_id?: string;
  details?: (AppDetails | null);

  /**
   * ID of the app view
   */
  id: string;
  media?: (MediaList | null);

  /**
   * If true, portal app can be listed although technical app is not yet available
   */
  non_functional?: (boolean | null);

  /**
   * Organization ID
   */
  organization_id: string;

  /**
   * ID of the portal app
   */
  portal_app_id: string;

  /**
   * If true, app is intended to be used for reasearch only
   */
  research_only?: boolean;

  /**
   * Review commet, i.e. in case of rejection
   */
  review_comment?: (string | null);

  /**
   * UNIX timestamp in seconds - set by server
   */
  reviewed_at?: (number | null);

  /**
   * ID of the reviewer
   */
  reviewer_id?: (string | null);
  status: AppStatus;
  tags?: (TagList | null);

  /**
   * Version of the currently active app
   */
  version?: (string | null);
}
