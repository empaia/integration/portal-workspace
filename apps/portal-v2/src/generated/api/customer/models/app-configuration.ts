/* tslint:disable */
/* eslint-disable */
export interface AppConfiguration {

  /**
   * App ID
   */
  app_id: string;

  /**
   * Customer app configuration as dictionary of key-value-pairs
   */
  customer?: {
[key: string]: (string | number | number | boolean);
};

  /**
   * Global app configuration as dictionary of key-value-pairs
   */
  global?: {
[key: string]: (string | number | number | boolean);
};
}
