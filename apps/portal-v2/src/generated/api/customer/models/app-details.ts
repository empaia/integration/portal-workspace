/* tslint:disable */
/* eslint-disable */
import { TextTranslation } from './text-translation';
export interface AppDetails {

  /**
   * Description
   */
  description: Array<TextTranslation>;

  /**
   * Url to app in the marketplace
   */
  marketplace_url: string;

  /**
   * Qualified app name displayed in the portal
   */
  name: string;
}
