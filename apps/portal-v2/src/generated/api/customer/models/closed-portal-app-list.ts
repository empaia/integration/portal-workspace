/* tslint:disable */
/* eslint-disable */
import { ClosedPortalApp } from './closed-portal-app';
export interface ClosedPortalAppList {

  /**
   * Count of all available apps
   */
  item_count: number;
  items: Array<ClosedPortalApp>;
}
