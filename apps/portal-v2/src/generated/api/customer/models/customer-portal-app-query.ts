/* tslint:disable */
/* eslint-disable */
export interface CustomerPortalAppQuery {

  /**
   * List of app IDs
   */
  apps?: (Array<string> | null);

  /**
   * Filter option for stain types
   */
  stains?: (Array<string> | null);

  /**
   * Filter option for tissue types
   */
  tissues?: (Array<string> | null);
}
