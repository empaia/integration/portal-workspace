/* tslint:disable */
/* eslint-disable */
export enum AppStatus {
  Approved = 'APPROVED',
  Rejected = 'REJECTED',
  Pending = 'PENDING',
  Draft = 'DRAFT'
}
