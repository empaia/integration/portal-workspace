/* tslint:disable */
/* eslint-disable */
import { ApiVersion } from './api-version';
export interface CustomerAppViewQuery {

  /**
   * List of supported API versions
   */
  api_versions?: (Array<ApiVersion> | null);

  /**
   * List of app IDs
   */
  apps: Array<string>;

  /**
   * Filter option for stain types
   */
  stains?: (Array<string> | null);

  /**
   * Filter option for tissue types
   */
  tissues?: (Array<string> | null);
}
