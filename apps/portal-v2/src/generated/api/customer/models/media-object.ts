/* tslint:disable */
/* eslint-disable */
import { ResizedMediaUrlsObject } from './resized-media-urls-object';
import { TextTranslation } from './text-translation';
export interface MediaObject {

  /**
   * Alternative text for media
   */
  alt_text?: (Array<TextTranslation> | null);

  /**
   * Media caption
   */
  caption?: (Array<TextTranslation> | null);

  /**
   * Content type of the media object
   */
  content_type: string;

  /**
   * Media ID
   */
  id: string;

  /**
   * Number of the step, required when media purpose is 'PREVIEW', 'BANNER' or 'WORKFLOW
   */
  index: number;

  /**
   * Internam Minio path
   */
  internal_path: string;

  /**
   * Presigned url to the media object
   */
  presigned_media_url?: (string | null);

  /**
   * Resized versions of an image media object
   */
  resized_presigned_media_urls?: (ResizedMediaUrlsObject | null);
}
