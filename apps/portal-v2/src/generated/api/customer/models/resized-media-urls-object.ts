/* tslint:disable */
/* eslint-disable */
export interface ResizedMediaUrlsObject {

  /**
   * Presigned url to the media object with max width 1200px
   */
  w1200?: (string | null);

  /**
   * Presigned url to the media object with max width 400px
   */
  w400?: (string | null);

  /**
   * Presigned url to the media object with max width 60px
   */
  w60?: (string | null);

  /**
   * Presigned url to the media object with max width 800x
   */
  w800?: (string | null);
}
