/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { Clearance } from '../models/clearance';
import { ClearanceItem } from '../models/clearance-item';
import { PostClearance } from '../models/post-clearance';
import { PostClearanceItem } from '../models/post-clearance-item';
import { PostClearanceItemActivation } from '../models/post-clearance-item-activation';

@Injectable({
  providedIn: 'root',
})
export class ClearancesService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation clearancesGet
   */
  static readonly ClearancesGetPath = '/clearances';

  /**
   * Get all clearance entries.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `clearancesGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  clearancesGet$Response(params?: {
  },
  context?: HttpContext

): Observable<StrictHttpResponse<Array<Clearance>>> {

    const rb = new RequestBuilder(this.rootUrl, ClearancesService.ClearancesGetPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<Clearance>>;
      })
    );
  }

  /**
   * Get all clearance entries.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `clearancesGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  clearancesGet(params?: {
  },
  context?: HttpContext

): Observable<Array<Clearance>> {

    return this.clearancesGet$Response(params,context).pipe(
      map((r: StrictHttpResponse<Array<Clearance>>) => r.body as Array<Clearance>)
    );
  }

  /**
   * Path part for operation clearancesPost
   */
  static readonly ClearancesPostPath = '/clearances';

  /**
   * Post a new empty clearance entry.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `clearancesPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  clearancesPost$Response(params: {
    body: PostClearance
  },
  context?: HttpContext

): Observable<StrictHttpResponse<Clearance>> {

    const rb = new RequestBuilder(this.rootUrl, ClearancesService.ClearancesPostPath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Clearance>;
      })
    );
  }

  /**
   * Post a new empty clearance entry.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `clearancesPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  clearancesPost(params: {
    body: PostClearance
  },
  context?: HttpContext

): Observable<Clearance> {

    return this.clearancesPost$Response(params,context).pipe(
      map((r: StrictHttpResponse<Clearance>) => r.body as Clearance)
    );
  }

  /**
   * Path part for operation clearancesClearanceIdGet
   */
  static readonly ClearancesClearanceIdGetPath = '/clearances/{clearance_id}';

  /**
   * Get clearance entry by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `clearancesClearanceIdGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  clearancesClearanceIdGet$Response(params: {

    /**
     * Clearance ID
     */
    clearance_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<Clearance>> {

    const rb = new RequestBuilder(this.rootUrl, ClearancesService.ClearancesClearanceIdGetPath, 'get');
    if (params) {
      rb.path('clearance_id', params.clearance_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Clearance>;
      })
    );
  }

  /**
   * Get clearance entry by ID.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `clearancesClearanceIdGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  clearancesClearanceIdGet(params: {

    /**
     * Clearance ID
     */
    clearance_id: string;
  },
  context?: HttpContext

): Observable<Clearance> {

    return this.clearancesClearanceIdGet$Response(params,context).pipe(
      map((r: StrictHttpResponse<Clearance>) => r.body as Clearance)
    );
  }

  /**
   * Path part for operation clearancesClearanceIdHidePut
   */
  static readonly ClearancesClearanceIdHidePutPath = '/clearances/{clearance_id}/hide';

  /**
   * Hide clearance entry.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `clearancesClearanceIdHidePut()` instead.
   *
   * This method doesn't expect any request body.
   */
  clearancesClearanceIdHidePut$Response(params: {

    /**
     * Clearance ID
     */
    clearance_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<Clearance>> {

    const rb = new RequestBuilder(this.rootUrl, ClearancesService.ClearancesClearanceIdHidePutPath, 'put');
    if (params) {
      rb.path('clearance_id', params.clearance_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Clearance>;
      })
    );
  }

  /**
   * Hide clearance entry.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `clearancesClearanceIdHidePut$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  clearancesClearanceIdHidePut(params: {

    /**
     * Clearance ID
     */
    clearance_id: string;
  },
  context?: HttpContext

): Observable<Clearance> {

    return this.clearancesClearanceIdHidePut$Response(params,context).pipe(
      map((r: StrictHttpResponse<Clearance>) => r.body as Clearance)
    );
  }

  /**
   * Path part for operation clearancesClearanceIdUnhidePut
   */
  static readonly ClearancesClearanceIdUnhidePutPath = '/clearances/{clearance_id}/unhide';

  /**
   * Unhide clearance entry.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `clearancesClearanceIdUnhidePut()` instead.
   *
   * This method doesn't expect any request body.
   */
  clearancesClearanceIdUnhidePut$Response(params: {

    /**
     * Clearance ID
     */
    clearance_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<Clearance>> {

    const rb = new RequestBuilder(this.rootUrl, ClearancesService.ClearancesClearanceIdUnhidePutPath, 'put');
    if (params) {
      rb.path('clearance_id', params.clearance_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Clearance>;
      })
    );
  }

  /**
   * Unhide clearance entry.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `clearancesClearanceIdUnhidePut$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  clearancesClearanceIdUnhidePut(params: {

    /**
     * Clearance ID
     */
    clearance_id: string;
  },
  context?: HttpContext

): Observable<Clearance> {

    return this.clearancesClearanceIdUnhidePut$Response(params,context).pipe(
      map((r: StrictHttpResponse<Clearance>) => r.body as Clearance)
    );
  }

  /**
   * Path part for operation clearancesClearanceIdItemPost
   */
  static readonly ClearancesClearanceIdItemPostPath = '/clearances/{clearance_id}/item';

  /**
   * Post clearance item by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `clearancesClearanceIdItemPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  clearancesClearanceIdItemPost$Response(params: {

    /**
     * Clearance ID
     */
    clearance_id: string;
    body: PostClearanceItem
  },
  context?: HttpContext

): Observable<StrictHttpResponse<Clearance>> {

    const rb = new RequestBuilder(this.rootUrl, ClearancesService.ClearancesClearanceIdItemPostPath, 'post');
    if (params) {
      rb.path('clearance_id', params.clearance_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Clearance>;
      })
    );
  }

  /**
   * Post clearance item by ID.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `clearancesClearanceIdItemPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  clearancesClearanceIdItemPost(params: {

    /**
     * Clearance ID
     */
    clearance_id: string;
    body: PostClearanceItem
  },
  context?: HttpContext

): Observable<Clearance> {

    return this.clearancesClearanceIdItemPost$Response(params,context).pipe(
      map((r: StrictHttpResponse<Clearance>) => r.body as Clearance)
    );
  }

  /**
   * Path part for operation clearancesClearanceIdItemItemIdGet
   */
  static readonly ClearancesClearanceIdItemItemIdGetPath = '/clearances/{clearance_id}/item/{item_id}';

  /**
   * Get clearance item by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `clearancesClearanceIdItemItemIdGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  clearancesClearanceIdItemItemIdGet$Response(params: {

    /**
     * Clearance ID
     */
    clearance_id: string;

    /**
     * Clearance item ID
     */
    item_id: string;
  },
  context?: HttpContext

): Observable<StrictHttpResponse<ClearanceItem>> {

    const rb = new RequestBuilder(this.rootUrl, ClearancesService.ClearancesClearanceIdItemItemIdGetPath, 'get');
    if (params) {
      rb.path('clearance_id', params.clearance_id, {});
      rb.path('item_id', params.item_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ClearanceItem>;
      })
    );
  }

  /**
   * Get clearance item by ID.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `clearancesClearanceIdItemItemIdGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  clearancesClearanceIdItemItemIdGet(params: {

    /**
     * Clearance ID
     */
    clearance_id: string;

    /**
     * Clearance item ID
     */
    item_id: string;
  },
  context?: HttpContext

): Observable<ClearanceItem> {

    return this.clearancesClearanceIdItemItemIdGet$Response(params,context).pipe(
      map((r: StrictHttpResponse<ClearanceItem>) => r.body as ClearanceItem)
    );
  }

  /**
   * Path part for operation clearancesClearanceIdActivatePut
   */
  static readonly ClearancesClearanceIdActivatePutPath = '/clearances/{clearance_id}/activate';

  /**
   * Post clearance item by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `clearancesClearanceIdActivatePut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  clearancesClearanceIdActivatePut$Response(params: {

    /**
     * Clearance ID
     */
    clearance_id: string;
    body: PostClearanceItemActivation
  },
  context?: HttpContext

): Observable<StrictHttpResponse<Clearance>> {

    const rb = new RequestBuilder(this.rootUrl, ClearancesService.ClearancesClearanceIdActivatePutPath, 'put');
    if (params) {
      rb.path('clearance_id', params.clearance_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Clearance>;
      })
    );
  }

  /**
   * Post clearance item by ID.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `clearancesClearanceIdActivatePut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  clearancesClearanceIdActivatePut(params: {

    /**
     * Clearance ID
     */
    clearance_id: string;
    body: PostClearanceItemActivation
  },
  context?: HttpContext

): Observable<Clearance> {

    return this.clearancesClearanceIdActivatePut$Response(params,context).pipe(
      map((r: StrictHttpResponse<Clearance>) => r.body as Clearance)
    );
  }

}
