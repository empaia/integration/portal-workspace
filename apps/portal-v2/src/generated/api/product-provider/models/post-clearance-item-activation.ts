/* tslint:disable */
/* eslint-disable */
export interface PostClearanceItemActivation {

  /**
   * Clearance item ID to activate
   */
  active_item_id: string;
}
