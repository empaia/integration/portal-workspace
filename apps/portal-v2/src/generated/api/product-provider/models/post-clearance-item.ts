/* tslint:disable */
/* eslint-disable */
import { DatePrecision } from './date-precision';
import { InfoSourceType } from './info-source-type';
import { PostAppTag } from './post-app-tag';
import { PostInfoSourceContent } from './post-info-source-content';
export interface PostClearanceItem {

  /**
   * Description
   */
  description: {
[key: string]: string;
};

  /**
   * List of info source contents
   */
  info_source_content?: (Array<PostInfoSourceContent> | null);

  /**
   * Source type where the product info was obtained
   */
  info_source_type: InfoSourceType;

  /**
   * Brand and product name
   */
  product_name: string;

  /**
   * UNIX timestamp in seconds - set by server
   */
  product_release_at: number;

  /**
   * Date precision type
   */
  product_release_at_precision: DatePrecision;

  /**
   * Additional information on product tags that can not be expressed through existing tags
   */
  tag_remark?: (string | null);
  tags?: (Array<PostAppTag> | null);
}
