/* tslint:disable */
/* eslint-disable */
export interface PostAppTag {

  /**
   * Tag group. See definitions for valid tag groups.
   */
  tag_group: string;

  /**
   * Tag name. See definitions for valid tag names.
   */
  tag_name: string;
}
