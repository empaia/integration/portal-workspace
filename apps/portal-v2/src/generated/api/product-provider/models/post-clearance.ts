/* tslint:disable */
/* eslint-disable */
import { PostClearanceItem } from './post-clearance-item';
export interface PostClearance {

  /**
   * When given, an initial active clearance item is posted
   */
  clearance_item?: (PostClearanceItem | null);

  /**
   * Flag that indicates if clearance is hidden
   */
  is_hidden: boolean;
}
