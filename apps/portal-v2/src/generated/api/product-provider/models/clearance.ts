/* tslint:disable */
/* eslint-disable */
import { ClearanceItem } from './clearance-item';
import { EditorType } from './editor-type';
export interface Clearance {

  /**
   * Active clearance item ID
   */
  active_clearance_item?: (ClearanceItem | null);

  /**
   * UNIX timestamp in seconds - set by server
   */
  created_at: number;

  /**
   * Creator ID
   */
  creator_id: string;

  /**
   * Creator type
   */
  creator_type: EditorType;
  history?: (Array<ClearanceItem> | null);

  /**
   * Clearance ID
   */
  id: string;

  /**
   * Flag that indicates if clearance is hidden
   */
  is_hidden: boolean;

  /**
   * Organization ID
   */
  organization_id: string;

  /**
   * UNIX timestamp in seconds - set by server
   */
  updated_at: number;

  /**
   * Updater ID
   */
  updater_id: string;

  /**
   * Updater type
   */
  updater_type: EditorType;
}
