/* tslint:disable */
/* eslint-disable */
import { AppTag } from './app-tag';
export interface ExtendedTagList {

  /**
   * List of analysis
   */
  analysis?: Array<AppTag>;

  /**
   * List of market clearances / certifications
   */
  clearances?: Array<AppTag>;

  /**
   * List of image types used for analysis
   */
  image_types?: Array<AppTag>;

  /**
   * List of indications
   */
  indications?: Array<AppTag>;

  /**
   * List of histopathological preparation procedures
   */
  procedures?: Array<AppTag>;

  /**
   * List of supported WSI scanners
   */
  scanners?: Array<AppTag>;

  /**
   * List of stains
   */
  stains?: Array<AppTag>;

  /**
   * List of tissues
   */
  tissues?: Array<AppTag>;
}
