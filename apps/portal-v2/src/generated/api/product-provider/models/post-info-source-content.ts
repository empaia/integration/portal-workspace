/* tslint:disable */
/* eslint-disable */
export interface PostInfoSourceContent {

  /**
   * Info source content label
   */
  label?: ({
[key: string]: string;
} | null);

  /**
   * Url of the info source content
   */
  reference_url: string;
}
