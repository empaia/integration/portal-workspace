import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutes } from '@router/router.constants';
import { AboutComponent } from './components/about/about.component';
import { DataPrivacyComponent } from './components/data-privacy/data-privacy.component';
import { LegalNoticeComponent } from './components/legal-notice/legal-notice.component';
import { TermsOfUseComponent } from './components/terms-of-use/terms-of-use.component';

const routes: Routes = [
  {
    path: AppRoutes.INFORMATION_ABOUT,
    component: AboutComponent,
  },
  {
    path: AppRoutes.INFORMATION_DATA_PRIVACY,
    component: DataPrivacyComponent,
  },
  {
    path: AppRoutes.INFORMATION_LEGAL_NOTICE,
    component: LegalNoticeComponent,
  },
  {
    path: AppRoutes.INFORMATION_TERMS_OF_USE,
    component: TermsOfUseComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FooterRoutingModule { }
