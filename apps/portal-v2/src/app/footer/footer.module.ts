import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@shared/shared.module';
import { CommonUiModule } from '@empaia/common-ui';

import { FooterRoutingModule } from './footer-routing.module';
import { AboutComponent } from './components/about/about.component';
import { DataPrivacyComponent } from './components/data-privacy/data-privacy.component';
import { TermsOfUseComponent } from './components/terms-of-use/terms-of-use.component';
import { LegalNoticeComponent } from './components/legal-notice/legal-notice.component';


@NgModule({
  declarations: [
    AboutComponent,
    DataPrivacyComponent,
    TermsOfUseComponent,
    LegalNoticeComponent
  ],
  imports: [
    CommonModule,
    CommonUiModule,
    FooterRoutingModule,
    SharedModule,
  ]
})
export class FooterModule { }
