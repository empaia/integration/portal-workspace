import { AppRoutes } from '@router/router.constants';

export enum FooterLinkType {
  ROUTE = 'route',
  URL = 'url',
}

export interface FooterLink {
  label: string;
  link: string;
  type: FooterLinkType;
}

export interface FooterLinkLibrary {
  marketplace?: FooterLink[];
  services?: FooterLink[];
  empaia: FooterLink[];
}

export const FOOTER_LINKS: FooterLinkLibrary = {
  empaia: [
    {
      label: 'footer.about',
      link: AppRoutes.INFORMATION_ABOUT,
      type: FooterLinkType.ROUTE,
    },
    {
      label: 'footer.developer',
      link: 'https://developer.empaia.org/',
      type: FooterLinkType.URL,
    },
    {
      label: 'footer.data_privacy',
      link: AppRoutes.INFORMATION_DATA_PRIVACY,
      type: FooterLinkType.ROUTE,
    },
    {
      label: 'footer.terms_of_use',
      link: AppRoutes.INFORMATION_TERMS_OF_USE,
      type: FooterLinkType.ROUTE,
    },
    {
      label: 'footer.legal_notice',
      link: AppRoutes.INFORMATION_LEGAL_NOTICE,
      type: FooterLinkType.ROUTE,
    }
  ]
};
