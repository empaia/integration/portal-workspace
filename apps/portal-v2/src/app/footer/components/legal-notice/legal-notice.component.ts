import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { LanguageService } from '@transloco/language.service';

@Component({
  selector: 'app-legal-notice',
  templateUrl: './legal-notice.component.html',
  styleUrls: ['./legal-notice.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LegalNoticeComponent {
  protected lang = inject(LanguageService).getLanguage$();
}
