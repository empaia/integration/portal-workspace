import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { LanguageService } from '@transloco/language.service';

@Component({
  selector: 'app-data-privacy',
  templateUrl: './data-privacy.component.html',
  styleUrls: ['./data-privacy.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DataPrivacyComponent {
  protected lang = inject(LanguageService).getLanguage$();
}
