import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { LanguageService } from '@transloco/language.service';

@Component({
  selector: 'app-terms-of-use',
  templateUrl: './terms-of-use.component.html',
  styleUrls: ['./terms-of-use.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TermsOfUseComponent {
  protected lang = inject(LanguageService).getLanguage$();
}
