import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, HostListener, inject, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ThemeService } from '@core/services/theme/theme.service';
import {  themes } from './app.models';
import { LanguageService } from '@transloco/language.service';
import { environment } from '@env/environment';
import { SidenavService } from '@core/services/sidenav/sidenav.service';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AuthSelectors } from '@auth/store/auth';
import { FOOTER_LINKS } from '@footer/footer.models';

@Component({
  selector: 'app-portal-v2',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'portal-v2';
  public currentTheme = themes[0];

  protected sidenavService = inject(SidenavService);

  protected isAuthenticated$: Observable<boolean>;

  public footerLinks = FOOTER_LINKS;

  @HostListener('document:keyup.control.alt.s')
  switchTheme() {
    this.theme.nextTheme();
  }

  @HostListener('document:keyup.control.alt.l')
  switchLang() {
    this.lang.switchLang();
  }

  @HostListener('document:keyup.control.alt.n')
  toggleSidenav() {
    this.sidenavService.toggle();
  }

  constructor(
    public dialog: MatDialog,
    private overlay: OverlayContainer,
    private theme: ThemeService,
    private lang: LanguageService,

    private store: Store
  ) {
    theme.setTheme(this.currentTheme);

    console.log('WINDOW ENVIRONMENT:');
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    console.log((window as any).env, null, 2);

    console.log('ENVIRONMENT:');
    console.log(environment, null, 2);

    // initialize sidenav only if user is logged in
    this.isAuthenticated$ = this.store.select(AuthSelectors.selectLoggedIn);
  }

  ngOnInit(): void {
    this.theme.getPrevTheme$().subscribe((name) => {
      if (name !== '') {
        this.overlay.getContainerElement().classList.remove(name);
      }
    });
    this.theme.getTheme$().subscribe((name) => {
      this.currentTheme = name;
      if (name !== '') {
        this.overlay.getContainerElement().classList.add(name);
      }
    });

  }
}
