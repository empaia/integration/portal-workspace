import { inject, Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Store } from '@ngrx/store';
import { NotificationActions } from '@user/store/notification';
import { EventSourcePolyfill } from 'event-source-polyfill';
import { evesApi } from '../app.models';
import { SSEventTypes, ActionCallback, SseEventProps, EventAction } from './event.models';
import { EventActions } from './store';


@Injectable({
  providedIn: 'root'
})
export class EventService {
  private eventSource!: EventSource;
  private store = inject(Store);

  private dict = new Map<SSEventTypes, ActionCallback<string>>([
    [
      SSEventTypes.NOTIFICATION,
      (sseRefreshNotification: SseEventProps) => this.sseRefreshNotification(sseRefreshNotification),
    ],
    [
      SSEventTypes.ORGANIZATION,
      (eventReceived: SseEventProps) => this.updateUserRole(eventReceived),
    ],
    [
      SSEventTypes.UNAPPROVED_ORGANIZATION,
      (eventReceived: SseEventProps) => this.updateUnapprovedOrganizations(eventReceived),
    ],
    [
      SSEventTypes.ROLE_REQUEST,
      (eventReceived: SseEventProps) => this.updateRoleRequests(eventReceived),
    ],
    [
      SSEventTypes.MEMBERSHIP_REQUEST,
      (eventReceived: SseEventProps) => this.updateMembershipRequests(eventReceived),
    ],
    [
      SSEventTypes.CATEGORY_REQUEST,
      (eventReceived: SseEventProps) => this.updateCategoryChange(eventReceived),
    ],
  ]);


  public createConnection(uid: string, token: string) {
    console.log('create SSE Connection for uid', uid);

    const url = new URL(environment.evesApiUrl);
    const apiUrl = `//${url.host}${evesApi.version}${evesApi.events}`;
    const headers = {
      'user-id': uid,
      'Authorization': 'Bearer ' + token
    };

    this.eventSource = new EventSourcePolyfill(
      apiUrl,
      { headers }
    );

    this.addErrorHandler();

    // this.DEBUG_EventHandler();
    for (const item of this.dict) {
      this.addEventHandler(item);
    }
  }

  private sseRefreshNotification(props: SseEventProps) {
    return NotificationActions.sSERefreshNotification({
      eventType: props.eventType,
      eventMsg: props.eventAction
    });
  }

  private updateMembershipRequests(props: SseEventProps) {
    return EventActions.sseUpdateMembershipRequests({
      eventType: props.eventType,
      eventMsg: props.eventAction
    });
  }

  private updateUnapprovedOrganizations(props: SseEventProps) {
    return EventActions.sseUpdateUnapprovedOrganizations({
      eventType: props.eventType,
      eventMsg: props.eventAction
    });
  }

  private updateRoleRequests(props: SseEventProps) {
    return EventActions.sseUpdateRoleRequests({
      eventType: props.eventType,
      eventMsg: props.eventAction
    });
  }

  private updateUserRole(props: SseEventProps) {
    return EventActions.sseUpdateUserRole({
      eventType: props.eventType,
      eventMsg: props.eventAction
    });
  }

  private updateCategoryChange(props: SseEventProps) {
    return EventActions.sseUpdateCategoryChange({
      eventType: props.eventType,
      eventMsg: props.eventAction
    });
  }

  // for debugging events
  private eventReceived(props: SseEventProps) {
    return EventActions.eventReceived({
      eventType: props.eventType,
      eventMsg: props.eventAction
    });
  }

  private addEventHandler(item: [SSEventTypes, ActionCallback<string>]) {
    const [evtType, action] = item;
    // console.log('addEventHandler', evtType, action);
    const eventType = this.validateEventType(evtType);
    this.eventSource.addEventListener(evtType, (message: MessageEvent) => {
      console.log('event', message);
      try {
        const myData = <EventAction>JSON.parse(message.data);
        this.store.dispatch(action({
          eventType, eventAction: myData
        }));
      } catch (err) {
        if (message.data) {
          // currently the data field is empty on all events - see
          this.store.dispatch(action({
            eventType, eventAction: message.data
          }));
          // this.store.dispatch(EventActions.eventReceivedFailure({
          //   eventType, eventMsg: message.data
          // }));
        }
      }
    });
  }

  private addErrorHandler() {
    this.eventSource.addEventListener('error', (message: MessageEvent) => {
      console.error('SSE Error', message);
    });
  }

  private validateEventType(eventString: string): SSEventTypes {
    switch (eventString) {
      case SSEventTypes.NOTIFICATION:
      case SSEventTypes.ORGANIZATION:
      case SSEventTypes.UNAPPROVED_ORGANIZATION:
      case SSEventTypes.ROLE_REQUEST:
      case SSEventTypes.MEMBERSHIP_REQUEST:
      case SSEventTypes.CATEGORY_REQUEST:
        return eventString; // no need to do 'as', TS knows that eventString is valid SSEEvents enum value
      case SSEventTypes.PORTAL_APP:
      case SSEventTypes.APP:
      case SSEventTypes.APP_VIEW:
      case SSEventTypes.APP_REVIEW_REQUEST:
      case SSEventTypes.APP_VIEW_REVIEW_REQUEST:
      case SSEventTypes.CONTAINER_IMAGE:
      case SSEventTypes.APP_UI_BUNDLE:
        throw new Error(`Unhandled server sent event ${eventString}.`);
      default:
        throw new Error(`Unknown server sent event ${eventString}.`);
    }
  }
}
