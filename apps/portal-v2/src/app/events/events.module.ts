import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import * as fromEvents from './store/event.reducer';
import { EventEffects } from './store';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(
      fromEvents.EVENT_FEATURE_KEY,
      fromEvents.reducer
    ),
    EffectsModule.forFeature([
      EventEffects,
    ]),
  ]
})
export class EventsModule { }
