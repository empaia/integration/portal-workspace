import { TypedAction } from '@ngrx/store/src/models';

export const enum SSEventTypes {
  NOTIFICATION = 'NOTIFICATION',
  // auth
  ORGANIZATION = 'ORGANIZATION',
  UNAPPROVED_ORGANIZATION = 'UNAPPROVED_ORGANIZATION',
  ROLE_REQUEST = 'ROLE_REQUEST',
  MEMBERSHIP_REQUEST = 'MEMBERSHIP_REQUEST',
  CATEGORY_REQUEST = 'CATEGORY_REQUEST',

  // mps

  PORTAL_APP = 'PORTAL_APP',
  APP = 'APP',
  APP_VIEW = 'APP_VIEW',
  APP_REVIEW_REQUEST = 'APP_REVIEW_REQUEST',
  APP_VIEW_REVIEW_REQUEST = 'APP_VIEW_REVIEW_REQUEST',
  CONTAINER_IMAGE = 'CONTAINER_IMAGE',
  APP_UI_BUNDLE = 'APP_UI_BUNDLE',
}

export const enum SSEventActions {
  CREATED = 'CREATED',
  UPDATED = 'UPDATED',
  REJECTED = 'REJECTED',
  APPROVED = 'APPROVED',
}

export type SseEventProps = { eventType: SSEventTypes, eventAction: EventAction };
export type EventAction = { action: SSEventActions};
export type ActionCallback<T extends string> = (p: SseEventProps) => TypedAction<T>;