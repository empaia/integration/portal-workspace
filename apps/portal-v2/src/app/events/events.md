// Org Created
Notification.USER_CREATES_ORGANIZATION,
PortalEvent.UNAPPROVED_ORGANIZATION,
EventAction.CREATED,

// org approved
Notification.ORGANIZATION_IS_APPROVED,
PortalEvent.UNAPPROVED_ORGANIZATION,
EventAction.APPROVED,

// org rejected
Notification.ORGANIZATION_IS_REJECTED,
PortalEvent.UNAPPROVED_ORGANIZATION,
EventAction.REJECTED,

// membership requested
Notification.USER_REQUESTS_ORGANIZATION_MEMBERSHIP,
PortalEvent.MEMBERSHIP_REQUEST,
EventAction.CREATED,

// membership request approved
Notification.ORGANIZATION_MEMBERSHIP_IS_APPROVED,
PortalEvent.MEMBERSHIP_REQUEST,
EventAction.APPROVED,

// membership request rejected
Notification.ORGANIZATION_MEMBERSHIP_IS_REJECTED,
PortalEvent.MEMBERSHIP_REQUEST,
EventAction.REJECTED,

// membership request revoked
// MISSING

// Role request created
Notification.ORGANIZATION_MEMBER_REQUESTS_ROLE,
PortalEvent.ROLE_REQUEST,
EventAction.CREATED,

// role request approved
Notification.ORGANIZATION_MEMBER_ROLE_APPROVED,
PortalEvent.ROLE_REQUEST,
EventAction.APPROVED,

// role request rejected
Notification.ORGANIZATION_MEMBER_ROLE_REJECTED,
PortalEvent.ROLE_REQUEST,
EventAction.REJECTED,

// role request revoked
// MISSING

// manager updates user role - ???
Notification.ORGANIZATION_MANAGER_UPDATES_USER_ROLE,
PortalEvent.ORGANIZATION,
EventAction.UPDATED,

// org category change created
Notification.MANAGER_REQUESTS_ORGANIZATION_CATEGORY_CHANGE,
PortalEvent.CATEGORY_REQUEST,
EventAction.CREATED,

// org category change approved
Notification.ORGANIZATION_CATEGORY_CHANGE_APPROVED,
PortalEvent.CATEGORY_REQUEST,
 EventAction.APPROVED,

// org category change rejected
Notification.ORGANIZATION_CATEGORY_CHANGE_REJECTED,
PortalEvent.CATEGORY_REQUEST,
EventAction.REJECTED,