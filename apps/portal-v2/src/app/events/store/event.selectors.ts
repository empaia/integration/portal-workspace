import { createFeatureSelector } from '@ngrx/store';
import * as fromEvent from './event.reducer';

export const selectEventState = createFeatureSelector<fromEvent.State>(
  fromEvent.EVENT_FEATURE_KEY
);
