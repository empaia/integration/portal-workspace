import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';

import { map } from 'rxjs/operators';
import * as EventActions from './event.actions';
import { AuthActions, AuthSelectors } from '@auth/store/auth';
import { Store } from '@ngrx/store';
import { AuthService } from '@auth/services/auth.service';
import { EventService } from '../event.service';
import { filterNullish } from '@shared/utility/rxjs-utility';

@Injectable()
export class EventEffects {

  initEvents$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.initialUserModuleLoadSuccessful),
      concatLatestFrom(() => this.store.select(AuthSelectors.selectUserId)),
      map(([_a, uid]) => uid),
      filterNullish(),
      map((uid) => this.eventService.createConnection(uid, this.authService.accessToken))
    );
  }, { dispatch: false });

  listenToEvents$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        EventActions.eventReceived,
        EventActions.sseUpdateCategoryChange,
        EventActions.sseUpdateMembershipRequests,
        EventActions.sseUpdateRoleRequests,
        EventActions.sseUpdateUnapprovedOrganizations,
        EventActions.sseUpdateUserRole,
      ),
      map((item: unknown) => console.log('sse', JSON.stringify(item, null, ' '))
      ));
  }, { dispatch: false });

  // listenToEvents$ = createEffect(() => {
  //   return this.actions$.pipe(
  //     // ofType(ROOT_EFFECTS_INIT),
  //     ofType(EventActions.initEvents),
  //     concatMap((a) => fromEvent(a.source, 'message')),
  //     map((e: any) => JSON.parse(e.data)),
  //     map((item: unknown) => console.log('sse', item))
  //   );
  // }, { dispatch: false });


  constructor(
    private actions$: Actions,
    private store: Store,
    private eventService: EventService,
    private authService: AuthService,
  ) { }
}
