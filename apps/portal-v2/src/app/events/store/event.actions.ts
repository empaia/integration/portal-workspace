/* eslint-disable @typescript-eslint/no-explicit-any */
import { createAction, props } from '@ngrx/store';
import { EventSourcePolyfill } from 'event-source-polyfill';
import { SSEventTypes, EventAction } from '../event.models';



export const loadEvents = createAction(
  '[Event] Load Events'
);

export const initEvents = createAction(
  '[Event] init Events',
  props<{ source: EventSourcePolyfill }>()
);

export const eventReceived = createAction(
  '[Event] Events received',
  props<{ eventType: SSEventTypes, eventMsg: EventAction }>()
);

export const eventReceivedFailure = createAction(
  '[Event] Event received failure',
  props<{ eventType: SSEventTypes, eventMsg: any }>()
);

// actions caught by other effects
export const sseUpdateMembershipRequests = createAction(
  '[Event] Update Membership Requests',
  props<{ eventType: SSEventTypes, eventMsg: EventAction }>()
);

// created/approve/reject
export const sseUpdateUnapprovedOrganizations = createAction(
  '[Event] Update Unapproved Organization',
  props<{ eventType: SSEventTypes, eventMsg: EventAction }>()
);

export const sseUpdateRoleRequests = createAction(
  '[Event] Update Role Requests',
  props<{ eventType: SSEventTypes, eventMsg: EventAction }>()
);

export const sseUpdateCategoryChange = createAction(
  '[Event] Update Category Change',
  props<{ eventType: SSEventTypes, eventMsg: EventAction }>()
);


export const sseUpdateUserRole = createAction(
  '[Event] Update User Role',
  props<{ eventType: SSEventTypes, eventMsg: EventAction }>()
);


