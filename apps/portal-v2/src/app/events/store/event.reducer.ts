import { createReducer, on } from '@ngrx/store';
import { EventSourcePolyfill } from 'event-source-polyfill';
import { SSEventTypes, SSEventActions } from '../event.models';
import * as EventActions from './event.actions';

export const EVENT_FEATURE_KEY = 'event';

interface SSE {
  type: SSEventTypes,
  action: SSEventActions
}

export interface State {
  events: SSE[];
  source: EventSourcePolyfill | undefined;
  initialized: boolean
}

export const initialState: State = {
  events: [],
  source: undefined,
  initialized: false,
};

export const reducer = createReducer(
  initialState,

  on(EventActions.loadEvents, (state): State => ({ ...state })),
  on(EventActions.initEvents,
    (state, { source }): State => ({
      ...state,
      source: source,
      initialized: true
    })
  ),
  on(EventActions.eventReceived,
    (state, { eventType, eventMsg }): State => ({
      ...state,
      events: [...state.events, {
        type: eventType,
        action: eventMsg.action
      }]
    })
  ),
);
