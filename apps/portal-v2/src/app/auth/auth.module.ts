import { APP_INITIALIZER, ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthConfig, OAuthModuleConfig, OAuthStorage } from 'angular-oauth2-oidc';
import { authInitializerStoreFactory } from './auth-app-initializer.factory';
import { AuthService } from './services/auth.service';
import { OAuthModule } from 'angular-oauth2-oidc';
import { authCodeFlowConfig, authModuleConfig } from './auth.config';

import { EffectsModule } from '@ngrx/effects';
import { Store, StoreModule } from '@ngrx/store';

import { HttpClientModule } from '@angular/common/http';
import * as fromAuth from './store/auth/auth.reducer';
import { AuthEffects } from './store/auth/auth.effects';
import { LocalNonceStorage, storageNonceFactory } from './auth-storage.factory';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    StoreModule.forFeature(fromAuth.AUTH_FEATURE_KEY, fromAuth.reducer),
    EffectsModule.forFeature([AuthEffects]),
    OAuthModule.forRoot(),
  ],
  providers: [
    AuthService,
  ]
})
export class AuthModule {

  static forRoot(): ModuleWithProviders<AuthModule> {
    return {
      ngModule: AuthModule,
      providers: [
        /* the order seems to be important here!
         * init OAuthStorage first!
         */
        // { provide: OAuthStorage, useFactory: storageFactory },
        { provide: LocalNonceStorage, useClass: LocalNonceStorage },
        { provide: OAuthStorage, useFactory: storageNonceFactory, deps: [LocalNonceStorage] },
        { provide: AuthConfig, useValue: authCodeFlowConfig },
        { provide: OAuthModuleConfig, useValue: authModuleConfig },
        { provide: APP_INITIALIZER, useFactory: authInitializerStoreFactory, deps: [AuthService, Store], multi: true },
      ]
    };
  }


  //prevent dev errors
  constructor(@Optional() @SkipSelf() parentModule: AuthModule) {
    if (parentModule) {
      throw new Error('AuthModule is already loaded. Import it in the AppModule only');
    }
  }
}
