
import { environment as env } from '@env/environment';
import { AuthConfig, OAuthModuleConfig } from 'angular-oauth2-oidc';
import { mpsApi } from '../app.models';

export const authCodeFlowConfig: AuthConfig = {
  // Url of the Identity Provider
  issuer: `${env.authKeycloakUrl}/auth/realms/${env.authRealmName}`,

  // URL of the SPA to redirect the user to after login
  redirectUri: window.location.origin, //+ '/index.html',

  // The SPA's id. The SPA is registered with this id at the auth-server
  clientId: `${env.authClientId}`,

  // Just needed if your auth server demands a secret. In general, this
  // is a sign that the auth server is not configured with SPAs in mind
  // and it might not enforce further best practices vital for security
  // such applications.
  // dummyClientSecret: 'secret',

  responseType: 'code',

  // set the scope for the permissions the client should request
  // The first four are defined by OIDC.
  // Important: Request offline_access to get a refresh token
  // The api scope is a usecase specific one
  scope: 'openid profile email offline_access',

  showDebugInformation: env.authShowDebugInformation,

  requireHttps: env.authRequireHttps,
  strictDiscoveryDocumentValidation: true,

  // Activate Session Checks:
  sessionChecksEnabled: env.authSyncSessionCheck,
};

export const authModuleConfig: OAuthModuleConfig = {
  resourceServer: {
    allowedUrls: [
      // `${env.mpsApiUrl}${mpsApi.version}${mpsApi.public}`,
      `${env.mpsApiUrl}${mpsApi.version}${mpsApi.customer}`,
      `${env.mpsApiUrl}${mpsApi.version}${mpsApi.vendor}`,
      `${env.mpsApiUrl}${mpsApi.version}${mpsApi.productProvider}`,
      `${env.mpsApiUrl}${mpsApi.version}${mpsApi.moderator}`,
      env.aaaApiUrl,
      env.evesApiUrl,
    ],
    sendAccessToken: true
  }
};
