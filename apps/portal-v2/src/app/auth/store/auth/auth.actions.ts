import { createAction, props } from '@ngrx/store';
import { OAuthErrorEvent } from 'angular-oauth2-oidc';
import { UserProfile } from './auth.model';

export const initialAuthSequence = createAction(
  '[Auth] Initial Auth Sequence Start',
);

export const initialAuthSequenceFailure = createAction(
  '[Auth] Initial Auth Sequence Failed',
  props<{ error: unknown }>()
);

export const loadOAuthProfile = createAction(
  '[Auth] load OAuth Profile',
);

export const initialUserModuleLoadSuccessful = createAction(
  '[Auth] initialUserModuleLoadSuccessful',
);

export const loadOAuthProfileSuccess = createAction(
  '[Auth] load OAuth Profile Success',
  props<{ userProfile: UserProfile }>()
);

export const loadOAuthProfileFailure = createAction(
  '[Auth] load OAuth Profile Failure',
  props<{ error: unknown }>()
);

export const login = createAction(
  '[Auth] login',
  props<{ redirect?: string }>()
);

export const logout = createAction(
  '[Auth] logout',
);

export const logoutSuccess = createAction(
  '[Auth] logout Success',
);

export const logoutFailure = createAction(
  '[Auth] logout Failure',
  props<{ error: unknown }>()
);

export const handleCodeError = createAction(
  '[Auth] handle code Error',
  props<{ error: OAuthErrorEvent }>()
);
