import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { map, filter, tap, catchError, concatMap, take, delay } from 'rxjs/operators';
import * as AuthActions from './auth.actions';
import { Router } from '@angular/router';
import { OAuthErrorEvent } from 'angular-oauth2-oidc';
import { AUTH_LOCAL_STORAGE_ITEMS, AUTH_STORAGE_ITEMS, KeycloakOrganizationUserRoles, UserProfile } from './auth.model';
import { AuthService } from '@auth/services/auth.service';
import { of } from 'rxjs';
import { AppRoutes } from '@router/router.constants';
import { Store } from '@ngrx/store';
import { LazyModuleLoaderService } from '@shared/services/lazy-module-loader/lazy-module-loader.service';
import { AuthSelectors } from '.';
import { OrganizationUserRoleV2 } from '@api/aaa/models';
import { EventActions } from 'src/app/events/store';

@Injectable()
export class AuthEffects {

  initAuth$ = createEffect(() => {
    const e = this.actions$.pipe(
      ofType(AuthActions.initialAuthSequence),
      concatMap(() => this.authService.loadDiscoveryDocument()),
      concatMap(() => this.authService.tryLoginCodeFlow()),
      map(() => {
        if (this.authService.hasValidToken()) {
          this.routeToStateUrl();
          return AuthActions.loadOAuthProfile();
        } else {
          // at this point it is possible that we have an expired token
          // in our storage -> remove it
          this.clearAuthStorageItems();
          return AuthActions.initialAuthSequenceFailure({
            error: 'initialAuthSequence: Error getting token'
          });
        }
      }),
      // map(() => AuthActions.loadOAuthProfile()),
      // tap(() => this.routeToStateUrl()),
      catchError((error, _action) => {
        console.error(error);
        return of(AuthActions.initialAuthSequenceFailure({ error }));
      })
    );
    return e;
  });


  // on manual login button
  login$ = createEffect(() => {
    const e = this.actions$.pipe(
      ofType(AuthActions.login),
      tap((a) => this.authService.login(a.redirect)),
    );
    return e;
  }, { dispatch: false });

  loadOAuthProfile$ = createEffect(() => {
    const a = this.actions$.pipe(
      ofType(AuthActions.loadOAuthProfile),
      concatMap(() => this.authService.loadUserProfile()),
      tap(r => console.log('loadOAuthProfile$', r)),
      // we do not have a model definition of User in the backend
      map((profile) => this._toUserProfile(profile)),
      map((userProfile) => AuthActions.loadOAuthProfileSuccess({ userProfile })),
      catchError(e => of(AuthActions.loadOAuthProfileFailure({ error: e })))
    );
    return a;
  });

  oAuthEventTokenReceived$ = createEffect(() => {
    const a = this.authService.events.pipe(
      filter(e => ['token_received'].includes(e.type)),
      map(() => AuthActions.loadOAuthProfile()),
      catchError((e) => of(AuthActions.handleCodeError({ error: e })))
    );
    return a;
  });

  loadUserModule$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.loadOAuthProfileSuccess),
      concatLatestFrom(() => this.store.select(AuthSelectors.selectRefreshIndex)),
      filter(([_action, idx]) => idx === 1),
      take(1),
      map(() => AuthActions.initialUserModuleLoadSuccessful())
    );
  });

  // get profile with new orgs / roles in token
  sseRefreshProfile$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        EventActions.sseUpdateMembershipRequests,
        EventActions.sseUpdateRoleRequests,
        EventActions.sseUpdateUnapprovedOrganizations,
        EventActions.sseUpdateUserRole,
      ),
      map(() => AuthActions.loadOAuthProfile()),
    );
  });


  codeError$ = createEffect(() => {
    const a = this.authService.events.pipe(
      filter(e => ['code_error'].includes(e.type)),
      map(e => e as OAuthErrorEvent),
      map((e) => AuthActions.handleCodeError({ error: e }))
    );
    return a;
  });

  // handles errors arising when no new refresh token could be retrieved
  // the library then tries to use the expired token for new requests
  // this causes errors even on public routes
  // see: https://github.com/manfredsteyer/angular-oauth2-oidc/issues/820
  refreshTokenError$ = createEffect(() => {
    const a = this.authService.events.pipe(
      filter(e => ['token_refresh_error'].includes(e.type)),
      map(e => e as OAuthErrorEvent),
      map((_e) => this.clearAuthStorageItems()),
      map((_e) => AuthActions.logout()),

    );
    return a;
  });


  private errorResponsesRequiringUserInteraction = [
    'interaction_required',
    'login_required',
    'account_selection_required',
    'consent_required',
  ];
  oAuthErrorRequiringUserInteraction$ = createEffect(() => {
    const a = this.actions$.pipe(
      ofType(AuthActions.handleCodeError),
      // map(action => action.error),
      map(action =>
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        this.errorResponsesRequiringUserInteraction.indexOf((action.error.reason as any)['error']) >= 0
      ),
      map(() => this.authService.login())
    );
    return a;
  }, { dispatch: false });

  logout$ = createEffect(() => {
    const a = this.actions$.pipe(
      ofType(AuthActions.logout),
      map(() => this.authService.logout(true, window.location.href)),
      tap(() => this.router.navigate([AppRoutes.LOGOUT_SUCCESS])),
      map(_action => AuthActions.logoutSuccess())
    );
    return a;
  });

  clearNonce$ = createEffect(() => {
    const a = this.actions$.pipe(
      ofType(
        AuthActions.loadOAuthProfileFailure,
        AuthActions.loadOAuthProfileSuccess
      ),
      delay(2000), // wait until app has settled
      tap(() => this.clearNonce()),
    );
    return a;
  }, { dispatch: false });


  // only relevant if authSyncSessionCheck is enabled
  // if syncing session - force logout on session termination
  sessionEnd$ = createEffect(() => {
    const a = this.authService.events.pipe(
      filter(e => e.type === 'session_terminated'),
      map(_e => console.log('Your session has been terminated!')),
      tap(() => this.authService.logout(true, window.location.href))
    );
    return a;
  }, { dispatch: false });

  private _toUserProfile(profile: unknown): UserProfile {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const up = (profile as any).info as UserProfile;
    // convert to intermediate type Record
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const roleRecord = ((profile as any).info.organizations as Record<string, { roles: KeycloakOrganizationUserRoles[] }>);

    Object.entries(roleRecord).map(([key, value]) => {
      // console.log(`orgs k,v ${key}-${JSON.stringify(value)}`);
      // keycloak return Pascal case strings for user role
      // convert to auth service enum
      const roles = value.roles.map(role => {
        const validRole = this.validateRoleEnumType(role);
        switch (validRole) {
          case KeycloakOrganizationUserRoles.KC_MANAGER:
            return OrganizationUserRoleV2.MANAGER;
          case KeycloakOrganizationUserRoles.KC_PATHOLOGIST:
            return OrganizationUserRoleV2.PATHOLOGIST;
          case KeycloakOrganizationUserRoles.KC_APP_MAINTAINER:
            return OrganizationUserRoleV2.APP_MAINTAINER;
          case KeycloakOrganizationUserRoles.KC_DATA_MANAGER:
            return OrganizationUserRoleV2.DATA_MANAGER;
          case KeycloakOrganizationUserRoles.KC_CLEARANCE_MAINTAINER:
            return OrganizationUserRoleV2.CLEARANCE_MAINTAINER;
          default:
            throw Error(`Unknown OrganizationUserRoleV2 ${role}`);
        }
      });
      up.organizations[key] = { roles };
    });
    return up;
  }

  private validateRoleEnumType(role: KeycloakOrganizationUserRoles) {
    if (Object.values(KeycloakOrganizationUserRoles).includes(role)) {
      // console.log(`Enum value ${role} is valid!`);
      return role;
    } else {
      console.error(`Unknown KeycloakOrganizationUserRoles ${role}`);
      throw new Error(`Unknown KeycloakOrganizationUserRoles ${role}`);
    }
  }

  private async routeToStateUrl() {
    const redirectUrl = this.authService.afterLoginRedirect();
    if (redirectUrl) {
      await this.router.navigateByUrl(redirectUrl);
    }
  }

  private clearNonce() {
    AUTH_LOCAL_STORAGE_ITEMS.map((item: string) => { localStorage.removeItem(item); });
  }


  private clearAuthStorageItems() {
    // would be cleaner to use storageFactory but it is not available at this point
    // to keep things simple we clean both storage locations
    AUTH_STORAGE_ITEMS.map((item: string) => { localStorage.removeItem(item); });
    AUTH_STORAGE_ITEMS.map((item: string) => { sessionStorage.removeItem(item); });
  }

  constructor(
    private readonly actions$: Actions,
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly store: Store,
    private readonly lazyModuleLoaderService: LazyModuleLoaderService,
  ) { }
}
