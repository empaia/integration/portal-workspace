import * as fromAuth from './auth.reducer';
import { selectAuthState } from './auth.selectors';

describe('Auth Selectors', () => {
  it('should select the feature state', () => {
    const result = selectAuthState({
      [fromAuth.AUTH_FEATURE_KEY]: {}
    });

    expect(result).toEqual({});
  });
});
