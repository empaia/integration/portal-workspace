import { OrganizationUserRoleV2 } from '@api/aaa/models';


export interface UserProfile {
  exp: number;
  iat: number;
  auth_time: number;
  email_verified: boolean;
  name: string;
  // contains email
  preferred_username: string;
  given_name: string;
  family_name: string;
  email: string;

  organizations: Organization;

  // user-id
  sub: string
}


export type Organization = {
  [organizationId: string]: {
    roles: OrganizationUserRoleV2[]
  }
};

export type OrganizationArray = {
  id: string
  roles: OrganizationUserRoleV2[]
};


export interface AuthLocaleParams {
  ui_locales: string,
  kc_locale: string
}


export enum KeycloakOrganizationUserRoles {
  KC_MANAGER = 'manager',
  KC_PATHOLOGIST = 'pathologist',
  KC_APP_MAINTAINER = 'app-maintainer',
  KC_DATA_MANAGER = 'data-manager',
  KC_CLEARANCE_MAINTAINER = 'clearance-maintainer'
}

// export type KeycloakOrganizationUserRolesType = `${KeycloakOrganizationUserRoles}`;

export const AUTH_STORAGE_ITEMS: string[] = [
  'access_token',
  'access_token_stored_at',
  'expires_at',
  'granted_scopes',
  'id_token',
  'id_token_claims_obj',
  'id_token_expires_at',
  'id_token_stored_at',
  'nonce',
  'PKCE_verifier',
  'refresh_token',
  'session_state'
];

export const AUTH_LOCAL_STORAGE_ITEMS: string[] = [
  'nonce',
  'PKCE_verifier',
];

