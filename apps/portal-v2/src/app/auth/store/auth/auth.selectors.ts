import { OrganizationUserRoleV2 } from '@api/aaa/models';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { selectUserOrganizationsDefaultOrgId } from '@user/store/user-organizations/user-organizations.selectors';
import { OrganizationArray } from './auth.model';
import { AUTH_FEATURE_KEY, State } from './auth.reducer';

export const selectAuthState = createFeatureSelector<State>(
  AUTH_FEATURE_KEY
);

export const selectAuthProfile = createSelector(
  selectAuthState,
  (state: State) => state.userProfile
);

export const selectUserId = createSelector(
  selectAuthState,
  (state: State) => state.userProfile?.sub
);

export const selectLoggedIn = createSelector(
  selectAuthState,
  (state: State) => state.loggedIn
);

export const selectIsDoneLoading = createSelector(
  selectAuthState,
  (state: State) => state.loaded
);

export const selectOrganizations = createSelector(
  selectAuthProfile,
  (profile) => (profile)?.organizations
);

export const selectRefreshIndex = createSelector(
  selectAuthState,
  (state: State) => state.refreshIndex
);

// converts keycloak struct {"org-id": {roles: [ROLE]}} to
// array [{"id":org-id,roles: [ROLE]}]
export const selectOrganizationsArray = createSelector(
  selectAuthProfile,
  (profile) => {
    const organizations: OrganizationArray[] = [];
    if (profile) {
      Object.entries(profile.organizations).map(([key, value]) => {
        // console.log(`orgs k,,v ${key}-${value}`);
        organizations.push({ id: key, roles: value.roles });
      });
    }
    return organizations;
  }
);

export const selectRolesInOrganization = (id: string) => createSelector(
  selectAuthProfile,
  (profile) => {
    const org = profile?.organizations[id];
    if (org) {
      return org.roles;
    } else {
      return [];
    }
  }
);

export const selectRolesInSelectedOrganization = createSelector(
  selectUserOrganizationsDefaultOrgId,
  selectAuthProfile,
  (id, profile) => {
    if (id) {
      const org = profile?.organizations[id];
      if (org) {
        return org.roles;
      }
    }
    return [];
  }
);

export const selectIsManager = createSelector(
  selectRolesInSelectedOrganization,
  (roles) => {
    return roles.includes(OrganizationUserRoleV2.MANAGER);
  }
);

export const selectIsClearanceMaintainer = createSelector(
  selectRolesInSelectedOrganization,
  (roles) => roles.includes(OrganizationUserRoleV2.CLEARANCE_MAINTAINER)
);


export const selectUserProfileName = createSelector(
  selectAuthProfile,
  (profile) => (profile)?.name
);

export const selectUserProfileEmail = createSelector(
  selectAuthProfile,
  (profile) => (profile)?.email
);
