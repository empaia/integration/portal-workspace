import { Action, createReducer, on } from '@ngrx/store';
import * as AuthActions from './auth.actions';
import { UserProfile } from './auth.model';


export const AUTH_FEATURE_KEY = 'auth';

export interface State {
  loggedIn: boolean; // is the user logged in / known
  loaded: boolean; // has the User list been loaded
  error?: string | undefined; // last known error (if any)
  userProfile: UserProfile | undefined;
  refreshIndex: number;
}

export const initialState: State = {
  loaded: false,
  loggedIn: false,
  error: undefined,
  userProfile: undefined,
  refreshIndex: 0,
};

export const authReducer = createReducer(
  initialState,

  on(AuthActions.loadOAuthProfileSuccess, (state, { userProfile }): State => ({
    ...state,
    userProfile,
    loaded: true,
    loggedIn: true,
    refreshIndex: state.refreshIndex + 1,
  })),
  on(AuthActions.loadOAuthProfileFailure, (state): State => ({
    ...state,
    userProfile: undefined,
    loaded: true,
    loggedIn: false,
  })),

  on(AuthActions.logoutSuccess, (state): State => ({
    ...state,
    userProfile: undefined,
    loaded: true,
    loggedIn: false,
    refreshIndex: 0,
  })),

  on(AuthActions.initialAuthSequenceFailure, (state): State => ({
    ...state,
    userProfile: undefined,
    loaded: true,
    loggedIn: false,
  })),

);

export function reducer(state: State | undefined, action: Action) {
  return authReducer(state, action);
}
