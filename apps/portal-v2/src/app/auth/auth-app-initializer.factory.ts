import { Store } from '@ngrx/store';
import { filter, first } from 'rxjs';
import { AuthService } from './services/auth.service';
import { selectIsDoneLoading } from './store/auth/auth.selectors';

export function authInitializerStoreFactory(authService: AuthService, store: Store): () => void {
  return () => new Promise(resolve => {
    // wait until store is ready before triggering auth sequence
    store.select(selectIsDoneLoading)
      .pipe(
        filter(ready => ready),
        first(),
      ).subscribe(() => resolve(true));
    authService.startInitialLoginSequence();
  });
}
