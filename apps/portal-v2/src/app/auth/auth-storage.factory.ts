
import { OAuthStorage } from 'angular-oauth2-oidc';
import { AUTH_LOCAL_STORAGE_ITEMS } from './store/auth/auth.model';


// We need a factory since localStorage is not available at AOT build time
// default storage factory
export function storageFactory(): OAuthStorage {
  return localStorage;
}

// some items need to be stored in local storage to be persistent across redirects
// see: https://github.com/manfredsteyer/angular-oauth2-oidc/issues/728
export class LocalNonceStorage implements OAuthStorage {
  private needsLocal(key: string): boolean {
    return AUTH_LOCAL_STORAGE_ITEMS.includes(key);
    // return key === 'nonce' || key === 'PKCE_verifier';
  }
  getItem(key: string) {
    if (this.needsLocal(key)) {
      return localStorage.getItem(key);
    }
    return sessionStorage.getItem(key);
  }
  removeItem(key: string) {
    if (this.needsLocal(key)) {
      return localStorage.removeItem(key);
    }
    return sessionStorage.removeItem(key);
  }
  setItem(key: string, data: string) {
    if (this.needsLocal(key)) {
      return localStorage.setItem(key, data);
    }
    return sessionStorage.setItem(key, data);
  }
}

export function storageNonceFactory(localNonceStorage: LocalNonceStorage): OAuthStorage {
  return localNonceStorage;
}