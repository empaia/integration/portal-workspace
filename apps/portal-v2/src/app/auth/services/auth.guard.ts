import { inject, Injectable } from '@angular/core';
import { Route, UrlSegment, Router } from '@angular/router';
import { AuthSelectors } from '@auth/store/auth';
import { concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { AppRoutes } from '@router/router.constants';
import { Observable } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard  {
  private store = inject(Store);
  private router = inject(Router);

  // // if module is loaded (user was logged in before)
  // canActivate(
  //   _route: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot,
  // ): Observable<boolean> {
  //   const redirectParam = state.url;
  //   return this.resolve(redirectParam);
  // }

  // // if module is not loaded
  // canLoad(
  //   _route: Route,
  //   segments: UrlSegment[]
  // ): Observable<boolean> {
  //   const redirectParam = segments.join('/');
  //   return this.resolve(redirectParam);
  // }

  canMatch(_route: Route, segments: UrlSegment[]): Observable<boolean> {
    const redirectParam = segments.join('/');
    return this.resolve(redirectParam);
  }

  private resolve(redirect: string): Observable<boolean> {
    const a = this.store.select(AuthSelectors.selectIsDoneLoading).pipe(
      filter(isDone => isDone),
      concatLatestFrom((_) => this.store.select(AuthSelectors.selectLoggedIn)),
      map(([loaded, loggedIn]) => loaded && loggedIn),
      // tap(x => console.log('this guard said ' + x)),
      map((isAuthenticated) => {
        if (!isAuthenticated) {
          // directly go to keycloak
          //this.store.dispatch(login());
          // -- or --
          // show prompt: login needed
          this.router.navigate([
            AppRoutes.LOGIN_REQUIRED
          ], {
            queryParams: { redirect }
          });
          return false;
        }
        return true;
      }),
      // tap(x => console.log('resolved to ' + x)),
      take(1)
    );
    return a;
  }
}