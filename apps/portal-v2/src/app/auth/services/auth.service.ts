import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { authCodeFlowConfig } from '@auth/auth.config';
import { AuthActions } from '@auth/store/auth';
import { AuthLocaleParams } from '@auth/store/auth/auth.model';
import { Store } from '@ngrx/store';
import { AppRoutes } from '@router/router.constants';
import { LanguageService } from '@transloco/language.service';
import { OAuthEvent, OAuthService } from 'angular-oauth2-oidc';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AuthService {

  // if user is on one of these pages and logs in again
  // we do not want to redirect back there
  // instead we redirect to homepage
  private noRedirectRoutes = [
    '/' + AppRoutes.LOGOUT_SUCCESS,
    '/' + AppRoutes.LOGIN_REQUIRED,
  ];

  constructor(
    private oauthService: OAuthService,
    private router: Router,
    private store: Store,
    private languageService: LanguageService,
  ) {
    this.oauthService.setupAutomaticSilentRefresh();
  }

  public startInitialLoginSequence(): void {
    this.store.dispatch(AuthActions.initialAuthSequence());
  }

  private _login(targetUrl: string | undefined, params: AuthLocaleParams) {
    // console.log('_login prams', params);
    this.oauthService.initLoginFlow(targetUrl || this.router.url, params);
  }

  public login(targetUrl?: string) {
    this._login(targetUrl, {
      ui_locales: this.languageService.getLanguage(),
      kc_locale: this.languageService.getLanguage()
    });
  }

  /**
   * use state set on login to redirect back to page that was originally requested
   */
  public afterLoginRedirect(): string | undefined {
    if (this.oauthService.state
      && this.oauthService.state !== 'undefined'
      && this.oauthService.state !== 'null') {
      let stateUrl = this.oauthService.state;
      let navTo = '';
      if (stateUrl.startsWith('/') === false) {
        stateUrl = decodeURIComponent(stateUrl);
        let url;
        try {
          url = new URL(stateUrl);
        } catch (_) { _;  /* ignore error */ }
        navTo = url ?
          url?.pathname + (url ? decodeURIComponent(url.search) : '')
          : stateUrl;

        // edge case scenario - user logged out - is on logout successful page - logs in again via header - is redirect back to logout successful page
        // we do not want this so we redirect home
        navTo = this.noRedirectRoutes.indexOf(navTo) >= 0 ? '/' + AppRoutes.MARKET_ROUTE : navTo;
      }
      console.log(`There was state of ${this.oauthService.state}, so we are sending you to: ${navTo}`);
      return navTo;
    }
    return undefined;
  }

  public logout(_noRedirectToLogoutUrl: boolean, _state: string): void {
    //this.oauthService.logOut(noRedirectToLogoutUrl, state);
    // without token revocation user can login again without entering password
    this.oauthService.revokeTokenAndLogout();
  }

  public hasValidToken(): boolean {
    return this.oauthService.hasValidAccessToken();
  }

  public loadUserProfile(): Promise<object> {
    return this.oauthService.loadUserProfile();
  }

  public tryLoginCodeFlow(): Promise<void> {
    return this.oauthService.tryLoginCodeFlow();
  }

  public loadDiscoveryDocument(): Promise<OAuthEvent> {
    return this.oauthService.loadDiscoveryDocument();
  }

  public get state(): string | undefined {
    return this.oauthService.state;
  }

  public get events(): Observable<OAuthEvent> {
    return this.oauthService.events;
  }

  // These normally won't be exposed from a service like this, but
  // for debugging it makes sense.
  public get accessToken() { return this.oauthService.getAccessToken(); }
  // public get refreshToken() { return this.oauthService.getRefreshToken(); }
  public get identityClaims() { return this.oauthService.getIdentityClaims(); }
  // public get idToken() { return this.oauthService.getIdToken(); }
  // public get logoutUrl() { return this.oauthService.logoutUrl; }

  public get registrationLink(): string | undefined {
    let url = this.oauthService.loginUrl;

    // console.log('loginUrl url', url);

    // !! no official support for registration links by keycloak - this can break
    url += `?client_id=${authCodeFlowConfig.clientId}`;
    url = url?.replace('/auth?', '/registrations?');

    url += `&response_type=${authCodeFlowConfig.responseType}`;
    if (authCodeFlowConfig.scope) {
      url += `&scope=${encodeURIComponent(authCodeFlowConfig.scope)}`;
    }
    if (authCodeFlowConfig.redirectUri) {
      url += `&redirect_uri=${encodeURIComponent(authCodeFlowConfig.redirectUri)}`;
    }
    url += `&kc_locale=${this.languageService.getLanguage()}`;
    // console.log('register url', url);

    return url;
  }

  public get changePasswordUrl(): string | undefined {
    // solution from: https://keycloak.discourse.group/t/how-to-trigger-the-update-password-page-in-keycloak-for-users/11408
    let url = this.oauthService.loginUrl;
    url += `?client_id=${authCodeFlowConfig.clientId}`;
    if (authCodeFlowConfig.redirectUri) {
      url += `&redirect_uri=${encodeURIComponent(authCodeFlowConfig.redirectUri)}`;
    }
    url += `&response_type=${authCodeFlowConfig.responseType}`;
    if (authCodeFlowConfig.scope) {
      url += `&scope=${encodeURIComponent(authCodeFlowConfig.scope)}`;
    }
    url += '&kc_action=UPDATE_PASSWORD';
    url += `&kc_locale=${this.languageService.getLanguage()}`;

    return url;
  }
}

