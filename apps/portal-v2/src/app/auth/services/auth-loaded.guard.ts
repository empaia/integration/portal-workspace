import { inject, Injectable } from '@angular/core';
import { Route, UrlSegment } from '@angular/router';
import { AuthSelectors } from '@auth/store/auth';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthLoadedGuard  {

  private store = inject(Store);

  canMatch(_route: Route, _segments: UrlSegment[]): Observable<boolean> {
    return this.resolve();
  }

  private resolve(): Observable<boolean> {
    const a = this.store.select(AuthSelectors.selectIsDoneLoading).pipe(
      filter(isDone => isDone),
      take(1)
    );
    return a;
  }

}
