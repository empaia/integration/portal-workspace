import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthLoadedGuard } from '@auth/services/auth-loaded.guard';
import { AuthGuard } from '@auth/services/auth.guard';
import { PageNotFoundComponent } from '@core/components/page-not-found/page-not-found.component';
import { LogoutSuccessComponent } from '@shared/components/logout-success/logout-success.component';
import { NeedLoginComponent } from '@shared/components/need-login/need-login.component';
import { NeedOrgMembershipComponent } from '@shared/components/need-org-membership/need-org-membership.component';
import { OrgSelectedGuard } from '@shared/services/org-selected-guard/org-selected-guard.guard';
import { AppRoutes } from './router.constants';


const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('../public/public.module').then(m => m.PublicModule),
    canMatch: [AuthLoadedGuard], // prevent accessing public routes with expired token
  },
  {
    path: '',
    loadChildren: () => import('../footer/footer.module').then(m => m.FooterModule)
  },
  {
    path: AppRoutes.MY_ROUTE,
    loadChildren: () => import('../user/user.module').then(m => m.UserModule),
    canMatch: [AuthGuard],
  },
  {
    path: AppRoutes.MANAGE_ROUTE,
    loadChildren: () => import('../manager/manager.module').then(m => m.ManagerModule),
    canMatch: [AuthGuard, OrgSelectedGuard],
  },
  {
    path: AppRoutes.MODERATE_ROUTE,
    loadChildren: () => import('../moderator/moderator.module').then(m => m.ModeratorModule),
    canMatch: [AuthGuard],
  },
  {
    path: AppRoutes.CLEARANCE_MAINTAINER_ROUTE,
    loadChildren: () => import('../clearance-maintainer/clearance-maintainer.module').then(m => m.ClearanceMaintainerModule),
    canMatch: [AuthGuard, OrgSelectedGuard],
  },
  { path: AppRoutes.LOGIN_REQUIRED, component: NeedLoginComponent },
  { path: AppRoutes.LOGOUT_SUCCESS, component: LogoutSuccessComponent },
  { path: AppRoutes.NEED_ORG_MEMBERSHIP, component: NeedOrgMembershipComponent },
  { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      // { useHash: true }
      // { enableTracing: true } // <-- debugging purposes only
      { scrollPositionRestoration: 'enabled' }
    ),
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
