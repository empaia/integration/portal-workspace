import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { createEffect, ofType, Actions } from '@ngrx/effects';
import * as RouterActions from './router.actions';
import { ActivatedRoute, Router } from '@angular/router';
import { concatMap, map, tap } from 'rxjs/operators';
import { ROUTER_NAVIGATED } from '@ngrx/router-store';
import { AppRoutes } from './router.constants';
import { Store } from '@ngrx/store';
import { SidenavService } from '@core/services/sidenav/sidenav.service';
import { ADMIN_SIDE_NAV_ITEMS } from '../app.models';
import { UserOrganizationsActions } from '@user/store/user-organizations';

@Injectable()
export class RouterEffects {
  navigate$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(RouterActions.navigate),
        tap(({ path, queryParams, extras }) =>
          this.router.navigate(path, { queryParams, ...extras })
        )
      );
    }, { dispatch: false, }
  );

  sidenavOpen$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(ROUTER_NAVIGATED),
        map(() => this.router.url),
        map(c => c.split('/')),
        map(c => {
          if (c && c.length >= 1) {
            if (c[1] === AppRoutes.MANAGE_ROUTE ||
              c[1] === AppRoutes.MODERATE_ROUTE) {
              this.sidenav.open();
            } else {
              this.sidenav.close();
            }
          }
        })
      );
    }, { dispatch: false, }
  );

  sidenavActive$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(ROUTER_NAVIGATED),
        map(() => this.router.url.substring(1)),
        map(route => ADMIN_SIDE_NAV_ITEMS.filter(n => route.includes(n.ref))),
        map(navItem => {
          if (navItem && navItem.length >= 1) {
            this.sidenav.activeLinkId = navItem[0].id;
          }
        })
      );
    }, { dispatch: false, }
  );

  // relative to  does not work with effect based routing - try again after upgrade
  // navigateRelative$ = createEffect(
  //   () => {
  //     return this.actions$.pipe(
  //       ofType(RouterActions.navigateRelativeTo),
  //       // tap(({ path, queryParams, extras }) =>
  //       //   this.router.navigate(path, { queryParams, ...extras })
  //       // ),
  //       // withLatestFrom(this.store.select(RouterSelectors.selectUrl)),
  //       tap(() => console.log(this.route.snapshot)),
  //       tap(({ path, queryParams, extras }) =>
  //         this.router.navigate(path,
  //           {
  //             queryParams,
  //             ...extras,
  //             relativeTo:this.route,
  //           })
  //       ),
  //     );
  //   }, { dispatch: false }
  // );

  // updateTitle$ = createEffect(
  //   () => {
  //     return this.actions$.pipe(
  //       ofType(routerNavigatedAction),
  //       concatLatestFrom(() => this.store.select(selectRouteData)),
  //       map(([, data]) => `Portalv2`),
  //       tap((title) => this.titleService.setTitle(title))
  //     );
  //   }, { dispatch: false, }
  // );

  // refreshing current component after successfully changing
  // the default organization
  refreshCurrentComponent$ = createEffect(() => {
    let tmpUrl = '';
    return this.actions$.pipe(
      ofType(UserOrganizationsActions.setDefaultOrganizationSuccess),
      map(() => tmpUrl = this.router.url),
      concatMap(() => this.router.navigateByUrl('/', { skipLocationChange: true })),
      concatMap(() => this.router.navigate([tmpUrl]))
    );
  }, { dispatch: false });

  constructor(
    private actions$: Actions,
    private titleService: Title,
    private router: Router,
    private route: ActivatedRoute,
    private readonly store: Store,
    private readonly sidenav: SidenavService,
  ) { }
}
