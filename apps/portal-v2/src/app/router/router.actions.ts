
import { NavigationExtras } from '@angular/router';
import { createAction, props } from '@ngrx/store';

export const navigate = createAction(
  '[Router] Navigate',
  props<{
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    path: any[];
    queryParams?: object;
    extras?: NavigationExtras;
  }>()
);


export const navigateRelativeTo = createAction(
  '[Router] Navigate Relative',
  props<{
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    path: any[];
    queryParams?: object;
    extras?: NavigationExtras;
  }>()
);

