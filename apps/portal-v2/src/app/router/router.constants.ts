import { FilterCategory } from '@public/store/portal-apps-filter/portal-apps-filter.models';

export class AppRoutes {
  // public
  static readonly ROOT_ROUTE = '/';
  static readonly PUBLIC_ORG_ID = 'organizationId';
  static readonly PUBLIC_ORG_ROUTE = 'organizations';

  static readonly PUBLIC_APP_ID = 'appId';
  static readonly PUBLIC_APP_ROUTE = 'app';

  static readonly PUBLIC_APP_API_VERSION = 'appApiVersion';

  static readonly MARKET_ROUTE = 'marketplace';

  static readonly CLEARANCES_ROUTE = 'clearances';

  // footer information
  static readonly INFORMATION_ABOUT = 'about';
  static readonly INFORMATION_DATA_PRIVACY = 'data-privacy';
  static readonly INFORMATION_TERMS_OF_USE ='terms-of-use';
  static readonly INFORMATION_LEGAL_NOTICE = 'legal-notice';

  // auth protected
  static readonly MY_ROUTE = 'my';

  // children of /my/..
  static readonly MY_ORGANIZATIONS_ROUTE = 'organizations';
  static readonly MY_ACTIVE_ORGANIZATION_ROUTE = 'active-organization';
  static readonly APPS_ROUTE = 'apps';
  static readonly USERS_ROUTE = 'users';
  static readonly PROFILE_ROUTE = 'profile';
  static readonly NOTIFICATIONS_ROUTE = 'notifications';


  static readonly UNAPPROVED_ORGANIZATION_ID = 'unapprovedOrganizationId';

  // auth & role manager
  static readonly MANAGE_ROUTE = 'manage';
  static readonly MANAGE_ACTIVE_ORG_ROUTE = 'active-org';
  static readonly MANAGE_MEMBERSHIP_REQUESTS = 'membership-requests';
  static readonly MANAGE_USERS = 'users';
  static readonly MANAGE_REQUEST_ORGANIZATION_CATEGORY_CHANGE = 'category-change';
  static readonly MANAGE_EDIT_ORGANIZATION = 'edit-organization';

  // moderate route
  static readonly MODERATE_ROUTE = 'moderate';
  static readonly MODERATE_USERS = 'users';
  static readonly MODERATE_USER_ORGANIZATIONS = 'user-organizations';
  static readonly MODERATE_ORGANIZATIONS = 'organizations';
  static readonly MODERATE_STATISTICS = 'statistics';

  // clearance maintainer route
  static readonly CLEARANCE_MAINTAINER_ROUTE = 'clearance-maintainer';
  static readonly CLEARANCE_MAINTAINER_CLEARANCES = 'clearances';

  // auth status
  static readonly LOGIN_REQUIRED = 'login-required';
  static readonly LOGOUT_SUCCESS = 'logout-success';

  // no org member
  static readonly NEED_ORG_MEMBERSHIP = 'need-org-error';

}


export class AppQueryParams {
  static readonly LANG = 'lang';
  static readonly SEARCH = 'search';
}

export type HeaderNavLink =
  | typeof AppRoutes.PUBLIC_ORG_ROUTE
  | typeof AppRoutes.MARKET_ROUTE
  | typeof AppRoutes.CLEARANCES_ROUTE
  | 'none';

export type ParamTypes =
  | typeof AppRoutes.PUBLIC_APP_ID
  | typeof AppRoutes.PUBLIC_APP_API_VERSION
  | typeof AppRoutes.PUBLIC_ORG_ID
  | typeof AppQueryParams.LANG
  | typeof AppQueryParams.SEARCH
  | typeof AppRoutes.UNAPPROVED_ORGANIZATION_ID;


export const sortTypes = [
  "sort"
] as const;
export type SortTypes = typeof sortTypes[number];
export type QueryParamTypes = SortTypes | FilterCategory | 'redirect' | 'search';

export { FilterCategory as FilterTypes };
