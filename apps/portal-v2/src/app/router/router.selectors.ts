import { getRouterSelectors } from '@ngrx/router-store';
import { createSelector } from '@ngrx/store';
import { AppRoutes, FilterTypes, ParamTypes, QueryParamTypes } from './router.constants';
import { SortBy } from '@shared/components/sort-selection/sort-selection.models';
import { ApiVersion, ClearanceQuery, PortalAppQuery } from '@api/public/models';

// `router` is used as the default feature name. You can use the feature name
// of your choice by creating a feature selector and pass it to the `getSelectors` function
// export const selectRouter = createFeatureSelector<RouterReducerState>('yourFeatureName');

export const {
  selectCurrentRoute, // select the current route
  selectFragment, // select the current route fragment
  selectQueryParams, // select the current route query params
  selectQueryParam, // factory function to select a query param
  selectRouteParams, // select the current route params
  selectRouteParam, // factory function to select a route param
  selectRouteData, // select the current route data
  selectUrl, // select the current url
} = getRouterSelectors();

const selectRouterStringParam = (paramName: ParamTypes) => createSelector(
  selectRouteParam(paramName),
  (param) => (typeof param === 'string' ? param as string : undefined)
);


export const selectRouteParamAppId = createSelector(
  selectRouterStringParam('appId'),
  (appId) => appId
);

export const selectRouterParamOrganizationId = createSelector(
  selectRouterStringParam('organizationId'),
  (organizationId) => organizationId
);

export const selectRouteParamAppApiVersionId = createSelector(
  selectRouterStringParam('appApiVersion'),
  (appId) => appId as ApiVersion
);

export const selectRouterParamUnapprovedOrganizationId = createSelector(
  selectRouterStringParam(AppRoutes.UNAPPROVED_ORGANIZATION_ID),
  (organizationId) => organizationId,
);

export const selectRouterParamLanguage = createSelector(
  selectRouterStringParam('lang'),
  (lang) => lang
);

// export const selectRouterParamOrgSearch = createSelector(
//   selectRouterStringParam('search'),
//   (search) => search
// );

export const selectHeaderNavActive = createSelector(
  selectUrl,
  (url) => {
    // console.log('url', url);
    if (url) {
      const urlPart = url.split('?');
      if (urlPart[0] === `${'/'}${AppRoutes.PUBLIC_ORG_ROUTE}`) {
        return AppRoutes.PUBLIC_ORG_ROUTE;
      } else if (urlPart[0] === `${'/'}${AppRoutes.MARKET_ROUTE}`) {
        return AppRoutes.MARKET_ROUTE;
      } else if (urlPart[0] === `${'/'}${AppRoutes.CLEARANCES_ROUTE}`) {
        return AppRoutes.CLEARANCES_ROUTE;
      }
    }
    return 'none';
  }
);

export const selectIsRouteActive = (route: string) => createSelector(
  selectUrl,
  (url) => {
    return (url.includes(route));
  }
);


export const selectRouterQueryParamType = (paramName: QueryParamTypes) =>
  createSelector(selectQueryParams, (params) => params[paramName]);

const selectRouterStringQueryParam = (paramName: QueryParamTypes) => createSelector(
  selectRouterQueryParamType(paramName),
  (param) => (typeof param === 'string' ? param as string : undefined)
);

//const selectRouterStringArrayParam = (paramName: QueryParamTypes) => createSelector(
//  selectRouterQueryParamType(paramName),
//  (param) => (Array.isArray(param) ? param as string[] : undefined)
//);

const selectFilterArrayParam = (paramName: FilterTypes) => createSelector(
  selectRouterQueryParamType(paramName),
  (param) => _parseQueryFilter(param)
);

// for Marketplace
export const selectRouteQueryFilters = createSelector(
  selectFilterArrayParam('tissues'),
  selectFilterArrayParam('stains'),
  selectFilterArrayParam('indications'),
  selectFilterArrayParam('analysis'),
  selectFilterArrayParam('clearances'),
  (tissues,
    stains,
    indications,
    analysis,
    clearances) => {
    const filters: PortalAppQuery = {
      tissues,
      indications,
      analysis,
      stains,
      clearances
    };
    return filters;
  }
);

// for Clearances
export const selectRouteClearancesQueryFilters = createSelector(
  selectFilterArrayParam('tissues'),
  selectFilterArrayParam('stains'),
  selectFilterArrayParam('indications'),
  selectFilterArrayParam('analysis'),
  selectFilterArrayParam('clearances'),
  selectFilterArrayParam('image_types'),
  selectFilterArrayParam('procedures'),
  selectFilterArrayParam('scanners'),
  (
    tissues,
    stains,
    indications,
    analysis,
    clearances,
    image_types,
    procedures,
    scanners,
  ) => {
    const filters: ClearanceQuery = {
      tissues,
      indications,
      analysis,
      stains,
      clearances,
      image_types,
      procedures,
      scanners,
    };
    return filters;
  }
);

export const selectRouterQuerySortBy = createSelector(
  selectRouterStringQueryParam('sort'),
  (sort) => sort as SortBy
);

export const selectRouterQueryRedirect = createSelector(
  selectRouterStringQueryParam('redirect'),
  (redirect) => redirect
);

export const selectRouterQueryOrgSearch = createSelector(
  selectRouterStringQueryParam('search'),
  (search) => search
);

const _parseQueryFilter = (filterParam: string | undefined): string[] | undefined => {
  const parsedParamT = _isJsonString(filterParam);
  return parsedParamT.result as string[];
};

// catch json parse error - removes invalid array from route
const _isJsonString = (str: string | undefined): { valid: boolean, result: unknown | undefined } => {
  const invalid = { valid: false, result: undefined };
  if (!str) { return invalid; }

  let parsed: unknown;
  try {
    parsed = JSON.parse(str);
  } catch (e) {
    return invalid;
  }
  return { valid: true, result: parsed };
};

export * as RouterSelectors from '@router/router.selectors';
