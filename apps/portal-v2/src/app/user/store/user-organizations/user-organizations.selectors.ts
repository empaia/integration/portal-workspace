import { createSelector } from '@ngrx/store';
import { selectRouterParamOrganizationId } from '@router/router.selectors';
import { selectUserRootState as selectUserRootState } from '../user-root.selectors';
import { State, userOrganizationAdapter, USER_ORGANIZATIONS_FEATURE_KEY } from './user-organizations.reducer';
import { OrganizationsSelectors } from '@public/store/organizations';

// user orgs selectors
const selectUserOrganizationsState = createSelector(
  selectUserRootState,
  (state) => state[USER_ORGANIZATIONS_FEATURE_KEY]
);


export const {
  selectAll,
  selectEntities,
  selectTotal,
} = userOrganizationAdapter.getSelectors();

export const selectUserOrganizationsLoaded = createSelector(
  selectUserOrganizationsState,
  (state) => state.loaded
);

export const selectUserOrganizationsError = createSelector(
  selectUserOrganizationsState,
  (state) => state.error
);

export const selectUserOrganizationsDefaultOrgId = createSelector(
  selectUserOrganizationsState,
  (state) => state.defaultOrganizationId
);

export const selectAllUserOrganizations = createSelector(
  selectUserOrganizationsState,
  selectAll
);


export const selectUserOrganizationsEntities = createSelector(
  selectUserOrganizationsState,
  selectEntities
);

export const selectFirstUserOrganization = createSelector(
  selectAllUserOrganizations,
  (orgs) => orgs[0]
);


export const selectUserOrganizationsNumberTotal = createSelector(
  selectUserOrganizationsState,
  selectTotal
);

export const selectUserOrganizationsEntity = (id: string) => createSelector(
  selectUserOrganizationsEntities,
  (e) => e[id]
);

export const selectRouterSelectedOrganization = createSelector(
  selectUserOrganizationsEntities,
  selectRouterParamOrganizationId,
  (organizations, id) => id ? organizations[id] : undefined
);

export const selectIsOrgSelectionFinished = createSelector(
  selectUserOrganizationsState,
  (state) => state.organizationSelectionFinished
);


export const selectSelectedUserOrganization = createSelector(
  selectUserOrganizationsState,
  selectUserOrganizationsDefaultOrgId,
  (state: State, orgId: string | undefined) => {
    if (orgId) {
      return state?.entities[orgId];
    }
    return undefined;
  }
);

export const selectSelectedUserOrganizationName = createSelector(
  selectSelectedUserOrganization,
  (userOrg) => userOrg?.organization_name
);

export const selectSelectedUserOrganizationImage400 = createSelector(
  selectSelectedUserOrganization,
  (userOrg) => userOrg?.resized_logo_urls?.w400
);

export const selectSelectedUserOrganizationImage60 = createSelector(
  selectSelectedUserOrganization,
  (userOrg) => userOrg?.resized_logo_urls?.w60
);

export const selectIsMemberOfSelectedPublicOrganization = createSelector(
  selectUserOrganizationsEntities,
  OrganizationsSelectors.selectSelectedOrganization,
  (organizations, org) => org ? !!organizations[org.organization_id] : false
);

export const selectSelectedUserOrganizationCategories = createSelector(
  selectSelectedUserOrganization,
  (organization) => {
    console.log('cat sel',organization);
    return organization?.details?.categories ?? [];
  }
);
