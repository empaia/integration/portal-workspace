import { HttpErrorResponse } from '@angular/common/http';
import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { UserOrganizationEntity } from './user-organizations.models';


export const UserOrganizationsActions = createActionGroup({
  source: 'UserOrganizations',
  events: {
    // defining events with payload using the `props` function
    'Load': emptyProps(),
    'Load Success': props<{ organizations: UserOrganizationEntity[] }>(),
    'Load Failure': props<{ error: HttpErrorResponse }>(),

    'Open Select Organization Dialog': props<{ organizationId?: string }>(),
    'Close Select Organization Dialog': emptyProps(),

    'Get Default Organization': emptyProps(),
    'Get Default Organization Success': props<{ organizationId: string }>(),
    'Get Default Organization Failure': props<{ error: HttpErrorResponse }>(),

    'Set Default Organization': props<{ organizationId: string }>(),
    'Set Default Organization Success': props<{ organizationId: string }>(),
    'Set Default Organization Failure': props<{ error: HttpErrorResponse }>(),

    'Unset Default Organization': emptyProps(),
    'Unset Default Organization Success': emptyProps(),
    'Unset Default Organization Failure': props<{ error: HttpErrorResponse }>(),

    'Load Id': props<{ organizationId: string }>(),
    'Load Id Success': props<{ organization: UserOrganizationEntity }>(),
    'Load Id Failure': props<{ error: HttpErrorResponse }>(),

    'Remove Organization': props<{ organizationId: string }>(),
  },
});
