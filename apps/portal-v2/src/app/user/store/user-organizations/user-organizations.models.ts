import { PublicOrganizationEntity, PublicOrganizationProfileEntity } from '@api/aaa/models';
import { Optional } from '@shared/utility/ts-utility';

export type PublicOrganizationProfile = Optional<PublicOrganizationProfileEntity, 'details' | 'contact_data'>;

// TODO: Remove Optional categories later
export type UserOrganization = Optional<PublicOrganizationEntity, 'categories'>;
export type UserOrganizationEntity  = UserOrganization & PublicOrganizationProfile;


interface DefaultOrganization {
  isDefault: boolean;
}


export type UserOrganizationTableData =
  Partial<UserOrganizationEntity> & DefaultOrganization;
