import { HttpErrorResponse } from '@angular/common/http';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { UserOrganizationsActions } from './user-organizations.actions';
import { UserOrganizationEntity } from './user-organizations.models';

export const USER_ORGANIZATIONS_FEATURE_KEY = 'userOrganizations';

export interface State extends EntityState<UserOrganizationEntity> {
  organizationSelectionFinished: boolean;
  defaultOrganizationId: string | undefined;

  loaded: boolean;
  error?: HttpErrorResponse | undefined;
}

export const userOrganizationAdapter = createEntityAdapter<UserOrganizationEntity>({
  selectId: organization => organization.organization_id
});

export const initialState: State = userOrganizationAdapter.getInitialState({
  selectedOrganizationId: undefined,
  defaultOrganizationId: undefined,
  loaded: false,
  organizationSelectionFinished: false,
});


export const reducer = createReducer(
  initialState,

  on(UserOrganizationsActions.load, (state): State => ({
    ...state,
    loaded: false,
  })),

  on(UserOrganizationsActions.loadSuccess, (state, { organizations }): State =>
    userOrganizationAdapter.setAll(organizations, {
      ...state,
      loaded: true,
    })
  ),

  on(UserOrganizationsActions.loadFailure, (state): State => ({
    ...state,
    loaded: true,
  })),

  on(UserOrganizationsActions.setDefaultOrganizationSuccess, (state, { organizationId }): State => ({
    ...state,
    defaultOrganizationId: organizationId,
    organizationSelectionFinished: true,
  })),

  on(UserOrganizationsActions.unsetDefaultOrganizationSuccess, (state): State => ({
    ...state,
    defaultOrganizationId: undefined,
    organizationSelectionFinished: false,
  })),

  on(UserOrganizationsActions.loadId, (state): State => ({
    ...state,
    loaded: false,
  })),

  on(UserOrganizationsActions.loadIdSuccess, (state, { organization }): State =>
    userOrganizationAdapter.setOne(organization, {
      ...state,
      loaded: true,
    })
  ),

  on(UserOrganizationsActions.loadFailure, (state): State => ({
    ...state,
    loaded: true,
  })),

  on(UserOrganizationsActions.getDefaultOrganizationSuccess, (state, { organizationId }): State => ({
    ...state,
    loaded: true,
    defaultOrganizationId: organizationId,
    organizationSelectionFinished: true,
  })),

  on(UserOrganizationsActions.getDefaultOrganizationFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error
  })),


  on(UserOrganizationsActions.removeOrganization, (state, { organizationId }): State =>
    userOrganizationAdapter.removeOne(organizationId, {
      ...state
    })
  ),
);
