import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { fetch } from '@ngrx/router-store/data-persistence';

import {
  catchError,
  exhaustMap,
  filter,
  map,
  mergeMap,
  tap,
} from 'rxjs/operators';
import { UserOrganizationsActions } from './user-organizations.actions';
import { State } from './user-organizations.reducer';
import { MyControllerService } from '@api/aaa/services/my-controller.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { filterNullish } from '@shared/utility/rxjs-utility';
import { UserOrganizationsSelectors } from '.';
import { AuthActions } from '@auth/store/auth';
import { PublicControllerService } from '@api/aaa/services';
import { of } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

import { SelectOrganizationDialogComponent } from '@user/my/components/select-organization-dialog/select-organization-dialog.component';
import { defaultPaginationParams } from '@shared/api/default-params';
import { ActiveOrganizationActions } from '../active-organization/active-organization.actions';
import { EventActions } from 'src/app/events/store';

@Injectable()
export class UserOrganizationsEffects {
  // after login load users organizations
  // we need the organizations to select the default org
  loadDefaultActiveOrg$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.initialUserModuleLoadSuccessful),
      map(() => UserOrganizationsActions.load())
    );
  });

  sseRefreshOnOrgApproval$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(EventActions.sseUpdateUnapprovedOrganizations),
      // TODO: filter - update on APPROVED
      map(() => UserOrganizationsActions.load())
    );
  });

  sseRefreshOnRoleChange$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(EventActions.sseUpdateUserRole),
      // TODO: filter - on event action type
      map(() => UserOrganizationsActions.load())
    );
  });

  sseRefreshOnMembershipRequest$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(EventActions.sseUpdateMembershipRequests),
      // TODO: filter - on event action type
      map(() => UserOrganizationsActions.load())
    );
  });

  loadUserOrganizations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserOrganizationsActions.load),
      fetch({
        run: (
          _action: ReturnType<typeof UserOrganizationsActions.load>,
          _state: State
        ) => {
          return this.myControllerService
            .getOrganizations({
              ...defaultPaginationParams,
            })
            .pipe(
              tap((response) => console.log('getOrganizations', response)),
              map((response) => response.organizations),
              map((organizations) =>
                UserOrganizationsActions.loadSuccess({
                  organizations: organizations,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof UserOrganizationsActions.load>,
          error: HttpErrorResponse
        ) => {
          return UserOrganizationsActions.loadFailure({ error });
        },
      })
    );
  });

  afterLoadGetDefaultSelected$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserOrganizationsActions.loadSuccess),
      map(() => UserOrganizationsActions.getDefaultOrganization())
    );
  });

  loadUserOrganization$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserOrganizationsActions.loadId),
      fetch({
        run: (
          action: ReturnType<typeof UserOrganizationsActions.loadId>,
          _state: State
        ) => {
          return this.publicService
            .getActiveOrganization({ organization_id: action.organizationId })
            .pipe(
              filterNullish(),
              map((organization) =>
                UserOrganizationsActions.loadIdSuccess({ organization })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof UserOrganizationsActions.loadId>,
          error: HttpErrorResponse
        ) => {
          return UserOrganizationsActions.loadIdFailure({ error });
        },
      })
    );
  });

  getDefaultActive$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserOrganizationsActions.getDefaultOrganization),
      fetch({
        run: (
          _action: ReturnType<
            typeof UserOrganizationsActions.getDefaultOrganization
          >,
          _state: State
        ) => {
          return this.myControllerService.getDefaultActiveOrganization().pipe(
            map((response) => response.organization_id),
            concatLatestFrom(() => [
              this.store.select(
                UserOrganizationsSelectors.selectAllUserOrganizations
              ),
            ]),
            map(([organizationId, userOrganizations]) => {
              if (organizationId) {
                return UserOrganizationsActions.getDefaultOrganizationSuccess({
                  organizationId,
                });
              } else if (userOrganizations?.length === 1) {
                return UserOrganizationsActions.setDefaultOrganization({
                  organizationId: userOrganizations[0].organization_id,
                });
              } else {
                return UserOrganizationsActions.openSelectOrganizationDialog(
                  {}
                );
              }
            })
          );
        },
        onError: (
          _action: ReturnType<
            typeof UserOrganizationsActions.getDefaultOrganization
          >,
          error: HttpErrorResponse
        ) => {
          console.error(error);
          return UserOrganizationsActions.getDefaultOrganizationFailure({
            error,
          });
        },
      })
    );
  });

  openSelectOrganizationDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserOrganizationsActions.openSelectOrganizationDialog),
      map((action) => action.organizationId),
      concatLatestFrom(() =>
        this.store.select(UserOrganizationsSelectors.selectAllUserOrganizations)
      ),
      // get sure to have organizations loaded before opening the dialog
      // otherwise the user couldn't choose a organization no're could
      // they close the dialog
      filter(([, orgs]) => !!orgs?.length),
      exhaustMap(([selectedOrganizationId, organizations]) =>
        this.dialog
          .open(SelectOrganizationDialogComponent, {
            data: { organizations, selectedOrganizationId },
            width: '80vw',
            height: '100%',
            panelClass: 'full-screen-modal',
            disableClose: !selectedOrganizationId,
            closeOnNavigation: false,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map((organizationId) =>
              organizationId
                ? UserOrganizationsActions.setDefaultOrganization({
                  organizationId,
                })
                : UserOrganizationsActions.closeSelectOrganizationDialog()
            )
          )
      )
    );
  });

  setSelectedOrg$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserOrganizationsActions.setDefaultOrganization),
      mergeMap((action) =>
        this.myControllerService
          .setDefaultActiveOrganization({
            body: { organization_id: action.organizationId },
          })
          .pipe(
            map((_response) =>
              UserOrganizationsActions.setDefaultOrganizationSuccess({
                organizationId: action.organizationId,
              })
            ),
            catchError((error) =>
              of(
                UserOrganizationsActions.setDefaultOrganizationFailure({
                  error,
                })
              )
            )
          )
      )
    );
  });

  unsetSelectedOrg$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserOrganizationsActions.unsetDefaultOrganization),
      mergeMap(() =>
        this.myControllerService.deleteDefaultActiveOrganization().pipe(
          map((_response) =>
            UserOrganizationsActions.unsetDefaultOrganizationSuccess()
          ),
          catchError((error) =>
            of(
              UserOrganizationsActions.unsetDefaultOrganizationFailure({
                error,
              })
            )
          )
        )
      )
    );
  });

  // remove organization from store after active organization was leaved
  removeOrganization$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ActiveOrganizationActions.leaveActiveOrganizationSuccess),
      map((action) => action.organizationId),
      map((organizationId) =>
        UserOrganizationsActions.removeOrganization({ organizationId })
      )
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly myControllerService: MyControllerService,
    private readonly publicService: PublicControllerService,
    private readonly dialog: MatDialog
  ) {}
}
