import { UserRole } from '@api/aaa/models';
import { createSelector } from '@ngrx/store';
import { selectUserRootState } from '../user-root.selectors';
import { State, USER_ENTITY_FEATURE_KEY } from './user.reducer';

// // Lookup the 'User' feature state managed by NgRx
// export const selectUserState = createFeatureSelector<State>(USER_ENTITY_FEATURE_KEY);

const selectUserState = createSelector(
  selectUserRootState,
  (state) => state[USER_ENTITY_FEATURE_KEY]
);


export const selectUserLoaded = createSelector(
  selectUserState,
  (state: State) => state.loaded
);

export const selectUserError = createSelector(
  selectUserState,
  (state: State) => state.error
);


export const selectUserProfile = createSelector(
  selectUserState,
  (state: State) => state?.user
);

export const selectIsModerator = createSelector(
  selectUserProfile,
  (profile) => {
    const hasRole = profile?.user_roles.some(r => r === UserRole.MODERATOR);
    return hasRole ? hasRole : false;
  }
);


export const selectProfilePicture400 = createSelector(
  selectUserProfile,
  (profile) => profile?.resized_profile_picture_urls?.w400
);

export const selectProfilePicture60 = createSelector(
  selectUserProfile,
  (profile) => profile?.resized_profile_picture_urls?.w60
);
