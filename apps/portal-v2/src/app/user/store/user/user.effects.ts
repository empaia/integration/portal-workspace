import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType, concatLatestFrom } from '@ngrx/effects';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';

import { UserActions } from './user.actions';
import * as UserSelectors from './user.selectors';
import { HttpErrorResponse } from '@angular/common/http';
import { MyControllerService } from '@api/aaa/services';
import { State } from './user.reducer';
import { AuthActions } from '@auth/store/auth';
import { exhaustMap, filter, map, tap } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { ProfileEditDetailsDialogComponent } from '@user/my/components/profile-edit-details-dialog/profile-edit-details-dialog.component';
import { filterNullish } from '@shared/utility/rxjs-utility';
import { UserContactDataEntity, UserDetailsEntity } from '@api/aaa/models';
import { ProfileEditContactDialogComponent } from '@user/my/components/profile-edit-contact-dialog/profile-edit-contact-dialog.component';
import { ProfileEditAvatarDialogComponent } from '@user/my/components/profile-edit-avatar-dialog/profile-edit-avatar-dialog.component';
import { Store } from '@ngrx/store';
import { CountriesSelectors } from '@shared/store/countries';
import { CriticalActionDialogComponent } from '@shared/components/critical-action-dialog/critical-action-dialog.component';
import { TranslocoService } from '@ngneat/transloco';

@Injectable()
export class UserEffects {
  loadOnLogin$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.initialUserModuleLoadSuccessful),
      map(() => UserActions.loadProfile())
    );
  });

  loadUserProfile$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.loadProfile),
      fetch({
        run: (
          _action: ReturnType<typeof UserActions.loadProfile>,
          _state: State
        ) => {
          return this.myControllerService.getProfile().pipe(
            map((response) => response),
            map((profile) => UserActions.loadProfileSuccess({ user: profile }))
          );
        },
        onError: (
          _action: ReturnType<typeof UserActions.loadProfile>,
          error: HttpErrorResponse
        ) => {
          return UserActions.loadProfileFailure({ error });
        },
      })
    );
  });

  setUserDetails$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.setUserDetails),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof UserActions.setUserDetails>,
          _state: State
        ) => {
          return this.myControllerService
            .updateDetails({
              body: action.details,
            })
            .pipe(
              // tap(user => console.log('set user details: ', user)),
              map((user) => UserActions.setUserDetailsSuccess({ user }))
            );
        },
        onError: (
          _action: ReturnType<typeof UserActions.setUserDetails>,
          error: HttpErrorResponse
        ) => {
          return UserActions.setUserDetailsFailure({ error });
        },
      })
    );
  });

  setUserContactData$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.setUserContactData),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof UserActions.setUserContactData>,
          _state: State
        ) => {
          return this.myControllerService
            .updateContactData({
              body: action.contactData,
            })
            .pipe(
              // tap(user => console.log('set user Contact: ', user)),
              map((user) => UserActions.setUserContactDataSuccess({ user }))
            );
        },
        onError: (
          _action: ReturnType<typeof UserActions.setUserContactData>,
          error: HttpErrorResponse
        ) => {
          return UserActions.setUserContactDataFailure({ error });
        },
      })
    );
  });

  setUserAvatar$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.setUserAvatar),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof UserActions.setUserAvatar>,
          _state: State
        ) => {
          return this.myControllerService
            .setProfilePicture({
              body: {
                picture: action.picture,
              },
            })
            .pipe(
              // this route has no response data, get the profile data again
              map(() => UserActions.setUserAvatarSuccess())
            );
        },
        onError: (
          _action: ReturnType<typeof UserActions.setUserAvatar>,
          error
        ) => {
          return UserActions.setUserAvatarFailure({ error });
        },
      })
    );
  });

  deleteUserAvatar$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.deleteUserAvatar),
      pessimisticUpdate({
        run: (
          _action: ReturnType<typeof UserActions.deleteUserAvatar>,
          _state: State
        ) => {
          return this.myControllerService.deleteProfilePictures().pipe(
            // this route has no response data, get the profile data again, Backend-Issue: #99
            map(() => UserActions.deleteUserAvatarSuccess())
          );
        },
        onError: (
          _action: ReturnType<typeof UserActions.deleteUserAvatar>,
          error
        ) => {
          return UserActions.deleteUserAvatarFailure({ error });
        },
      })
    );
  });

  deleteUserAccount$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.deleteUserAccount),
      pessimisticUpdate({
        run: (
          _action: ReturnType<typeof UserActions.deleteUserAccount>,
          _state: State
        ) => {
          return this.myControllerService
            .deleteMyUserAccount()
            .pipe(map(() => UserActions.deleteUserAccountSuccess()));
        },
        onError: (
          _action: ReturnType<typeof UserActions.deleteUserAccount>,
          error: HttpErrorResponse
        ) => {
          return UserActions.deleteUserAccountFailure({ error });
        },
      })
    );
  });

  refreshProfile$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        UserActions.setUserAvatarSuccess,
        UserActions.deleteUserAvatarSuccess
      ),
      map(() => UserActions.loadProfile())
    );
  });

  // logout after delete user account
  logoutAfterDelete$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.deleteUserAccountSuccess),
      map(() => AuthActions.logout())
    );
  });

  openEditDetailsDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.openEditUserDetailsDialog),
      concatLatestFrom(() =>
        this.store.select(UserSelectors.selectUserProfile)
      ),
      map(([, userProfile]) => userProfile),
      filterNullish(),
      map((userProfile) => userProfile.details),
      exhaustMap((userDetails) => {
        const dialogRef = this.dialog.open(ProfileEditDetailsDialogComponent, {
          data: userDetails,
        });
        return dialogRef.afterClosed();
      }),
      filterNullish(),
      map((details: UserDetailsEntity) =>
        UserActions.setUserDetails({ details })
      )
    );
  });

  openEditContactDataDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.openEditUserContactDataDialog),
      concatLatestFrom(() => [
        this.store.select(UserSelectors.selectUserProfile),
        this.store.select(CountriesSelectors.selectAllCountries),
      ]),
      map(([, userProfile, countries]) => ({ userProfile, countries })),
      filter((data) => !!data.userProfile),
      map((data) => ({
        contactData: data.userProfile?.contact_data,
        countries: data.countries,
      })),
      exhaustMap((data) => {
        const dialogRef = this.dialog.open(ProfileEditContactDialogComponent, {
          data: data,
        });
        return dialogRef.afterClosed();
      }),
      filterNullish(),
      // tap(contactData => console.log('Data: ', contactData)),
      map((contactData: UserContactDataEntity) =>
        UserActions.setUserContactData({ contactData })
      )
    );
  });

  openEditAvatarDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.openEditUserAvatarDialog),
      concatLatestFrom(() =>
        this.store.select(UserSelectors.selectUserProfile)
      ),
      map(([, userProfile]) => userProfile),
      filterNullish(),
      map((userProfile) => userProfile.profile_picture_url),
      exhaustMap((avatarUrl) => {
        const dialogRef = this.dialog.open(ProfileEditAvatarDialogComponent, {
          data: avatarUrl,
        });
        return dialogRef.afterClosed();
      }),
      filter((picture) => !!picture || picture === null),
      // if null the user wants to delete/remove the old picture
      map((picture: Blob | null) =>
        picture
          ? UserActions.setUserAvatar({ picture })
          : UserActions.deleteUserAvatar()
      )
    );
  });

  openDeleteUserAccountDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.openDeleteUserAccountDialog),
      exhaustMap(() =>
        this.dialog
          .open(CriticalActionDialogComponent, {
            data: {
              message: this.translocoService.translate(
                'profile.delete_user_confirm'
              ),
            },
          })
          .afterClosed()
          .pipe(
            filter((response) => !!response),
            map(() => UserActions.deleteUserAccount())
          )
      )
    );
  });

  setLanguageFromProfile$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(UserActions.loadProfileSuccess),
        map((profile) => profile.user.language_code),
        tap((l) => this.translocoService.setActiveLang(l.toLowerCase()))
      );
    },
    { dispatch: false }
  );

  updateLanguage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.setUserLanguage),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof UserActions.setUserLanguage>,
          _state: State
        ) => {
          return this.myControllerService
            .updateLanguage({
              body: { language_code: action.code },
            })
            .pipe(map(() => UserActions.setUserLanguageSuccess()));
        },
        onError: (
          _action: ReturnType<typeof UserActions.setUserLanguage>,
          error: HttpErrorResponse
        ) => {
          return UserActions.setUserLanguageFailure({ error });
        },
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly myControllerService: MyControllerService,
    private readonly dialog: MatDialog,
    private readonly translocoService: TranslocoService
  ) {}
}
