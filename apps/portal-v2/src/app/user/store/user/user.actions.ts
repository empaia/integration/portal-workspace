import { HttpErrorResponse } from '@angular/common/http';
import { LanguageCode, PostUserContactDataEntity, UserDetailsEntity } from '@api/aaa/models';
import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { UserEntity } from './user.models';

export const UserActions = createActionGroup({
  source: 'UserProfile',
  events: {
    'Load Profile': emptyProps(),
    'Load Profile Success': props<{ user: UserEntity }>(),
    'Load Profile Failure': props<{ error: HttpErrorResponse }>(),

    'Set User Details': props<{ details: UserDetailsEntity }>(),
    'Set User Details Success': props<{ user: UserEntity }>(),
    'Set User Details Failure': props<{ error: HttpErrorResponse }>(),

    'Set User Contact Data': props<{ contactData: PostUserContactDataEntity }>(),
    'Set User Contact Data Success': props<{ user: UserEntity }>(),
    'Set User Contact Data Failure': props<{ error: HttpErrorResponse }>(),

    'Set User Avatar': props<{ picture: Blob }>(),
    'Set User Avatar Success': emptyProps(),
    'Set User Avatar Failure': props<{ error: HttpErrorResponse }>(),

    'Delete User Avatar': emptyProps(),
    'Delete User Avatar Success': emptyProps(),
    'Delete User Avatar Failure': props<{ error: HttpErrorResponse }>(),

    'Delete User Account': emptyProps(),
    'Delete User Account Success': emptyProps(),
    'Delete User Account Failure': props<{ error: HttpErrorResponse }>(),

    'Open Edit User Details Dialog': emptyProps(),
    'Open Edit User Contact Data Dialog': emptyProps(),
    'Open Edit User Avatar Dialog': emptyProps(),
    'Open Delete User Account Dialog': emptyProps(),

    'Set User Language': props<{ code: LanguageCode }>(),
    'Set User Language Success': emptyProps(),
    'Set User Language Failure': props<{ error: HttpErrorResponse }>(),
  }
});

