import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on, Action } from '@ngrx/store';

import { UserActions } from './user.actions';
import { UserEntity } from './user.models';

export const USER_ENTITY_FEATURE_KEY = 'userEntity';

export interface State {
  // loggedIn: boolean; // is the user logged in / known
  loaded: boolean; // has the User list been loaded
  error?: HttpErrorResponse | undefined; // last known error (if any)

  user: UserEntity | undefined;
  // userProfile: UserInfo | undefined;
}

export interface UserPartialState {
  readonly [USER_ENTITY_FEATURE_KEY]: State;
}

// export const usersAdapter: EntityAdapter<UserEntity> =
//   createEntityAdapter<UserEntity>();

export const initialState: State = {
  // set initial required properties
  loaded: true,
  // loggedIn: false,
  error: undefined,
  user: undefined,

};

const usersReducer = createReducer(
  initialState,
  on(UserActions.loadProfileSuccess, (state, { user }): State => ({
    ...state,
    loaded: true,
    user,
  })),
  on(UserActions.loadProfileFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(UserActions.setUserDetails, (state): State => ({
    ...state,
    loaded: false
  })),
  on(UserActions.setUserDetailsSuccess, (state, { user }): State => ({
    ...state,
    loaded: true,
    user,
  })),
  on(UserActions.setUserDetailsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(UserActions.setUserContactData, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(UserActions.setUserContactDataSuccess, (state, { user }): State => ({
    ...state,
    loaded: true,
    user,
  })),
  on(UserActions.setUserContactDataFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(UserActions.setUserAvatar, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(UserActions.setUserAvatarFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(UserActions.deleteUserAvatar, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(UserActions.deleteUserAvatarFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(UserActions.deleteUserAccount, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(UserActions.deleteUserAccountSuccess, (state): State => ({
    ...state,
    ...initialState,
  })),
  on(UserActions.deleteUserAccountFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
);

export function reducer(state: State | undefined, action: Action) {
  return usersReducer(state, action);
}
