import { RegisteredUserEntity, UserProfileEntity } from '@api/aaa/models';
import { Optional } from '@shared/utility/ts-utility';


export type RegisteredUser = Optional<RegisteredUserEntity, 'member_of' | 'is_active'>;

/**
 * Interface for the 'User' data
 */
export type UserEntity = UserProfileEntity & RegisteredUser;


// export interface UserEntity {
//   id: string; // Primary ID
//   name: string;
// }

