import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslocoModule } from '@ngneat/transloco';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import * as fromUserRoot from './user-root.selectors';

import { MembershipRequestEffects } from './membership-request';
import { UnapprovedOrganizationsEffects } from './unapproved-organizations';
import { UserOrganizationsEffects } from './user-organizations';
import { UserEffects } from './user/user.effects';
import { RoleRequestEffects } from './role-request';
import { OrganizationMemberEffects } from './organization-member';
import { ActiveOrganizationEffects } from './active-organization';
import { NotificationEffects } from './notification';


/**
 * NOTE: Module is not loaded dynamically anymore
 * before: was loaded dynamically after user successfully logs in
 * Dynamic loading is initiated in AuthEffect.
 */

@NgModule({
  declarations: [

  ],
  imports: [
    CommonModule,
    TranslocoModule,
    StoreModule.forFeature(
      fromUserRoot.USER_ROOT_FEATURE_KEY,
      fromUserRoot.reducers
    ),
    EffectsModule.forFeature([
      UserEffects,
      UserOrganizationsEffects,
      MembershipRequestEffects,
      RoleRequestEffects,
      OrganizationMemberEffects,
      UnapprovedOrganizationsEffects,
      ActiveOrganizationEffects,
      NotificationEffects,
    ]),
  ],
  exports: [

  ],
})
export class UserRootStoreModule { }
