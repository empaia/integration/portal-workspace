import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';

import * as fromUser from './user/user.reducer';
import * as fromUserOrganizations from './user-organizations/user-organizations.reducer';
import * as fromMembershipRequest from './membership-request/membership-request.reducer';
import * as fromRoleRequest from './role-request/role-request.reducer';
import * as fromOrganizationMember from './organization-member/organization-member.reducer';
import * as fromUnapprovedOrganizations from './unapproved-organizations/unapproved-organizations.reducer';
import * as fromActiveOrganization from './active-organization/active-organization.reducer';
import * as fromNotification from './notification/notification.reducer';

export const USER_ROOT_FEATURE_KEY = 'user';

export interface UserRootState {
  [fromUser.USER_ENTITY_FEATURE_KEY]:
  fromUser.State;
  [fromUserOrganizations.USER_ORGANIZATIONS_FEATURE_KEY]:
  fromUserOrganizations.State;
  [fromMembershipRequest.MEMBERSHIP_REQUEST_FEATURE_KEY]:
  fromMembershipRequest.State;
  [fromRoleRequest.ROLE_REQUEST_FEATURE_KEY]:
  fromRoleRequest.State;
  [fromOrganizationMember.ORGANIZATION_MEMBER_FEATURE_KEY]:
  fromOrganizationMember.State;
  [fromUnapprovedOrganizations.UNAPPROVED_ORGANIZATIONS_FEATURE_KEY]: fromUnapprovedOrganizations.State;
  [fromActiveOrganization.ACTIVE_ORGANIZATION_FEATURE_KEY]: fromActiveOrganization.State;
  [fromNotification.NOTIFICATION_FEATURE_KEY]: fromNotification.State;
}

export interface State {
  [USER_ROOT_FEATURE_KEY]: UserRootState;
}

export function reducers(state: UserRootState | undefined, action: Action) {
  return combineReducers({
    [fromUser.USER_ENTITY_FEATURE_KEY]:
      fromUser.reducer,
    [fromUserOrganizations.USER_ORGANIZATIONS_FEATURE_KEY]:
      fromUserOrganizations.reducer,
    [fromMembershipRequest.MEMBERSHIP_REQUEST_FEATURE_KEY]:
      fromMembershipRequest.reducer,
    [fromRoleRequest.ROLE_REQUEST_FEATURE_KEY]:
      fromRoleRequest.reducer,
    [fromOrganizationMember.ORGANIZATION_MEMBER_FEATURE_KEY]:
      fromOrganizationMember.reducer,
    [fromUnapprovedOrganizations.UNAPPROVED_ORGANIZATIONS_FEATURE_KEY]:
      fromUnapprovedOrganizations.reducer,
    [fromActiveOrganization.ACTIVE_ORGANIZATION_FEATURE_KEY]:
      fromActiveOrganization.reducer,
    [fromNotification.NOTIFICATION_FEATURE_KEY]:
      fromNotification.reducer,
  })(state, action);
}

export const selectUserRootState = createFeatureSelector<UserRootState>(
  USER_ROOT_FEATURE_KEY
);
