import { UnapprovedConfidentialOrganizationProfileEntity } from '@api/aaa/models';

export type UnapprovedOrganizationsEntity = UnapprovedConfidentialOrganizationProfileEntity;
