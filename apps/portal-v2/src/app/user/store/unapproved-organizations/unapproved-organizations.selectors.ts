import { checkOrganizationContactDataStatus, checkOrganizationDetailsStatus, checkOrganizationNameStatus } from '@edit-organization/models/edit-organization.models';
import { createSelector } from '@ngrx/store';
import { RouterSelectors } from '@router/router.selectors';
import { selectUserRootState } from '../user-root.selectors';
import { UNAPPROVED_ORGANIZATIONS_FEATURE_KEY, unapprovedOrganizationsAdapter } from './unapproved-organizations.reducer';


export const {
  selectAll,
  selectEntities
} = unapprovedOrganizationsAdapter.getSelectors();

const selectUnapprovedOrganizationsEntityState = createSelector(
  selectUserRootState,
  (state) => state[UNAPPROVED_ORGANIZATIONS_FEATURE_KEY]
);

export const selectUnapprovedOrganizations = createSelector(
  selectUnapprovedOrganizationsEntityState,
  selectAll
);

export const selectUnapprovedOrganizationEntities = createSelector(
  selectUnapprovedOrganizationsEntityState,
  selectEntities,
);

export const selectSelectedUnapprovedOrganization = createSelector(
  selectUnapprovedOrganizationEntities,
  RouterSelectors.selectRouterParamUnapprovedOrganizationId,
  (orgs, orgId) => orgId ? orgs[orgId] : undefined
);

export const selectIsContactDataReady = createSelector(
  selectSelectedUnapprovedOrganization,
  (organization) => checkOrganizationContactDataStatus(organization)
);

export const selectIsDetailsReady = createSelector(
  selectSelectedUnapprovedOrganization,
  (organization) => checkOrganizationDetailsStatus(organization)
);

export const selectIsNameReady = createSelector(
  selectSelectedUnapprovedOrganization,
  (organization) => checkOrganizationNameStatus(organization)
);

export const selectIsUnapprovedOrganizationReadyToApprove = createSelector(
  selectIsNameReady,
  selectIsDetailsReady,
  selectIsContactDataReady,
  (name, details, contact) => name && details && contact
);
