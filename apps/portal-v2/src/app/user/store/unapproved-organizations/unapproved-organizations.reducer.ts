import { HttpErrorResponse } from '@angular/common/http';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { UnapprovedOrganizationsActions } from './unapproved-organizations.actions';
import { UnapprovedOrganizationsEntity } from './unapproved-organizations.model';

export const UNAPPROVED_ORGANIZATIONS_FEATURE_KEY = 'unapprovedOrganizations';


export interface State extends EntityState<UnapprovedOrganizationsEntity> {
  loaded: boolean;
  error?: HttpErrorResponse | undefined;
}

export const unapprovedOrganizationsAdapter = createEntityAdapter<UnapprovedOrganizationsEntity>({
  selectId: org => org.organization_id
});

export const initialState: State = unapprovedOrganizationsAdapter.getInitialState({
  loaded: false,
});

export const reducer = createReducer(
  initialState,

  on(UnapprovedOrganizationsActions.load, (state): State => ({
    ...state,
    loaded: false,
  })),

  on(UnapprovedOrganizationsActions.loadSuccess, (state, { orgs }): State =>
    unapprovedOrganizationsAdapter.setAll(orgs, {
      ...state,
      loaded: true,
    })
  ),
  on(UnapprovedOrganizationsActions.loadFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(UnapprovedOrganizationsActions.loadOrganization, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(UnapprovedOrganizationsActions.loadOrganizationSuccess, (state, { organization }): State =>
    unapprovedOrganizationsAdapter.upsertOne(organization, {
      ...state,
      loaded: true,
    })
  ),
  on(UnapprovedOrganizationsActions.loadOrganizationFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(UnapprovedOrganizationsActions.createOrganization, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(UnapprovedOrganizationsActions.createOrganizationSuccess, (state, { organization }): State =>
    unapprovedOrganizationsAdapter.upsertOne(organization, {
      ...state,
      loaded: true,
    })
  ),
  on(UnapprovedOrganizationsActions.createOrganizationFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(UnapprovedOrganizationsActions.deleteOrganization, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(UnapprovedOrganizationsActions.deleteOrganizationSuccess, (state, { organizationId }): State =>
    unapprovedOrganizationsAdapter.removeOne(organizationId, {
      ...state,
      loaded: true,
    })
  ),
  on(UnapprovedOrganizationsActions.deleteOrganizationFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(UnapprovedOrganizationsActions.setOrganizationName, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(UnapprovedOrganizationsActions.setOrganizationNameSuccess, (state, { organization }): State =>
    unapprovedOrganizationsAdapter.upsertOne(organization, {
      ...state,
      loaded: true,
    })
  ),
  on(UnapprovedOrganizationsActions.setOrganizationNameFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(UnapprovedOrganizationsActions.setOrganizationDetails, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(UnapprovedOrganizationsActions.setOrganizationDetailsSuccess, (state, { organization }): State =>
    unapprovedOrganizationsAdapter.upsertOne(organization, {
      ...state,
      loaded: true,
    })
  ),
  on(UnapprovedOrganizationsActions.setOrganizationDetailsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(UnapprovedOrganizationsActions.setOrganizationContactData, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(UnapprovedOrganizationsActions.setOrganizationContactDataSuccess, (state, { organization }): State =>
    unapprovedOrganizationsAdapter.upsertOne(organization, {
      ...state,
      loaded: true,
    })
  ),
  on(UnapprovedOrganizationsActions.setOrganizationContactDataFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(UnapprovedOrganizationsActions.requestApprovalFailure, (state, { error }): State => ({
    ...state,
    error,
  }))
);
