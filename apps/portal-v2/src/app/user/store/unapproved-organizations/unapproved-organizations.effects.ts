import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';

import {
  distinctUntilChanged,
  exhaustMap,
  filter,
  map,
  tap,
} from 'rxjs/operators';
import { UnapprovedOrganizationsActions } from './unapproved-organizations.actions';
import * as UnapprovedOrganizationsSelectors from './unapproved-organizations.selectors';
import { MyUnapprovedOrganizationsControllerService } from '@api/aaa/services';
import { HttpErrorResponse } from '@angular/common/http';
import { filterNullish } from '@shared/utility/rxjs-utility';
import { State } from './unapproved-organizations.reducer';
import { defaultPaged } from 'src/app/app.models';
import { OrganizationAccountState } from '@api/aaa/models';
import { ROUTER_NAVIGATED } from '@ngrx/router-store';
import { Store } from '@ngrx/store';
import { RouterSelectors } from '@router/router.selectors';
import { MatDialog } from '@angular/material/dialog';
import { EditOrganizationLogoDialogComponent } from '@edit-organization/components/edit-organization-logo-dialog/edit-organization-logo-dialog.component';
import {
  EditOrganizationDetailsDialogComponent,
  EditOrganizationDetailsDialogData,
} from '@edit-organization/components/edit-organization-details-dialog/edit-organization-details-dialog.component';
import { EditOrganizationContactDialogComponent } from '@edit-organization/components/edit-organization-contact-dialog/edit-organization-contact-dialog.component';
import { CountriesSelectors } from '@shared/store/countries';
import { EditOrganizationNameDialogComponent } from '@edit-organization/components/edit-organization-name-dialog/edit-organization-name-dialog.component';
import { AppRoutes } from '@router/router.constants';
import * as RouterActions from '@router/router.actions';
import {
  BIG_DIALOG_WIDTH,
  SMALL_DIALOG_WIDTH,
} from '@edit-organization/models/edit-organization.models';
import { EventActions } from 'src/app/events/store';

@Injectable()
export class UnapprovedOrganizationsEffects {
  initializeMyUnapprovedOrganizationsPage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        UnapprovedOrganizationsActions.initializeMyUnapprovedOrganizationsPage
      ),
      map(() => UnapprovedOrganizationsActions.load())
    );
  });

  sseRefresh$ = createEffect(() => {
    // TODO: filter - on event action type
    return this.actions$.pipe(
      ofType(EventActions.sseUpdateUnapprovedOrganizations),
      map(() => UnapprovedOrganizationsActions.load())
    );
  });

  // when a user edits an organization and reloads the page
  initializeUnapprovedOrganizationOnRoute$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ROUTER_NAVIGATED),
      concatLatestFrom(() =>
        this.store.select(
          RouterSelectors.selectRouterParamUnapprovedOrganizationId
        )
      ),
      map(([, id]) => id),
      distinctUntilChanged(),
      filterNullish(),
      map((organizationId) =>
        UnapprovedOrganizationsActions.loadOrganization({ organizationId })
      )
    );
  });

  loadUnapprovedOrganizations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UnapprovedOrganizationsActions.load),
      fetch({
        run: (
          _action: ReturnType<typeof UnapprovedOrganizationsActions.load>,
          _state: State
        ) => {
          return this.myUnapprovedOrganizationsControllerService
            .queryUnapprovedOrganizations({
              ...defaultPaged,
              body: {
                organization_account_states: [
                  OrganizationAccountState.AWAITING_ACTIVATION,
                  OrganizationAccountState.REQUIRES_ACTIVATION,
                ],
              },
            })
            .pipe(
              map((response) => response.organizations),
              tap((response) =>
                console.log('getUnapprovedOrganizations', response)
              ),
              filterNullish(),
              map((orgs) =>
                UnapprovedOrganizationsActions.loadSuccess({ orgs })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof UnapprovedOrganizationsActions.load>,
          error: HttpErrorResponse
        ) => {
          return UnapprovedOrganizationsActions.loadFailure({ error });
        },
      })
    );
  });

  loadUnapprovedOrganization$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UnapprovedOrganizationsActions.loadOrganization),
      fetch({
        run: (
          action: ReturnType<
            typeof UnapprovedOrganizationsActions.loadOrganization
          >,
          _state: State
        ) => {
          return this.myUnapprovedOrganizationsControllerService
            .getUnapprovedOrganization({
              organization_id: action.organizationId,
            })
            .pipe(
              map((organization) =>
                UnapprovedOrganizationsActions.loadOrganizationSuccess({
                  organization,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof UnapprovedOrganizationsActions.loadOrganization
          >,
          error: HttpErrorResponse
        ) => {
          return UnapprovedOrganizationsActions.loadOrganizationFailure({
            error,
          });
        },
      })
    );
  });

  createOrganizationReady$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UnapprovedOrganizationsActions.createOrganization),
      pessimisticUpdate({
        run: (
          _action: ReturnType<
            typeof UnapprovedOrganizationsActions.createOrganization
          >,
          _state: State
        ) => {
          return this.myUnapprovedOrganizationsControllerService
            .createUnapprovedOrganization({
              body: {},
            })
            .pipe(
              map((organization) =>
                UnapprovedOrganizationsActions.createOrganizationSuccess({
                  organization,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof UnapprovedOrganizationsActions.createOrganization
          >,
          error: HttpErrorResponse
        ) => {
          return UnapprovedOrganizationsActions.createOrganizationFailure({
            error,
          });
        },
      })
    );
  });

  // navigate to edit unapproved organization page after user created an organization
  navigateAfterOrganizationCreation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UnapprovedOrganizationsActions.createOrganizationSuccess),
      map((action) => action.organization.organization_id),
      map((id) =>
        RouterActions.navigate({
          path: [AppRoutes.MY_ROUTE, AppRoutes.MY_ORGANIZATIONS_ROUTE, id],
        })
      )
    );
  });

  // navigate back to my organizations overview page after approved was clicked
  navigateAfterApprovedClicked$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UnapprovedOrganizationsActions.requestApprovalSuccess),
      map(() =>
        RouterActions.navigate({
          path: [AppRoutes.MY_ROUTE, AppRoutes.MY_ORGANIZATIONS_ROUTE],
        })
      )
    );
  });

  deleteOrganization$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UnapprovedOrganizationsActions.deleteOrganization),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof UnapprovedOrganizationsActions.deleteOrganization
          >,
          _state: State
        ) => {
          return this.myUnapprovedOrganizationsControllerService
            .deleteUnapprovedOrganization({
              organization_id: action.organizationId,
            })
            .pipe(
              // remove from state when removed in backend
              map(() =>
                UnapprovedOrganizationsActions.deleteOrganizationSuccess({
                  organizationId: action.organizationId,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof UnapprovedOrganizationsActions.deleteOrganization
          >,
          error: HttpErrorResponse
        ) => {
          return UnapprovedOrganizationsActions.deleteOrganizationFailure({
            error,
          });
        },
      })
    );
  });

  setOrganizationLogo$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UnapprovedOrganizationsActions.setOrganizationLogo),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof UnapprovedOrganizationsActions.setOrganizationLogo
          >,
          _state: State
        ) => {
          return this.myUnapprovedOrganizationsControllerService
            .setUnapprovedOrganizationLogo({
              organization_id: action.organizationId,
              body: {
                logo: action.logo,
              },
            })
            .pipe(
              // this route has no response data, get the organization data again, Backend-Issue: #99
              map(() =>
                UnapprovedOrganizationsActions.loadOrganization({
                  organizationId: action.organizationId,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof UnapprovedOrganizationsActions.setOrganizationLogo
          >,
          error: HttpErrorResponse
        ) => {
          return UnapprovedOrganizationsActions.setOrganizationLogoFailure({
            error,
          });
        },
      })
    );
  });

  setOrganizationName$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UnapprovedOrganizationsActions.setOrganizationName),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof UnapprovedOrganizationsActions.setOrganizationName
          >,
          _state: State
        ) => {
          return this.myUnapprovedOrganizationsControllerService
            .updateUnapprovedOrganizationName({
              organization_id: action.organizationId,
              body: {
                organization_name: action.organizationName,
              },
            })
            .pipe(
              map((organization) =>
                UnapprovedOrganizationsActions.setOrganizationNameSuccess({
                  organization,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof UnapprovedOrganizationsActions.setOrganizationName
          >,
          error: HttpErrorResponse
        ) => {
          return UnapprovedOrganizationsActions.setOrganizationNameFailure({
            error,
          });
        },
      })
    );
  });

  setOrganizationDetails$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UnapprovedOrganizationsActions.setOrganizationDetails),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof UnapprovedOrganizationsActions.setOrganizationDetails
          >,
          _state: State
        ) => {
          return this.myUnapprovedOrganizationsControllerService
            .updateUnapprovedOrganizationDetails({
              organization_id: action.organizationId,
              body: action.details,
            })
            .pipe(
              map((organization) =>
                UnapprovedOrganizationsActions.setOrganizationDetailsSuccess({
                  organization,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof UnapprovedOrganizationsActions.setOrganizationDetails
          >,
          error: HttpErrorResponse
        ) => {
          return UnapprovedOrganizationsActions.setOrganizationDetailsFailure({
            error,
          });
        },
      })
    );
  });

  setOrganizationContactData$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UnapprovedOrganizationsActions.setOrganizationContactData),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof UnapprovedOrganizationsActions.setOrganizationContactData
          >,
          _state: State
        ) => {
          return this.myUnapprovedOrganizationsControllerService
            .updateUnapprovedOrganizationContactData({
              organization_id: action.organizationId,
              body: action.contactData,
            })
            .pipe(
              map((organization) =>
                UnapprovedOrganizationsActions.setOrganizationContactDataSuccess(
                  { organization }
                )
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof UnapprovedOrganizationsActions.setOrganizationContactData
          >,
          error: HttpErrorResponse
        ) => {
          return UnapprovedOrganizationsActions.setOrganizationContactDataFailure(
            { error }
          );
        },
      })
    );
  });

  requestApproval$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UnapprovedOrganizationsActions.requestApproval),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof UnapprovedOrganizationsActions.requestApproval
          >,
          _state: State
        ) => {
          return this.myUnapprovedOrganizationsControllerService
            .requestUnapprovedOrganizationApproval({
              organization_id: action.organizationId,
            })
            .pipe(
              map(() => UnapprovedOrganizationsActions.requestApprovalSuccess())
            );
        },
        onError: (
          _action: ReturnType<
            typeof UnapprovedOrganizationsActions.requestApproval
          >,
          error: HttpErrorResponse
        ) => {
          return UnapprovedOrganizationsActions.requestApprovalFailure({
            error,
          });
        },
      })
    );
  });

  openUnapprovedOrganizationLogoDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UnapprovedOrganizationsActions.openEditOrganizationLogoDialog),
      map((action) => action.organizationId),
      concatLatestFrom(() =>
        this.store.select(
          UnapprovedOrganizationsSelectors.selectUnapprovedOrganizationEntities
        )
      ),
      map(([id, organizations]) => organizations[id]),
      filterNullish(),
      exhaustMap((organization) =>
        this.dialog
          .open(EditOrganizationLogoDialogComponent, {
            data: organization.logo_url,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map((logo) =>
              UnapprovedOrganizationsActions.setOrganizationLogo({
                organizationId: organization.organization_id,
                logo,
              })
            )
          )
      )
    );
  });

  openUnapprovedOrganizationNameDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UnapprovedOrganizationsActions.openEditOrganizationNameDialog),
      map((action) => action.organizationId),
      concatLatestFrom(() =>
        this.store.select(
          UnapprovedOrganizationsSelectors.selectUnapprovedOrganizationEntities
        )
      ),
      map(([id, organizations]) => organizations[id]),
      filterNullish(),
      exhaustMap((organization) =>
        this.dialog
          .open(EditOrganizationNameDialogComponent, {
            data: organization.organization_name,
            width: SMALL_DIALOG_WIDTH,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map((organizationName) =>
              UnapprovedOrganizationsActions.setOrganizationName({
                organizationId: organization.organization_id,
                organizationName,
              })
            )
          )
      )
    );
  });

  openUnapprovedOrganizationDetailsDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UnapprovedOrganizationsActions.openEditOrganizationDetailsDialog),
      map((action) => action.organizationId),
      concatLatestFrom(() =>
        this.store.select(
          UnapprovedOrganizationsSelectors.selectUnapprovedOrganizationEntities
        )
      ),
      map(([id, organizations]) => organizations[id]),
      filterNullish(),
      exhaustMap((organization) =>
        this.dialog
          .open(EditOrganizationDetailsDialogComponent, {
            data: <EditOrganizationDetailsDialogData>{
              details: organization.details,
              editableCategories: true,
            },
            width: BIG_DIALOG_WIDTH,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map((details) =>
              UnapprovedOrganizationsActions.setOrganizationDetails({
                organizationId: organization.organization_id,
                details,
              })
            )
          )
      )
    );
  });

  openUnapprovedOrganizationContactDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UnapprovedOrganizationsActions.openEditOrganizationContactDialog),
      map((action) => action.organizationId),
      concatLatestFrom(() => [
        this.store.select(
          UnapprovedOrganizationsSelectors.selectUnapprovedOrganizationEntities
        ),
        this.store.select(CountriesSelectors.selectAllCountries),
      ]),
      map(([id, organizations, countries]) => ({
        organization: organizations[id],
        countries,
      })),
      filter((data) => !!data.organization),
      exhaustMap((data) =>
        this.dialog
          .open(EditOrganizationContactDialogComponent, {
            data: {
              contactData: data.organization?.contact_data,
              countries: data.countries,
            },
            width: BIG_DIALOG_WIDTH,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map((contactData) =>
              UnapprovedOrganizationsActions.setOrganizationContactData({
                organizationId: data.organization?.organization_id as string,
                contactData,
              })
            )
          )
      )
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly dialog: MatDialog,
    private readonly myUnapprovedOrganizationsControllerService: MyUnapprovedOrganizationsControllerService
  ) {}
}
