import { HttpErrorResponse } from '@angular/common/http';
import { ConfidentialOrganizationContactDataEntity } from '@api/aaa/models';
import { PostOrganizationDetailsEntity } from '@edit-organization/models/edit-organization.models';
import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { UnapprovedOrganizationsEntity } from './unapproved-organizations.model';



export const UnapprovedOrganizationsActions = createActionGroup({
  source: 'UnapprovedOrganizations',
  events: {
    'Initialize My Unapproved Organizations Page': emptyProps(),
    
    'Load': emptyProps(),
    'Load Success': props<{ orgs: UnapprovedOrganizationsEntity[] }>(),
    'Load Failure': props<{ error: HttpErrorResponse }>(),
    'Load Organization': props<{ organizationId: string }>(),
    'Load Organization Success': props<{ organization: UnapprovedOrganizationsEntity }>(),
    'Load Organization Failure': props<{ error: HttpErrorResponse }>(),
    'Create Organization': emptyProps(),
    'Create Organization Success': props<{ organization: UnapprovedOrganizationsEntity }>(),
    'Create Organization Failure': props<{ error: HttpErrorResponse }>(),
    'Delete Organization': props<{ organizationId: string }>(),
    'Delete Organization Success': props<{ organizationId: string }>(),
    'Delete Organization Failure': props<{ error: HttpErrorResponse }>(),
    'Set Organization Logo': props<{ organizationId: string, logo: Blob }>(),
    'Set Organization Logo Failure': props<{ error: HttpErrorResponse }>(),
    'Set Organization Name': props<{
      organizationId: string,
      organizationName: string
    }>(),
    'Set Organization Name Success': props<{ organization: UnapprovedOrganizationsEntity }>(),
    'Set Organization Name Failure': props<{ error: HttpErrorResponse }>(),
    'Set Organization Details': props<{
      organizationId: string,
      details: PostOrganizationDetailsEntity,
    }>(),
    'Set Organization Details Success': props<{ organization: UnapprovedOrganizationsEntity }>(),
    'Set Organization Details Failure': props<{ error: HttpErrorResponse }>(),
    'Set Organization Contact Data': props<{
      organizationId: string,
      contactData: ConfidentialOrganizationContactDataEntity
    }>(),
    'Set Organization Contact Data Success': props<{ organization: UnapprovedOrganizationsEntity }>(),
    'set Organization Contact Data Failure': props<{ error: HttpErrorResponse }>(),
    'Request Approval': props<{ organizationId: string }>(),
    'Request Approval Success': emptyProps(),
    'Request Approval Failure': props<{ error: HttpErrorResponse }>(),


    'Open Edit Organization Logo Dialog': props<{ organizationId: string }>(),
    'Open Edit Organization Name Dialog': props<{ organizationId: string }>(),
    'Open Edit Organization Details Dialog': props<{ organizationId: string }>(),
    'Open Edit Organization Contact Dialog': props<{ organizationId: string }>()
  },
});
