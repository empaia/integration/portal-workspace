import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { OrganizationControllerV2Service } from '@api/aaa/services';
import { TranslocoService } from '@ngneat/transloco';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { State } from './active-organization.reducer';
import { ActiveOrganizationActions } from './active-organization.actions';
import * as UserOrganizationsSelectors from '@user/store/user-organizations/user-organizations.selectors';
import * as RouterActions from '@router/router.actions';
import { exhaustMap, filter, map, mergeMap } from 'rxjs/operators';
import { CriticalActionDialogComponent } from '@shared/components/critical-action-dialog/critical-action-dialog.component';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';
import { filterNullish } from '@shared/utility/rxjs-utility';
import { AppRoutes } from '@router/router.constants';
import { UserOrganizationsActions } from '@user/store/user-organizations';
import { HttpErrorResponse } from '@angular/common/http';
import { ActiveOrganizationContainerActions } from '@user/my/containers/active-organization-container/active-organization-container.actions';

@Injectable()
export class ActiveOrganizationEffects {
  openLeaveConfirmDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ActiveOrganizationActions.openLeaveActiveOrganizationDialog),
      concatLatestFrom(() =>
        this.store.select(
          UserOrganizationsSelectors.selectSelectedUserOrganizationName
        )
      ),
      map(([, organizationName]) => organizationName),
      exhaustMap((organizationName) =>
        this.dialog
          .open(CriticalActionDialogComponent, {
            data: {
              message: this.translocoService.translate(
                'active_organization.leave_confirm',
                { orgName: organizationName }
              ),
            },
          })
          .afterClosed()
          .pipe(
            filter((response) => !!response),
            map(() => ActiveOrganizationActions.leaveActiveOrganization())
          )
      )
    );
  });

  leaveActiveOrg$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ActiveOrganizationActions.leaveActiveOrganization),
      pessimisticUpdate({
        run: (
          _action: ReturnType<
            typeof ActiveOrganizationActions.leaveActiveOrganization
          >,
          _state: State
        ) => {
          return this.organizationControllerV2Service.leaveOrganization().pipe(
            concatLatestFrom(() =>
              this.store.select(
                UserOrganizationsSelectors.selectUserOrganizationsDefaultOrgId
              )
            ),
            map(([_, id]) => id),
            filterNullish(),
            map((id) =>
              ActiveOrganizationActions.leaveActiveOrganizationSuccess({
                organizationId: id,
              })
            )
          );
        },
        onError: (
          _action: ReturnType<
            typeof ActiveOrganizationActions.leaveActiveOrganization
          >,
          error: HttpErrorResponse
        ) => {
          return ActiveOrganizationActions.leaveActiveOrganizationFailure({
            error,
          });
        },
      })
    );
  });

  canLeaveOrg$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ActiveOrganizationActions.canLeaveActiveOrganization),
      fetch({
        run: (
          _action: ReturnType<
            typeof ActiveOrganizationActions.canLeaveActiveOrganization
          >,
          _state: State
        ) => {
          return this.organizationControllerV2Service
            .canLeaveOrganization()
            .pipe(
              map((response) => response.can_leave),
              map((canLeave) =>
                ActiveOrganizationActions.canLeaveActiveOrganizationSuccess({
                  canLeave,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ActiveOrganizationActions.canLeaveActiveOrganization
          >,
          error: HttpErrorResponse
        ) => {
          return ActiveOrganizationActions.canLeaveActiveOrganizationFailure({
            error,
          });
        },
      })
    );
  });

  leaveSuccessNavigateAway$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ActiveOrganizationActions.leaveActiveOrganizationSuccess),
      map((_action) =>
        RouterActions.navigate({
          path: [AppRoutes.MY_ROUTE, AppRoutes.MY_ORGANIZATIONS_ROUTE],
        })
      )
    );
  });

  leaveSuccessActions$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ActiveOrganizationActions.leaveActiveOrganizationSuccess),
      map((action) => action.organizationId),
      mergeMap((organizationId) => [
        UserOrganizationsActions.unsetDefaultOrganization(),
        UserOrganizationsActions.removeOrganization({ organizationId }),
      ])
    );
  });

  initCantLeave$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ActiveOrganizationContainerActions.initializeActiveOrganizationContainer,
      ),
      map(() => ActiveOrganizationActions.canLeaveActiveOrganization())
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly organizationControllerV2Service: OrganizationControllerV2Service,
    private readonly dialog: MatDialog,
    private readonly translocoService: TranslocoService
  ) {}
}
