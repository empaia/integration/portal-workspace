import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import { ActiveOrganizationActions } from './active-organization.actions';


export const ACTIVE_ORGANIZATION_FEATURE_KEY = 'activeOrganization';

export interface State {
  canLeave: boolean;
  loaded: boolean;
  error?: HttpErrorResponse | undefined;
}

export const initialState: State = {
  canLeave: false,
  loaded: true,
  error: undefined,
};

export const reducer = createReducer(
  initialState,
  on(ActiveOrganizationActions.canLeaveActiveOrganization, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ActiveOrganizationActions.canLeaveActiveOrganizationSuccess, (state, { canLeave }): State => ({
    ...state,
    canLeave,
    loaded: true,
  })),
  on(ActiveOrganizationActions.canLeaveActiveOrganizationFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
);
