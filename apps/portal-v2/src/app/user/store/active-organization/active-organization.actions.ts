import { HttpErrorResponse } from '@angular/common/http';
import { createActionGroup, emptyProps, props } from '@ngrx/store';

export const ActiveOrganizationActions = createActionGroup({
  source: 'Active Organization',
  events: {
    'Can Leave Active Organization': emptyProps(),
    'Can Leave Active Organization Success': props<{ canLeave: boolean }>(),
    'Can Leave Active Organization Failure': props<{ error: HttpErrorResponse }>(),
    'Leave Active Organization': emptyProps(),
    'Leave Active Organization Success': props<{ organizationId: string }>(),
    'Leave Active Organization Failure': props<{ error: HttpErrorResponse }>(),
    'Open Leave Active Organization Dialog': emptyProps(),
  }
});
