import { createSelector } from '@ngrx/store';
import { selectUserRootState } from '../user-root.selectors';
import { ACTIVE_ORGANIZATION_FEATURE_KEY } from './active-organization.reducer';

export const selectActiveOrganizationState = createSelector(
  selectUserRootState,
  (state) => state[ACTIVE_ORGANIZATION_FEATURE_KEY]
);

export const selectCanLeaveActiveOrganization = createSelector(
  selectActiveOrganizationState,
  (state) => state.canLeave
);

export const selectActiveOrganizationLoaded = createSelector(
  selectActiveOrganizationState,
  (state) => state.loaded
);

export const selectActiveOrganizationError = createSelector(
  selectActiveOrganizationState,
  (state) => state.error
);
