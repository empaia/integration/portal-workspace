import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';

import { exhaustMap, map, tap } from 'rxjs/operators';
import { RoleRequestActions } from './role-request.actions';
import { OrganizationControllerV2Service } from '@api/aaa/services';
import { HttpErrorResponse } from '@angular/common/http';
import { filterNullish } from '@shared/utility/rxjs-utility';
import { State } from './role-request.reducer';
import { defaultPaginationParams } from '@shared/api/default-params';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
import { AuthSelectors } from '@auth/store/auth';
import { RoleRequestDialogComponent } from '@user/my/components/role-request-dialog/role-request-dialog.component';
import { UserOrganizationsSelectors } from '../user-organizations';
import { EventActions } from 'src/app/events/store';
import { PostOrganizationUserRoleRequestEntity } from '@api/aaa/models';
import { ActiveOrganizationContainerActions } from '@user/my/containers/active-organization-container/active-organization-container.actions';

@Injectable()
export class RoleRequestEffects {
  initializeMyRoleRequestPage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        RoleRequestActions.initializeRoleRequestPage,
        ActiveOrganizationContainerActions.initializeActiveOrganizationContainer,
      ),
      map(() => RoleRequestActions.load())
    );
  });

  sseRefresh$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(EventActions.sseUpdateRoleRequests),
      // TODO: filter - update on APPROVED/REJECTED
      map(() => RoleRequestActions.load())
    );
  });

  loadRoleRequests$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(RoleRequestActions.load),
      fetch({
        run: (
          _action: ReturnType<typeof RoleRequestActions.load>,
          _state: State
        ) => {
          return this.organizationControllerV2Service
            .getOrganizationUserRoleRequests({
              ...defaultPaginationParams,
            })
            .pipe(
              map((response) => response.organization_user_role_requests),
              // tap(requests => console.log('getRoleRequests', requests)),
              map((requests) =>
                RoleRequestActions.loadSuccess({ requests: requests })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof RoleRequestActions.load>,
          error: HttpErrorResponse
        ) => {
          return RoleRequestActions.loadFailure({ error });
        },
      })
    );
  });

  revokeRoleRequest$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(RoleRequestActions.revoke),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof RoleRequestActions.revoke>,
          _state: State
        ) => {
          return this.organizationControllerV2Service
            .revokeOrganizationUserRoleRequest({
              role_request_id: action.id,
            })
            .pipe(
              map(() => RoleRequestActions.revokeSuccess({ id: action.id }))
            );
        },
        onError: (
          _action: ReturnType<typeof RoleRequestActions.revoke>,
          error
        ) => {
          return RoleRequestActions.revokeFailure({ error });
        },
      })
    );
  });

  refreshOnRevoke$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(RoleRequestActions.revokeSuccess),
      map(() => RoleRequestActions.load())
    );
  });

  openRequestRoleChangeDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(RoleRequestActions.openNewRequestDialog),
      concatLatestFrom((_action) => [
        this.store.select(
          UserOrganizationsSelectors.selectSelectedUserOrganization
        ),
        this.store.select(AuthSelectors.selectRolesInSelectedOrganization),
      ]),
      exhaustMap(([, organization, roles]) =>
        this.dialog
          .open(RoleRequestDialogComponent, {
            data: { organization, roles },
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map((request) => request as PostOrganizationUserRoleRequestEntity),
            map((request) => RoleRequestActions.sendNewRequest({ request }))
          )
      )
    );
  });

  // requestRoleChange$ = createEffect(() => {
  //   return this.actions$.pipe(
  //     ofType(RoleRequestActions.sendNewRequest),
  //     mergeMap((action) =>
  //       this.organizationControllerV2Service.requestOrganizationUserRole({
  //         ...selectedOrgIdParam,
  //         body: { organization_user_role: action.role }
  //       }).pipe(
  //         // catchError((err) =>
  //         //   of(''))
  //       )
  //     )
  //   );
  // }, { dispatch: false });

  requestRoleChangeV2$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(RoleRequestActions.sendNewRequest),
      tap((res) => console.log('sendNewRequest ', res)),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof RoleRequestActions.sendNewRequest>,
          _state: State
        ) => {
          return this.organizationControllerV2Service
            .requestOrganizationUserRole({
              body: action.request,
            })
            .pipe(
              tap((res) => console.log('sendNewRequest ', res)),
              map((_res) => RoleRequestActions.sendNewRequestSuccess())
            );
        },
        onError: (
          _action: ReturnType<typeof RoleRequestActions.sendNewRequest>,
          error
        ) => {
          return RoleRequestActions.sendNewRequestFailure({ error });
        },
      })
    );
  });

  refreshOnSend$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(RoleRequestActions.sendNewRequestSuccess),
      map(() => RoleRequestActions.load())
    );
  });

  constructor(
    private actions$: Actions,
    private readonly organizationControllerV2Service: OrganizationControllerV2Service,
    private readonly store: Store,
    private readonly dialog: MatDialog
  ) {}
}
