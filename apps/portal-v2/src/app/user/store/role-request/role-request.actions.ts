import { HttpErrorResponse } from '@angular/common/http';
import { PostOrganizationUserRoleRequestEntity } from '@api/aaa/models';
import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { RoleRequestEntity } from './role-request.model';



export const RoleRequestActions = createActionGroup({
  source: 'RoleRequest',
  events: {
    'Initialize Role Request Page': emptyProps(),

    'Load': emptyProps(),
    'Load Success': props<{ requests: RoleRequestEntity[] }>(),
    'Load Failure': props<{ error: HttpErrorResponse }>(),

    'Revoke': props<{ id: number }>(),
    'Revoke Success': props<{ id: number }>(),
    'Revoke Failure': props<{ error: HttpErrorResponse }>(),

    'Open new Request Dialog': emptyProps(),
    'Cancel Request Dialog': emptyProps(),
    // 'Send new Request': props<{ organizationId: string, role: OrganizationUserRoleV2 }>(),
    'Send new Request': props<{ request: PostOrganizationUserRoleRequestEntity }>(),
    'Send new Request Success': emptyProps(),
    'Send new Request Failure': props<{ error: HttpErrorResponse }>(),
  },
});
