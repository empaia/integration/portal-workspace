import { RequestState } from '@api/aaa/models';
import { createSelector } from '@ngrx/store';
import { selectUserRootState } from '../user-root.selectors';
import { roleRequestsAdapter, ROLE_REQUEST_FEATURE_KEY } from './role-request.reducer';

// role requests
const selectRoleRequestState = createSelector(
  selectUserRootState,
  (state) => state[ROLE_REQUEST_FEATURE_KEY]
);


export const {
  selectAll,
  selectEntities
} = roleRequestsAdapter.getSelectors();


export const selectRoleRequests = createSelector(
  selectRoleRequestState,
  selectAll
);

export const selectHasOpenRequests = createSelector(
  selectRoleRequests,
  (req) => req.some(r => r.request_state === RequestState.REQUESTED)
);
