import { HttpErrorResponse } from '@angular/common/http';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { RoleRequestActions } from './role-request.actions';
import { RoleRequestEntity } from './role-request.model';

export const ROLE_REQUEST_FEATURE_KEY = 'roleRequest';


export interface State extends EntityState<RoleRequestEntity> {
  loaded: boolean;
  error?: HttpErrorResponse | undefined;
}

// TODO: sort by date
export function sortByDate(a: RoleRequestEntity, b: RoleRequestEntity): number {
  return a.created_at > b.created_at ? -1 : 1;
}


export const roleRequestsAdapter = createEntityAdapter<RoleRequestEntity>({
  selectId: request => request.organization_user_role_request_id
});

export const initialState: State = roleRequestsAdapter.getInitialState({
  loaded: false,
});



export const reducer = createReducer(
  initialState,

  on(RoleRequestActions.load, (state): State => ({
    ...state,
    loaded: false,
  })),

  on(RoleRequestActions.loadSuccess, (state, { requests }): State =>
    roleRequestsAdapter.setAll(requests, {
      ...state,
      loaded: true,
    })
  ),

  on(RoleRequestActions.revokeSuccess, (state, { id }): State =>
    roleRequestsAdapter.removeOne(id, {
      ...state,
      loaded: true,
    })
  ),

);