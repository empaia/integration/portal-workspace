import { OrganizationUserRoleRequestEntity} from '@api/aaa/models';
export type RoleRequestEntity = OrganizationUserRoleRequestEntity;
