import { HttpErrorResponse } from '@angular/common/http';
import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { MembershipRequestEntity } from './membership-request.model';



export const MembershipRequestActions = createActionGroup({
  source: 'MembershipRequest',
  events: {
    'Initialize My Membership Request Page': emptyProps(),

    'Load': emptyProps(),
    'Load Success': props<{ requests: MembershipRequestEntity[] }>(),
    'Load Failure': props<{ error: HttpErrorResponse }>(),

    'Request Organization Membership': props<{ organizationId: string }>(),
    'Request Organization Membership Success': props<{ request: MembershipRequestEntity }>(),
    'Request Organization Membership Failure': props<{ error: HttpErrorResponse }>(),

    'Revoke': props<{ id: number }>(),
    'Revoke Success': props<{ request: MembershipRequestEntity }>(),
    // 'Revoke Success': props<{ id: number }>(),
    'Revoke Failure': props<{ error: HttpErrorResponse }>(),
  },
});
