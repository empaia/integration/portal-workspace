import { HttpErrorResponse } from '@angular/common/http';
import { RequestState } from '@api/aaa/models';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { MembershipRequestActions } from './membership-request.actions';
import { MembershipRequestEntity } from './membership-request.model';

export const MEMBERSHIP_REQUEST_FEATURE_KEY = 'membershipRequest';


export interface State extends EntityState<MembershipRequestEntity> {
  loaded: boolean;
  error?: HttpErrorResponse | undefined;
}

export function sortByDate(a: MembershipRequestEntity, b: MembershipRequestEntity): number {
  // sort descending by creation date (newest first)
  return a.created_at > b.created_at ? -1 : 1;
}


export const userOrganizationAdapter = createEntityAdapter<MembershipRequestEntity>({
  selectId: request => request.membership_request_id,
  sortComparer: sortByDate,
});

export const initialState: State = userOrganizationAdapter.getInitialState({
  loaded: false,
});


export const reducer = createReducer(
  initialState,

  on(MembershipRequestActions.load, (state): State => ({
    ...state,
    loaded: false,
  })),

  on(MembershipRequestActions.loadSuccess, (state, { requests }): State =>
    userOrganizationAdapter.setAll(requests, {
      ...state,
      loaded: true,
    })
  ),
  on(MembershipRequestActions.loadFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),

  on(MembershipRequestActions.requestOrganizationMembership, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(MembershipRequestActions.requestOrganizationMembershipSuccess, (state, { request }): State =>
    userOrganizationAdapter.upsertOne(request, {
      ...state,
      loaded: true,
    })
  ),
  on(MembershipRequestActions.requestOrganizationMembershipFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),

  // on(MembershipRequestActions.revoke, (state): State => ({
  //   ...state,
  //   loaded: false,
  // })),
  on(MembershipRequestActions.revoke, (state, { id }): State =>
    // TODO: use upsert
    userOrganizationAdapter.updateOne({
      id,
      changes: {
        request_state: RequestState.REVOKED
      }
    }, {
      ...state,
      loaded: true,
    })),

  on(MembershipRequestActions.revokeSuccess, (state): State => ({
    ...state,
    loaded: true,
  })),
  // TODO: use upsert
  // userOrganizationAdapter.upsertOne(request, {
  //   ...state,
  //   loaded: true,
  // })
  on(MembershipRequestActions.revokeFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),

);
