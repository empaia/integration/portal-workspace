import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';

import { map, mergeMap, tap } from 'rxjs/operators';
import { MembershipRequestActions } from './membership-request.actions';
import { MyControllerService } from '@api/aaa/services';
import { HttpErrorResponse } from '@angular/common/http';
import { State } from './membership-request.reducer';
import { DEFAULT_PAGE_SIZE } from 'src/app/app.models';
import { UserActions } from '../user';
import { EventActions } from 'src/app/events/store';

@Injectable()
export class MembershipRequestEffects {
  // after login load users organizations
  // we need the organizations to select the default org
  loadAfterLogin$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.loadProfileSuccess),
      map(() => MembershipRequestActions.load())
    );
  });

  // // TODO remove if loading after login
  // initializeMyMembershipRequestPage$ = createEffect(() => {
  //   return this.actions$.pipe(
  //     ofType(MembershipRequestActions.initializeMyMembershipRequestPage),
  //     map(() => MembershipRequestActions.load()),
  //   );
  // });

  sseRefresh$ = createEffect(() => {
    // TODO: filter - on event action type
    return this.actions$.pipe(
      ofType(EventActions.sseUpdateMembershipRequests),
      tap(() => ''),
      map(() => MembershipRequestActions.load())
    );
  });

  loadMembershipRequests$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(MembershipRequestActions.load),
      fetch({
        run: (
          _action: ReturnType<typeof MembershipRequestActions.load>,
          _state: State
        ) => {
          return this.myControllerService
            .getMembershipRequests({
              page_size: DEFAULT_PAGE_SIZE,
              page: 0,
            })
            .pipe(
              map((response) => response.membership_requests),
              // tap(requests => console.log('getMembershipRequests', requests)),
              map((requests) =>
                MembershipRequestActions.loadSuccess({ requests: requests })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof MembershipRequestActions.load>,
          error: HttpErrorResponse
        ) => {
          return MembershipRequestActions.loadFailure({ error });
        },
      })
    );
  });

  joinOrganizationRequest$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(MembershipRequestActions.requestOrganizationMembership),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof MembershipRequestActions.requestOrganizationMembership
          >,
          _state: State
        ) => {
          return this.myControllerService
            .requestMembership({
              body: {
                organization_id: action.organizationId,
              },
            })
            .pipe(
              map((request) =>
                MembershipRequestActions.requestOrganizationMembershipSuccess({
                  request,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof MembershipRequestActions.requestOrganizationMembership
          >,
          error: HttpErrorResponse
        ) => {
          return MembershipRequestActions.requestOrganizationMembershipFailure({
            error,
          });
        },
      })
    );
  });

  deleteRequest$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(MembershipRequestActions.revoke),
        mergeMap((action) =>
          this.myControllerService.revokeMembershipRequest({
            membership_request_id: action.id,
          })
        )
      );
    },
    { dispatch: false }
  );

  // TODO: backend issue is solved - is this still needed? or already fixed somewhere else?
  // revokeRequest$ = createEffect(() => {
  //   return this.actions$.pipe(
  //     ofType(MembershipRequestActions.revoke),
  //     pessimisticUpdate({
  //       run: (
  //         action: ReturnType<typeof MembershipRequestActions.revoke>,
  //         _state: State
  //       ) => {
  //         return this.myControllerService.revokeMembershipRequest({
  //           membership_request_id: action.id
  //         }).pipe(
  //           tap(response => console.log(response)),

  //           // map(request => MembershipRequestActions.revokeSuccess({ request }))
  //           map((request) => MembershipRequestActions.revokeSuccess({ request }))
  //         );
  //       },
  //       onError: (_action: ReturnType<typeof MembershipRequestActions.revoke>, error: HttpErrorResponse) => {
  //         return MembershipRequestActions.revokeFailure({ error });
  //       }
  //     })
  //   );
  // });

  constructor(
    private actions$: Actions,
    private readonly myControllerService: MyControllerService
  ) {}
}
