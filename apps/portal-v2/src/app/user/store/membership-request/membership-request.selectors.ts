import { createSelector } from '@ngrx/store';
import { OrganizationsSelectors } from '@public/store/organizations';
import { selectUserRootState } from '../user-root.selectors';
import { MEMBERSHIP_REQUEST_FEATURE_KEY, userOrganizationAdapter } from './membership-request.reducer';

// membership requests
const selectMembershipRequestState = createSelector(
  selectUserRootState,
  (state) => state[MEMBERSHIP_REQUEST_FEATURE_KEY]
);


export const {
  selectAll,
  selectEntities
} = userOrganizationAdapter.getSelectors();


export const selectMembershipRequests = createSelector(
  selectMembershipRequestState,
  selectAll
);

export const selectMembershipRequestsOfSelectedOrg = createSelector(
  selectMembershipRequests,
  OrganizationsSelectors.selectSelectedOrganization,
  (req, org) => req.filter(r => r.organization_id === org?.organization_id)
);

export const selectLastMembershipRequestsOfSelectedOrg = createSelector(
  selectMembershipRequestsOfSelectedOrg,
  (req) => req.sort((a, b) => a.created_at - b.created_at).at(-1)
);

