import { createSelector } from '@ngrx/store';
import { selectUserRootState } from '../user-root.selectors';
import { State, NOTIFICATION_FEATURE_KEY, notificationsAdapter } from './notification.reducer';


const selectNotificationState = createSelector(
  selectUserRootState,
  (state) => state[NOTIFICATION_FEATURE_KEY]
);

const {
  selectAll,
  selectTotal,
} = notificationsAdapter.getSelectors();



export const selectNotificationLoaded = createSelector(
  selectNotificationState,
  (state: State) => state.loaded
);

export const selectNotificationError = createSelector(
  selectNotificationState,
  (state: State) => state.error
);


export const selectNotifications = createSelector(
  selectNotificationState,
  selectAll
);

export const selectNotificationCount = createSelector(
  selectNotificationState,
  selectTotal
);