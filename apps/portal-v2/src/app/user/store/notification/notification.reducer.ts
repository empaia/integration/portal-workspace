import { HttpErrorResponse } from '@angular/common/http';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on, Action } from '@ngrx/store';

import { NotificationActions } from './notification.actions';
import { AppNotification } from './notification.models';

export const NOTIFICATION_FEATURE_KEY = 'notification';

export interface State extends EntityState<AppNotification> {
  loaded: boolean; // has the Notification list been loaded
  error?: HttpErrorResponse | undefined; // last known error (if any)
}

export interface NotificationPartialState {
  readonly [NOTIFICATION_FEATURE_KEY]: State;
}

export const notificationsAdapter: EntityAdapter<AppNotification> = createEntityAdapter<AppNotification>({
  selectId: notification => notification.id
});

export const initialState: State = notificationsAdapter.getInitialState({
  loaded: true,
  error: undefined,
});

const notificationsReducer = createReducer(
  initialState,
  on(NotificationActions.notificationReceived, (state, { notification }): State => notificationsAdapter.setOne(notification, {
    ...state,
    loaded: true,
  })),
  on(NotificationActions.loadNotificationsSuccess, (state, { notifications }): State => notificationsAdapter.setAll(notifications, {
    ...state,
    loaded: true,
  })),
  on(NotificationActions.loadNotificationsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(NotificationActions.notificationConfirmSuccess, (state, { notifications }): State => notificationsAdapter.setAll(notifications, {
    ...state,
    loaded: true,
  })),
);

export function reducer(state: State | undefined, action: Action) {
  return notificationsReducer(state, action);
}
