import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';

import { NotificationActions } from './notification.actions';
import { HttpErrorResponse } from '@angular/common/http';
import { State } from './notification.reducer';
import { AuthActions } from '@auth/store/auth';
import { map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { NotificationsService } from '@api/eves/services';

@Injectable()
export class NotificationEffects {
  loadOnLogin$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.initialUserModuleLoadSuccessful),
      map(() => NotificationActions.loadNotifications())
    );
  });

  refreshOnSse$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(NotificationActions.sSERefreshNotification),
      map(() => NotificationActions.loadNotifications())
    );
  });

  loadNotifications$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(NotificationActions.loadNotifications),
      fetch({
        run: (
          _action: ReturnType<typeof NotificationActions.loadNotifications>,
          _state: State
        ) => {
          return this.notificationsService.notificationsGet().pipe(
            map((response) => response.items),
            map((notifications) =>
              NotificationActions.loadNotificationsSuccess({ notifications })
            )
          );
        },
        onError: (
          _action: ReturnType<typeof NotificationActions.loadNotifications>,
          error: HttpErrorResponse
        ) => {
          return NotificationActions.loadNotificationsFailure({ error });
        },
      })
    );
  });

  setNotificationConfirm$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(NotificationActions.notificationConfirm),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof NotificationActions.notificationConfirm>,
          _state: State
        ) => {
          return this.notificationsService
            .notificationsConfirmPut({
              body: { notification_id: action.id },
            })
            .pipe(
              map((response) => response.items),
              map((notifications) =>
                NotificationActions.notificationConfirmSuccess({
                  notifications,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof NotificationActions.notificationConfirm>,
          error: HttpErrorResponse
        ) => {
          return NotificationActions.notificationConfirmFailure({ error });
        },
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly notificationsService: NotificationsService
  ) {}
}
