import { HttpErrorResponse } from '@angular/common/http';
import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { EventAction, SSEventTypes } from 'src/app/events/event.models';

import { AppNotification } from './notification.models';

export const NotificationActions = createActionGroup({
  source: 'Notification',
  events: {
    'Init Notification Page': emptyProps(),
    'SSE Refresh Notification': props<{ eventType: SSEventTypes, eventMsg: EventAction }>(),

    'Notification Received': props<{ notification: AppNotification }>(),

    'Load Notifications': emptyProps(),
    'Load Notifications Success': props<{ notifications: AppNotification[] }>(),
    'Load Notifications Failure': props<{ error: HttpErrorResponse }>(),

    'Notification Confirm': props<{ id: string }>(),
    'Notification Confirm Success': props<{ notifications: AppNotification[] }>(),
    'Notification Confirm Failure': props<{ error: HttpErrorResponse }>(),
  }
});

