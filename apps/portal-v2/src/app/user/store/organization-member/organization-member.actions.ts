import { HttpErrorResponse } from '@angular/common/http';
import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { OrganizationMemberEntity } from './organization-member.model';



export const OrganizationMemberActions = createActionGroup({
  source: 'OrganizationMember',
  events: {
    'Load': emptyProps(),
    'Load Success': props<{ members: OrganizationMemberEntity[] }>(),
    'Load Failure': props<{ error: HttpErrorResponse }>(),

    'Initialize Organization Members Page': emptyProps(),
  },
});
