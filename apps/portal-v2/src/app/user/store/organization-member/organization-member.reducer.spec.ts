import { reducer, initialState } from './organization-member.reducer';

describe('OrganizationMember Reducer', () => {
  describe('an unknown action', () => {
    it('should return the previous state', () => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
