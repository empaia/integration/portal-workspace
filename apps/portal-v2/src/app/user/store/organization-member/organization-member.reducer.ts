import { HttpErrorResponse } from '@angular/common/http';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { OrganizationMemberActions } from './organization-member.actions';
import { OrganizationMemberEntity } from './organization-member.model';

export const ORGANIZATION_MEMBER_FEATURE_KEY = 'organizationMember';


export interface State extends EntityState<OrganizationMemberEntity> {
  loaded: boolean;
  error?: HttpErrorResponse | undefined;
}

// TODO: sort by date
// export function sortByDate(a: OrganizationMemberEntity, b: OrganizationMemberEntity): number {
//   return a.email_address > b.email_address ? -1 : 1;
// }


export const userOrganizationAdapter = createEntityAdapter<OrganizationMemberEntity>({
  selectId: request => request.user_id
});

export const initialState: State = userOrganizationAdapter.getInitialState({
  loaded: false,
});



export const reducer = createReducer(
  initialState,

  on(OrganizationMemberActions.load, (state): State => ({
    ...state,
    loaded: false,
  })),

  on(OrganizationMemberActions.loadSuccess, (state, { members }): State =>
    userOrganizationAdapter.setAll(members, {
      ...state,
      loaded: true,
    })
  ),


);