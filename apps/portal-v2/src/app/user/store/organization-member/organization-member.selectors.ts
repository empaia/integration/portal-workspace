import { createSelector } from '@ngrx/store';
import { selectUserRootState } from '../user-root.selectors';
import { ORGANIZATION_MEMBER_FEATURE_KEY, userOrganizationAdapter } from './organization-member.reducer';

// active organization members 
const selectOrganizationMemberState = createSelector(
  selectUserRootState,
  (state) => state[ORGANIZATION_MEMBER_FEATURE_KEY]
);


export const {
  selectAll,
  selectEntities
} = userOrganizationAdapter.getSelectors();


export const selectOrganizationMembers = createSelector(
  selectOrganizationMemberState,
  selectAll
);


