import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { fetch } from '@ngrx/router-store/data-persistence';

import { map, tap } from 'rxjs/operators';
import { OrganizationMemberActions } from './organization-member.actions';
import { OrganizationControllerV2Service } from '@api/aaa/services';
import { HttpErrorResponse } from '@angular/common/http';
import { filterNullish } from '@shared/utility/rxjs-utility';
import { State } from './organization-member.reducer';
import { EventActions } from 'src/app/events/store';
import { ActiveOrganizationContainerActions } from '@user/my/containers/active-organization-container/active-organization-container.actions';

@Injectable()
export class OrganizationMemberEffects {
  initializeOrganizationMembersPage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        OrganizationMemberActions.initializeOrganizationMembersPage,
        ActiveOrganizationContainerActions.initializeActiveOrganizationContainer,
      ),
      map(() => OrganizationMemberActions.load())
    );
  });

  sseRefreshOnMembershipApproval$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(EventActions.sseUpdateMembershipRequests),
      // TODO: filter - update on APPROVED
      map(() => OrganizationMemberActions.load())
    );
  });

  loadOrganizationMembers$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(OrganizationMemberActions.load),
      fetch({
        run: (
          _action: ReturnType<typeof OrganizationMemberActions.load>,
          _state: State
        ) => {
          return this.organizationControllerV2Service.getMemberProfiles().pipe(
            tap((response) => console.log('getOrganizationMembers', response)),
            filterNullish(),
            map((response) => response.users),
            map((members) => OrganizationMemberActions.loadSuccess({ members }))
          );
        },
        onError: (
          _action: ReturnType<typeof OrganizationMemberActions.load>,
          error: HttpErrorResponse
        ) => {
          return OrganizationMemberActions.loadFailure({ error });
        },
      })
    );
  });

  constructor(
    private actions$: Actions,
    private readonly organizationControllerV2Service: OrganizationControllerV2Service
  ) {}
}
