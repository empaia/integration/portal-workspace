import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutes } from '@router/router.constants';
import { ProfileContainerComponent } from './containers/profile-container/profile-container.component';
import { MyOrganizationsContainerComponent } from './containers/my-organizations-container/my-organizations-container.component';
import { EditMyUnapprovedOrganizationContainerComponent } from './containers/edit-my-unapproved-organization-container/edit-my-unapproved-organization-container.component';
import { ActiveOrganizationContainerComponent } from './containers/active-organization-container/active-organization-container.component';
import { OrgSelectedGuard } from '@shared/services/org-selected-guard/org-selected-guard.guard';
import { NotificationContainerComponent } from './containers/notification-container/notification-container.component';

const routes: Routes = [
  {
    path: AppRoutes.MY_ORGANIZATIONS_ROUTE,
    component: MyOrganizationsContainerComponent
  },
  {
    path: AppRoutes.MY_ORGANIZATIONS_ROUTE + '/:' + AppRoutes.UNAPPROVED_ORGANIZATION_ID,
    component: EditMyUnapprovedOrganizationContainerComponent
  },
  {
    path: AppRoutes.MY_ACTIVE_ORGANIZATION_ROUTE,
    component: ActiveOrganizationContainerComponent,
    canMatch: [OrgSelectedGuard],
  },
  {
    path: AppRoutes.PROFILE_ROUTE,
    component: ProfileContainerComponent,
  },
  {
    path: AppRoutes.NOTIFICATIONS_ROUTE,
    component: NotificationContainerComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyRoutingModule { }
