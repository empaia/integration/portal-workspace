import { ChangeDetectionStrategy, Component, inject, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { UserDetailsEntity } from '@api/aaa/models';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MAX_FORM_FIELD_LENGTH, NO_START_END_WHITE_SPACE } from '@shared/models/form.models';

@Component({
  selector: 'app-profile-edit-details-dialog',
  templateUrl: './profile-edit-details-dialog.component.html',
  styleUrls: ['./profile-edit-details-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileEditDetailsDialogComponent implements OnInit {
  private fb = inject(FormBuilder);
  public detailsForm = this.fb.nonNullable.group({
    first_name: ['',
      [
        Validators.required,
        Validators.maxLength(MAX_FORM_FIELD_LENGTH),
        Validators.pattern(NO_START_END_WHITE_SPACE)
      ]
    ],
    last_name: ['',
      [
        Validators.required,
        Validators.maxLength(MAX_FORM_FIELD_LENGTH),
        Validators.pattern(NO_START_END_WHITE_SPACE)
      ]
    ],
    title: ['',
      [
        Validators.maxLength(MAX_FORM_FIELD_LENGTH)
      ]
    ]
  });

  public readonly MAX_FORM_LENGTH = MAX_FORM_FIELD_LENGTH;

  constructor(
    private dialogRef: MatDialogRef<ProfileEditDetailsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: UserDetailsEntity,
  ) {}

  public ngOnInit(): void {
    this.setFields();
  }

  public get firstName(): FormControl<string> {
    return this.detailsForm.controls.first_name;
  }

  public get lastName(): FormControl<string> {
    return this.detailsForm.controls.last_name;
  }

  public get title(): FormControl<string> {
    return this.detailsForm.controls.title;
  }

  public onSubmit(): void {
    const details = {...this.detailsForm.value} as UserDetailsEntity;
    this.dialogRef.close(details);
  }

  private setFields(): void {
    this.detailsForm.patchValue({...this.data});
  }
}
