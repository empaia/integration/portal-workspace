import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { ProfileEditDetailsDialogComponent } from './profile-edit-details-dialog.component';

describe('ProfileEditDetailsDialogComponent', () => {
  let spectator: Spectator<ProfileEditDetailsDialogComponent>;
  const createComponent = createComponentFactory({
    component: ProfileEditDetailsDialogComponent,
    imports: [
      MaterialModule,
    ],
    providers: [
      FormBuilder,
      {
        provide: MatDialogRef, useValue: {}
      },
      {
        provide: MAT_DIALOG_DATA, useValue: {}
      }
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
