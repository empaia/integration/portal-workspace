import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-profile-account-control-card',
  templateUrl: './profile-account-control-card.component.html',
  styleUrls: ['./profile-account-control-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileAccountControlCardComponent {
  @Output() public changeClicked = new EventEmitter<void>();
  @Output() public deleteClicked = new EventEmitter<void>();
}
