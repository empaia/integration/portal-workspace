import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { TranslocoDirective } from '@ngneat/transloco';
import { MockDirectives } from 'ng-mocks';
import { ProfileAccountControlCardComponent } from './profile-account-control-card.component';

describe('ProfileAccountControlCardComponent', () => {
  let spectator: Spectator<ProfileAccountControlCardComponent>;
  const createComponent = createComponentFactory({
    component: ProfileAccountControlCardComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      MockDirectives(
        TranslocoDirective
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
