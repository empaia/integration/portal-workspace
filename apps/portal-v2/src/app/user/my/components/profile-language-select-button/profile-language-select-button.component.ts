import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { LangDefinition } from '@ngneat/transloco';
import { availableLanguages } from 'src/app/app.models';

@Component({
  selector: 'app-profile-language-select-button',
  templateUrl: './profile-language-select-button.component.html',
  styleUrls: ['./profile-language-select-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileLanguageSelectButtonComponent {
  public availLang = availableLanguages;

  @Input() public currentLang!: string;
  @Output() public langSelected = new EventEmitter<LangDefinition>();

  public languageSelected(lang: LangDefinition): void {
    this.langSelected.emit(lang);
  }
}
