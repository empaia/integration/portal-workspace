import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { TranslocoDirective } from '@ngneat/transloco';
import { MockDirectives } from 'ng-mocks';
import { ProfileLanguageSelectButtonComponent } from './profile-language-select-button.component';

describe('ProfileLanguageSelectButtonComponent', () => {
  let spectator: Spectator<ProfileLanguageSelectButtonComponent>;
  const createComponent = createComponentFactory({
    component: ProfileLanguageSelectButtonComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      MockDirectives(
        TranslocoDirective
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
