import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { AppNotification, NotificationIds, notificationRoutes } from '@user/store/notification';


@Component({
  selector: 'app-notification-item',
  templateUrl: './notification-item.component.html',
  styleUrls: ['./notification-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotificationItemComponent {


  private _notification: AppNotification | undefined;
  @Input() public get notification() {
    return this._notification;
  }

  public set notification(val: AppNotification | undefined) {
    this._notification = val;
    this.link = this.getLink(this.notification?.content);
  }

  @Output() public deleteItem = new EventEmitter<{ id: string }>();
  @Output() public navigateTo = new EventEmitter<{ route: string }>();

  protected link: string | undefined = undefined;

  navTo() {
    if (this.link) {
      this.navigateTo.emit({ route: this.link });
    }
  }

  getLink(id: string | undefined): string | undefined {
    if (id) {
      const nId = id as NotificationIds;
      return notificationRoutes[nId];
    }
    return undefined;
  }

  deleteClicked($event: MouseEvent, id: string | undefined) {
    $event.stopImmediatePropagation();
    if (id) {
      this.deleteItem.emit({ id });
    }
  }
}
