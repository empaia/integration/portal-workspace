import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { OrganizationAccountState } from '@api/aaa/models';
import { UnapprovedOrganizationsEntity } from '@user/store/unapproved-organizations/unapproved-organizations.model';

@Component({
  selector: 'app-my-unapproved-organizations-table',
  templateUrl: './my-unapproved-organizations-table.component.html',
  styleUrls: ['./my-unapproved-organizations-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MyUnapprovedOrganizationsTableComponent implements AfterViewInit {

  @ViewChild(MatSort) sort!: MatSort;

  private _orgs!: UnapprovedOrganizationsEntity[];
  @Input() public get unapprovedOrganizations() {
    return this._orgs;
  }
  public set unapprovedOrganizations(val: UnapprovedOrganizationsEntity[] | null) {
    if (val) {
      this._orgs = val;
      this.dataSource.data = val;
      this.dataSource.sort = this.sort;
    }
    // console.log('dataSource', this.dataSource);
  }

  displayedColumns: string[] = ['organization_name', 'status', 'actions'];
  dataSource = new MatTableDataSource<Partial<UnapprovedOrganizationsEntity>>(
    [{ organization_name: 'Loading Data...', organization_id: '' }]
  );

  protected OrganizationAccountState = OrganizationAccountState;

  @Output() public editOrganizationClicked = new EventEmitter<{ id: string }>();
  @Output() public deleteOrganizationClicked = new EventEmitter<{ id: string }>();

  constructor() {
    // Assign the data to the data source for the table to render
    // this.dataSource = new MatTableDataSource(undefined);
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  // applyFilter(event: Event) {
  //   const filterValue = (event.target as HTMLInputElement).value;
  //   this.dataSource.filter = filterValue.trim().toLowerCase();
  // }

  announceSortChange(event: Sort) {
    const sortState = event;
    // This example uses English messages. If your application supports
    // multiple language, you would internationalize these strings.
    // Furthermore, you can customize the message to add additional
    // details about the values being sorted.
    if (sortState.direction) {
      console.log(`Sorted ${sortState.direction}ending`);
    } else {
      console.log('Sorting cleared');
    }
  }

}
