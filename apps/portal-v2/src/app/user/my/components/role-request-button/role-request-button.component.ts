import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-role-request-button',
  templateUrl: './role-request-button.component.html',
  styleUrls: ['./role-request-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RoleRequestButtonComponent {

  @Input() public enabled = false;
  @Input() public canLeaveOrganization: boolean | undefined = false;

  @Output() public newRequest = new EventEmitter<void>();
  @Output() public leaveOrg = new EventEmitter<void>();

}
