import {  ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SelectOrganizationDialogData } from './select-organization-dialog.models';

@Component({
  selector: 'app-select-organization-dialog',
  templateUrl: './select-organization-dialog.component.html',
  styleUrls: ['./select-organization-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectOrganizationDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<SelectOrganizationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SelectOrganizationDialogData,
  ) { }

  public selectOrganization(id: string): void {
    this.dialogRef.close(id);
  }

}
