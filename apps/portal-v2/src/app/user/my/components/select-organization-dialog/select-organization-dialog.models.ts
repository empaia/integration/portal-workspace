import { UserOrganizationEntity } from '@user/store/user-organizations/user-organizations.models';

export interface SelectOrganizationDialogData {
  organizations: UserOrganizationEntity[],
  selectedOrganizationId?: string | undefined;
}
