/* eslint-disable @typescript-eslint/no-explicit-any */
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { blankProfileImage } from 'src/app/app.models';

@Component({
  selector: 'app-profile-edit-avatar-dialog',
  templateUrl: './profile-edit-avatar-dialog.component.html',
  styleUrls: ['./profile-edit-avatar-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileEditAvatarDialogComponent {
  public fallback = blankProfileImage;
  private image: Blob | null = null;

  constructor(
    private dialogRef: MatDialogRef<ProfileEditAvatarDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public avatarUrl: string | undefined,
  ) {}

  public onImageChanged(event: Blob | null): void {
    this.image = event;
  }

  public saveImage(): void {
    this.dialogRef.close(this.image);
  }
}
