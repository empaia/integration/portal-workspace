import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { ImageCropperComponent } from 'ngx-image-cropper';
import { ProfileEditAvatarDialogComponent } from './profile-edit-avatar-dialog.component';

describe('ProfileEditAvatarDialogComponent', () => {
  let spectator: Spectator<ProfileEditAvatarDialogComponent>;
  const createComponent = createComponentFactory({
    component: ProfileEditAvatarDialogComponent,
    imports: [
      MaterialModule,
      ImageCropperComponent,
    ],
    providers: [
      {
        provide: MatDialogRef, useValue: {}
      },
      {
        provide: MAT_DIALOG_DATA, useValue: {}
      }
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
