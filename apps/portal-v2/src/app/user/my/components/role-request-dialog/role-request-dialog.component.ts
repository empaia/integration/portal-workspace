import { ChangeDetectionStrategy, Component, inject, Inject } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { OrganizationCategory, OrganizationUserRoleV2, PostOrganizationUserRoleRequestEntity } from '@api/aaa/models';
import { TranslocoService } from '@ngneat/transloco';
import { OrganizationEntity } from '@public/store/organizations/organizations.models';
import { Observable, of } from 'rxjs';

export interface RoleRequestDialogInputData {
  organization: OrganizationEntity,
  roles: OrganizationUserRoleV2[],
}

// export interface PostOrganizationUserRoleRequestEntity {
//   organizationId: string,
//   role: OrganizationUserRoleV2,
//   value: boolean
// }


export type OrganizationUserRoleV2Type = `${OrganizationUserRoleV2}`;

@Component({
  selector: 'app-role-request-dialog',
  templateUrl: './role-request-dialog.component.html',
  styleUrls: ['./role-request-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RoleRequestDialogComponent {
  private translocoService = inject(TranslocoService);

  protected organization: OrganizationEntity;
  protected currentRoles: OrganizationUserRoleV2[];

  private initModel = new Map<OrganizationUserRoleV2, boolean>();

  // protected availableRoles: string[] = Object.keys(OrganizationUserRoleV2);

  protected roleForm = this.buildFromFromEnum();

  public readonly ORGANIZATION_ROLES = OrganizationUserRoleV2;

  constructor(
    public dialogRef: MatDialogRef<RoleRequestDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RoleRequestDialogInputData,
  ) {
    this.organization = data.organization;
    this.currentRoles = data.roles;

    this.initModel.forEach((_, key) => this.initModel.set(key, false));

    this.currentRoles.forEach(role => {
      const checkbox = this.roleForm.get(role);
      if (checkbox) {
        // disable checkboxes with roles user already has
        checkbox.setValue(true);
        // checkbox.disable();
        this.initModel.set(role, true);
      }
    });
    if (!this.organization.details?.categories?.includes(OrganizationCategory.APP_VENDOR)) {
      this.disableCheckbox(OrganizationUserRoleV2.APP_MAINTAINER);
    }

    // disable selection of pathologist and data manager role if organization is not an app customer
    if (!this.organization.details?.categories?.includes(OrganizationCategory.APP_CUSTOMER)) {
      this.disableCheckbox(OrganizationUserRoleV2.PATHOLOGIST);
      this.disableCheckbox(OrganizationUserRoleV2.DATA_MANAGER);
    }

    // disable selection of clearance maintainer role if organization is not a product provider
    if (!this.organization.details?.categories?.includes(OrganizationCategory.PRODUCT_PROVIDER)) {
      this.disableCheckbox(OrganizationUserRoleV2.CLEARANCE_MAINTAINER);
    }
  }

  public buildFromFromEnum(): FormGroup {
    const r = new FormGroup({});
    Object.keys(OrganizationUserRoleV2).map((k) => {
      // console.log('add control', k);
      r.addControl(k, new FormControl(false));
    });
    return r;
  }

  public get controls(): string[] {
    return Object.keys(this.roleForm.controls);
  }

  getCheckBoxControl(ctrlType: OrganizationUserRoleV2) {
    return this.roleForm.controls[ctrlType];
  }

  public get hasChanged(): boolean {
    let change = false;
    this.initModel.forEach((v, k) => {
      if (this.roleForm.controls[k].value !== v) { change = true; }
    });
    return change;
  }

  public disableCheckbox(boxType: OrganizationUserRoleV2): void {
    const checkbox = this.roleForm.get(boxType);
    checkbox?.setValue(false);
    checkbox?.disable();
    this.initModel.set(boxType, false);
  }

  public getTooltip(role: string): Observable<string> {
    let r;
    if (role === OrganizationUserRoleV2.PATHOLOGIST.valueOf() ||
      role === OrganizationUserRoleV2.DATA_MANAGER.valueOf()) {
      r = this.translocoService.selectTranslate('user_role_dialog.org_category_customer_tt');
    }
    else if (role === OrganizationUserRoleV2.APP_MAINTAINER.valueOf()) {
      r = this.translocoService.selectTranslate('user_role_dialog.org_category_vendor_tt');
    }
    return r ?? of('');
  }

  public getSubmitData(): PostOrganizationUserRoleRequestEntity {
    const requested_roles: Array<OrganizationUserRoleV2> = [];

    for (const key of Object.values(OrganizationUserRoleV2)) {
      if (this.getCheckBoxControl(key).value) {
        requested_roles.push(key);
      }
    }
    return <PostOrganizationUserRoleRequestEntity>{
      requested_roles
    };
  }

  getSubmitDataOLD() {
    // const out: PostOrganizationUserRoleRequestEntity = {
    //   manager: this.getCheckBoxControl(OrganizationUserRoleV2.MANAGER).value,
    //   pathologist: this.getCheckBoxControl(OrganizationUserRoleV2.PATHOLOGIST).value,
    //   app_maintainer: this.getCheckBoxControl(OrganizationUserRoleV2.APP_MAINTAINER).value,
    //   data_manager: this.getCheckBoxControl(OrganizationUserRoleV2.DATA_MANAGER).value,
    // };
    // return out;
  }

}
