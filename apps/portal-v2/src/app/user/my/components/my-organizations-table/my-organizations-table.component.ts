import { AfterViewInit, ChangeDetectionStrategy, Component, Input, ViewChild } from '@angular/core';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UserOrganizationTableData } from '@user/store/user-organizations/user-organizations.models';


@Component({
  selector: 'app-my-organizations-table',
  templateUrl: './my-organizations-table.component.html',
  styleUrls: ['./my-organizations-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MyOrganizationsTableComponent implements AfterViewInit {

  @ViewChild(MatSort) sort!: MatSort;

  private _orgs!: UserOrganizationTableData[];
  @Input() public set userOrganizations(val: UserOrganizationTableData[] | null) {
    if (val) {
      this._orgs = val;
      this.dataSource.data = val;
      this.dataSource.sort = this.sort;
    }
  }
  public get userOrganizations() {
    return this._orgs;
  }

  displayedColumns: string[] = [
    'organization_name',
    'role',
    'number_of_members'
  ];
  dataSource = new MatTableDataSource<Partial<UserOrganizationTableData>>([{
    organization_name: 'Loading Data',
    organization_id: '',
    isDefault: false
  }]);

  public ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  public applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public announceSortChange(event: Sort): void {
    const sortState = event;
    // This example uses English messages. If your application supports
    // multiple language, you would internationalize these strings.
    // Furthermore, you can customize the message to add additional
    // details about the values being sorted.
    if (sortState.direction) {
      console.log(`Sorted ${sortState.direction}ending`);
    } else {
      console.log('Sorting cleared');
    }
  }

  public get numTableItems(): number | undefined {
    return this.userOrganizations?.length;
  }

}
