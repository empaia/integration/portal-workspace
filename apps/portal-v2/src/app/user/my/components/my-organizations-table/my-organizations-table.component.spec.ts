import { Spectator, createComponentFactory } from '@ngneat/spectator';

import { MyOrganizationsTableComponent } from '../my-organizations-table/my-organizations-table.component';

describe('MyOrganizationsTableComponent', () => {
  let spectator: Spectator<MyOrganizationsTableComponent>;
  const createComponent = createComponentFactory(MyOrganizationsTableComponent);

  it('should create', () => {
    spectator = createComponent();

    expect(spectator.component).toBeTruthy();
  });
});
