import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { ProfileEditContactDialogComponent } from './profile-edit-contact-dialog.component';

describe('ProfileEditContactDialogComponent', () => {
  let spectator: Spectator<ProfileEditContactDialogComponent>;
  const createComponent = createComponentFactory({
    component: ProfileEditContactDialogComponent,
    imports: [
      MaterialModule
    ],
    providers: [
      FormBuilder,
      {
        provide: MatDialogRef, useValue: {}
      },
      {
        provide: MAT_DIALOG_DATA, useValue: {}
      }
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
