import { ChangeDetectionStrategy, Component, inject, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CountryEntityV2, PostUserContactDataEntity, UserContactDataEntity } from '@api/aaa/models';
import { MAX_FORM_FIELD_LENGTH, PHONE_NUMBER_PATTERN, ZIP_CODE_PATTERN } from '@shared/models/form.models';

export interface ContactDataCountriesEntity {
  contactData: UserContactDataEntity,
  countries: CountryEntityV2[],
}

@Component({
  selector: 'app-profile-edit-contact-dialog',
  templateUrl: './profile-edit-contact-dialog.component.html',
  styleUrls: ['./profile-edit-contact-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileEditContactDialogComponent implements OnInit {
  
  private fb = inject(FormBuilder);
  private dialogRef = inject(MatDialogRef<ProfileEditContactDialogComponent>);

  public contactForm = this.fb.nonNullable.group({
    country_code: ['',
      [
        Validators.required
      ]
    ],
    place_name: ['',
      [
        Validators.required,
        Validators.maxLength(MAX_FORM_FIELD_LENGTH)
      ]
    ],
    street_name: ['',
      [
        Validators.required,
        Validators.maxLength(MAX_FORM_FIELD_LENGTH)
      ]
    ],
    street_number: ['',
      [
        Validators.required,
        Validators.maxLength(MAX_FORM_FIELD_LENGTH)
      ]
    ],
    zip_code: ['',
      [
        Validators.required,
        Validators.maxLength(MAX_FORM_FIELD_LENGTH),
        Validators.pattern(ZIP_CODE_PATTERN),
      ]
    ],
    phone_number: ['',
      [
        Validators.maxLength(MAX_FORM_FIELD_LENGTH),
        Validators.pattern(PHONE_NUMBER_PATTERN)
      ]
    ]
  });

  public readonly MAX_FORM_LENGTH = MAX_FORM_FIELD_LENGTH;

  constructor(
    // private dialogRef: MatDialogRef<ProfileEditContactDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ContactDataCountriesEntity,
  ) {}

  public ngOnInit(): void {
    this.setFields();
  }

  public get countryCode(): FormControl<string> {
    return this.contactForm.controls.country_code;
  }

  public get placeName(): FormControl<string> {
    return this.contactForm.controls.place_name;
  }

  public get streetName(): FormControl<string> {
    return this.contactForm.controls.street_name;
  }

  public get streetNumber(): FormControl<string> {
    return this.contactForm.controls.street_number;
  }

  public get zipCode(): FormControl<string> {
    return this.contactForm.controls.zip_code;
  }

  public get phoneNumber(): FormControl<string> {
    return this.contactForm.controls.phone_number;
  }

  public onSubmit(): void {
    const contactData = {...this.contactForm.value} as PostUserContactDataEntity;
    this.dialogRef.close(contactData);
  }

  private setFields(): void {
    this.contactForm.patchValue({...this.data.contactData});
  }
}
