import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { blankProfileImage } from 'src/app/app.models';

@Component({
  selector: 'app-profile-avatar-picture-card',
  templateUrl: './profile-avatar-picture-card.component.html',
  styleUrls: ['./profile-avatar-picture-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileAvatarPictureCardComponent {

  protected readonly blankProfile = blankProfileImage;

  @Input() public pictureUrl: string | undefined;

  @Output() public editClicked = new EventEmitter<void>();
}
