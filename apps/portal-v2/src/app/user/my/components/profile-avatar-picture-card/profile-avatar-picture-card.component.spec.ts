import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { ProfileAvatarPictureCardComponent } from './profile-avatar-picture-card.component';

describe('ProfileAvatarPictureCardComponent', () => {
  let spectator: Spectator<ProfileAvatarPictureCardComponent>;
  const createComponent = createComponentFactory({
    component: ProfileAvatarPictureCardComponent,
    imports: [
      MaterialModule,
    ],
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
