import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { OrganizationMemberEntity } from '@user/store/organization-member';

@Component({
  selector: 'app-organization-members-table',
  templateUrl: './organization-members-table.component.html',
  styleUrls: ['./organization-members-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OrganizationMembersTableComponent implements AfterViewInit {
  @ViewChild(MatSort) sort!: MatSort;

  private _members!: OrganizationMemberEntity[];
  @Input() public get members() {
    return this._members;
  }
  public set members(val: OrganizationMemberEntity[] | undefined) {
    if (val) {
      this._members = val;
      this.dataSource.data = val;
      this.dataSource.sort = this.sort;
    }
    // console.log('dataSource', this.dataSource);
  }

  displayedColumns: string[] = [
    // 'created_at',
    //'email_address',
    'first_name',
    'last_name',
    'organization_user_role',
    // 'title',
  ];

  dataSource = new MatTableDataSource<Partial<OrganizationMemberEntity>>([
    { first_name: 'Loading Data...' }
  ]);

  @Output() public deleteRequest = new EventEmitter<{ id: number }>();

  constructor() {
    // Assign the data to the data source for the table to render
    // this.dataSource = new MatTableDataSource(undefined);
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  // applyFilter(event: Event) {
  //   const filterValue = (event.target as HTMLInputElement).value;
  //   this.dataSource.filter = filterValue.trim().toLowerCase();
  // }

  announceSortChange(event: Sort) {
    const sortState = event;
    // This example uses English messages. If your application supports
    // multiple language, you would internationalize these strings.
    // Furthermore, you can customize the message to add additional
    // details about the values being sorted.
    if (sortState.direction) {
      console.log(`Sorted ${sortState.direction}ending`);
    } else {
      console.log('Sorting cleared');
    }
  }
}
