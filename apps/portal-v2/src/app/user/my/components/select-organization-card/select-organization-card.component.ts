import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { UserOrganizationEntity } from '@user/store/user-organizations/user-organizations.models';
import { blankOrgImage } from 'src/app/app.models';

@Component({
  selector: 'app-select-organization-card',
  templateUrl: './select-organization-card.component.html',
  styleUrls: ['./select-organization-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectOrganizationCardComponent {

  readonly fallbackImage = blankOrgImage;

  @Input() public organization!: UserOrganizationEntity;
  @Input() public selectedOrganization: string | undefined;


}
