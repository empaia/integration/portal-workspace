import { ChangeDetectionStrategy, Component, EventEmitter, inject, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';

@Component({
  selector: 'app-select-organization-checkbox',
  templateUrl: './select-organization-checkbox.component.html',
  styleUrls: ['./select-organization-checkbox.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectOrganizationCheckboxComponent {
  private formBuilder = inject(FormBuilder);

  fg = this.formBuilder.nonNullable.group({
    setAsDefault: false,
  });

  @Output() public checkboxChange = new EventEmitter<{ checked: boolean }>();

  checkboxChecked($event: MatCheckboxChange) {
    this.checkboxChange.emit({ checked: $event.checked });
  }

}
