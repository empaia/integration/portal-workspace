import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { OrganizationUserRoleV2, RequestState } from '@api/aaa/models';
import { RoleRequestEntity } from '@user/store/role-request';

@Component({
  selector: 'app-role-requests-table',
  templateUrl: './role-requests-table.component.html',
  styleUrls: ['./role-requests-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RoleRequestsTableComponent implements AfterViewInit {

  readonly RequestState = RequestState;
  @ViewChild(MatSort) sort!: MatSort;
  private _requests!: RoleRequestEntity[];
  @Input() public get roleRequests() {
    return this._requests;
  }
  public set roleRequests(val: RoleRequestEntity[] | undefined) {
    if (val) {
      this._requests = val;
      this.dataSource.data = val;
      this.dataSource.sort = this.sort;
    }
    // console.log('dataSource', this.dataSource);
  }

  @Input() public roles: OrganizationUserRoleV2[] = [];

  displayedColumns: string[] = [
    'created_at',
    'organization_user_role',
    // 'first_name',
    // 'last_name',
    // 'title',
    // 'email_address',
    'request_state',
    'reviewer_comment',
    'actions'
  ];

  dataSource = new MatTableDataSource<Partial<RoleRequestEntity>>([
    { reviewer_comment: 'Loading Data...' }
  ]);

  @Output() public revokeRequest = new EventEmitter<{ id: number }>();

  constructor() {
    // Assign the data to the data source for the table to render
    // this.dataSource = new MatTableDataSource(undefined);
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  // applyFilter(event: Event) {
  //   const filterValue = (event.target as HTMLInputElement).value;
  //   this.dataSource.filter = filterValue.trim().toLowerCase();
  // }

  announceSortChange(event: Sort) {
    const sortState = event;
    // This example uses English messages. If your application supports
    // multiple language, you would internationalize these strings.
    // Furthermore, you can customize the message to add additional
    // details about the values being sorted.
    if (sortState.direction) {
      console.log(`Sorted ${sortState.direction}ending`);
    } else {
      console.log('Sorting cleared');
    }
  }
}
