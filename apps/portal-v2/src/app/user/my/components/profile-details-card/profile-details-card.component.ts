import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { UserDetailsEntity } from '@api/aaa/models';

@Component({
  selector: 'app-profile-details-card',
  templateUrl: './profile-details-card.component.html',
  styleUrls: ['./profile-details-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileDetailsCardComponent {
  @Input() public userDetails!: UserDetailsEntity;

  @Output() public editClicked = new EventEmitter<void>();
}
