import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { TranslocoDirective } from '@ngneat/transloco';
import { MockDirectives } from 'ng-mocks';
import { ProfileDetailsCardComponent } from './profile-details-card.component';

describe('ProfileDetailsCardComponent', () => {
  let spectator: Spectator<ProfileDetailsCardComponent>;
  const createComponent = createComponentFactory({
    component: ProfileDetailsCardComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      MockDirectives(
        TranslocoDirective
      ),
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
