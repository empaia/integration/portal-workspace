import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MembershipRequestEntity, RequestState } from '@api/aaa/models';

@Component({
  selector: 'app-my-membership-requests-table',
  templateUrl: './my-membership-requests-table.component.html',
  styleUrls: ['./my-membership-requests-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MyMembershipRequestsTableComponent implements AfterViewInit {

  protected readonly RequestState = RequestState;

  @ViewChild(MatSort) sort!: MatSort;

  private _requests!: MembershipRequestEntity[];
  @Input() public get membershipRequest() {
    return this._requests;
  }
  public set membershipRequest(val: MembershipRequestEntity[] | null) {
    if (val) {
      this._requests = val;
      this.dataSource.data = val;
      this.dataSource.sort = this.sort;
    }
    // console.log('dataSource', this.dataSource);
  }

  displayedColumns: string[] = ['organization_name', 'request_state', 'actions'];
  dataSource = new MatTableDataSource<Partial<MembershipRequestEntity>>([{ organization_name: 'Loading Data...', organization_id: '' }]);

  @Output() public deleteRequest = new EventEmitter<{ id: number }>();

  constructor() {
    // Assign the data to the data source for the table to render
    // this.dataSource = new MatTableDataSource(undefined);
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  // applyFilter(event: Event) {
  //   const filterValue = (event.target as HTMLInputElement).value;
  //   this.dataSource.filter = filterValue.trim().toLowerCase();
  // }

  announceSortChange(event: Sort) {
    const sortState = event;
    // This example uses English messages. If your application supports
    // multiple language, you would internationalize these strings.
    // Furthermore, you can customize the message to add additional
    // details about the values being sorted.
    if (sortState.direction) {
      console.log(`Sorted ${sortState.direction}ending`);
    } else {
      console.log('Sorting cleared');
    }
  }

}
