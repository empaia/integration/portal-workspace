import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { TranslocoDirective, TranslocoPipe } from '@ngneat/transloco';
import { MockComponents, MockDirectives, MockPipes } from 'ng-mocks';
import { ProfileLanguageSelectButtonComponent } from '../profile-language-select-button/profile-language-select-button.component';

import { ProfileUserDataCardComponent } from './profile-user-data-card.component';

describe('ProfileUserDataCardComponent', () => {
  let spectator: Spectator<ProfileUserDataCardComponent>;
  const createComponent = createComponentFactory({
    component: ProfileUserDataCardComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      MockComponents(
        ProfileLanguageSelectButtonComponent,
      ),
      MockDirectives(
        TranslocoDirective
      ),
      MockPipes(
        TranslocoPipe
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
