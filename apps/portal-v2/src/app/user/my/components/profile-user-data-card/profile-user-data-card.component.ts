import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { UserContactDataEntity, UserRole } from '@api/aaa/models';
import { LangDefinition } from '@ngneat/transloco';

@Component({
  selector: 'app-profile-user-data-card',
  templateUrl: './profile-user-data-card.component.html',
  styleUrls: ['./profile-user-data-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileUserDataCardComponent {
  @Input() public userContactData!: UserContactDataEntity;
  @Input() public roles!: UserRole[];
  @Input() public currentLanguage!: string;

  @Output() public setLanguage = new EventEmitter<LangDefinition>();

}
