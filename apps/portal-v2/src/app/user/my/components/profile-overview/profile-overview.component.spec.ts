import { MatDialogModule } from '@angular/material/dialog';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { TranslocoDirective } from '@ngneat/transloco';
import { MockComponents, MockDirectives } from 'ng-mocks';
import { ProfileAccountControlCardComponent } from '../profile-account-control-card/profile-account-control-card.component';
import { ProfileAvatarPictureCardComponent } from '../profile-avatar-picture-card/profile-avatar-picture-card.component';
import { ProfileContactCardComponent } from '../profile-contact-card/profile-contact-card.component';
import { ProfileDetailsCardComponent } from '../profile-details-card/profile-details-card.component';
import { ProfileUserDataCardComponent } from '../profile-user-data-card/profile-user-data-card.component';
import { ProfileOverviewComponent } from './profile-overview.component';

describe('ProfileOverviewComponent', () => {
  let spectator: Spectator<ProfileOverviewComponent>;
  const createComponent = createComponentFactory({
    component: ProfileOverviewComponent,
    imports: [
      MatDialogModule
    ],
    declarations: [
      MockComponents(
        ProfileAvatarPictureCardComponent,
        ProfileDetailsCardComponent,
        ProfileUserDataCardComponent,
        ProfileContactCardComponent,
        ProfileAccountControlCardComponent,
      ),
      MockDirectives(
        TranslocoDirective
      )
    ],
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
