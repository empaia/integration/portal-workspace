import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { LangDefinition } from '@ngneat/transloco';
import { UserEntity } from '@user/store/user';

@Component({
  selector: 'app-profile-overview',
  templateUrl: './profile-overview.component.html',
  styleUrls: ['./profile-overview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileOverviewComponent {
  @Input() public userProfile!: UserEntity;
  @Input() public currentLanguage!: string;

  @Output() public setLanguage = new EventEmitter<LangDefinition>();

  @Output() public changePasswordClicked = new EventEmitter<void>();
  @Output() public deleteProfileClicked = new EventEmitter<void>();

  @Output() public openUserDetailsDialog = new EventEmitter<void>();
  @Output() public openUserContactDataDialog = new EventEmitter<void>();
  @Output() public openUserAvatarDialog = new EventEmitter<void>();
}
