import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { UserContactDataEntity } from '@api/aaa/models';

@Component({
  selector: 'app-profile-contact-card',
  templateUrl: './profile-contact-card.component.html',
  styleUrls: ['./profile-contact-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileContactCardComponent {
  @Input() public userContactData!: UserContactDataEntity;

  @Output() public editClicked = new EventEmitter<void>();
}
