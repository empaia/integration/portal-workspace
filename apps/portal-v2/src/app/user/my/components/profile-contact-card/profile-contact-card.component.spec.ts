import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { TranslocoDirective } from '@ngneat/transloco';
import { MockDirectives } from 'ng-mocks';
import { ProfileContactCardComponent } from './profile-contact-card.component';

describe('ProfileContactCardComponent', () => {
  let spectator: Spectator<ProfileContactCardComponent>;
  const createComponent = createComponentFactory({
    component: ProfileContactCardComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      MockDirectives(
        TranslocoDirective
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
