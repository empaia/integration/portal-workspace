import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActiveOrganizationActions, ActiveOrganizationSelectors } from '@user/store/active-organization';
import { OrganizationMemberEntity, OrganizationMemberSelectors } from '@user/store/organization-member';
import { RoleRequestActions, RoleRequestEntity, RoleRequestSelectors } from '@user/store/role-request';
import { UserOrganizationsSelectors } from '@user/store/user-organizations';
import { Observable } from 'rxjs';
import { ActiveOrganizationContainerActions } from './active-organization-container.actions';

@Component({
  selector: 'app-active-organization-container',
  templateUrl: './active-organization-container.component.html',
  styleUrls: ['./active-organization-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActiveOrganizationContainerComponent {
  private store = inject(Store);

  protected activeOrgName$: Observable<string | undefined>;

  protected roleRequests$: Observable<RoleRequestEntity[]>;

  protected members$: Observable<OrganizationMemberEntity[]>;

  protected hasOpenRequests$: Observable<boolean>;

  public canLeaveOrganization$: Observable<boolean>;

  constructor() {
    this.store.dispatch(ActiveOrganizationContainerActions.initializeActiveOrganizationContainer());

    this.activeOrgName$ = this.store.select(UserOrganizationsSelectors.selectSelectedUserOrganizationName);

    this.hasOpenRequests$ = this.store.select(RoleRequestSelectors.selectHasOpenRequests);

    this.roleRequests$ = this.store.select(RoleRequestSelectors.selectRoleRequests);

    this.members$ = this.store.select(OrganizationMemberSelectors.selectOrganizationMembers);

    this.canLeaveOrganization$ = this.store.select(ActiveOrganizationSelectors.selectCanLeaveActiveOrganization);
  }

  public revokeRequestClicked($event: { id: number }): void {
    this.store.dispatch(RoleRequestActions.revoke({ id: $event.id }));
  }

  public openRoleRequestDialog(): void {
    this.store.dispatch(RoleRequestActions.openNewRequestDialog());
  }

  public leaveOrgClicked(): void {
    this.store.dispatch(ActiveOrganizationActions.openLeaveActiveOrganizationDialog());
  }

}
