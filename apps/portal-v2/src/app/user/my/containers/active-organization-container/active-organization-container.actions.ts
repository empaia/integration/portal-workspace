import { createActionGroup, emptyProps } from '@ngrx/store';

export const ActiveOrganizationContainerActions = createActionGroup({
  source: 'ActiveOrganizationContainer',
  events: {
    'Initialize Active Organization Container': emptyProps(),
  }
});
