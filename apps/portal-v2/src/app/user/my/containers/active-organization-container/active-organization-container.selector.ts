import { OrganizationUserRoleV2 } from '@api/aaa/models';
import { selectRolesInSelectedOrganization } from '@auth/store/auth/auth.selectors';
import { createSelector } from '@ngrx/store';
import { RoleRequestEntity } from '@user/store/role-request';
import { selectRoleRequests } from '@user/store/role-request/role-request.selectors';

export type RoleRequests = { requests: RoleRequestEntity[] } & { roles: OrganizationUserRoleV2[] };

export const selectUserRoleRequests = createSelector(
  selectRolesInSelectedOrganization,
  selectRoleRequests,
  (roles: OrganizationUserRoleV2[], requestedRoles: RoleRequestEntity[]): RoleRequests => <RoleRequests>({
    requests: requestedRoles,
    roles: roles,
  })
);