import { ChangeDetectionStrategy, Component } from '@angular/core';
import { EditOrganizationId } from '@edit-organization/models/edit-organization.models';
import { Store } from '@ngrx/store';
import { UnapprovedOrganizationsActions, UnapprovedOrganizationsEntity, UnapprovedOrganizationsSelectors } from '@user/store/unapproved-organizations';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-edit-my-unapproved-organization-container',
  templateUrl: './edit-my-unapproved-organization-container.component.html',
  styleUrls: ['./edit-my-unapproved-organization-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditMyUnapprovedOrganizationContainerComponent {
  public organization$: Observable<UnapprovedOrganizationsEntity | undefined>;
  public isOrganizationReady$: Observable<boolean>;
  public isNameReady$: Observable<boolean>;
  public isDetailsReady$: Observable<boolean>;
  public isContactReady$: Observable<boolean>;

  constructor(private store: Store) {
    this.organization$ = this.store.select(UnapprovedOrganizationsSelectors.selectSelectedUnapprovedOrganization);
    this.isOrganizationReady$ = this.store.select(UnapprovedOrganizationsSelectors.selectIsUnapprovedOrganizationReadyToApprove);
    this.isNameReady$ = this.store.select(UnapprovedOrganizationsSelectors.selectIsNameReady);
    this.isDetailsReady$ = this.store.select(UnapprovedOrganizationsSelectors.selectIsDetailsReady);
    this.isContactReady$ = this.store.select(UnapprovedOrganizationsSelectors.selectIsContactDataReady);
  }

  public onOrganizationLogoEdit(event: EditOrganizationId): void {
    this.store.dispatch(UnapprovedOrganizationsActions.openEditOrganizationLogoDialog({ organizationId: event.id }));
  }

  public onOrganizationNameEdit(event: EditOrganizationId): void {
    this.store.dispatch(UnapprovedOrganizationsActions.openEditOrganizationNameDialog({ organizationId: event.id }));
  }

  public onOrganizationDetailsEdit(event: EditOrganizationId): void {
    this.store.dispatch(UnapprovedOrganizationsActions.openEditOrganizationDetailsDialog({ organizationId: event.id }));
  }

  public onOrganizationContactEdit(event: EditOrganizationId): void {
    this.store.dispatch(UnapprovedOrganizationsActions.openEditOrganizationContactDialog({ organizationId: event.id }));
  }

  public onRequestApproval(event: EditOrganizationId): void {
    this.store.dispatch(UnapprovedOrganizationsActions.requestApproval({ organizationId: event.id }));
  }
}
