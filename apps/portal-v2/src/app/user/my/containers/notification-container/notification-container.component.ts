import { ChangeDetectionStrategy, Component, inject } from '@angular/core';

import { Store } from '@ngrx/store';
import * as RouterActions from '@router/router.actions';
import { NotificationActions, AppNotification, NotificationSelectors } from '@user/store/notification';

import { Observable } from 'rxjs';

@Component({
  selector: 'app-notification-container',
  templateUrl: './notification-container.component.html',
  styleUrls: ['./notification-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotificationContainerComponent {
  private store = inject(Store);

  protected notifications$: Observable<AppNotification[]>;

  constructor() {
    this.store.dispatch(NotificationActions.initNotificationPage());

    this.notifications$ = this.store.select(NotificationSelectors.selectNotifications);
  }

  public navigateTo($event: { route: string }): void {
    this.store.dispatch(RouterActions.navigate({ path: [$event.route] }));
  }

  public deleteItem($event: { id: string}) {
    if ($event.id) {
      this.store.dispatch(NotificationActions.notificationConfirm({ id: $event.id }));
    }
  }
}
