import { Component, inject } from '@angular/core';
import { AuthSelectors } from '@auth/store/auth';
import { UserProfile } from '@auth/store/auth/auth.model';
import { UserEntity } from '@user/store/user/user.models';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { UserActions, UserSelectors } from '@user/store/user';
import { LanguageService } from '@transloco/language.service';
import { LangDefinition } from '@ngneat/transloco';
import { AuthService } from '@auth/services/auth.service';

@Component({
  selector: 'app-profile-container',
  templateUrl: './profile-container.component.html',
  styleUrls: ['./profile-container.component.scss']
})
export class ProfileContainerComponent {
  private store = inject(Store);
  protected authProfileInfo$!: Observable<UserProfile | undefined>;
  protected profile$!: Observable<UserEntity | undefined>;
  protected loggedIn$!: Observable<boolean>;
  protected ready$!: Observable<boolean>;

  protected isManager$: Observable<boolean>;
  protected isModerator$: Observable<boolean>;

  protected currentLang$: Observable<string>;

  constructor(
    private languageService: LanguageService,
    private authService: AuthService,
  ) {
    this.authProfileInfo$ = this.store.select(AuthSelectors.selectAuthProfile);
    this.profile$ = this.store.select(UserSelectors.selectUserProfile);
    this.loggedIn$ = this.store.select(AuthSelectors.selectLoggedIn);
    this.ready$ = this.store.select(AuthSelectors.selectIsDoneLoading);

    this.isManager$ = this.store.select(AuthSelectors.selectIsManager);
    this.isModerator$ = this.store.select(UserSelectors.selectIsModerator);

    this.currentLang$ = this.languageService.getLanguage$();

    this.store.dispatch(UserActions.loadProfile());
  }

  public setLanguage(language: LangDefinition): void {
    this.languageService.setActiveLanguage(language);
    this.store.dispatch(UserActions.setUserLanguage({
      code: this.languageService.langDefinitionToCode(language)
    }));
  }

  public onOpenDetailsDialog(): void {
    this.store.dispatch(UserActions.openEditUserDetailsDialog());
  }

  public onOpenContactDataDialog(): void {
    this.store.dispatch(UserActions.openEditUserContactDataDialog());
  }

  public onOpenAvatarDialog(): void {
    this.store.dispatch(UserActions.openEditUserAvatarDialog());
  }

  public onPasswordReset(): void {
    const url = this.authService.changePasswordUrl;
    // console.log('update password url',url);
    if (url) {
      window.location.href = url;
    }
  }

  public onOpenDeleteAccountDialog(): void {
    this.store.dispatch(UserActions.openDeleteUserAccountDialog());
  }

}
