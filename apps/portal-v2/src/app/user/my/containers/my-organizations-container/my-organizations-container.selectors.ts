import { createSelector } from '@ngrx/store';
import { UserOrganizationTableData } from '@user/store/user-organizations/user-organizations.models';
import { selectAllUserOrganizations, selectUserOrganizationsDefaultOrgId } from '@user/store/user-organizations/user-organizations.selectors';



export const selectMyOrganizationTableData = createSelector(
  selectAllUserOrganizations,
  selectUserOrganizationsDefaultOrgId,
  (orgs, defaultOrgId) => {
    return orgs.map(o =>
      <UserOrganizationTableData>({
        ...o,
        isDefault: (o.organization_id === defaultOrgId) ? true : false
      })
    );
  }
);
