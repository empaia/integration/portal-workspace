import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MembershipRequestActions, MembershipRequestEntity, MembershipRequestSelectors } from '@user/store/membership-request';
import { UnapprovedOrganizationsEntity } from '@user/store/unapproved-organizations/unapproved-organizations.model';

import { UserOrganizationsSelectors } from '@user/store/user-organizations';
import { UserOrganizationTableData } from '@user/store/user-organizations/user-organizations.models';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { UnapprovedOrganizationsActions, UnapprovedOrganizationsSelectors } from '@user/store/unapproved-organizations';
import * as RouterActions from '@router/router.actions';
import { selectMyOrganizationTableData } from './my-organizations-container.selectors';
import { AppRoutes } from '@router/router.constants';

@Component({
  selector: 'app-my-organizations-container',
  templateUrl: './my-organizations-container.component.html',
  styleUrls: ['./my-organizations-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MyOrganizationsContainerComponent {
  private store = inject(Store);

  protected myOrgs$: Observable<UserOrganizationTableData[]>;
  protected myRequests$: Observable<MembershipRequestEntity[]>;
  protected myUnapprovedOrgs$: Observable<UnapprovedOrganizationsEntity[]>;

  protected defaultOrg$: Observable<string | undefined>;


  constructor() {
    this.store.dispatch(UnapprovedOrganizationsActions.initializeMyUnapprovedOrganizationsPage());

    this.myOrgs$ = this.store.select(selectMyOrganizationTableData);
    this.myRequests$ = this.store.select(MembershipRequestSelectors.selectMembershipRequests);
    this.myUnapprovedOrgs$ = this.store.select(UnapprovedOrganizationsSelectors.selectUnapprovedOrganizations);

    this.defaultOrg$ = this.store.select(UserOrganizationsSelectors.selectUserOrganizationsDefaultOrgId);
  }


  // from my-membership-requests-table
  public deleteRequestClicked(event: { id: number }): void {
    this.store.dispatch(MembershipRequestActions.revoke(event));
  }

  // from my-unapproved-organizations-table
  public editUnapprovedOrganizationClicked(event: { id: string }): void {
    this.store.dispatch(RouterActions.navigate({
      path: [AppRoutes.MY_ROUTE, AppRoutes.MY_ORGANIZATIONS_ROUTE, event.id]
    }));
  }

  public deleteUnapprovedOrganizationClicked(event: { id: string }): void {
    this.store.dispatch(UnapprovedOrganizationsActions.deleteOrganization({
      organizationId: event.id
    }));
  }
}
