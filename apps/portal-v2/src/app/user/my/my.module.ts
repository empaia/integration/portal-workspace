import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { ProfileContainerComponent } from './containers/profile-container/profile-container.component';
import { MyRoutingModule } from './my-routing.module';
import { MyOrganizationsContainerComponent } from './containers/my-organizations-container/my-organizations-container.component';
import { MyOrganizationsTableComponent } from './components/my-organizations-table/my-organizations-table.component';
import { MyMembershipRequestsTableComponent } from './components/my-membership-requests-table/my-membership-requests-table.component';
import { MyUnapprovedOrganizationsTableComponent } from './components/my-unapproved-organizations-table/my-unapproved-organizations-table.component';
import { SelectOrganizationCardComponent } from './components/select-organization-card/select-organization-card.component';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '@material/material.module';
import { TranslocoRootModule } from '@transloco/transloco-root.module';

import { RoleRequestDialogComponent } from './components/role-request-dialog/role-request-dialog.component';
import { ProfileOverviewComponent } from './components/profile-overview/profile-overview.component';
import { ProfileDetailsCardComponent } from './components/profile-details-card/profile-details-card.component';
import { ProfileContactCardComponent } from './components/profile-contact-card/profile-contact-card.component';
import { ProfileUserDataCardComponent } from './components/profile-user-data-card/profile-user-data-card.component';
import { ProfileAccountControlCardComponent } from './components/profile-account-control-card/profile-account-control-card.component';
import { ProfileAvatarPictureCardComponent } from './components/profile-avatar-picture-card/profile-avatar-picture-card.component';
import { ProfileLanguageSelectButtonComponent } from './components/profile-language-select-button/profile-language-select-button.component';
import { ProfileEditDetailsDialogComponent } from './components/profile-edit-details-dialog/profile-edit-details-dialog.component';
import { ProfileEditContactDialogComponent } from './components/profile-edit-contact-dialog/profile-edit-contact-dialog.component';
import { ProfileEditAvatarDialogComponent } from './components/profile-edit-avatar-dialog/profile-edit-avatar-dialog.component';
import { ImageCropperComponent } from 'ngx-image-cropper';
import { EditMyUnapprovedOrganizationContainerComponent } from './containers/edit-my-unapproved-organization-container/edit-my-unapproved-organization-container.component';
import { EditOrganizationModule } from '@edit-organization/edit-organization.module';
import { SelectOrganizationCheckboxComponent } from './components/select-organization-checkbox/select-organization-checkbox.component';
import { SelectOrganizationDialogComponent } from './components/select-organization-dialog/select-organization-dialog.component';
import { ActiveOrganizationContainerComponent } from './containers/active-organization-container/active-organization-container.component';
import { RoleRequestsTableComponent } from './components/role-requests-table/role-requests-table.component';
import { OrganizationMembersTableComponent } from './components/organization-members-table/organization-members-table.component';
import { RoleRequestButtonComponent } from './components/role-request-button/role-request-button.component';
import { NotificationContainerComponent } from './containers/notification-container/notification-container.component';
import { NotificationListComponent } from './components/notification-list/notification-list.component';
import { NotificationItemComponent } from './components/notification-item/notification-item.component';

@NgModule({
  declarations: [
    ProfileContainerComponent,
    MyOrganizationsContainerComponent,
    MyOrganizationsTableComponent,
    MyMembershipRequestsTableComponent,
    MyUnapprovedOrganizationsTableComponent,
    SelectOrganizationCardComponent,
    RoleRequestDialogComponent,
    ProfileOverviewComponent,
    ProfileDetailsCardComponent,
    ProfileContactCardComponent,
    ProfileUserDataCardComponent,
    ProfileAccountControlCardComponent,
    ProfileAvatarPictureCardComponent,
    ProfileLanguageSelectButtonComponent,
    ProfileEditDetailsDialogComponent,
    ProfileEditContactDialogComponent,
    ProfileEditAvatarDialogComponent,
    EditMyUnapprovedOrganizationContainerComponent,
    SelectOrganizationCheckboxComponent,
    SelectOrganizationDialogComponent,
    ActiveOrganizationContainerComponent,
    RoleRequestsTableComponent,
    OrganizationMembersTableComponent,
    RoleRequestButtonComponent,
    NotificationContainerComponent,
    NotificationListComponent,
    NotificationItemComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    RouterModule,

    TranslocoRootModule,
    MyRoutingModule,
    ImageCropperComponent,
    EditOrganizationModule
  ]
})
export class MyModule { }
