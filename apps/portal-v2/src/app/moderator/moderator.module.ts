import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '@material/material.module';
import { ModeratorRoutingModule } from './moderator-routing.module';
import { ModerateUsersContainerComponent } from './containers/moderate-users-container/moderate-users-container.component';
import { ModerateOrganizationsContainerComponent } from './containers/moderate-organizations-container/moderate-organizations-container.component';
import { ModerateApprovedOrganizationsTableComponent } from './components/moderate-approved-organizations-table/moderate-approved-organizations-table.component';
import { ModerateRootStoreModule } from './store/moderate-root-store.module';
import { ModerateUnapprovedOrganizationsTableComponent } from './components/moderate-unapproved-organizations-table/moderate-unapproved-organizations-table.component';
import { ModerateCategoryRequestsTableComponent } from './components/moderate-category-requests-table/moderate-category-requests-table.component';
import { ModerateReviewUnapprovedOrganizationContainerComponent } from './containers/moderate-review-unapproved-organization-container/moderate-review-unapproved-organization-container.component';
import { EditOrganizationModule } from '@edit-organization/edit-organization.module';
import { ModerateEditOrgCategoriesDialogComponent } from './components/moderate-edit-org-categories-dialog/moderate-edit-org-categories-dialog.component';
import { ModerateUserOrganizationsContainerComponent } from './containers/moderate-user-organizations-container/moderate-user-organizations-container.component';
import { ModerateUnapprovedUsersTableComponent } from './components/moderate-unapproved-users-table/moderate-unapproved-users-table.component';
import { ModerateApprovedUsersTableComponent } from './components/moderate-approved-users-table/moderate-approved-users-table.component';
import { ModerateUserOrganizationsTableComponent } from './components/moderate-user-organizations-table/moderate-user-organizations-table.component';
import { ModerateUserProfileDialogComponent } from './components/moderate-user-profile-dialog/moderate-user-profile-dialog.component';
import { ModerateUserRolesDialogComponent } from './components/moderate-user-roles-dialog/moderate-user-roles-dialog.component';
import { ModerateSendInvitationDialogComponent } from './components/moderate-send-invitation-dialog/moderate-send-invitation-dialog.component';
import { ModerateStatisticsContainerComponent } from './containers/moderate-statistics-container/moderate-statistics-container.component';
import { ModerateTrafficStatisticsTableComponent } from './components/moderate-traffic-statistics-table/moderate-traffic-statistics-table.component';


@NgModule({
  declarations: [

    ModerateUsersContainerComponent,
    ModerateOrganizationsContainerComponent,
    ModerateApprovedOrganizationsTableComponent,
    ModerateUnapprovedOrganizationsTableComponent,
    ModerateCategoryRequestsTableComponent,
    ModerateReviewUnapprovedOrganizationContainerComponent,
    ModerateEditOrgCategoriesDialogComponent,
    ModerateUserOrganizationsContainerComponent,
    ModerateUnapprovedUsersTableComponent,
    ModerateApprovedUsersTableComponent,
    ModerateUserOrganizationsTableComponent,
    ModerateUserProfileDialogComponent,
    ModerateUserRolesDialogComponent,
    ModerateSendInvitationDialogComponent,
    ModerateStatisticsContainerComponent,
    ModerateTrafficStatisticsTableComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    RouterModule,
    ModeratorRoutingModule,
    ModerateRootStoreModule,
    EditOrganizationModule,
  ]
})
export class ModeratorModule { }
