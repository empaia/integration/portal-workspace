import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutes } from '@router/router.constants';
import { ModerateOrganizationsContainerComponent } from './containers/moderate-organizations-container/moderate-organizations-container.component';
import { ModerateReviewUnapprovedOrganizationContainerComponent } from './containers/moderate-review-unapproved-organization-container/moderate-review-unapproved-organization-container.component';
import { ModerateUserOrganizationsContainerComponent } from './containers/moderate-user-organizations-container/moderate-user-organizations-container.component';
import { ModerateUsersContainerComponent } from './containers/moderate-users-container/moderate-users-container.component';
import { ModerateStatisticsContainerComponent } from './containers/moderate-statistics-container/moderate-statistics-container.component';

const routes: Routes = [
  {

    path: AppRoutes.MODERATE_USERS,
    component: ModerateUsersContainerComponent,
  },
  {
    path: AppRoutes.MODERATE_USER_ORGANIZATIONS,
    component: ModerateUserOrganizationsContainerComponent,
  },
  {
    path: AppRoutes.MODERATE_ORGANIZATIONS,
    component: ModerateOrganizationsContainerComponent,
  },
  {
    path: AppRoutes.MODERATE_ORGANIZATIONS + '/:' + AppRoutes.UNAPPROVED_ORGANIZATION_ID,
    component: ModerateReviewUnapprovedOrganizationContainerComponent,
  },
  {
    path: AppRoutes.MODERATE_STATISTICS,
    component: ModerateStatisticsContainerComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModeratorRoutingModule { }
