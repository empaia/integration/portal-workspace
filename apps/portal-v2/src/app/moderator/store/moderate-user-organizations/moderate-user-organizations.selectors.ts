import { createSelector } from '@ngrx/store';
import { selectModerateRootState } from '../moderate-root.selectors';
import { moderateUserOrganizationsAdapter, MODERATE_USER_ORGANIZATIONS_FEATURE_KEY } from './moderate-user-organizations.reducer';

const {
  selectAll,
  selectEntities,
} = moderateUserOrganizationsAdapter.getSelectors();

export const selectModerateUserOrganizationsState = createSelector(
  selectModerateRootState,
  (state) => state[MODERATE_USER_ORGANIZATIONS_FEATURE_KEY]
);

export const selectAllUserOrganizations = createSelector(
  selectModerateUserOrganizationsState,
  selectAll
);

export const selectUserOrganizationEntities = createSelector(
  selectModerateUserOrganizationsState,
  selectEntities
);

export const selectUserOrganizationsPage = createSelector(
  selectModerateUserOrganizationsState,
  (state) => state.page
);

export const selectUserOrganizationsLoaded = createSelector(
  selectModerateUserOrganizationsState,
  (state) => state.loaded
);

export const selectUserOrganizationsError = createSelector(
  selectModerateUserOrganizationsState,
  (state) => state.error
);
