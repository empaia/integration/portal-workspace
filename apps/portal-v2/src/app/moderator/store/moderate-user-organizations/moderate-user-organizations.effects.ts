import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ModeratorControllerService } from '@api/aaa/services';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { fetch } from '@ngrx/router-store/data-persistence';
import { defaultPaginationParams } from '@shared/api/default-params';
import { map } from 'rxjs/operators';
import { ModerateApprovedUsersActions } from '../moderate-approved-users';
import { ModerateUserOrganizationsActions } from './moderate-user-organizations.actions';
import { State } from './moderate-user-organizations.reducer';

@Injectable()
export class ModerateUserOrganizationsEffects {
  loadOnUserSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateApprovedUsersActions.selectUser),
      map((action) => action.selected),
      map((userId) =>
        ModerateUserOrganizationsActions.loadAllUserOrganizations({
          userId,
          page: defaultPaginationParams,
        })
      )
    );
  });

  loadAllUserOrganizations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateUserOrganizationsActions.loadAllUserOrganizations),
      fetch({
        run: (
          action: ReturnType<
            typeof ModerateUserOrganizationsActions.loadAllUserOrganizations
          >,
          _state: State
        ) => {
          return this.moderatorControllerService
            .getOrganizationsOfUser({
              target_user_id: action.userId,
              ...action.page,
            })
            .pipe(
              map((response) => response.organizations),
              map((organizations) =>
                ModerateUserOrganizationsActions.loadAllUserOrganizationsSuccess(
                  { organizations }
                )
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ModerateUserOrganizationsActions.loadAllUserOrganizations
          >,
          error: HttpErrorResponse
        ) => {
          return ModerateUserOrganizationsActions.loadAllUserOrganizationsFailure(
            { error }
          );
        },
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly moderatorControllerService: ModeratorControllerService
  ) {}
}
