import { HttpErrorResponse } from '@angular/common/http';
import { createActionGroup, props } from '@ngrx/store';
import { PaginationParameters } from '@shared/api/default-params';
import { ModerateUserOrganizationEntity } from './moderate-user-organizations.models';

export const ModerateUserOrganizationsActions = createActionGroup({
  source: 'ModerateUserOrganizations',
  events: {
    'Load All User Organizations': props<{ userId: string, page: PaginationParameters }>(),
    'Load All User Organizations Success': props<{ organizations: ModerateUserOrganizationEntity[] }>(),
    'Load All User Organizations Failure': props<{ error: HttpErrorResponse }>(),
  }
});
