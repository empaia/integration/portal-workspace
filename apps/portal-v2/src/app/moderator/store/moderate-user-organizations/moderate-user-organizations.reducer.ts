import { HttpErrorResponse } from '@angular/common/http';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { defaultPaginationParams, PaginationParameters } from '@shared/api/default-params';
import { ModerateUserOrganizationsActions } from './moderate-user-organizations.actions';
import { ModerateUserOrganizationEntity } from './moderate-user-organizations.models';


export const MODERATE_USER_ORGANIZATIONS_FEATURE_KEY = 'moderateUserOrganizations';

export interface State extends EntityState<ModerateUserOrganizationEntity> {
  loaded: boolean;
  page: PaginationParameters;
  error?: HttpErrorResponse | undefined;
}

export const moderateUserOrganizationsAdapter = createEntityAdapter<ModerateUserOrganizationEntity>({
  selectId: organization => organization.organization_id
});

export const initialState: State = moderateUserOrganizationsAdapter.getInitialState({
  loaded: true,
  page: defaultPaginationParams,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(ModerateUserOrganizationsActions.loadAllUserOrganizations, (state, { page }): State => ({
    ...state,
    loaded: false,
    page,
  })),
  on(ModerateUserOrganizationsActions.loadAllUserOrganizationsSuccess, (state, { organizations }): State =>
    moderateUserOrganizationsAdapter.setAll(organizations, {
      ...state,
      loaded: true,
    })
  ),
  on(ModerateUserOrganizationsActions.loadAllUserOrganizationsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
);
