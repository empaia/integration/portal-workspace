import { PublicOrganizationEntity } from '@api/aaa/models';

export type ModerateUserOrganizationEntity = PublicOrganizationEntity;
