import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { MODERATE_ROOT_FEATURE_KEY, reducers } from './moderate-root.selectors';
import { EffectsModule } from '@ngrx/effects';
import { ModerateOrganizationsEffects } from './moderate-organizations/moderate-organizations.effects';
import { ModerateOrganizationCategoryRequestsEffects } from './moderate-organization-category-requests/moderate-organization-category-requests.effects';
import { ModerateUnapprovedOrganizationsEffects } from './moderate-unapproved-organizations/moderate-unapproved-organizations.effects';
import { ModerateUnapprovedUsersEffects } from './moderate-unapproved-users/moderate-unapproved-users.effects';
import { ModerateApprovedUsersEffects } from './moderate-approved-users';
import { ModerateUserOrganizationsEffects } from './moderate-user-organizations/moderate-user-organizations.effects';
import { ModerateStatisticsEffects } from './moderate-statistics';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(
      MODERATE_ROOT_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      ModerateApprovedUsersEffects,
      ModerateUnapprovedUsersEffects,
      ModerateUserOrganizationsEffects,
      ModerateOrganizationsEffects,
      ModerateOrganizationCategoryRequestsEffects,
      ModerateUnapprovedOrganizationsEffects,
      ModerateStatisticsEffects,
    ])
  ]
})
export class ModerateRootStoreModule { }
