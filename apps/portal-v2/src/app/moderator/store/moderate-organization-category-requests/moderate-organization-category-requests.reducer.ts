import { HttpErrorResponse } from '@angular/common/http';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { defaultPaginationParams, PaginationParameters } from '@shared/api/default-params';
import { ModerateOrganizationCategoryRequestsActions } from './moderate-organization-category-requests.actions';
import { ModerateOrganizationCategoryRequestEntity } from './moderate-organization-category-requests.models';


export const MODERATE_ORGANIZATION_CATEGORY_REQUESTS_FEATURE_KEY = 'moderateOrganizationCategoryRequests';

export interface State extends EntityState<ModerateOrganizationCategoryRequestEntity> {
  loaded: boolean;
  page: PaginationParameters;
  error?: HttpErrorResponse | undefined;
}

export const moderateOrganizationCategoryRequestAdapter = createEntityAdapter<ModerateOrganizationCategoryRequestEntity>({
  selectId: request => request.organization_category_request_id
});

export const initialState: State = moderateOrganizationCategoryRequestAdapter.getInitialState({
  loaded: true,
  page: defaultPaginationParams,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(ModerateOrganizationCategoryRequestsActions.loadCategoryRequests, (state, { page }): State => ({
    ...state,
    loaded: false,
    page,
  })),
  on(ModerateOrganizationCategoryRequestsActions.loadCategoryRequestsSuccess, (state, { requests }): State =>
    moderateOrganizationCategoryRequestAdapter.setAll(requests, {
      ...state,
      loaded: true,
    })
  ),
  on(ModerateOrganizationCategoryRequestsActions.loadCategoryRequestsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
);
