import { HttpErrorResponse } from '@angular/common/http';
import { createActionGroup, props } from '@ngrx/store';
import { PaginationParameters } from '@shared/api/default-params';
import { ModerateOrganizationCategoryRequestEntity } from './moderate-organization-category-requests.models';

export const ModerateOrganizationCategoryRequestsActions = createActionGroup({
  source: 'ModerateOrganizationsCategoryRequests',
  events: {
    'Load Category Requests': props<{ page: PaginationParameters}>(),
    'Load Category Requests Success': props<{ requests: ModerateOrganizationCategoryRequestEntity[] }>(),
    'Load Category Requests Failure': props<{ error: HttpErrorResponse }>(),

    'Accept Category Request': props<{ categoryRequestId: number }>(),
    'Accept Category Request Failure': props<{ error: HttpErrorResponse }>(),
    'Deny Category Request': props<{ categoryRequestId: number, comment: string }>(),
    'Deny Category Request Failure': props<{ error: HttpErrorResponse }>(),

    'Open Accept Category Request Dialog': props<{ categoryRequestId: number }>(),
    'Open Deny Category Request Dialog': props<{ categoryRequestId: number }>(),
  }
});
