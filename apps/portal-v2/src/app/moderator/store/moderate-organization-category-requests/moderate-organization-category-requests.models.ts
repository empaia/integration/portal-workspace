import { OrganizationCategoryRequestEntity } from '@api/aaa/models';
import { AcceptDialogEntity } from '@shared/components/accept-dialog/accept-dialog.component';
import { DenyDialogEntity } from '@shared/components/deny-dialog/deny-dialog.component';

export type ModerateOrganizationCategoryRequestEntity = OrganizationCategoryRequestEntity;

export interface CategoryRequestId {
  id: number;
}

export const DEFAULT_ACCEPT_CATEGORY_REQUESTS_DIALOG_TEXT: AcceptDialogEntity = {
  headlineText: 'moderate_forms.accept_category_request',
  contentText: 'moderate_forms.category_change_accept_confirmation',
  confirmButtonText: 'moderate_forms.confirm',
  cancelButtonText: 'moderate_forms.cancel',
};

export const DEFAULT_DENY_CATEGORY_REQUESTS_DIALOG_TEXT: DenyDialogEntity = {
  headlineText: 'moderate_forms.deny_category_request',
  reviewLabel: 'moderate_forms.review',
  confirmButtonText: 'moderate_forms.confirm',
  cancelButtonText: 'moderate_forms.cancel'
};
