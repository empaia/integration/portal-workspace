import { createSelector } from '@ngrx/store';
import { selectModerateRootState } from '../moderate-root.selectors';
import {
  moderateOrganizationCategoryRequestAdapter,
  MODERATE_ORGANIZATION_CATEGORY_REQUESTS_FEATURE_KEY
} from './moderate-organization-category-requests.reducer';

const {
  selectAll,
  selectEntities,
} = moderateOrganizationCategoryRequestAdapter.getSelectors();

export const selectModerateOrganizationCategoryRequestsState = createSelector(
  selectModerateRootState,
  (state) => state[MODERATE_ORGANIZATION_CATEGORY_REQUESTS_FEATURE_KEY]
);

export const selectAllOrganizationCategoryRequests = createSelector(
  selectModerateOrganizationCategoryRequestsState,
  selectAll
);

export const selectOrganizationCategoryRequestEntities = createSelector(
  selectModerateOrganizationCategoryRequestsState,
  selectEntities
);

export const selectOrganizationCategoryRequestsLoaded = createSelector(
  selectModerateOrganizationCategoryRequestsState,
  (state) => state.loaded
);

export const selectOrganizationCategoryRequestsError = createSelector(
  selectModerateOrganizationCategoryRequestsState,
  (state) => state.error
);

export const selectOrganizationCategoryRequestsPage = createSelector(
  selectModerateOrganizationCategoryRequestsState,
  (state) => state.page
);
