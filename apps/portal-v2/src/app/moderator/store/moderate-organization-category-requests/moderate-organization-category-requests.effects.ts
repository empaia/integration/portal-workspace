import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ModeratorControllerService } from '@api/aaa/services';
import { SMALL_DIALOG_WIDTH } from '@edit-organization/models/edit-organization.models';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';
import { defaultPaginationParams } from '@shared/api/default-params';
import { AcceptDialogComponent } from '@shared/components/accept-dialog/accept-dialog.component';
import { DenyDialogComponent } from '@shared/components/deny-dialog/deny-dialog.component';
import { filterNullish } from '@shared/utility/rxjs-utility';
import { exhaustMap, map } from 'rxjs/operators';
import { EventActions } from 'src/app/events/store';
import { ModerateOrganizationsActions } from '../moderate-organizations';
import { ModerateOrganizationCategoryRequestsActions } from './moderate-organization-category-requests.actions';
import {
  DEFAULT_ACCEPT_CATEGORY_REQUESTS_DIALOG_TEXT,
  DEFAULT_DENY_CATEGORY_REQUESTS_DIALOG_TEXT,
} from './moderate-organization-category-requests.models';
import { State } from './moderate-organization-category-requests.reducer';

@Injectable()
export class ModerateOrganizationCategoryRequestsEffects {
  initialLoadRequests$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateOrganizationsActions.initialLoadOrganizations),
      map(() =>
        ModerateOrganizationCategoryRequestsActions.loadCategoryRequests({
          page: defaultPaginationParams,
        })
      )
    );
  });

  sseRefresh$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(EventActions.sseUpdateCategoryChange),
      // TODO: filter - update on SSEventActions.CREATED
      map(() =>
        ModerateOrganizationCategoryRequestsActions.loadCategoryRequests({
          page: defaultPaginationParams,
        })
      )
    );
  });

  loadAllCategoryRequests$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateOrganizationCategoryRequestsActions.loadCategoryRequests),
      fetch({
        run: (
          action: ReturnType<
            typeof ModerateOrganizationCategoryRequestsActions.loadCategoryRequests
          >,
          _state: State
        ) => {
          return this.moderatorControllerService
            .getAllOrganizationCategoryRequests(action.page)
            .pipe(
              map((response) => response.organization_category_requests),
              map((requests) =>
                ModerateOrganizationCategoryRequestsActions.loadCategoryRequestsSuccess(
                  { requests }
                )
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ModerateOrganizationCategoryRequestsActions.loadCategoryRequests
          >,
          error: HttpErrorResponse
        ) => {
          return ModerateOrganizationCategoryRequestsActions.loadCategoryRequestsFailure(
            { error }
          );
        },
      })
    );
  });

  acceptCategoryRequest$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateOrganizationCategoryRequestsActions.acceptCategoryRequest),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof ModerateOrganizationCategoryRequestsActions.acceptCategoryRequest
          >,
          _state: State
        ) => {
          return this.moderatorControllerService
            .acceptOrganizationCategoryRequest({
              category_request_id: action.categoryRequestId,
            })
            .pipe(
              // Reload category requests, later persist the answer of the backend, Backend-Issue: #99
              map(() =>
                ModerateOrganizationCategoryRequestsActions.loadCategoryRequests(
                  { page: defaultPaginationParams }
                )
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ModerateOrganizationCategoryRequestsActions.acceptCategoryRequest
          >,
          error: HttpErrorResponse
        ) => {
          return ModerateOrganizationCategoryRequestsActions.acceptCategoryRequestFailure(
            { error }
          );
        },
      })
    );
  });

  denyCategoryRequest$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateOrganizationCategoryRequestsActions.denyCategoryRequest),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof ModerateOrganizationCategoryRequestsActions.denyCategoryRequest
          >,
          _state: State
        ) => {
          return this.moderatorControllerService
            .denyOrganizationCategoryRequest({
              category_request_id: action.categoryRequestId,
              body: {
                reviewer_comment: action.comment,
              },
            })
            .pipe(
              // Reload category requests, later persist the answer of the backend, Backend-Issue: #99
              map(() =>
                ModerateOrganizationCategoryRequestsActions.loadCategoryRequests(
                  { page: defaultPaginationParams }
                )
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ModerateOrganizationCategoryRequestsActions.denyCategoryRequest
          >,
          error: HttpErrorResponse
        ) => {
          return ModerateOrganizationCategoryRequestsActions.denyCategoryRequestFailure(
            { error }
          );
        },
      })
    );
  });

  openAcceptDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ModerateOrganizationCategoryRequestsActions.openAcceptCategoryRequestDialog
      ),
      map((action) => action.categoryRequestId),
      exhaustMap((categoryRequestId) =>
        this.dialog
          .open(AcceptDialogComponent, {
            data: DEFAULT_ACCEPT_CATEGORY_REQUESTS_DIALOG_TEXT,
            width: SMALL_DIALOG_WIDTH,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map(() =>
              ModerateOrganizationCategoryRequestsActions.acceptCategoryRequest(
                { categoryRequestId }
              )
            )
          )
      )
    );
  });

  openDenyDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ModerateOrganizationCategoryRequestsActions.openDenyCategoryRequestDialog
      ),
      map((action) => action.categoryRequestId),
      exhaustMap((categoryRequestId) =>
        this.dialog
          .open(DenyDialogComponent, {
            data: DEFAULT_DENY_CATEGORY_REQUESTS_DIALOG_TEXT,
            width: SMALL_DIALOG_WIDTH,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map((comment) =>
              ModerateOrganizationCategoryRequestsActions.denyCategoryRequest({
                categoryRequestId,
                comment,
              })
            )
          )
      )
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly moderatorControllerService: ModeratorControllerService,
    private readonly dialog: MatDialog
  ) {}
}
