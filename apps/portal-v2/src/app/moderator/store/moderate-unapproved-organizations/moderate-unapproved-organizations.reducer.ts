import { HttpErrorResponse } from '@angular/common/http';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { defaultPaginationParams, PaginationParameters } from '@shared/api/default-params';
import { ModerateUnapprovedOrganizationsActions } from './moderate-unapproved-organizations.actions';
import { ModerateUnapprovedOrganizationEntity, ModerateUnapprovedOrganizationProfileEntity } from './moderate-unapproved-organizations.models';


export const MODERATE_UNAPPROVED_ORGANIZATIONS_FEATURE_KEY = 'moderateUnapprovedOrganizations';

export interface State extends EntityState<ModerateUnapprovedOrganizationEntity> {
  loaded: boolean;
  page: PaginationParameters;
  profile: ModerateUnapprovedOrganizationProfileEntity | undefined,
  error?: HttpErrorResponse | undefined;
}

export const moderateUnapprovedOrganizationsAdapter = createEntityAdapter<ModerateUnapprovedOrganizationEntity>({
  selectId: organization => organization.organization_id
});

export const initialState: State = moderateUnapprovedOrganizationsAdapter.getInitialState({
  loaded: true,
  page: defaultPaginationParams,
  profile: undefined,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(ModerateUnapprovedOrganizationsActions.loadAllUnapprovedOrganizations, (state, { page }): State => ({
    ...state,
    loaded: false,
    page,
  })),
  on(ModerateUnapprovedOrganizationsActions.loadAllUnapprovedOrganizationsSuccess, (state, { organizations }): State =>
    moderateUnapprovedOrganizationsAdapter.setAll(organizations, {
      ...state,
      loaded: true,
    })
  ),
  on(ModerateUnapprovedOrganizationsActions.loadAllUnapprovedOrganizationsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ModerateUnapprovedOrganizationsActions.loadUnapprovedOrganizationProfile, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ModerateUnapprovedOrganizationsActions.loadUnapprovedOrganizationProfileSuccess, (state, { profile }): State => ({
    ...state,
    loaded: true,
    profile,
  })),
  on(ModerateUnapprovedOrganizationsActions.loadUnapprovedOrganizationProfileFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
);
