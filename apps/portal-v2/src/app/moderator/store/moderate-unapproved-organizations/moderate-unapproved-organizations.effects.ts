import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { OrganizationAccountState } from '@api/aaa/models';
import { ModeratorControllerService } from '@api/aaa/services';
import { SMALL_DIALOG_WIDTH } from '@edit-organization/models/edit-organization.models';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { ROUTER_NAVIGATED } from '@ngrx/router-store';
import { Store } from '@ngrx/store';
import { pessimisticUpdate, fetch } from '@ngrx/router-store/data-persistence';
import { RouterSelectors } from '@router/router.selectors';
import { defaultPaginationParams } from '@shared/api/default-params';
import { filterNullish } from '@shared/utility/rxjs-utility';
import { distinctUntilChanged, exhaustMap, map } from 'rxjs/operators';
import { ModerateOrganizationsActions } from '../moderate-organizations';
import { ModerateUnapprovedOrganizationsActions } from './moderate-unapproved-organizations.actions';
import { State } from './moderate-unapproved-organizations.reducer';
import * as RouterActions from '@router/router.actions';
import { AppRoutes } from '@router/router.constants';
import { AcceptDialogComponent } from '@shared/components/accept-dialog/accept-dialog.component';
import {
  DEFAULT_ACCEPT_UNAPPROVED_DIALOG_TEXT,
  DEFAULT_DENY_UNAPPROVED_DIALOG_TEXT,
} from './moderate-unapproved-organizations.models';
import { DenyDialogComponent } from '@shared/components/deny-dialog/deny-dialog.component';
import { EventActions } from 'src/app/events/store';

@Injectable()
export class ModerateUnapprovedOrganizationsEffects {
  initialLoadUnapprovedOrganizations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateOrganizationsActions.initialLoadOrganizations),
      map(() =>
        ModerateUnapprovedOrganizationsActions.loadAllUnapprovedOrganizations({
          page: defaultPaginationParams,
        })
      )
    );
  });

  sseRefresh$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(EventActions.sseUpdateUnapprovedOrganizations),
      // TODO: filter - update on CREATED
      map(() =>
        ModerateUnapprovedOrganizationsActions.loadAllUnapprovedOrganizations({
          page: defaultPaginationParams,
        })
      )
    );
  });

  loadAllUnapprovedOrganizations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ModerateUnapprovedOrganizationsActions.loadAllUnapprovedOrganizations
      ),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof ModerateUnapprovedOrganizationsActions.loadAllUnapprovedOrganizations
          >,
          _state: State
        ) => {
          return this.moderatorControllerService
            .getUnapprovedOrganizationsEntity({
              ...action.page,
              body: {
                organization_account_states: [
                  OrganizationAccountState.AWAITING_ACTIVATION,
                ],
              },
            })
            .pipe(
              map((response) => response.organizations),
              map((organizations) =>
                ModerateUnapprovedOrganizationsActions.loadAllUnapprovedOrganizationsSuccess(
                  { organizations }
                )
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ModerateUnapprovedOrganizationsActions.loadAllUnapprovedOrganizations
          >,
          error: HttpErrorResponse
        ) => {
          return ModerateUnapprovedOrganizationsActions.loadAllUnapprovedOrganizationsFailure(
            { error }
          );
        },
      })
    );
  });

  loadProfile$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ModerateUnapprovedOrganizationsActions.loadUnapprovedOrganizationProfile
      ),
      fetch({
        run: (
          action: ReturnType<
            typeof ModerateUnapprovedOrganizationsActions.loadUnapprovedOrganizationProfile
          >,
          _state: State
        ) => {
          return this.moderatorControllerService
            .getUnapprovedOrganizationProfile({
              organization_id: action.organizationId,
            })
            .pipe(
              map((profile) =>
                ModerateUnapprovedOrganizationsActions.loadUnapprovedOrganizationProfileSuccess(
                  { profile }
                )
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ModerateUnapprovedOrganizationsActions.loadUnapprovedOrganizationProfile
          >,
          error: HttpErrorResponse
        ) => {
          return ModerateUnapprovedOrganizationsActions.loadAllUnapprovedOrganizationsFailure(
            { error }
          );
        },
      })
    );
  });

  acceptUnapprovedOrganization$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ModerateUnapprovedOrganizationsActions.acceptUnapprovedOrganization
      ),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof ModerateUnapprovedOrganizationsActions.acceptUnapprovedOrganization
          >,
          _state: State
        ) => {
          return this.moderatorControllerService
            .acceptOrganization({
              organization_id: action.organizationId,
            })
            .pipe(
              map(() =>
                ModerateUnapprovedOrganizationsActions.acceptUnapprovedOrganizationSuccess(
                  { ...action }
                )
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ModerateUnapprovedOrganizationsActions.acceptUnapprovedOrganization
          >,
          error: HttpErrorResponse
        ) => {
          return ModerateUnapprovedOrganizationsActions.acceptUnapprovedOrganizationFailure(
            { error }
          );
        },
      })
    );
  });

  denyUnapprovedOrganization$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateUnapprovedOrganizationsActions.denyUnapprovedOrganization),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof ModerateUnapprovedOrganizationsActions.denyUnapprovedOrganization
          >,
          _state: State
        ) => {
          return this.moderatorControllerService
            .denyOrganization({
              organization_id: action.organizationId,
              body: { reviewer_comment: action.comment },
            })
            .pipe(
              map(() =>
                ModerateUnapprovedOrganizationsActions.denyUnapprovedOrganizationSuccess()
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ModerateUnapprovedOrganizationsActions.denyUnapprovedOrganization
          >,
          error: HttpErrorResponse
        ) => {
          return ModerateUnapprovedOrganizationsActions.denyUnapprovedOrganizationFailure(
            { error }
          );
        },
      })
    );
  });

  loadProfileOnRoute$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ROUTER_NAVIGATED),
      concatLatestFrom(() =>
        this.store.select(
          RouterSelectors.selectRouterParamUnapprovedOrganizationId
        )
      ),
      map(([, id]) => id),
      distinctUntilChanged(),
      filterNullish(),
      map((organizationId) =>
        ModerateUnapprovedOrganizationsActions.loadUnapprovedOrganizationProfile(
          { organizationId }
        )
      )
    );
  });

  // when an organization was successfully accepted or denied
  // route back to moderate organizations overview
  routeToOverviewAfterReview$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ModerateUnapprovedOrganizationsActions.acceptUnapprovedOrganizationSuccess,
        ModerateUnapprovedOrganizationsActions.denyUnapprovedOrganizationSuccess
      ),
      map(() =>
        RouterActions.navigate({
          path: [AppRoutes.MODERATE_ROUTE, AppRoutes.MODERATE_ORGANIZATIONS],
        })
      )
    );
  });

  openAcceptDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateUnapprovedOrganizationsActions.openAcceptDialog),
      map((action) => action.organizationId),
      exhaustMap((organizationId) =>
        this.dialog
          .open(AcceptDialogComponent, {
            data: DEFAULT_ACCEPT_UNAPPROVED_DIALOG_TEXT,
            width: SMALL_DIALOG_WIDTH,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map(() =>
              ModerateUnapprovedOrganizationsActions.acceptUnapprovedOrganization(
                {
                  organizationId,
                }
              )
            )
          )
      )
    );
  });

  openDenyDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateUnapprovedOrganizationsActions.openDenyDialog),
      map((action) => action.organizationId),
      exhaustMap((organizationId) =>
        this.dialog
          .open(DenyDialogComponent, {
            data: DEFAULT_DENY_UNAPPROVED_DIALOG_TEXT,
            width: SMALL_DIALOG_WIDTH,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map((comment) =>
              ModerateUnapprovedOrganizationsActions.denyUnapprovedOrganization(
                {
                  organizationId,
                  comment,
                }
              )
            )
          )
      )
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly moderatorControllerService: ModeratorControllerService,
    private readonly dialog: MatDialog
  ) {}
}
