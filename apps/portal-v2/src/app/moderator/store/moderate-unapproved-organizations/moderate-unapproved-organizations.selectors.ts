import { createSelector } from '@ngrx/store';
import { selectModerateRootState } from '../moderate-root.selectors';
import {
  moderateUnapprovedOrganizationsAdapter,
  MODERATE_UNAPPROVED_ORGANIZATIONS_FEATURE_KEY
} from './moderate-unapproved-organizations.reducer';

const {
  selectAll,
  selectEntities,
} = moderateUnapprovedOrganizationsAdapter.getSelectors();

export const selectModerateUnapprovedOrganizationsState = createSelector(
  selectModerateRootState,
  (state) => state[MODERATE_UNAPPROVED_ORGANIZATIONS_FEATURE_KEY]
);

export const selectAllUnapprovedOrganizations = createSelector(
  selectModerateUnapprovedOrganizationsState,
  selectAll
);

export const selectUnapprovedOrganizationEntities = createSelector(
  selectModerateUnapprovedOrganizationsState,
  selectEntities
);

export const selectUnapprovedOrganizationProfile = createSelector(
  selectModerateUnapprovedOrganizationsState,
  (state) => state.profile
);

export const selectUnapprovedOrganizationsLoaded = createSelector(
  selectModerateUnapprovedOrganizationsState,
  (state) => state.loaded
);

export const selectUnapprovedOrganizationsError = createSelector(
  selectModerateUnapprovedOrganizationsState,
  (state) => state.error
);

export const selectUnapprovedOrganizationsPage = createSelector(
  selectModerateUnapprovedOrganizationsState,
  (state) => state.page
);
