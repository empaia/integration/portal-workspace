import { ConfidentialOrganizationEntity, ConfidentialOrganizationProfileEntity } from '@api/aaa/models';
import { AcceptDialogEntity } from '@shared/components/accept-dialog/accept-dialog.component';
import { DenyDialogEntity } from '@shared/components/deny-dialog/deny-dialog.component';

export type ModerateUnapprovedOrganizationEntity = ConfidentialOrganizationEntity;
export type ModerateUnapprovedOrganizationProfileEntity = ConfidentialOrganizationProfileEntity;

export const DEFAULT_ACCEPT_UNAPPROVED_DIALOG_TEXT: AcceptDialogEntity = {
  headlineText: 'moderate_forms.accept_unapproved_organization',
  contentText: 'moderate_forms.accept_unapproved_organization_confirmation',
  confirmButtonText: 'moderate_forms.confirm',
  cancelButtonText: 'moderate_forms.cancel',
};

export const DEFAULT_DENY_UNAPPROVED_DIALOG_TEXT: DenyDialogEntity = {
  headlineText: 'moderate_forms.deny_unapproved_organization',
  reviewLabel: 'moderate_forms.review',
  confirmButtonText: 'moderate_forms.confirm',
  cancelButtonText: 'moderate_forms.cancel'
};
