import { HttpErrorResponse } from '@angular/common/http';
import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { PaginationParameters } from '@shared/api/default-params';
import { ModerateUnapprovedOrganizationEntity, ModerateUnapprovedOrganizationProfileEntity } from './moderate-unapproved-organizations.models';

export const ModerateUnapprovedOrganizationsActions = createActionGroup({
  source: 'ModerateUnapprovedOrganizations',
  events: {
    'Load All Unapproved Organizations': props<{ page: PaginationParameters }>(),
    'Load All Unapproved Organizations Success': props<{ organizations: ModerateUnapprovedOrganizationEntity[] }>(),
    'Load All Unapproved Organizations Failure': props<{ error: HttpErrorResponse }>(),
    'Load Unapproved Organization Profile': props<{ organizationId: string }>(),
    'Load Unapproved Organization Profile Success': props<{ profile: ModerateUnapprovedOrganizationProfileEntity }>(),
    'Load Unapproved Organization Profile Failure': props<{ error: HttpErrorResponse }>(),

    'Accept Unapproved Organization': props<{ organizationId: string }>(),
    'Accept Unapproved Organization Success': props<{ organizationId: string }>(),
    'Accept Unapproved Organization Failure': props<{ error: HttpErrorResponse}>(),
    'Deny Unapproved Organization': props<{ organizationId: string, comment: string }>(),
    'Deny Unapproved Organization Success': emptyProps(),
    'Deny Unapproved Organization Failure': props<{ error: HttpErrorResponse }>(),

    'Open Accept Dialog': props<{ organizationId: string }>(),
    'Open Deny Dialog': props<{ organizationId: string }>(),
  }
});
