import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ModeratorControllerService } from '@api/aaa/services';
import {
  BIG_DIALOG_WIDTH,
  SMALL_DIALOG_WIDTH,
} from '@edit-organization/models/edit-organization.models';
import { ModerateSendInvitationDialogComponent } from '@moderator/components/moderate-send-invitation-dialog/moderate-send-invitation-dialog.component';
import { ModerateUserProfileDialogComponent } from '@moderator/components/moderate-user-profile-dialog/moderate-user-profile-dialog.component';
import { ModerateUserRolesDialogComponent } from '@moderator/components/moderate-user-roles-dialog/moderate-user-roles-dialog.component';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';
import { defaultPaginationParams } from '@shared/api/default-params';
import { AcceptDialogComponent } from '@shared/components/accept-dialog/accept-dialog.component';
import { filterNullish } from '@shared/utility/rxjs-utility';
import { exhaustMap, map } from 'rxjs/operators';
import { ModerateApprovedUsersActions } from './moderate-approved-users.actions';
import {
  DEFAULT_ACTIVATE_APPROVED_USER_DIALOG_TEXT,
  DEFAULT_DEACTIVATE_APPROVED_USER_DIALOG_TEXT,
  ModerateDialogs,
} from './moderate-approved-users.models';
import { State } from './moderate-approved-users.reducer';

@Injectable()
export class ModerateApprovedUsersEffects {
  initialLoad$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateApprovedUsersActions.initialLoadApprovedUsers),
      map(() =>
        ModerateApprovedUsersActions.loadAllApprovedUsers({
          page: defaultPaginationParams,
        })
      )
    );
  });

  loadAllUsers$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateApprovedUsersActions.loadAllApprovedUsers),
      fetch({
        run: (
          action: ReturnType<
            typeof ModerateApprovedUsersActions.loadAllApprovedUsers
          >,
          _state: State
        ) => {
          return this.moderatorControllerService.getUsers(action.page).pipe(
            map((response) => response.users),
            map((users) =>
              ModerateApprovedUsersActions.loadAllApprovedUsersSuccess({
                users,
              })
            )
          );
        },
        onError: (
          _action: ReturnType<
            typeof ModerateApprovedUsersActions.loadAllApprovedUsers
          >,
          error: HttpErrorResponse
        ) => {
          return ModerateApprovedUsersActions.loadAllApprovedUsersFailure({
            error,
          });
        },
      })
    );
  });

  loadUserProfile$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateApprovedUsersActions.loadApprovedUserProfile),
      fetch({
        run: (
          action: ReturnType<
            typeof ModerateApprovedUsersActions.loadApprovedUserProfile
          >,
          _state: State
        ) => {
          return this.moderatorControllerService
            .getUserProfile({
              target_user_id: action.userId,
            })
            .pipe(
              map((profile) =>
                ModerateApprovedUsersActions.loadApprovedUserProfileSuccess({
                  profile,
                  ...action,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ModerateApprovedUsersActions.loadApprovedUserProfile
          >,
          error: HttpErrorResponse
        ) => {
          return ModerateApprovedUsersActions.loadApprovedUserProfileFailure({
            error,
          });
        },
      })
    );
  });

  activateUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateApprovedUsersActions.activateApprovedUser),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof ModerateApprovedUsersActions.activateApprovedUser
          >,
          _state: State
        ) => {
          return this.moderatorControllerService
            .activateUser({
              target_user_id: action.userId,
            })
            .pipe(
              // Reload approved users, later persist the answer of the backend, Backend-Issue: #99
              map(() =>
                ModerateApprovedUsersActions.loadAllApprovedUsers({
                  page: defaultPaginationParams,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ModerateApprovedUsersActions.activateApprovedUser
          >,
          error: HttpErrorResponse
        ) => {
          return ModerateApprovedUsersActions.activateApprovedUserFailure({
            error,
          });
        },
      })
    );
  });

  deactivateUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateApprovedUsersActions.deactivateApprovedUser),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof ModerateApprovedUsersActions.deactivateApprovedUser
          >,
          _state: State
        ) => {
          return this.moderatorControllerService
            .deactivateUser({
              target_user_id: action.userId,
            })
            .pipe(
              // Reload approved users, later persist the answer of the backend, Backend-Issue: #99
              map(() =>
                ModerateApprovedUsersActions.loadAllApprovedUsers({
                  page: defaultPaginationParams,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ModerateApprovedUsersActions.deactivateApprovedUser
          >,
          error: HttpErrorResponse
        ) => {
          return ModerateApprovedUsersActions.deactivateApprovedUserFailure({
            error,
          });
        },
      })
    );
  });

  setUserRoles$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateApprovedUsersActions.setApprovedUserRoles),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof ModerateApprovedUsersActions.setApprovedUserRoles
          >,
          _state: State
        ) => {
          return this.moderatorControllerService
            .setUserRoles({
              target_user_id: action.userId,
              body: {
                roles: action.roles,
              },
            })
            .pipe(
              // Reload approved users, later persist the answer of the backend, Backend-Issue: #99
              map(() =>
                ModerateApprovedUsersActions.loadAllApprovedUsers({
                  page: defaultPaginationParams,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ModerateApprovedUsersActions.setApprovedUserRoles
          >,
          error: HttpErrorResponse
        ) => {
          return ModerateApprovedUsersActions.setApprovedUserRolesFailure({
            error,
          });
        },
      })
    );
  });

  sendInvitation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateApprovedUsersActions.sendInvitation),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof ModerateApprovedUsersActions.sendInvitation
          >,
          _state: State
        ) => {
          return this.moderatorControllerService
            .sendRegistrationInvitation({
              body: {
                email_addresses: [action.email],
              },
            })
            .pipe(
              // Reload approved users, later persist the answer of the backend, Backend-Issue: #99
              map(() =>
                ModerateApprovedUsersActions.loadAllApprovedUsers({
                  page: defaultPaginationParams,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ModerateApprovedUsersActions.sendInvitation
          >,
          error: HttpErrorResponse
        ) => {
          return ModerateApprovedUsersActions.sendInvitationFailure({ error });
        },
      })
    );
  });

  // Check which dialog should be opened after fetching user profile
  checkForUserDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateApprovedUsersActions.loadApprovedUserProfileSuccess),
      map((action) => {
        switch (action.dialog) {
          case ModerateDialogs.USER_PROFILE:
            return ModerateApprovedUsersActions.openUserProfileDialog({
              profile: action.profile,
            });
          case ModerateDialogs.USER_ROLES:
            return ModerateApprovedUsersActions.openSetUserRolesDialog({
              profile: action.profile,
            });
          default:
            return ModerateApprovedUsersActions.loadAllApprovedUsers({
              page: defaultPaginationParams,
            });
        }
      })
    );
  });

  openUserProfileDialog$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(ModerateApprovedUsersActions.openUserProfileDialog),
        map((action) => action.profile),
        exhaustMap((profile) =>
          this.dialog
            .open(ModerateUserProfileDialogComponent, {
              data: profile,
              width: BIG_DIALOG_WIDTH,
            })
            .afterClosed()
        )
      );
    },
    { dispatch: false }
  );

  openSetUserRolesDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateApprovedUsersActions.openSetUserRolesDialog),
      map((action) => action.profile),
      exhaustMap((profile) =>
        this.dialog
          .open(ModerateUserRolesDialogComponent, {
            data: profile.user_roles,
            width: SMALL_DIALOG_WIDTH,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map((roles) =>
              ModerateApprovedUsersActions.setApprovedUserRoles({
                userId: profile.user_id,
                roles,
              })
            )
          )
      )
    );
  });

  openSendInvitationDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateApprovedUsersActions.openSendInvitationDialog),
      exhaustMap(() =>
        this.dialog
          .open(ModerateSendInvitationDialogComponent, {
            width: SMALL_DIALOG_WIDTH,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map((email) =>
              ModerateApprovedUsersActions.sendInvitation({ email })
            )
          )
      )
    );
  });

  openActivateUserDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateApprovedUsersActions.openActivateApprovedUserDialog),
      map((action) => action.userId),
      exhaustMap((userId) =>
        this.dialog
          .open(AcceptDialogComponent, {
            data: DEFAULT_ACTIVATE_APPROVED_USER_DIALOG_TEXT,
            width: SMALL_DIALOG_WIDTH,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map(() =>
              ModerateApprovedUsersActions.activateApprovedUser({ userId })
            )
          )
      )
    );
  });

  openDeactivateUserDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateApprovedUsersActions.openDeactivateApprovedUserDialog),
      map((action) => action.userId),
      exhaustMap((userId) =>
        this.dialog
          .open(AcceptDialogComponent, {
            data: DEFAULT_DEACTIVATE_APPROVED_USER_DIALOG_TEXT,
            width: SMALL_DIALOG_WIDTH,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map(() =>
              ModerateApprovedUsersActions.deactivateApprovedUser({ userId })
            )
          )
      )
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly moderatorControllerService: ModeratorControllerService,
    private readonly dialog: MatDialog
  ) {}
}
