import { RegisteredUserEntity, UserRole } from '@api/aaa/models';
import { AcceptDialogEntity } from '@shared/components/accept-dialog/accept-dialog.component';

export type ModerateApprovedUserEntity = RegisteredUserEntity;

export interface ModerateUserId {
  id: string;
}

export enum ModerateDialogs {
  'USER_PROFILE',
  'USER_ROLES',
}

export const DEFAULT_ACTIVATE_APPROVED_USER_DIALOG_TEXT: AcceptDialogEntity = {
  headlineText: 'moderate_forms.activate_user',
  contentText: 'moderate_forms.activate_user_confirmation',
  confirmButtonText: 'moderate_forms.confirm',
  cancelButtonText: 'moderate_forms.cancel',
};

export const DEFAULT_DEACTIVATE_APPROVED_USER_DIALOG_TEXT: AcceptDialogEntity = {
  headlineText: 'moderate_forms.deactivate_user',
  contentText: 'moderate_forms.deactivate_user_confirmation',
  confirmButtonText: 'moderate_forms.confirm',
  cancelButtonText: 'moderate_forms.cancel',
};

export const USER_ROLES = Object.values(UserRole);

export function convertBooleanToUserRoleArray(roles: boolean[]): UserRole[] {
  return USER_ROLES.filter((_r, i) => roles[i]);
}

export function convertUserRolesToBooleanArray(roles: UserRole[] | undefined): boolean[] {
  return USER_ROLES.map(r => roles?.includes(r) ?? false);
}
