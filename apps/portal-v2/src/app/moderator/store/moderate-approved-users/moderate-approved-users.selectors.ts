import { createSelector } from '@ngrx/store';
import { selectModerateRootState } from '../moderate-root.selectors';
import { moderateApprovedUsersAdapter, MODERATE_APPROVED_USERS_FEATURE_KEY } from './moderate-approved-users.reducer';

const {
  selectAll,
  selectEntities,
} = moderateApprovedUsersAdapter.getSelectors();

export const selectModerateApprovedUsersState = createSelector(
  selectModerateRootState,
  (state) => state[MODERATE_APPROVED_USERS_FEATURE_KEY]
);

export const selectAllApprovedUsers = createSelector(
  selectModerateApprovedUsersState,
  selectAll
);

export const selectApprovedUserEntities = createSelector(
  selectModerateApprovedUsersState,
  selectEntities
);

export const selectSelectedApprovedUserProfile = createSelector(
  selectModerateApprovedUsersState,
  (state) => state.profile
);

export const selectSelectedApprovedUserId = createSelector(
  selectModerateApprovedUsersState,
  (state) => state.selected
);

export const selectSelectedApprovedUsername = createSelector(
  selectApprovedUserEntities,
  selectSelectedApprovedUserId,
  (users, id) => id ? users[id]?.username : undefined
);

export const selectApprovedUsersPage = createSelector(
  selectModerateApprovedUsersState,
  (state) => state.page
);

export const selectApprovedUsersLoaded = createSelector(
  selectModerateApprovedUsersState,
  (state) => state.loaded
);

export const selectApprovedUsersError = createSelector(
  selectModerateApprovedUsersState,
  (state) => state.error
);
