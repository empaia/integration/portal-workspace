import { HttpErrorResponse } from '@angular/common/http';
import { UserProfileEntity } from '@api/aaa/models';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { defaultPaginationParams, PaginationParameters } from '@shared/api/default-params';
import { ModerateApprovedUsersActions } from './moderate-approved-users.actions';
import { ModerateApprovedUserEntity } from './moderate-approved-users.models';


export const MODERATE_APPROVED_USERS_FEATURE_KEY = 'moderateApprovedUsers';

export interface State extends EntityState<ModerateApprovedUserEntity> {
  loaded: boolean;
  page: PaginationParameters;
  profile: UserProfileEntity | undefined;
  selected?: string | undefined;
  error?: HttpErrorResponse | undefined;
}

export const moderateApprovedUsersAdapter = createEntityAdapter<ModerateApprovedUserEntity>({
  selectId: user => user.user_id
});

export const initialState: State = moderateApprovedUsersAdapter.getInitialState({
  loaded: true,
  page: defaultPaginationParams,
  profile: undefined,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(ModerateApprovedUsersActions.loadAllApprovedUsers, (state, { page }): State => ({
    ...state,
    loaded: false,
    page,
  })),
  on(ModerateApprovedUsersActions.loadAllApprovedUsersSuccess, (state, { users }): State =>
    moderateApprovedUsersAdapter.setAll(users, {
      ...state,
      loaded: true,
    })
  ),
  on(ModerateApprovedUsersActions.loadAllApprovedUsersFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ModerateApprovedUsersActions.loadApprovedUserProfile, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ModerateApprovedUsersActions.loadApprovedUserProfileSuccess, (state, { profile }): State => ({
    ...state,
    loaded: true,
    profile,
  })),
  on(ModerateApprovedUsersActions.loadApprovedUserProfileFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ModerateApprovedUsersActions.activateApprovedUserFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ModerateApprovedUsersActions.deactivateApprovedUserFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ModerateApprovedUsersActions.sendInvitationFailure, (state, { error }): State => ({
    ...state,
    error,
  })),
  on(ModerateApprovedUsersActions.selectUser, (state, { selected }): State => ({
    ...state,
    selected,
  })),
);
