import { HttpErrorResponse } from '@angular/common/http';
import { UserProfileEntity, UserRole } from '@api/aaa/models';
import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { PaginationParameters } from '@shared/api/default-params';
import { ModerateApprovedUserEntity, ModerateDialogs } from './moderate-approved-users.models';

export const ModerateApprovedUsersActions = createActionGroup({
  source: 'ModerateApprovedUsers',
  events: {
    'Load All Approved Users': props<{ page: PaginationParameters }>(),
    'Load All Approved Users Success': props<{ users: ModerateApprovedUserEntity[] }>(),
    'Load All Approved Users Failure': props<{ error: HttpErrorResponse }>(),
    'Load Approved User Profile': props<{ userId: string, dialog: ModerateDialogs }>(),
    'Load Approved User Profile Success': props<{ profile: UserProfileEntity, dialog: ModerateDialogs }>(),
    'Load Approved User Profile Failure': props<{ error: HttpErrorResponse }>(),

    'Activate Approved User': props<{ userId: string }>(),
    'Activate Approved User Failure': props<{ error: HttpErrorResponse }>(),
    'Deactivate Approved User': props<{ userId: string }>(),
    'Deactivate Approved User Failure': props<{ error: HttpErrorResponse }>(),
    'Set Approved User Roles': props<{ userId: string, roles: UserRole[] }>(),
    'Set Approved User Roles Failure': props<{ error: HttpErrorResponse }>(),
    'Send Invitation': props<{ email: string }>(),
    'Send Invitation Failure': props<{ error: HttpErrorResponse }>(),

    'Select User': props<{ selected: string }>(),
    'Open User Profile Dialog': props<{ profile: UserProfileEntity }>(),
    'Open Set User Roles Dialog': props<{ profile: UserProfileEntity }>(),
    'Open Send Invitation Dialog': emptyProps(),
    'Open Activate Approved User Dialog': props<{ userId: string }>(),
    'Open Deactivate Approved User Dialog': props<{ userId: string }>(),

    'Initial Load Approved Users': emptyProps(),
  }
});
