import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';

import * as fromModerateApprovedUsers from './moderate-approved-users/moderate-approved-users.reducer';
import * as fromModerateUnapprovedUsers from './moderate-unapproved-users/moderate-unapproved-users.reducer';
import * as fromModerateUserOrganizations from './moderate-user-organizations/moderate-user-organizations.reducer';

import * as fromModerateOrganization from './moderate-organizations/moderate-organizations.reducer';
import * as fromModerateOrganizationCategoryRequests from './moderate-organization-category-requests/moderate-organization-category-requests.reducer';
import * as fromModerateUnapprovedOrganizations from './moderate-unapproved-organizations/moderate-unapproved-organizations.reducer';

import * as fromModerateStatistics from './moderate-statistics/moderate-statistics.reducer';

export const MODERATE_ROOT_FEATURE_KEY = 'moderateRoot';

export const selectModerateRootState = createFeatureSelector<ModerateRootState>(
  MODERATE_ROOT_FEATURE_KEY
);

export interface ModerateRootState {
  [fromModerateApprovedUsers.MODERATE_APPROVED_USERS_FEATURE_KEY]: fromModerateApprovedUsers.State;
  [fromModerateUnapprovedUsers.MODERATE_UNAPPROVED_USERS_FEATURE_KEY]: fromModerateUnapprovedUsers.State;
  [fromModerateUserOrganizations.MODERATE_USER_ORGANIZATIONS_FEATURE_KEY]: fromModerateUserOrganizations.State;
  [fromModerateOrganization.MODERATE_ORGANIZATIONS_FEATURE_KEY]: fromModerateOrganization.State;
  [fromModerateOrganizationCategoryRequests.MODERATE_ORGANIZATION_CATEGORY_REQUESTS_FEATURE_KEY]: fromModerateOrganizationCategoryRequests.State;
  [fromModerateUnapprovedOrganizations.MODERATE_UNAPPROVED_ORGANIZATIONS_FEATURE_KEY]: fromModerateUnapprovedOrganizations.State;
  [fromModerateStatistics.MODERATE_STATISTICS_FEATURE_KEY]: fromModerateStatistics.State;
}

export interface State {
  [MODERATE_ROOT_FEATURE_KEY]: ModerateRootState;
}

export function reducers(state: ModerateRootState | undefined, action: Action) {
  return combineReducers({
    [fromModerateApprovedUsers.MODERATE_APPROVED_USERS_FEATURE_KEY]: fromModerateApprovedUsers.reducer,
    [fromModerateUnapprovedUsers.MODERATE_UNAPPROVED_USERS_FEATURE_KEY]: fromModerateUnapprovedUsers.reducer,
    [fromModerateUserOrganizations.MODERATE_USER_ORGANIZATIONS_FEATURE_KEY]: fromModerateUserOrganizations.reducer,
    [fromModerateOrganization.MODERATE_ORGANIZATIONS_FEATURE_KEY]: fromModerateOrganization.reducer,
    [fromModerateOrganizationCategoryRequests.MODERATE_ORGANIZATION_CATEGORY_REQUESTS_FEATURE_KEY]: fromModerateOrganizationCategoryRequests.reducer,
    [fromModerateUnapprovedOrganizations.MODERATE_UNAPPROVED_ORGANIZATIONS_FEATURE_KEY]: fromModerateUnapprovedOrganizations.reducer,
    [fromModerateStatistics.MODERATE_STATISTICS_FEATURE_KEY]: fromModerateStatistics.reducer,
  })(state, action);
}
