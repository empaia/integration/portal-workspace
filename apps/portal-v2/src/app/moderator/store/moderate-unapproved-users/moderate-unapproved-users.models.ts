import { RegisteredUser } from '@user/store/user';

export type ModerateUnapprovedUserEntity = RegisteredUser;
