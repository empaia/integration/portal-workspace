import { HttpErrorResponse } from '@angular/common/http';
import { UserProfileEntity } from '@api/aaa/models';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { defaultPaginationParams, PaginationParameters } from '@shared/api/default-params';
import { ModerateUnapprovedUsersActions } from './moderate-unapproved-users.actions';
import { ModerateUnapprovedUserEntity } from './moderate-unapproved-users.models';


export const MODERATE_UNAPPROVED_USERS_FEATURE_KEY = 'moderateUnapprovedUsers';

export interface State extends EntityState<ModerateUnapprovedUserEntity> {
  loaded: boolean;
  page: PaginationParameters;
  profile: UserProfileEntity | undefined;
  error?: HttpErrorResponse | undefined;
}

export const moderateUnapprovedUsersAdapter = createEntityAdapter<ModerateUnapprovedUserEntity>({
  selectId: user => user.user_id
});

export const initialState: State = moderateUnapprovedUsersAdapter.getInitialState({
  loaded: true,
  page: defaultPaginationParams,
  profile: undefined,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(ModerateUnapprovedUsersActions.loadAllUnapprovedUsers, (state, { page }): State => ({
    ...state,
    loaded: false,
    page,
  })),
  on(ModerateUnapprovedUsersActions.loadAllUnapprovedUsersSuccess, (state, { users }): State =>
    moderateUnapprovedUsersAdapter.setAll(users, {
      ...state,
      loaded: true,
    })
  ),
  on(ModerateUnapprovedUsersActions.loadAllUnapprovedUsersFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ModerateUnapprovedUsersActions.loadUnapprovedUserProfile, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ModerateUnapprovedUsersActions.loadUnapprovedUserProfileSuccess, (state, { profile }): State => ({
    ...state,
    loaded: true,
    profile,
  })),
  on(ModerateUnapprovedUsersActions.loadUnapprovedUserProfileFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ModerateUnapprovedUsersActions.deleteUnapprovedUser, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ModerateUnapprovedUsersActions.deleteUnapprovedUserSuccess, (state, { unapprovedUserId }): State =>
    moderateUnapprovedUsersAdapter.removeOne(unapprovedUserId, {
      ...state,
      loaded: true,
    })
  ),
  on(ModerateUnapprovedUsersActions.deleteUnapprovedUserFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
);
