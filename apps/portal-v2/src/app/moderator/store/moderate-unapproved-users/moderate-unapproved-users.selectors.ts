import { createSelector } from '@ngrx/store';
import { selectModerateRootState } from '../moderate-root.selectors';
import { moderateUnapprovedUsersAdapter, MODERATE_UNAPPROVED_USERS_FEATURE_KEY } from './moderate-unapproved-users.reducer';

const {
  selectAll,
  selectEntities,
} = moderateUnapprovedUsersAdapter.getSelectors();

export const selectModerateUnapprovedUsersState = createSelector(
  selectModerateRootState,
  (state) => state[MODERATE_UNAPPROVED_USERS_FEATURE_KEY]
);

export const selectAllUnapprovedUsers = createSelector(
  selectModerateUnapprovedUsersState,
  selectAll
);

export const selectUnapprovedUserEntities = createSelector(
  selectModerateUnapprovedUsersState,
  selectEntities
);

export const selectSelectedUnapprovedUserProfile = createSelector(
  selectModerateUnapprovedUsersState,
  (state) => state.profile
);

export const selectUnapprovedUsersPage = createSelector(
  selectModerateUnapprovedUsersState,
  (state) => state.page
);

export const selectUnapprovedUsersLoaded = createSelector(
  selectModerateUnapprovedUsersState,
  (state) => state.loaded
);

export const selectUnapprovedUsersError = createSelector(
  selectModerateUnapprovedUsersState,
  (state) => state.error
);
