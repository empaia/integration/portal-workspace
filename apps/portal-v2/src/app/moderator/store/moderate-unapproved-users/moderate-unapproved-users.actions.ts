import { HttpErrorResponse } from '@angular/common/http';
import { UserProfileEntity } from '@api/aaa/models';
import { createActionGroup, props } from '@ngrx/store';
import { PaginationParameters } from '@shared/api/default-params';
import { ModerateUnapprovedUserEntity } from './moderate-unapproved-users.models';

export const ModerateUnapprovedUsersActions = createActionGroup({
  source: 'ModerateUnapprovedUsers',
  events: {
    'Load All Unapproved Users': props<{ page: PaginationParameters }>(),
    'Load All Unapproved Users Success': props<{ users: ModerateUnapprovedUserEntity[] }>(),
    'Load All Unapproved Users Failure': props<{ error: HttpErrorResponse }>(),

    'Delete Unapproved User': props<{ unapprovedUserId: string }>(),
    'Delete Unapproved User Success': props<{ unapprovedUserId: string }>(),
    'Delete Unapproved User Failure': props<{ error: HttpErrorResponse }>(),
    'Load Unapproved User Profile': props<{ unapprovedUserId: string }>(),
    'Load Unapproved User Profile Success': props<{ profile: UserProfileEntity }>(),
    'Load Unapproved User Profile Failure': props<{ error: HttpErrorResponse }>(),
  }
});
