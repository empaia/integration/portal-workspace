import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ModeratorControllerService } from '@api/aaa/services';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';
import { defaultPaginationParams } from '@shared/api/default-params';
import { map } from 'rxjs/operators';
import { ModerateApprovedUsersActions } from '../moderate-approved-users';
import { ModerateUnapprovedUsersActions } from './moderate-unapproved-users.actions';
import { State } from './moderate-unapproved-users.reducer';

@Injectable()
export class ModerateUnapprovedUsersEffects {
  initialLoad$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateApprovedUsersActions.initialLoadApprovedUsers),
      map(() =>
        ModerateUnapprovedUsersActions.loadAllUnapprovedUsers({
          page: defaultPaginationParams,
        })
      )
    );
  });

  loadAllUnapprovedUsers$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateUnapprovedUsersActions.loadAllUnapprovedUsers),
      fetch({
        run: (
          action: ReturnType<
            typeof ModerateUnapprovedUsersActions.loadAllUnapprovedUsers
          >,
          _state: State
        ) => {
          return this.moderatorControllerService
            .getUnapprovedUsers(action.page)
            .pipe(
              map((response) => response.users),
              map((users) =>
                ModerateUnapprovedUsersActions.loadAllUnapprovedUsersSuccess({
                  users,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ModerateUnapprovedUsersActions.loadAllUnapprovedUsers
          >,
          error: HttpErrorResponse
        ) => {
          return ModerateUnapprovedUsersActions.loadAllUnapprovedUsersFailure({
            error,
          });
        },
      })
    );
  });

  loadUnapprovedUserProfile$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateUnapprovedUsersActions.loadUnapprovedUserProfile),
      fetch({
        run: (
          action: ReturnType<
            typeof ModerateUnapprovedUsersActions.loadUnapprovedUserProfile
          >,
          _state: State
        ) => {
          return this.moderatorControllerService
            .getUserProfileOfUnapprovedUser({
              unapproved_user_id: action.unapprovedUserId,
            })
            .pipe(
              map((profile) =>
                ModerateUnapprovedUsersActions.loadUnapprovedUserProfileSuccess(
                  { profile }
                )
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ModerateUnapprovedUsersActions.loadUnapprovedUserProfile
          >,
          error: HttpErrorResponse
        ) => {
          return ModerateUnapprovedUsersActions.loadUnapprovedUserProfileFailure(
            { error }
          );
        },
      })
    );
  });

  deleteUnprovedUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateUnapprovedUsersActions.deleteUnapprovedUser),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof ModerateUnapprovedUsersActions.deleteUnapprovedUser
          >,
          _state: State
        ) => {
          return this.moderatorControllerService
            .deleteUnapprovedUser({
              unapproved_user_id: action.unapprovedUserId,
            })
            .pipe(
              // Remove user from store
              map(() =>
                ModerateUnapprovedUsersActions.deleteUnapprovedUserSuccess({
                  unapprovedUserId: action.unapprovedUserId,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ModerateUnapprovedUsersActions.deleteUnapprovedUser
          >,
          error: HttpErrorResponse
        ) => {
          return ModerateUnapprovedUsersActions.deleteUnapprovedUserFailure({
            error,
          });
        },
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly moderatorControllerService: ModeratorControllerService
  ) {}
}
