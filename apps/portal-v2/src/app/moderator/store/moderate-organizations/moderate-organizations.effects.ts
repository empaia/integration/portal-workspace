import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ModeratorControllerService } from '@api/aaa/services';
import { ModerateEditOrgCategoriesDialogComponent } from '@moderator/components/moderate-edit-org-categories-dialog/moderate-edit-org-categories-dialog.component';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';
import { defaultPaginationParams } from '@shared/api/default-params';
import { exhaustMap, map } from 'rxjs/operators';
import { ModerateOrganizationsActions } from './moderate-organizations.actions';
import * as ModerateOrganizationsSelectors from './moderate-organizations.selectors';
import { State } from './moderate-organizations.reducer';
import { filterNullish } from '@shared/utility/rxjs-utility';
import { AcceptDialogComponent } from '@shared/components/accept-dialog/accept-dialog.component';
import {
  DEFAULT_ACTIVATE_APPROVED_ORGANIZATION_DIALOG_TEXT,
  DEFAULT_DEACTIVATE_APPROVED_ORGANIZATION_DIALOG_TEXT,
} from './moderate-organizations.models';
import { SMALL_DIALOG_WIDTH } from '@edit-organization/models/edit-organization.models';

@Injectable()
export class ModerateOrganizationsEffects {
  initialOrganizationLoad$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateOrganizationsActions.initialLoadOrganizations),
      map(() =>
        ModerateOrganizationsActions.loadAllOrganizations({
          page: defaultPaginationParams,
        })
      )
    );
  });

  loadAllOrganizations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateOrganizationsActions.loadAllOrganizations),
      fetch({
        run: (
          action: ReturnType<
            typeof ModerateOrganizationsActions.loadAllOrganizations
          >,
          _state: State
        ) => {
          return this.moderatorControllerService
            .getApprovedOrganizationsEntity(action.page)
            .pipe(
              map((response) => response.organizations),
              map((organizations) =>
                ModerateOrganizationsActions.loadAllOrganizationsSuccess({
                  organizations,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ModerateOrganizationsActions.loadAllOrganizations
          >,
          error: HttpErrorResponse
        ) => {
          return ModerateOrganizationsActions.loadAllOrganizationsFailure({
            error,
          });
        },
      })
    );
  });

  setOrganizationCategories$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateOrganizationsActions.setOrganizationCategories),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof ModerateOrganizationsActions.setOrganizationCategories
          >,
          _state: State
        ) => {
          return this.moderatorControllerService
            .setOrganizationCategories({
              organization_id: action.organizationId,
              body: {
                categories: action.categories,
              },
            })
            .pipe(
              // Reload approved organizations, later persist the answer of the backend, Backend-Issue: #99
              map(() =>
                ModerateOrganizationsActions.loadAllOrganizations({
                  page: defaultPaginationParams,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ModerateOrganizationsActions.setOrganizationCategories
          >,
          error: HttpErrorResponse
        ) => {
          return ModerateOrganizationsActions.setOrganizationCategoriesFailure({
            error,
          });
        },
      })
    );
  });

  activateOrganization$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateOrganizationsActions.activateOrganization),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof ModerateOrganizationsActions.activateOrganization
          >,
          _state: State
        ) => {
          return this.moderatorControllerService
            .activateOrganization({
              organization_id: action.organizationId,
            })
            .pipe(
              // Reload approved organizations, later persist the answer of the backend, Backend-Issue: #99
              map(() =>
                ModerateOrganizationsActions.loadAllOrganizations({
                  page: defaultPaginationParams,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ModerateOrganizationsActions.activateOrganization
          >,
          error: HttpErrorResponse
        ) => {
          return ModerateOrganizationsActions.activateOrganizationFailure({
            error,
          });
        },
      })
    );
  });

  deactivateOrganization$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateOrganizationsActions.deactivateOrganization),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof ModerateOrganizationsActions.deactivateOrganization
          >,
          _state: State
        ) => {
          return this.moderatorControllerService
            .deactivateOrganization({
              organization_id: action.organizationId,
            })
            .pipe(
              // Reload approved organizations, later persist the answer of the backend, Backend-Issue: #99
              map(() =>
                ModerateOrganizationsActions.loadAllOrganizations({
                  page: defaultPaginationParams,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ModerateOrganizationsActions.deactivateOrganization
          >,
          error: HttpErrorResponse
        ) => {
          return ModerateOrganizationsActions.deactivateOrganizationFailure({
            error,
          });
        },
      })
    );
  });

  openCategoriesDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateOrganizationsActions.openEditCategoriesDialog),
      map((action) => action.organizationId),
      concatLatestFrom(() =>
        this.store.select(
          ModerateOrganizationsSelectors.selectApprovedOrganizationEntities
        )
      ),
      map(([id, organizations]) => organizations[id]),
      filterNullish(),
      exhaustMap((organization) =>
        this.dialog
          .open(ModerateEditOrgCategoriesDialogComponent, {
            data: organization.categories,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map((categories) =>
              ModerateOrganizationsActions.setOrganizationCategories({
                organizationId: organization.organization_id,
                categories,
              })
            )
          )
      )
    );
  });

  openActivateOrganizationDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateOrganizationsActions.openActivateOrganizationDialog),
      map((action) => action.organizationId),
      exhaustMap((organizationId) =>
        this.dialog
          .open(AcceptDialogComponent, {
            data: DEFAULT_ACTIVATE_APPROVED_ORGANIZATION_DIALOG_TEXT,
            width: SMALL_DIALOG_WIDTH,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map(() =>
              ModerateOrganizationsActions.activateOrganization({
                organizationId,
              })
            )
          )
      )
    );
  });

  openDeactivateOrganizationDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateOrganizationsActions.openDeactivateOrganizationDialog),
      map((action) => action.organizationId),
      exhaustMap((organizationId) =>
        this.dialog
          .open(AcceptDialogComponent, {
            data: DEFAULT_DEACTIVATE_APPROVED_ORGANIZATION_DIALOG_TEXT,
            width: SMALL_DIALOG_WIDTH,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map(() =>
              ModerateOrganizationsActions.deactivateOrganization({
                organizationId,
              })
            )
          )
      )
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly moderatorControllerService: ModeratorControllerService,
    private readonly dialog: MatDialog
  ) {}
}
