import { createSelector } from '@ngrx/store';
import { selectModerateRootState } from '../moderate-root.selectors';
import { moderateOrganizationAdapter, MODERATE_ORGANIZATIONS_FEATURE_KEY } from './moderate-organizations.reducer';

const {
  selectAll,
  selectEntities,
} = moderateOrganizationAdapter.getSelectors();

export const selectModerateOrganizationsSate = createSelector(
  selectModerateRootState,
  (state) => state[MODERATE_ORGANIZATIONS_FEATURE_KEY]
);

export const selectAllApprovedOrganizations = createSelector(
  selectModerateOrganizationsSate,
  selectAll
);

export const selectApprovedOrganizationEntities = createSelector(
  selectModerateOrganizationsSate,
  selectEntities
);

export const selectApprovedOrganizationsLoaded = createSelector(
  selectModerateOrganizationsSate,
  (state) => state.loaded
);

export const selectApprovedOrganizationsError = createSelector(
  selectModerateOrganizationsSate,
  (state) => state.error
);

export const selectApprovedOrganizationPage = createSelector(
  selectModerateOrganizationsSate,
  (state) => state.page
);
