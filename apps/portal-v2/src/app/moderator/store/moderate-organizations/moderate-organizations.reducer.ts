import { HttpErrorResponse } from '@angular/common/http';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { defaultPaginationParams, PaginationParameters } from '@shared/api/default-params';
import { ModerateOrganizationsActions } from './moderate-organizations.actions';
import { ModerateOrganizationEntity } from './moderate-organizations.models';


export const MODERATE_ORGANIZATIONS_FEATURE_KEY = 'moderateOrganizations';

export interface State extends EntityState<ModerateOrganizationEntity> {
  loaded: boolean;
  page: PaginationParameters; // later to safe the current set page
  error?: HttpErrorResponse | undefined;
}

export const moderateOrganizationAdapter = createEntityAdapter<ModerateOrganizationEntity>({
  selectId: organization => organization.organization_id
});

export const initialState: State = moderateOrganizationAdapter.getInitialState({
  loaded: true,
  page: defaultPaginationParams,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(ModerateOrganizationsActions.loadAllOrganizations, (state, { page }): State => ({
    ...state,
    loaded: false,
    page,
  })),
  on(ModerateOrganizationsActions.loadAllOrganizationsSuccess, (state, { organizations }): State =>
    moderateOrganizationAdapter.setAll(organizations, {
      ...state,
      loaded: true,
    })
  ),
  on(ModerateOrganizationsActions.loadAllOrganizationsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
);
