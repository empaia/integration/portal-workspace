import { ConfidentialOrganizationEntity } from '@api/aaa/models';
import { AcceptDialogEntity } from '@shared/components/accept-dialog/accept-dialog.component';

export type ModerateOrganizationEntity = ConfidentialOrganizationEntity;

export const DEFAULT_ACTIVATE_APPROVED_ORGANIZATION_DIALOG_TEXT: AcceptDialogEntity = {
  headlineText: 'moderate_forms.activate_organization',
  contentText: 'moderate_forms.activate_organization_confirmation',
  confirmButtonText: 'moderate_forms.confirm',
  cancelButtonText: 'moderate_forms.cancel',
};

export const DEFAULT_DEACTIVATE_APPROVED_ORGANIZATION_DIALOG_TEXT: AcceptDialogEntity = {
  headlineText: 'moderate_forms.deactivate_organization',
  contentText: 'moderate_forms.deactivate_organization_confirmation',
  confirmButtonText: 'moderate_forms.confirm',
  cancelButtonText: 'moderate_forms.cancel',
};
