import { HttpErrorResponse } from '@angular/common/http';
import { OrganizationCategory } from '@api/aaa/models';
import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { PaginationParameters } from '@shared/api/default-params';
import { ModerateOrganizationEntity } from './moderate-organizations.models';

export const ModerateOrganizationsActions = createActionGroup({
  source: 'ModerateOrganizations',
  events: {
    'Load All Organizations': props<{ page: PaginationParameters }>(),
    'Load All Organizations Success': props<{ organizations: ModerateOrganizationEntity[] }>(),
    'Load All Organizations Failure': props<{ error: HttpErrorResponse }>(),

    'Set Organization Categories': props<{ organizationId: string, categories: OrganizationCategory[] }>(),
    'Set Organization Categories Failure': props<{ error: HttpErrorResponse }>(),
    'Activate Organization': props<{ organizationId: string }>(),
    'Activate Organization Failure': props<{ error: HttpErrorResponse }>(),
    'Deactivate Organization': props<{ organizationId: string }>(),
    'Deactivate Organization Failure': props<{ error: HttpErrorResponse }>(),

    'Initial Load Organizations': emptyProps(),
    'Open edit categories dialog': props<{ organizationId: string }>(),
    'Open Activate Organization Dialog': props<{ organizationId: string }>(),
    'Open Deactivate Organization Dialog': props<{ organizationId: string }>(),
  }
});
