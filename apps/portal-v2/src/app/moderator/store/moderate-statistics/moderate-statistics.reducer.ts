import { createReducer, on } from '@ngrx/store';
import { ModerateStatisticsActions } from './moderate-statistics.actions';
import { ModerateStatisticsEntity } from './moderate-statistics.models';
import { HttpErrorResponse } from '@angular/common/http';

export const MODERATE_STATISTICS_FEATURE_KEY = 'moderateStatistics';

export interface State {
  rawStatistics: ModerateStatisticsEntity[];
  loaded: boolean;
  error?: HttpErrorResponse | undefined;
}

export const initialState: State = {
  rawStatistics: [],
  loaded: true,
  error: undefined,
};

export const reducer = createReducer(
  initialState,
  on(ModerateStatisticsActions.loadModerateStatistics, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ModerateStatisticsActions.loadModerateStatisticsSuccess, (state, { rawStatistics }): State => ({
    ...state,
    rawStatistics,
    loaded: true,
  })),
  on(ModerateStatisticsActions.loadModerateStatisticsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ModerateStatisticsActions.clearStatistics, (state): State => ({
    ...state,
    ...initialState
  })),
);

