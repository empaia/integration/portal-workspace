import { Statistics, StatisticsPage } from '@api/moderator/models';

export type ModerateStatisticsEntity = Statistics;

export interface TrafficStatistics {
  week: number;
  month: number;
  quarter: number;
  year: number;
  oneTimeVisit: number;
  twoToFiveTimes: number;
  moreThanFiveTimes: number;
}

export interface TrafficStatisticPages {
  [StatisticsPage.PUBLIC_MARKETPLACE]: TrafficStatistics;
  [StatisticsPage.PUBLIC_AI_REGISTER]: TrafficStatistics;
}

const TIME_CORRECTION_FACTOR = 1000;
const DAY_IN_MILLISECONDS = 24 * 60 * 60 * 1000;

export function calculateTrafficStatistics(statistics: ModerateStatisticsEntity[], page: StatisticsPage): TrafficStatistics {
  const pageStatistics = statistics.filter(s => s.page === page);
  const statisticsIds = new Set<string>(pageStatistics.map(s => s.statistics_id));

  const currentDate = new Date();
  const lastWeek = new Date(currentDate.getTime() - DAY_IN_MILLISECONDS * 7);
  const lastMonth = new Date(currentDate.getTime() - DAY_IN_MILLISECONDS * 30);
  const lastQuarter = new Date(currentDate.getTime() - DAY_IN_MILLISECONDS * 90);
  const lastYear = new Date(currentDate.getTime() - DAY_IN_MILLISECONDS * 365);

  let week = 0;
  let month = 0;
  let quarter = 0;
  let year = 0;
  let oneTimeVisit = 0;
  let twoToFiveTimes = 0;
  let moreThanFiveTimes = 0;

  pageStatistics.forEach(s => {
    const time = s.accessed_at * TIME_CORRECTION_FACTOR;
    if (time > lastWeek.getTime()) {
      ++week;
    }
    if (time > lastMonth.getTime()) {
      ++month;
    }
    if (time > lastQuarter.getTime()) {
      ++quarter;
    }
    if (time > lastYear.getTime()) {
      ++year;
    }
  });

  statisticsIds.forEach(id => {
    const count = pageStatistics.filter(s => s.statistics_id === id).length;
    if (count > 5) {
      ++moreThanFiveTimes;
    } else if (count > 1 && count <= 5) {
      ++twoToFiveTimes;
    } else {
      ++oneTimeVisit;
    }
  });

  return {
    week,
    month,
    quarter,
    year,
    oneTimeVisit,
    twoToFiveTimes,
    moreThanFiveTimes,
  };
}
