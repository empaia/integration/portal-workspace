import { createSelector } from '@ngrx/store';
import { selectModerateRootState } from '../moderate-root.selectors';
import { MODERATE_STATISTICS_FEATURE_KEY } from './moderate-statistics.reducer';
import { TrafficStatisticPages, calculateTrafficStatistics } from './moderate-statistics.models';
import { StatisticsPage } from '@api/moderator/models';

export const selectModerateStatisticsState = createSelector(
  selectModerateRootState,
  (state) => state[MODERATE_STATISTICS_FEATURE_KEY]
);

export const selectRawStatistics = createSelector(
  selectModerateStatisticsState,
  (state) => state.rawStatistics
);

export const selectRawStatisticsLoaded = createSelector(
  selectModerateStatisticsState,
  (state) => state.loaded
);

export const selectRawStatisticsError = createSelector(
  selectModerateStatisticsState,
  (state) => state.error
);

export const selectAllPagesTrafficStatistics = createSelector(
  selectRawStatistics,
  (statistics) => ({
    PUBLIC_MARKETPLACE: calculateTrafficStatistics(statistics, StatisticsPage.PUBLIC_MARKETPLACE),
    PUBLIC_AI_REGISTER: calculateTrafficStatistics(statistics, StatisticsPage.PUBLIC_AI_REGISTER)
  } as TrafficStatisticPages)
);

export const selectPublicMarketplaceTrafficStatistics = createSelector(
  selectAllPagesTrafficStatistics,
  (traffic) => [traffic.PUBLIC_MARKETPLACE]
);

export const selectPublicAiRegisterTrafficStatistics = createSelector(
  selectAllPagesTrafficStatistics,
  (traffic) => [traffic.PUBLIC_AI_REGISTER]
);
