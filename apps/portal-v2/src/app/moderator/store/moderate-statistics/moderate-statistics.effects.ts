import { Injectable } from '@angular/core';
import { StatisticsService } from '@api/moderator/services';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ModerateStatisticsActions } from './moderate-statistics.actions';
import { fetch } from '@ngrx/router-store/data-persistence';
import { HttpErrorResponse } from '@angular/common/http';
import { State } from './moderate-statistics.reducer';
import { map } from 'rxjs';



@Injectable()
export class ModerateStatisticsEffects {

  loadStatistics$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ModerateStatisticsActions.loadModerateStatistics),
      fetch({
        run: (
          _action: ReturnType<typeof ModerateStatisticsActions.loadModerateStatistics>,
          _state: State,
        ) => {
          return this.statisticsService.statisticsGet().pipe(
            map(response => response.items),
            map(rawStatistics => ModerateStatisticsActions.loadModerateStatisticsSuccess({ rawStatistics }))
          );
        },
        onError: (
          _action: ReturnType<typeof ModerateStatisticsActions.loadModerateStatistics>,
          error: HttpErrorResponse
        ) => {
          return ModerateStatisticsActions.loadModerateStatisticsFailure({ error });
        }
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly statisticsService: StatisticsService,
  ) {}
}
