import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { ModerateStatisticsEntity } from './moderate-statistics.models';
import { HttpErrorResponse } from '@angular/common/http';

export const ModerateStatisticsActions = createActionGroup({
  source: 'ModerateStatistics',
  events: {
    'Load ModerateStatistics': emptyProps(),
    'Load ModerateStatistics Success': props<{
      rawStatistics: ModerateStatisticsEntity[],
    }>(),
    'Load ModerateStatistics Failure': props<{
      error: HttpErrorResponse,
    }>(),
    'Clear Statistics': emptyProps()
  }
});
