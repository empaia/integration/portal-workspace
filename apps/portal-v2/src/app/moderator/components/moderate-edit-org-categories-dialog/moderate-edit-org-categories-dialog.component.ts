import { ChangeDetectionStrategy, Component, inject, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { OrganizationCategory } from '@api/aaa/models';
import { convertBooleanToCategoryArray, convertCategoryToBooleanArray, ORGANIZATION_CATEGORY } from '@edit-organization/models/edit-organization.models';

@Component({
  selector: 'app-moderate-edit-org-categories-dialog',
  templateUrl: './moderate-edit-org-categories-dialog.component.html',
  styleUrls: ['./moderate-edit-org-categories-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModerateEditOrgCategoriesDialogComponent implements OnInit {
  private fb = inject(FormBuilder);
  public categoriesForm = this.fb.nonNullable.group({
    categories: this.fb.nonNullable.array(
      ORGANIZATION_CATEGORY.map(() => false),
    )
  });

  public readonly ORGANIZATION_CATEGORY_SELECTION = ORGANIZATION_CATEGORY;

  constructor(
    private dialogRef: MatDialogRef<ModerateEditOrgCategoriesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: OrganizationCategory[],
  ) {}

  public ngOnInit(): void {
    this.setFields();
  }

  public get categories(): FormArray<FormControl<boolean>> {
    return this.categoriesForm.controls.categories;
  }

  public onSubmit(): void {
    const categories = convertBooleanToCategoryArray(this.categories.value);
    this.dialogRef.close(categories);
  }

  private setFields(): void {
    this.categoriesForm.patchValue({
      categories: convertCategoryToBooleanArray(this.data)
    });
  }
}
