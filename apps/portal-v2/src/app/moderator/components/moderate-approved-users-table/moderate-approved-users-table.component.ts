import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ModerateApprovedUserEntity, ModerateUserId } from '@moderator/store/moderate-approved-users';

@Component({
  selector: 'app-moderate-approved-users-table',
  templateUrl: './moderate-approved-users-table.component.html',
  styleUrls: ['./moderate-approved-users-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModerateApprovedUsersTableComponent implements AfterViewInit {
  private _users!: ModerateApprovedUserEntity[];
  @Input() public set approvedUsers(val: ModerateApprovedUserEntity[] | undefined) {
    if (val) {
      this._users = val;
      this.dataSource.data = val;
      this.dataSource.sort = this.sort;
    }
  }

  public get approvedUsers() {
    return this._users;
  }

  @Input() public selectedUserId: string | undefined;

  @Output() public showUserDetailsClicked = new EventEmitter<ModerateUserId>();
  @Output() public activateUserClicked = new EventEmitter<ModerateUserId>();
  @Output() public deactivateUserClicked = new EventEmitter<ModerateUserId>();
  @Output() public selectUserClicked = new EventEmitter<ModerateUserId>();
  @Output() public setUserRolesClicked = new EventEmitter<ModerateUserId>();

  @ViewChild(MatSort) private sort!: MatSort;
  public dataSource = new MatTableDataSource<Partial<ModerateApprovedUserEntity>>(
    [{ username: 'Loading data...', user_id: '' }]
  );
  public displayColumns: string[] = ['username', 'roles', 'status', 'actions'];

  public ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }
}
