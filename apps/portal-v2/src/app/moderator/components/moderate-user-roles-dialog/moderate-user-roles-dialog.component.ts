import { ChangeDetectionStrategy, Component, inject, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserRole } from '@api/aaa/models';
import { convertBooleanToUserRoleArray, convertUserRolesToBooleanArray, USER_ROLES } from '@moderator/store/moderate-approved-users';

@Component({
  selector: 'app-moderate-user-roles-dialog',
  templateUrl: './moderate-user-roles-dialog.component.html',
  styleUrls: ['./moderate-user-roles-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModerateUserRolesDialogComponent implements OnInit {
  private fb = inject(FormBuilder);
  public rolesForm = this.fb.nonNullable.group({
    roles: this.fb.nonNullable.array(
      USER_ROLES.map(() => false)
    )
  });

  public USER_ROLES_SELECTION = USER_ROLES;

  constructor(
    private dialogRef: MatDialogRef<ModerateUserRolesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: UserRole[] | undefined
  ) {}

  public ngOnInit(): void {
    this.setFields();
  }

  public get roles(): FormArray<FormControl<boolean>> {
    return this.rolesForm.controls.roles;
  }

  public onSubmit(): void {
    const userRoles = convertBooleanToUserRoleArray(this.roles.value);
    this.dialogRef.close(userRoles);
  }

  private setFields(): void {
    this.rolesForm.patchValue({
      roles: convertUserRolesToBooleanArray(this.data)
    });
  }
}
