import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { OrganizationAccountState } from '@api/aaa/models';
import { EditOrganizationId } from '@edit-organization/models/edit-organization.models';
import { ModerateOrganizationEntity } from '@moderator/store/moderate-organizations';

@Component({
  selector: 'app-moderate-approved-organizations-table',
  templateUrl: './moderate-approved-organizations-table.component.html',
  styleUrls: ['./moderate-approved-organizations-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModerateApprovedOrganizationsTableComponent implements AfterViewInit {
  private _organizations!: ModerateOrganizationEntity[];
  @Input() public set approvedOrganizations(val: ModerateOrganizationEntity[] | undefined) {
    if (val) {
      this._organizations = val;
      this.dataSource.data = val;
      this.dataSource.sort = this.sort;
    }
  }

  public get approvedOrganizations() {
    return this._organizations;
  }

  @Output() public activateOrganizationClicked = new EventEmitter<EditOrganizationId>();
  @Output() public deactivateOrganizationClicked = new EventEmitter<EditOrganizationId>();
  @Output() public editOrganizationCategoriesClicked = new EventEmitter<EditOrganizationId>();

  @ViewChild(MatSort) private sort!: MatSort;
  public dataSource = new MatTableDataSource<Partial<ModerateOrganizationEntity>>(
    [{ organization_name: 'Loading data...', organization_id: '' }]
  );
  public displayColumns: string[] = ['organization_name', 'categories', 'status', 'actions'];
  public readonly ORGANIZATION_STATES = OrganizationAccountState;

  public ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }
}
