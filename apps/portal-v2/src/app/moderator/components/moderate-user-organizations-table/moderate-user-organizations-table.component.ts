import { AfterViewInit, ChangeDetectionStrategy, Component, Input, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ModerateUserOrganizationEntity } from '@moderator/store/moderate-user-organizations';

@Component({
  selector: 'app-moderate-user-organizations-table',
  templateUrl: './moderate-user-organizations-table.component.html',
  styleUrls: ['./moderate-user-organizations-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModerateUserOrganizationsTableComponent implements AfterViewInit {
  private _organizations!: ModerateUserOrganizationEntity[];
  @Input() public set userOrganizations(val: ModerateUserOrganizationEntity[] | undefined) {
    if (val) {
      this._organizations = val;
      this.dataSource.data = val;
      this.dataSource.sort = this.sort;
    }
  }

  public get userOrganizations() {
    return this._organizations;
  }

  @Input() public selectedUsername: string | undefined;

  @ViewChild(MatSort) private sort!: MatSort;
  public dataSource = new MatTableDataSource<Partial<ModerateUserOrganizationEntity>>(
    [{ organization_name: 'Loading data...', organization_id: '' }]
  );
  public displayColumns: string[] = ['organization_name', 'categories', 'roles'];

  public ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }
}
