import { AfterViewInit, ChangeDetectionStrategy, Component, Input, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TrafficStatistics } from '@moderator/store/moderate-statistics';

@Component({
  selector: 'app-moderate-traffic-statistics-table',
  templateUrl: './moderate-traffic-statistics-table.component.html',
  styleUrls: ['./moderate-traffic-statistics-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModerateTrafficStatisticsTableComponent implements AfterViewInit {
  private _trafficStatistics!: TrafficStatistics[];
  @Input() public set trafficStatistics(val: TrafficStatistics[] | undefined) {
    if (val) {
      this._trafficStatistics = val;
      this.dataSource.data = val;
      this.dataSource.sort = this.sort;
    }
  }

  public get trafficStatistics(): TrafficStatistics[] {
    return this._trafficStatistics;
  }
  @Input() public page!: string;

  @ViewChild(MatSort) private sort!: MatSort;
  public dataSource = new MatTableDataSource<Partial<TrafficStatistics>>();
  public displayColumns: string[] = [
    'week',
    'month',
    'quarter',
    'year',
    'oneTimeVisit',
    'twoToFiveTimes',
    'moreThanFiveTimes',
  ];

  public ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }
}
