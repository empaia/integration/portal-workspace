import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserProfileEntity } from '@api/aaa/models';
import { blankProfileImage } from 'src/app/app.models';

@Component({
  selector: 'app-moderate-user-profile-dialog',
  templateUrl: './moderate-user-profile-dialog.component.html',
  styleUrls: ['./moderate-user-profile-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModerateUserProfileDialogComponent {
  public readonly blankProfile = blankProfileImage;
  constructor(
    @Inject(MAT_DIALOG_DATA) public profile: UserProfileEntity
  ) {}
}
