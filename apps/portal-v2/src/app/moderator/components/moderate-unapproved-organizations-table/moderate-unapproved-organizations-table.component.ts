import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { EditOrganizationId } from '@edit-organization/models/edit-organization.models';
import { ModerateUnapprovedOrganizationEntity } from '@moderator/store/moderate-unapproved-organizations';

@Component({
  selector: 'app-moderate-unapproved-organizations-table',
  templateUrl: './moderate-unapproved-organizations-table.component.html',
  styleUrls: ['./moderate-unapproved-organizations-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModerateUnapprovedOrganizationsTableComponent implements AfterViewInit {
  private _organizations!: ModerateUnapprovedOrganizationEntity[];
  @Input() public set unapprovedOrganizations(val: ModerateUnapprovedOrganizationEntity[] | undefined) {
    if (val) {
      this._organizations = val;
      this.dataSource.data = val;
      this.dataSource.sort = this.sort;
    }
  }

  public get unapprovedOrganizations() {
    return this._organizations;
  }

  @Output() public reviewUnapprovedOrganizationClicked = new EventEmitter<EditOrganizationId>();

  @ViewChild(MatSort) public sort!: MatSort;
  public dataSource = new MatTableDataSource<Partial<ModerateUnapprovedOrganizationEntity>>(
    [{ organization_name: 'Loading unapproved organizations...', organization_id: '' }]
  );
  public displayColumns: string[] = ['organization_name', 'categories', 'status'];

  public ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }
}
