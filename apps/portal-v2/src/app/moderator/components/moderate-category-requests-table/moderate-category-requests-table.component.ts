import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CategoryRequestId, ModerateOrganizationCategoryRequestEntity } from '@moderator/store/moderate-organization-category-requests';
import { AppRoutes } from '@router/router.constants';

@Component({
  selector: 'app-moderate-category-requests-table',
  templateUrl: './moderate-category-requests-table.component.html',
  styleUrls: ['./moderate-category-requests-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModerateCategoryRequestsTableComponent implements AfterViewInit {
  public readonly PUBLIC_ORG_ROUTE = AppRoutes.PUBLIC_ORG_ROUTE;

  private _requests!: ModerateOrganizationCategoryRequestEntity[];
  @Input() public set categoryRequests(val: ModerateOrganizationCategoryRequestEntity[] | undefined) {
    if (val) {
      this._requests = val;
      this.dataSource.data = val;
      this.dataSource.sort = this.sort;
    }
  }
  public get categoryRequests() {
    return this._requests;
  }

  @Output() public acceptRequestClicked = new EventEmitter<CategoryRequestId>();
  @Output() public denyRequestClicked = new EventEmitter<CategoryRequestId>();

  @ViewChild(MatSort) public sort!: MatSort;
  public dataSource = new MatTableDataSource<Partial<ModerateOrganizationCategoryRequestEntity>>(
    [{ organization_id: '', organization_category_request_id: -1 }]
  );
  public displayColumns: string[] = ['organization_name', 'email_address', 'first_name', 'last_name', 'categories', 'status', 'actions'];

  public ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }
}
