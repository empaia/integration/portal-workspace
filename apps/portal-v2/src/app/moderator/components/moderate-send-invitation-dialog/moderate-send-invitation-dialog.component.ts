import { inject } from '@angular/core';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MAX_FORM_FIELD_LENGTH } from '@shared/models/form.models';

@Component({
  selector: 'app-moderate-send-invitation-dialog',
  templateUrl: './moderate-send-invitation-dialog.component.html',
  styleUrls: ['./moderate-send-invitation-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModerateSendInvitationDialogComponent {
  private fb = inject(FormBuilder);
  public emailForm = this.fb.nonNullable.control(
    '',
    [
      Validators.required,
      Validators.email,
      Validators.maxLength(MAX_FORM_FIELD_LENGTH)
    ]
  );

  public readonly MAX_FORM_LENGTH = MAX_FORM_FIELD_LENGTH;

  constructor(
    private dialogRef: MatDialogRef<ModerateSendInvitationDialogComponent>,
  ) {}

  public onSubmit(): void {
    const email = this.emailForm.value;
    this.dialogRef.close(email);
  }
}
