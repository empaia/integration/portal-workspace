import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ModerateUserId } from '@moderator/store/moderate-approved-users';
import { ModerateUnapprovedUserEntity } from '@moderator/store/moderate-unapproved-users';

@Component({
  selector: 'app-moderate-unapproved-users-table',
  templateUrl: './moderate-unapproved-users-table.component.html',
  styleUrls: ['./moderate-unapproved-users-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModerateUnapprovedUsersTableComponent implements AfterViewInit {
  private _users!: ModerateUnapprovedUserEntity[];
  @Input() public set unapprovedUsers(val: ModerateUnapprovedUserEntity[] | undefined) {
    if (val) {
      this._users = val;
      this.dataSource.data = val;
      this.dataSource.sort = this.sort;
    }
  }

  public get unapprovedUsers() {
    return this._users;
  }

  @Output() public deleteUserClicked = new EventEmitter<ModerateUserId>();

  @ViewChild(MatSort) private sort!: MatSort;
  public dataSource = new MatTableDataSource<Partial<ModerateUnapprovedUserEntity>>(
    [{ username: 'Loading data...', user_id: '' }]
  );
  public displayColumns: string[] = ['username', 'actions'];

  public ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }
}
