import { ChangeDetectionStrategy, Component } from '@angular/core';
import { EditOrganizationId } from '@edit-organization/models/edit-organization.models';
import { CategoryRequestId, ModerateOrganizationCategoryRequestEntity, ModerateOrganizationCategoryRequestsActions, ModerateOrganizationCategoryRequestsSelectors } from '@moderator/store/moderate-organization-category-requests';
import { ModerateOrganizationEntity, ModerateOrganizationsActions, ModerateOrganizationsSelectors } from '@moderator/store/moderate-organizations';
import { ModerateUnapprovedOrganizationEntity, ModerateUnapprovedOrganizationsSelectors } from '@moderator/store/moderate-unapproved-organizations';
import * as RouterActions from '@router/router.actions';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppRoutes } from '@router/router.constants';

@Component({
  selector: 'app-moderate-organizations-container',
  templateUrl: './moderate-organizations-container.component.html',
  styleUrls: ['./moderate-organizations-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModerateOrganizationsContainerComponent {
  public approvedOrganizations$: Observable<ModerateOrganizationEntity[]>;
  public unapprovedOrganizations$: Observable<ModerateUnapprovedOrganizationEntity[]>;
  public categoryRequests$: Observable<ModerateOrganizationCategoryRequestEntity[]>;

  constructor(private store: Store) {
    this.approvedOrganizations$ = this.store.select(ModerateOrganizationsSelectors.selectAllApprovedOrganizations);
    this.unapprovedOrganizations$ = this.store.select(ModerateUnapprovedOrganizationsSelectors.selectAllUnapprovedOrganizations);
    this.categoryRequests$ = this.store.select(ModerateOrganizationCategoryRequestsSelectors.selectAllOrganizationCategoryRequests);

    this.store.dispatch(ModerateOrganizationsActions.initialLoadOrganizations());
  }

  public openUnapprovedOrganizationDetails(event: EditOrganizationId): void {
    this.store.dispatch(RouterActions.navigate({
      path: [AppRoutes.MODERATE_ROUTE, AppRoutes.MODERATE_ORGANIZATIONS, event.id]
    }));
  }

  public activateApprovedOrganization(event: EditOrganizationId): void {
    this.store.dispatch(ModerateOrganizationsActions.openActivateOrganizationDialog({ organizationId: event.id }));
  }

  public deactivateApprovedOrganization(event: EditOrganizationId): void {
    this.store.dispatch(ModerateOrganizationsActions.openDeactivateOrganizationDialog({ organizationId: event.id }));
  }

  public editCategoriesApprovedOrganization(event: EditOrganizationId): void {
    this.store.dispatch(ModerateOrganizationsActions.openEditCategoriesDialog({ organizationId: event.id }));
  }

  public acceptCategoryRequest(event: CategoryRequestId): void {
    this.store.dispatch(ModerateOrganizationCategoryRequestsActions.openAcceptCategoryRequestDialog({ categoryRequestId: event.id }));
  }

  public denyCategoryRequest(event: CategoryRequestId): void {
    this.store.dispatch(ModerateOrganizationCategoryRequestsActions.openDenyCategoryRequestDialog({ categoryRequestId: event.id }));
  }
}
