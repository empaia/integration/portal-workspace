import { ChangeDetectionStrategy, Component } from '@angular/core';
import { EditOrganizationId } from '@edit-organization/models/edit-organization.models';
import { ModerateUnapprovedOrganizationProfileEntity, ModerateUnapprovedOrganizationsActions, ModerateUnapprovedOrganizationsSelectors } from '@moderator/store/moderate-unapproved-organizations';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-moderate-review-unapproved-organization-container',
  templateUrl: './moderate-review-unapproved-organization-container.component.html',
  styleUrls: ['./moderate-review-unapproved-organization-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModerateReviewUnapprovedOrganizationContainerComponent {
  public profile$: Observable<ModerateUnapprovedOrganizationProfileEntity | undefined>;

  constructor(private store: Store) {
    this.profile$ = this.store.select(ModerateUnapprovedOrganizationsSelectors.selectUnapprovedOrganizationProfile);
  }

  public onAcceptClicked(event: EditOrganizationId): void {
    this.store.dispatch(ModerateUnapprovedOrganizationsActions.openAcceptDialog({ organizationId: event.id }));
  }

  public onDenyClicked(event: EditOrganizationId): void {
    this.store.dispatch(ModerateUnapprovedOrganizationsActions.openDenyDialog({ organizationId: event.id }));
  }
}
