import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ModerateApprovedUserEntity, ModerateApprovedUsersActions, ModerateApprovedUsersSelectors, ModerateDialogs, ModerateUserId } from '@moderator/store/moderate-approved-users';
import { ModerateUnapprovedUserEntity, ModerateUnapprovedUsersActions, ModerateUnapprovedUsersSelectors } from '@moderator/store/moderate-unapproved-users';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-moderate-users-container',
  templateUrl: './moderate-users-container.component.html',
  styleUrls: ['./moderate-users-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModerateUsersContainerComponent {
  public approvedUsers$: Observable<ModerateApprovedUserEntity[]>;
  public selectedUserId$: Observable<string | undefined>;
  public unapprovedUsers$: Observable<ModerateUnapprovedUserEntity[]>;

  constructor(private store: Store) {
    this.approvedUsers$ = this.store.select(ModerateApprovedUsersSelectors.selectAllApprovedUsers);
    this.selectedUserId$ = this.store.select(ModerateApprovedUsersSelectors.selectSelectedApprovedUserId);
    this.unapprovedUsers$ = this.store.select(ModerateUnapprovedUsersSelectors.selectAllUnapprovedUsers);

    this.store.dispatch(ModerateApprovedUsersActions.initialLoadApprovedUsers());
  }

  public deleteUnapprovedUser(event: ModerateUserId): void {
    this.store.dispatch(ModerateUnapprovedUsersActions.deleteUnapprovedUser({ unapprovedUserId: event.id }));
  }

  public selectApprovedUser(event: ModerateUserId): void {
    this.store.dispatch(ModerateApprovedUsersActions.selectUser({ selected: event.id }));
  }

  public showApprovedUserProfile(event: ModerateUserId): void {
    this.store.dispatch(ModerateApprovedUsersActions.loadApprovedUserProfile({ userId: event.id, dialog: ModerateDialogs.USER_PROFILE }));
  }

  public activateUser(event: ModerateUserId): void {
    this.store.dispatch(ModerateApprovedUsersActions.openActivateApprovedUserDialog({ userId: event.id }));
  }

  public deactivateUser(event: ModerateUserId): void {
    this.store.dispatch(ModerateApprovedUsersActions.openDeactivateApprovedUserDialog({ userId: event.id }));
  }

  public openSetUserRolesDialog(event: ModerateUserId): void {
    this.store.dispatch(ModerateApprovedUsersActions.loadApprovedUserProfile({ userId: event.id, dialog: ModerateDialogs.USER_ROLES }));
  }

  public openSendInvitationDialog(): void {
    this.store.dispatch(ModerateApprovedUsersActions.openSendInvitationDialog());
  }
}
