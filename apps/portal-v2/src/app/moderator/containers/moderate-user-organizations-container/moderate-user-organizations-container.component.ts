import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ModerateApprovedUsersSelectors } from '@moderator/store/moderate-approved-users';
import { ModerateUserOrganizationEntity, ModerateUserOrganizationsSelectors } from '@moderator/store/moderate-user-organizations';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-moderate-user-organizations-container',
  templateUrl: './moderate-user-organizations-container.component.html',
  styleUrls: ['./moderate-user-organizations-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModerateUserOrganizationsContainerComponent {
  public userOrganizations$: Observable<ModerateUserOrganizationEntity[]>;
  public selectedUsername$: Observable<string | undefined>;

  constructor(private store: Store) {
    this.userOrganizations$ = this.store.select(ModerateUserOrganizationsSelectors.selectAllUserOrganizations);
    this.selectedUsername$ = this.store.select(ModerateApprovedUsersSelectors.selectSelectedApprovedUsername);
  }
}
