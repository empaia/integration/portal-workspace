import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { StatisticsPage } from '@api/moderator/models';
import { ModerateStatisticsActions, ModerateStatisticsSelectors, TrafficStatistics } from '@moderator/store/moderate-statistics';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-moderate-statistics-container',
  templateUrl: './moderate-statistics-container.component.html',
  styleUrls: ['./moderate-statistics-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModerateStatisticsContainerComponent implements OnInit {
  public readonly PAGES = StatisticsPage;
  public publicMarketplaceTraffic$: Observable<TrafficStatistics[]>;
  public aiRegisterTraffic$: Observable<TrafficStatistics[]>;

  constructor(private store: Store) {
    this.publicMarketplaceTraffic$ = this.store.select(ModerateStatisticsSelectors.selectPublicMarketplaceTrafficStatistics);
    this.aiRegisterTraffic$ = this.store.select(ModerateStatisticsSelectors.selectPublicAiRegisterTrafficStatistics);
  }

  public ngOnInit(): void {
    this.store.dispatch(ModerateStatisticsActions.loadModerateStatistics());
  }
}
