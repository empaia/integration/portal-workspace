import { LangDefinition } from '@ngneat/transloco';
import { AppRoutes } from '@router/router.constants';

// theme names must mirror css class names in styles.scss
export const themes = [
  'light-theme',
  //'dark-solarized-theme',
  'dark-theme'
];

export const availableLanguages: LangDefinition[] = [
  { id: 'en', label: 'EN' },
  { id: 'de', label: 'DE' },
];

// export const languages = ['en', 'de'] as const;
// export type Language = typeof languages[number];

export const fallbackImage = '/assets/logos/fallback.jpg';
export const blankProfileImage = './assets/profile-picture/empty-user-avatar.png';
export const blankOrgImage = './assets/organization-logo/empty-org-logo.png';


export const mpsApi = {
  version: '/v1/',
  customer: 'customer',
  public: 'public',
  vendor: 'vendor',
  productProvider: 'product-provider',
  moderator: 'moderator',
};

export const evesApi = {

  version: '/v1',
  events: '/events',
};

export const DEFAULT_PAGE_SIZE = 999;

export const defaultPaged = {
  page_size: DEFAULT_PAGE_SIZE,
  page: 0,
};


export interface UserProfileInfo {
  orgId?: string;
  orgName?: string;
  userName?: string;
  email?: string;
  profilePicture?: string;
  orgPicture?: string;
}


export interface ElevatedRoles {
  manager: boolean | undefined;
  moderator: boolean | undefined,
  maintainer: boolean | undefined,
}

export const enum UserRoleUI {
  USER = 'USER',
  MANAGER = 'MANAGER',
  MODERATOR = 'MODERATOR',
  MAINTAINER = 'MAINTAINER',
}

const NAV_LINK_ID = 'nav-id';
const NAV_LINK_MANAGE = 'manage';
const NAV_LINK_MODERATE = 'moderate';

const adminSidenavLinkIds = [
  // MANAGE
  `${NAV_LINK_ID}-${NAV_LINK_MANAGE}-${AppRoutes.MANAGE_MEMBERSHIP_REQUESTS}`,
  `${NAV_LINK_ID}-${NAV_LINK_MANAGE}-${AppRoutes.MANAGE_EDIT_ORGANIZATION}`,
  `${NAV_LINK_ID}-${NAV_LINK_MANAGE}-${AppRoutes.MANAGE_REQUEST_ORGANIZATION_CATEGORY_CHANGE}`,
  `${NAV_LINK_ID}-${NAV_LINK_MANAGE}-${AppRoutes.MANAGE_USERS}`,
  // MODERATE
  `${NAV_LINK_ID}-${NAV_LINK_MODERATE}-${AppRoutes.MODERATE_USERS}`,
  `${NAV_LINK_ID}-${NAV_LINK_MODERATE}-${AppRoutes.MODERATE_USER_ORGANIZATIONS}`,
  `${NAV_LINK_ID}-${NAV_LINK_MODERATE}-${AppRoutes.MODERATE_ORGANIZATIONS}`,
  `${NAV_LINK_ID}-${NAV_LINK_MODERATE}-${AppRoutes.MODERATE_STATISTICS}`,
] as const;
export type NavLinkId = typeof adminSidenavLinkIds[number];

export interface NavLink {
  ref: string,
  id: NavLinkId,
  role: UserRoleUI,
}

export const ADMIN_SIDE_NAV_ITEMS: NavLink[] = [
  {
    ref: AppRoutes.MANAGE_ROUTE + '/' + AppRoutes.MANAGE_USERS,
    id: 'nav-id-manage-users',
    role: UserRoleUI.MANAGER,
  },
  {
    ref: AppRoutes.MANAGE_ROUTE + '/' + AppRoutes.MANAGE_MEMBERSHIP_REQUESTS,
    id: 'nav-id-manage-membership-requests',
    role: UserRoleUI.MANAGER,
  },
  {
    ref: AppRoutes.MANAGE_ROUTE + '/' + AppRoutes.MANAGE_EDIT_ORGANIZATION,
    id: 'nav-id-manage-edit-organization',
    role: UserRoleUI.MANAGER,
  },
  {
    ref: AppRoutes.MANAGE_ROUTE + '/' + AppRoutes.MANAGE_REQUEST_ORGANIZATION_CATEGORY_CHANGE,
    id: 'nav-id-manage-category-change',
    role: UserRoleUI.MANAGER,
  },
  {
    ref: AppRoutes.MODERATE_ROUTE + '/' + AppRoutes.MODERATE_USERS,
    id: 'nav-id-moderate-users',
    role: UserRoleUI.MODERATOR,
  },
  {
    ref: AppRoutes.MODERATE_ROUTE + '/' + AppRoutes.MODERATE_USER_ORGANIZATIONS,
    id: 'nav-id-moderate-user-organizations',
    role: UserRoleUI.MODERATOR,
  },
  {
    ref: AppRoutes.MODERATE_ROUTE + '/' + AppRoutes.MODERATE_ORGANIZATIONS,
    id: 'nav-id-moderate-organizations',
    role: UserRoleUI.MODERATOR,
  },
  {
    ref: AppRoutes.MODERATE_ROUTE + '/' + AppRoutes.MODERATE_STATISTICS,
    id: 'nav-id-moderate-statistics',
    role: UserRoleUI.MODERATOR,
  }
];

