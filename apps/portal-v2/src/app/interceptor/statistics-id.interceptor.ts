import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { StatisticsService } from '@shared/services/statistics/statistics.service';
import { environment } from '@env/environment';

@Injectable()
export class StatisticsIdInterceptor implements HttpInterceptor {

  constructor(private statistics: StatisticsService) {}

  public intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const isMpsPublic = this.isHostAccepted(request.url, environment.mpsApiUrl, '/v1/public/');
    const newRequest = isMpsPublic ? this.addStatisticsId(this.statistics.statisticsId, request) : request;
    return next.handle(newRequest);
  }

  private isHostAccepted(requestUrl: string, hostUrl: string, additional?: string): boolean {
    const requestHost = this.parseUrl(requestUrl)?.host;
    const host = this.parseUrl(hostUrl)?.host;

    return (
      requestHost === host
    ) && (
      additional
        ? requestUrl.includes(additional)
        : true
    );
  }

  private parseUrl(requestUrl: string): URL | undefined {
    let url: URL | undefined = undefined;
    try {
      url = new URL(requestUrl);
    } catch(error) {
      // ignore invalid urls and return undefined
    }
    return url;
  }

  private addStatisticsId(id: string, request: HttpRequest<unknown>): HttpRequest<unknown> {
    return request.clone({ setHeaders: { 'statistics-id': id } });
  }
}
