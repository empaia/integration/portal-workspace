import { Injectable, inject } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthSelectors } from '@auth/store/auth';
import { Store } from '@ngrx/store';
import { first, mergeMap } from 'rxjs/operators';
import { environment } from '@env/environment';

@Injectable()
export class UserIdInterceptor implements HttpInterceptor {

  private store = inject(Store);

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.store.select(AuthSelectors.selectUserId).pipe(
      first(),
      mergeMap(id => {
        const exception = this.hostException(request.url);

        const newReq = (
          exception
            ? request
            : this.addUserId(id, request)
        );
        return next.handle(newReq);
      }),
    );
  }


  // if we are talking to keycloak do not send 'user-id' header
  // keycloak would need extra configuration to accept an unexpected header
  private hostException(requestUrl: string): boolean {
    const requestHost = this.parseUrl(requestUrl)?.host;
    const keycloakHost = this.parseUrl(environment.authKeycloakUrl)?.host;
    return (requestHost === keycloakHost);
  }

  private parseUrl(reqUrl: string): URL | undefined {
    let url: URL | undefined = undefined;
    try {
      url = new URL(reqUrl);
    } catch (error) {
      // ignore invalid urls & return undefined
    }
    return url;
  }

  private addUserId(id: string | undefined, request: HttpRequest<unknown>): HttpRequest<unknown> {
    return (id
      ? request.clone({ setHeaders: { 'user-id': id }, })
      : request);
  }
}
