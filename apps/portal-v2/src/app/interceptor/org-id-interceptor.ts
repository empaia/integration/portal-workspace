import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Store } from '@ngrx/store';
import { UserOrganizationsSelectors } from '@user/store/user-organizations';
import { Observable } from 'rxjs';
import { first, mergeMap } from 'rxjs/operators';

@Injectable()
export class SelectedOrgIdInterceptor implements HttpInterceptor {
  private store = inject(Store);

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.store
      .select(UserOrganizationsSelectors.selectUserOrganizationsDefaultOrgId)
      .pipe(
        first(),
        mergeMap(orgId => {
          const exception = this.hostException(request.url);

          const newReq = exception ? request : this.addOrgId(orgId, request);
          return next.handle(newReq);
        })
      );
  }

  // if we are talking to keycloak do not send 'organization-id' header
  // keycloak would need extra configuration to accept an unexpected header
  private hostException(requestUrl: string): boolean {
    const requestHost = this.parseUrl(requestUrl)?.host;
    const keycloakHost = this.parseUrl(environment.authKeycloakUrl)?.host;
    return (requestHost === keycloakHost);
  }

  private parseUrl(reqUrl: string): URL | undefined {
    let url: URL | undefined = undefined;
    try {
      url = new URL(reqUrl);
    } catch (error) {
      // ignore invalid urls & return undefined
    }
    return url;
  }

  private addOrgId(orgId: string | undefined, request: HttpRequest<unknown>): HttpRequest<unknown> {
    return orgId
      ? request.clone({ setHeaders: { 'organization-id': orgId } })
      : request;
  }
}
