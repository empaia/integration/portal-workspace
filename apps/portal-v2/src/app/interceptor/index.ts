/* "Barrel" of Http Interceptors */
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Provider } from '@angular/core';
import { SelectedOrgIdInterceptor } from 'src/app/interceptor/org-id-interceptor';

import { UserIdInterceptor } from './user-id.interceptor';
import { StatisticsIdInterceptor } from './statistics-id.interceptor';


export const SELECTED_ORG_API_INTERCEPTOR_PROVIDER: Provider = {
  provide: HTTP_INTERCEPTORS,
  useClass: SelectedOrgIdInterceptor,
  multi: true
};


export const USER_ID_API_INTERCEPTOR_PROVIDER: Provider = {
  provide: HTTP_INTERCEPTORS,
  useClass: UserIdInterceptor,
  multi: true
};

export const STATISTICS_ID_API_INTERCEPTOR_PROVIDER: Provider = {
  provide: HTTP_INTERCEPTORS,
  useClass: StatisticsIdInterceptor,
  multi: true
};

/** Http interceptor providers in outside-in order */
export const HTTP_INTERCEPTOR_PROVIDERS = [
  // { provide: HTTP_INTERCEPTORS, useClass: UserIdInterceptor, multi: true },
  // { provide: HTTP_INTERCEPTORS, useClass: SelectedOrgIdInterceptor, multi: true },
  USER_ID_API_INTERCEPTOR_PROVIDER,
  SELECTED_ORG_API_INTERCEPTOR_PROVIDER,
  STATISTICS_ID_API_INTERCEPTOR_PROVIDER,
];
