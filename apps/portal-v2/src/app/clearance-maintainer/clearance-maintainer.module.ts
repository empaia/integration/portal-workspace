import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClearanceMaintainerRoutingModule } from './clearance-maintainer-routing.module';
import { ClearancesContainerComponent } from './containers/clearances-container/clearances-container.component';
import { ClearancesRootStoreModule } from './store/clearance-root-store.module';
import { EditClearanceItemDialogComponent } from './components/edit-clearance-item-dialog/edit-clearance-item-dialog.component';
import { MaterialModule } from '@material/material.module';
import { SharedModule } from '@shared/shared.module';
import { ClearanceItemDetailsDialogComponent } from './components/clearance-item-details-dialog/clearance-item-details-dialog.component';
import { TranslocoRootModule } from '@transloco/transloco-root.module';


@NgModule({
  declarations: [
    ClearancesContainerComponent,
    EditClearanceItemDialogComponent,
    ClearanceItemDetailsDialogComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    ClearanceMaintainerRoutingModule,
    ClearancesRootStoreModule,
    TranslocoRootModule,
  ],
})
export class ClearanceMaintainerModule { }
