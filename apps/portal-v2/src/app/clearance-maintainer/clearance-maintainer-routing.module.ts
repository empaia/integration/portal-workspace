import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutes } from '@router/router.constants';
import { ClearancesContainerComponent } from './containers/clearances-container/clearances-container.component';

const routes: Routes = [
  {
    path: AppRoutes.CLEARANCE_MAINTAINER_CLEARANCES,
    component: ClearancesContainerComponent,
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: AppRoutes.CLEARANCE_MAINTAINER_CLEARANCES
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClearanceMaintainerRoutingModule { }
