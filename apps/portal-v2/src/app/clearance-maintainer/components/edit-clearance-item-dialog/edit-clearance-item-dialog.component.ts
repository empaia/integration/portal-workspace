import { ChangeDetectionStrategy, Component, Inject, OnInit, ViewChild, inject } from '@angular/core';
import {FormBuilder, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {
  DescriptionForm,
  EditClearanceItemDialogData,
  EditClearanceItemDialogOutput,
  InfoSourceForm,
  convertReleaseDateNumberToDate,
  covertTextTranslationToLanguageText
} from './edit-clearance-item-dialog.models';
import { DatePrecision, InfoSourceType } from '@api/product-provider/models';
import { ClearancesTagList } from '@clearance-maintainer/store/clearances-tags';
import { CustomFormValidatorsService } from '@shared/services/custom-form-validators/custom-form-validators.service';
import { MatDatepicker } from '@angular/material/datepicker';

@Component({
  selector: 'app-edit-clearance-item-dialog',
  templateUrl: './edit-clearance-item-dialog.component.html',
  styleUrls: ['./edit-clearance-item-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditClearanceItemDialogComponent implements OnInit {
  private fb = inject(FormBuilder);

  @ViewChild('picker') private picker!: MatDatepicker<Date>;

  public clearanceItemForm = this.fb.nonNullable.group({
    product_name: ['', [Validators.required]],
    product_release_at: this.fb.control<Date|null>(null, [Validators.required]),
    product_release_at_precision: ['' as DatePrecision, [Validators.required]],
    info_source_content: this.fb.nonNullable.array(
      [
        this.fb.nonNullable.group({
          label: this.fb.group({
            EN: [''],
          }),
          reference_url: ['', [
            Validators.required,
            CustomFormValidatorsService.url
          ]]
        })
      ]
    ),
    description: this.fb.nonNullable.group({
      EN: ['', [Validators.required]],
      DE: ['']
    })
  });

  public activeTags = this.createEmptyTagList();

  public readonly DATE_PRECISION = DatePrecision;
  public readonly INFO_SOURCE_TYPE = InfoSourceType;

  public readonly DATE_PRECISION_ARRAY = Object.values(DatePrecision);
  public readonly INFO_SOURCE_TYPE_ARRAY = Object.values(InfoSourceType);

  constructor(
    private dialogRef: MatDialogRef<EditClearanceItemDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: EditClearanceItemDialogData,
  ) { }

  public get productName(): FormControl<string> {
    return this.clearanceItemForm.controls.product_name;
  }

  public get productReleaseAt(): FormControl<Date | null> {
    return this.clearanceItemForm.controls.product_release_at;
  }

  public get productReleaseAtPrecision(): FormControl<DatePrecision> {
    return this.clearanceItemForm.controls.product_release_at_precision;
  }

  public get infoSourceContent(): InfoSourceForm {
    return this.clearanceItemForm.controls.info_source_content;
  }

  public get description(): DescriptionForm {
    return this.clearanceItemForm.controls.description;
  }

  public ngOnInit(): void {
    if (this.data.clearanceItem) {
      this.setFields();
    }
  }

  public addContent(event?: MouseEvent): void {
    event?.preventDefault();
    event?.stopImmediatePropagation();

    this.infoSourceContent.push(
      this.fb.nonNullable.group({
        label: this.fb.group({
          EN: [''],
          // DE: ['']
        }),
        reference_url: ['',
          [
            Validators.required,
            CustomFormValidatorsService.url
          ]
        ]
      })
    );
  }

  public removeContent(index: number): void {
    this.infoSourceContent.removeAt(index);
  }

  public onReleasePrecisionChanged(): void {
    switch(this.productReleaseAtPrecision.value) {
      case DatePrecision.DATE:
      case DatePrecision.MONTH_YEAR:
      case DatePrecision.QUARTER:
      case DatePrecision.YEAR:
        this.clearanceItemForm.patchValue({
          product_release_at: null
        });
        break;
      case DatePrecision.TBA:
      case DatePrecision.UNKNOWN:
        this.clearanceItemForm.patchValue({
          product_release_at: new Date(0)
        });
        break;
    }
  }

  public onSelectedFiltersChanged(event: ClearancesTagList): void {
    this.activeTags = event;
  }

  public onMonthSelected(event: Date): void {
    if (
      this.productReleaseAtPrecision.value === DatePrecision.MONTH_YEAR
      || this.productReleaseAtPrecision.value === DatePrecision.QUARTER
    ) {
      this.picker.close();
      this.picker.select(event);
    }
  }

  public onYearSelected(event: Date): void {
    if (this.productReleaseAtPrecision.value === DatePrecision.YEAR) {
      this.picker.close();
      this.picker.select(event);
    }
  }

  public onSubmit(): void {
    if (this.clearanceItemForm.valid) {
      const output = {
        ...this.clearanceItemForm.value,
        product_release_at: this.productReleaseAt.value ? this.productReleaseAt.value.valueOf() : 0,
        tags: this.activeTags,
      } as EditClearanceItemDialogOutput;
      this.dialogRef.close(output);
    }
  }

  private setFields(): void {
    this.removeContent(0);
    this.data.clearanceItem?.info_source_content?.forEach(() => this.addContent());

    this.clearanceItemForm.patchValue({
      ...this.data.clearanceItem,
      product_release_at: convertReleaseDateNumberToDate(this.data.clearanceItem?.product_release_at),
      description: covertTextTranslationToLanguageText(this.data.clearanceItem?.description),
      info_source_content: this.data.clearanceItem?.info_source_content?.map(content => ({
        ...content,
        label: covertTextTranslationToLanguageText(content.label),
      }))
    });
    this.activeTags = this.data.clearanceItem?.tags ?? this.createEmptyTagList();
  }

  private createEmptyTagList(): ClearancesTagList {
    return {
      tissues: [],
      stains: [],
      indications: [],
      analysis: [],
      procedures: [],
      image_types: [],
      clearances: [],
      scanners: [],
    };
  }
}
