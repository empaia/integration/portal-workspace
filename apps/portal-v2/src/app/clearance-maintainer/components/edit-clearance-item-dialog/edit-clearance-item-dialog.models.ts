import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { ClearanceItem, DatePrecision, Language, TextTranslation } from '@api/product-provider/models';
import { ClearancesTagList } from '@clearance-maintainer/store/clearances-tags';
import { MILLISECONDS_SECONDS_FACTOR } from '@clearance-maintainer/store/clearances/clearances.models';

export interface EditClearanceItemDialogData {
  clearanceTags: ClearancesTagList;
  clearanceItem?: ClearanceItem;
}

export interface LanguageText {
  EN: string;
  DE?: string;
}

export interface InfoSourceContentDialog {
  label: LanguageText;
  reference_url: string;
}

export interface EditClearanceItemDialogOutput {
  product_name: string;
  product_release_at: number;
  product_release_at_precision: DatePrecision;
  info_source_content?: InfoSourceContentDialog[];
  description: LanguageText;
  tags: ClearancesTagList;
}

export type InfoSourceForm = FormArray<FormGroup<{ label: FormGroup<{ EN: FormControl<string | null> }>, reference_url: FormControl<string> }>>;

export type DescriptionForm = FormGroup<{ EN: FormControl<string>, DE: FormControl<string> }>;

export function covertTextTranslationToLanguageText(textTranslation?: TextTranslation[] | null): LanguageText | undefined {
  if (!textTranslation) { return undefined; }

  return {
    EN: textTranslation.find(d => d.lang === Language.EN)?.text ?? '',
    DE: textTranslation.find(d => d.lang === Language.DE)?.text,
  };
}

export function convertReleaseDateNumberToDate(releasedAt?: number): Date | null {
  return releasedAt !== undefined ? new Date(releasedAt * MILLISECONDS_SECONDS_FACTOR) : null;
}
