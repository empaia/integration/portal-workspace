import { ClearanceItem } from '@api/product-provider/models';

export interface ClearanceItemDetailsDialogData {
  clearanceItem: ClearanceItem;
  isActiveItem: boolean;
}
