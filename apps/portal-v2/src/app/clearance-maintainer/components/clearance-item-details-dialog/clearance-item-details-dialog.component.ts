import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { InfoSourceType } from '@api/product-provider/models';
import { ClearanceItemDetailsDialogData } from './clearance-item-details.models';

@Component({
  selector: 'app-clearance-item-details-dialog',
  templateUrl: './clearance-item-details-dialog.component.html',
  styleUrls: ['./clearance-item-details-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClearanceItemDetailsDialogComponent {
  public INFO_SOURCE_TYPE = InfoSourceType;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: ClearanceItemDetailsDialogData,
  ) {}
}
