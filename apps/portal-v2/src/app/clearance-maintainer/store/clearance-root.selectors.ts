import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';

import * as fromClearances from './clearances/clearances.reducer';
import * as fromClearancesTags from './clearances-tags/clearances-tags.reducer';

export const CLEARANCES_ROOT_FEATURE_KEY = 'clearancesRoot';

export const selectClearancesRootState = createFeatureSelector<State>(
  CLEARANCES_ROOT_FEATURE_KEY
);

export interface State {
  [fromClearances.CLEARANCES_FEATURE_KEY]: fromClearances.State;
  [fromClearancesTags.CLEARANCES_TAGS_FEATURE_KEY]: fromClearancesTags.State;
}

export function reducers(state: State, action: Action) {
  return combineReducers({
    [fromClearances.CLEARANCES_FEATURE_KEY]: fromClearances.reducer,
    [fromClearancesTags.CLEARANCES_TAGS_FEATURE_KEY]: fromClearancesTags.reducer,
  })(state, action);
}
