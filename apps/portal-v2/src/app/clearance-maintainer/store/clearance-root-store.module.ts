import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { CLEARANCES_ROOT_FEATURE_KEY, reducers } from './clearance-root.selectors';
import { EffectsModule } from '@ngrx/effects';
import { ClearancesEffects } from './clearances';
import { ClearancesTagsEffects } from './clearances-tags';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(
      CLEARANCES_ROOT_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      ClearancesEffects,
      ClearancesTagsEffects,
    ])
  ]
})
export class ClearancesRootStoreModule { }
