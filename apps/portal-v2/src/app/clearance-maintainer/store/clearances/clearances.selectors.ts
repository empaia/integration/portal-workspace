import { createSelector } from '@ngrx/store';
import { selectClearancesRootState } from '../clearance-root.selectors';
import { clearanceAdapter, CLEARANCES_FEATURE_KEY } from './clearances.reducer';
import { ClearanceItemTableEntity } from '@shared/models/clearances.models';

const {
  selectAll,
  selectEntities,
} = clearanceAdapter.getSelectors();

export const selectClearancesState = createSelector(
  selectClearancesRootState,
  (state) => state[CLEARANCES_FEATURE_KEY]
);

export const selectAllClearances = createSelector(
  selectClearancesState,
  selectAll,
);

export const selectClearancesEntities = createSelector(
  selectClearancesState,
  selectEntities,
);

export const selectAllActiveClearances = createSelector(
  selectAllClearances,
  (clearances) => clearances.map(clearance => ({
    ...clearance.active_clearance_item,
    isHidden: clearance.is_hidden,
    clearanceId: clearance.id
  } as ClearanceItemTableEntity))
);

export const selectActiveClearance = (clearanceId?: string) => createSelector(
  selectClearancesEntities,
  (clearances) => clearanceId ? clearances[clearanceId]?.active_clearance_item : undefined
);

export const selectAllClearanceItems = (clearanceId: string) => createSelector(
  selectClearancesEntities,
  (clearances) => clearances[clearanceId]?.history
);

export const selectClearanceItem = (clearanceId: string, itemId: string) => createSelector(
  selectAllClearanceItems(clearanceId),
  (clearanceItems) => clearanceItems?.find(item => item.id === itemId)
);

export const selectIsActiveClearanceItem = (clearanceId: string, itemId: string) => createSelector(
  selectActiveClearance(clearanceId),
  (clearanceItem) => clearanceItem ? clearanceItem.id === itemId : false
);

export const selectClearancesLoaded = createSelector(
  selectClearancesState,
  (state) => state.loaded
);

export const selectClearancesError = createSelector(
  selectClearancesState,
  (state) => state.error
);
