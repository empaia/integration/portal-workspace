import { HttpErrorResponse } from '@angular/common/http';
import { Clearance, PostClearance, PostClearanceItem } from '@api/product-provider/models';
import { createActionGroup, emptyProps, props } from '@ngrx/store';

export const ClearancesActions = createActionGroup({
  source: 'Clearances',
  events: {
    'Load All Clearances': emptyProps(),
    'Load All Clearances Success': props<{ clearances: Clearance[] }>(),
    'Load All Clearances Failure': props<{ error: HttpErrorResponse }>(),
    'Load Clearance': props<{ clearanceId: string }>(),
    'Load Clearance Success': props<{ clearance: Clearance }>(),
    'Load Clearance Failure': props<{ error: HttpErrorResponse }>(),
    'Create Clearance': props<{ postClearance: PostClearance }>(),
    'Create Clearance Success': props<{ clearance: Clearance }>(),
    'Create Clearance Failure': props<{ error: HttpErrorResponse }>(),
    'Hide Clearance': props<{ clearanceId: string }>(),
    'Hide Clearance Success': props<{ clearance: Clearance }>(),
    'Hide Clearance Failure': props<{ error: HttpErrorResponse }>(),
    'Unhide Clearance': props<{ clearanceId: string }>(),
    'Unhide Clearance Success': props<{ clearance: Clearance }>(),
    'Unhide Clearance Failure': props<{ error: HttpErrorResponse }>(),
    'Create Clearance Item': props<{
      clearanceId: string,
      postItem: PostClearanceItem
    }>(),
    'Create Clearance Item Success': props<{ clearance: Clearance }>(),
    'Create Clearance Item Failure': props<{ error: HttpErrorResponse }>(),
    'Activate Clearance Item': props<{
      clearanceId: string,
      clearanceItemId: string
    }>(),
    'Activate Clearance Item Success': props<{ clearance: Clearance }>(),
    'Activate Clearance Item Failure': props<{ error: HttpErrorResponse }>(),

    'Open Clearance Item Edit Dialog Ready': props<{
      clearanceId?: string,
      firstItem: boolean
    }>(),
    'Open Clearance Create Dialog': emptyProps(),
    'Open Clearance Item Edit Dialog': props<{ clearanceId: string }>(),
    'Open Clearance Item Details Dialog': props<{
      clearanceId: string,
      clearanceItemId: string
    }>(),
  }
});

