import { createReducer, on } from '@ngrx/store';
import { ClearancesActions } from './clearances.actions';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { Clearance } from '@api/product-provider/models';
import { HttpErrorResponse } from '@angular/common/http';
import { sortByCreatedDate } from './clearances.models';

export const CLEARANCES_FEATURE_KEY = 'clearances';

export interface State extends EntityState<Clearance> {
  loaded: boolean;
  error?: HttpErrorResponse | undefined;
}

export const clearanceAdapter = createEntityAdapter<Clearance>({
  sortComparer: sortByCreatedDate
});

export const initialState: State = clearanceAdapter.getInitialState({
  loaded: true,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(ClearancesActions.loadAllClearances, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ClearancesActions.loadAllClearancesSuccess, (state, { clearances }): State =>
    clearanceAdapter.setAll(clearances, {
      ...state,
      loaded: true,
    })
  ),
  on(ClearancesActions.loadAllClearancesFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ClearancesActions.loadClearance, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ClearancesActions.loadClearanceSuccess, (state, { clearance }): State =>
    clearanceAdapter.upsertOne(clearance, {
      ...state,
      loaded: true,
    })
  ),
  on(ClearancesActions.loadClearanceFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ClearancesActions.createClearance, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ClearancesActions.createClearanceSuccess, (state, { clearance }): State =>
    clearanceAdapter.addOne(clearance, {
      ...state,
      loaded: true,
    })
  ),
  on(ClearancesActions.createClearanceFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ClearancesActions.createClearanceItem, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ClearancesActions.createClearanceItemSuccess, (state, { clearance }): State =>
    clearanceAdapter.upsertOne(clearance, {
      ...state,
      loaded: true,
    })
  ),
  on(ClearancesActions.createClearanceItemFailure, (state, { error }): State => ({
    ...state,
    loaded: false,
    error,
  })),
  on(ClearancesActions.hideClearance, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ClearancesActions.hideClearanceSuccess, (state, { clearance }): State =>
    clearanceAdapter.upsertOne(clearance, {
      ...state,
      loaded: true,
    })
  ),
  on(ClearancesActions.hideClearanceFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ClearancesActions.unhideClearance, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ClearancesActions.unhideClearanceSuccess, (state, { clearance }): State =>
    clearanceAdapter.upsertOne(clearance, {
      ...state,
      loaded: true,
    })
  ),
  on(ClearancesActions.unhideClearanceFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ClearancesActions.activateClearanceItem, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ClearancesActions.activateClearanceItemSuccess, (state, { clearance }): State =>
    clearanceAdapter.upsertOne(clearance, {
      ...state,
      loaded: true,
    })
  ),
  on(ClearancesActions.activateClearanceItemFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
);

