import { ExtendedTagList, PostAppTag, AppTag, PostClearanceItem, InfoSourceType, PostInfoSourceContent, Clearance } from '@api/product-provider/models';
import { EditClearanceItemDialogOutput, InfoSourceContentDialog } from '@clearance-maintainer/components/edit-clearance-item-dialog/edit-clearance-item-dialog.models';
import { pluralSingularTags } from '../clearances-tags/clearances-tags.models';

export const MILLISECONDS_SECONDS_FACTOR = 1000;

export interface ClearanceItemIdObject {
  clearanceId: string,
  clearanceItemId: string
}

export function convertTagsToPostTags(tags: ExtendedTagList): PostAppTag[] {
  const postTags = Object
    .entries(tags)
    .filter(([_key, values]) => !!values && values?.length)
    .map(([key, values]) =>
      (values as AppTag[]).map(t => ({
        tag_group:  !pluralSingularTags.includes(key) ? key.slice(0, key.length - 1).toUpperCase() : key.toUpperCase(),
        tag_name: t.name
      } as PostAppTag))
    )
    .flat();

  return postTags;
}

export function convertSourceContentDialogToSourceContent(dialogContent: InfoSourceContentDialog): PostInfoSourceContent {
  return {
    ...dialogContent,
    label: dialogContent.label.EN?.length || dialogContent.label?.DE?.length
      ? {
        ...dialogContent.label,
        DE: dialogContent.label?.DE?.length ? dialogContent.label.DE : dialogContent.label.EN,
      }
      : undefined,
  };
}

export function convertDialogOutputToPostClearanceItem(dialogOutput: EditClearanceItemDialogOutput): PostClearanceItem {
  return {
    ...dialogOutput,
    description: {
      ...dialogOutput.description,
      DE: dialogOutput.description.EN
    },
    info_source_type: dialogOutput.info_source_content?.length
      ? InfoSourceType.PUBLIC_WEBSITE
      : InfoSourceType.UNKNOWN,
    info_source_content: dialogOutput.info_source_content?.length
      ? dialogOutput.info_source_content.map(convertSourceContentDialogToSourceContent)
      : undefined,
    product_release_at: dialogOutput.product_release_at / MILLISECONDS_SECONDS_FACTOR,
    tags: convertTagsToPostTags(dialogOutput.tags)
  };
}

export function sortByCreatedDate(a: Clearance, b: Clearance): number {
  return b.created_at - a.created_at;
}
