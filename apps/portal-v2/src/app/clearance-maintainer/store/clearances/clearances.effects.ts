import { Injectable } from '@angular/core';
import { ClearancesService } from '@api/product-provider/services';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { ClearancesActions } from './clearances.actions';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';
import { State } from './clearances.reducer';
import { exhaustMap, map } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import * as ClearancesTagsSelectors from '@clearance-maintainer/store/clearances-tags/clearances-tags.selectors';
import * as ClearancesSelectors from './clearances.selectors';
import { EditClearanceItemDialogComponent } from '@clearance-maintainer/components/edit-clearance-item-dialog/edit-clearance-item-dialog.component';
import { BIG_DIALOG_WIDTH } from '@edit-organization/models/edit-organization.models';
import { EditClearanceItemDialogData } from '@clearance-maintainer/components/edit-clearance-item-dialog/edit-clearance-item-dialog.models';
import { filterNullish } from '@shared/utility/rxjs-utility';
import { convertDialogOutputToPostClearanceItem } from './clearances.models';
import { ClearanceItemDetailsDialogComponent } from '@clearance-maintainer/components/clearance-item-details-dialog/clearance-item-details-dialog.component';

@Injectable()
export class ClearancesEffects {
  loadAllClearances$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClearancesActions.loadAllClearances),
      fetch({
        run: (
          _action: ReturnType<typeof ClearancesActions.loadAllClearances>,
          _state: State
        ) => {
          return this.clearancesService
            .clearancesGet()
            .pipe(
              map((clearances) =>
                ClearancesActions.loadAllClearancesSuccess({ clearances })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof ClearancesActions.loadAllClearances>,
          error
        ) => {
          return ClearancesActions.loadAllClearancesFailure({ error });
        },
      })
    );
  });

  loadClearance$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClearancesActions.loadClearance),
      fetch({
        run: (
          action: ReturnType<typeof ClearancesActions.loadClearance>,
          _state: State
        ) => {
          return this.clearancesService
            .clearancesClearanceIdGet({
              clearance_id: action.clearanceId,
            })
            .pipe(
              map((clearance) =>
                ClearancesActions.loadClearanceSuccess({ clearance })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof ClearancesActions.loadClearance>,
          error
        ) => {
          return ClearancesActions.loadClearanceFailure({ error });
        },
      })
    );
  });

  createClearance$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClearancesActions.createClearance),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof ClearancesActions.createClearance>,
          _state: State
        ) => {
          return this.clearancesService
            .clearancesPost({
              body: action.postClearance,
            })
            .pipe(
              map((clearance) =>
                ClearancesActions.createClearanceSuccess({ clearance })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof ClearancesActions.createClearance>,
          error
        ) => {
          return ClearancesActions.createClearanceFailure({ error });
        },
      })
    );
  });

  hideClearance$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClearancesActions.hideClearance),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof ClearancesActions.hideClearance>,
          _state: State
        ) => {
          return this.clearancesService
            .clearancesClearanceIdHidePut({
              clearance_id: action.clearanceId,
            })
            .pipe(
              map((clearance) =>
                ClearancesActions.hideClearanceSuccess({ clearance })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof ClearancesActions.hideClearance>,
          error
        ) => {
          return ClearancesActions.hideClearanceFailure({ error });
        },
      })
    );
  });

  unhideClearance$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClearancesActions.unhideClearance),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof ClearancesActions.unhideClearance>,
          _state: State
        ) => {
          return this.clearancesService
            .clearancesClearanceIdUnhidePut({
              clearance_id: action.clearanceId,
            })
            .pipe(
              map((clearance) =>
                ClearancesActions.unhideClearanceSuccess({ clearance })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof ClearancesActions.unhideClearance>,
          error
        ) => {
          return ClearancesActions.unhideClearanceFailure({ error });
        },
      })
    );
  });

  createClearanceItem$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClearancesActions.createClearanceItem),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof ClearancesActions.createClearanceItem>,
          _state: State
        ) => {
          return this.clearancesService
            .clearancesClearanceIdItemPost({
              clearance_id: action.clearanceId,
              body: action.postItem,
            })
            .pipe(
              map((clearance) =>
                ClearancesActions.createClearanceItemSuccess({ clearance })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof ClearancesActions.createClearanceItem>,
          error
        ) => {
          return ClearancesActions.createClearanceItemFailure({ error });
        },
      })
    );
  });

  activateClearanceItem$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClearancesActions.activateClearanceItem),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof ClearancesActions.activateClearanceItem>,
          _state: State
        ) => {
          return this.clearancesService
            .clearancesClearanceIdActivatePut({
              clearance_id: action.clearanceId,
              body: {
                active_item_id: action.clearanceItemId,
              },
            })
            .pipe(
              map((clearance) =>
                ClearancesActions.activateClearanceItemSuccess({ clearance })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof ClearancesActions.activateClearanceItem>,
          error
        ) => {
          return ClearancesActions.activateClearanceItemFailure({ error });
        },
      })
    );
  });

  openEditClearanceItemDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClearancesActions.openClearanceItemEditDialogReady),
      concatLatestFrom((action) => [
        this.store.select(ClearancesTagsSelectors.selectAllClearancesTags),
        this.store.select(
          ClearancesSelectors.selectActiveClearance(action.clearanceId)
        ),
      ]),
      exhaustMap(([action, tags, activeItem]) =>
        this.dialog
          .open(EditClearanceItemDialogComponent, {
            width: BIG_DIALOG_WIDTH,
            data: {
              clearanceTags: tags,
              clearanceItem: activeItem,
            } as EditClearanceItemDialogData,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map(convertDialogOutputToPostClearanceItem),
            map((postItem) =>
              action.firstItem
                ? ClearancesActions.createClearance({
                  postClearance: {
                    is_hidden: false,
                    clearance_item: postItem,
                  },
                })
                : ClearancesActions.createClearanceItem({
                  postItem,
                  clearanceId: action.clearanceId as string,
                })
            )
          )
      )
    );
  });

  prepareOpenEditClearanceItemDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClearancesActions.openClearanceItemEditDialog),
      map((action) => action.clearanceId),
      map((clearanceId) =>
        ClearancesActions.openClearanceItemEditDialogReady({
          clearanceId,
          firstItem: false,
        })
      )
    );
  });

  openCreateClearanceDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClearancesActions.openClearanceCreateDialog),
      map(() =>
        ClearancesActions.openClearanceItemEditDialogReady({ firstItem: true })
      )
    );
  });

  openClearanceItemDetailsDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClearancesActions.openClearanceItemDetailsDialog),
      concatLatestFrom((action) => [
        this.store
          .select(
            ClearancesSelectors.selectClearanceItem(
              action.clearanceId,
              action.clearanceItemId
            )
          )
          .pipe(filterNullish()),
        this.store.select(
          ClearancesSelectors.selectIsActiveClearanceItem(
            action.clearanceId,
            action.clearanceItemId
          )
        ),
      ]),
      exhaustMap(([action, clearanceItem, isActiveItem]) =>
        this.dialog
          .open(ClearanceItemDetailsDialogComponent, {
            width: BIG_DIALOG_WIDTH,
            data: { clearanceItem, isActiveItem },
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map(() => ClearancesActions.activateClearanceItem({ ...action }))
          )
      )
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly clearancesService: ClearancesService,
    private readonly dialog: MatDialog,
    private readonly store: Store
  ) {}
}
