import { createSelector } from '@ngrx/store';
import { selectClearancesRootState } from '../clearance-root.selectors';
import { CLEARANCES_TAGS_FEATURE_KEY } from './clearances-tags.reducer';

export const selectClearancesTagsState = createSelector(
  selectClearancesRootState,
  (state) => state[CLEARANCES_TAGS_FEATURE_KEY]
);

export const selectAllClearancesTags = createSelector(
  selectClearancesTagsState,
  (state) => state.tags
);

export const selectClearancesTagsLoaded = createSelector(
  selectClearancesTagsState,
  (state) => state.loaded
);

export const selectClearancesTagsError = createSelector(
  selectClearancesTagsState,
  (state) => state.error
);
