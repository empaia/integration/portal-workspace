import { ExtendedTagList } from '@api/public/models';

export type ClearancesTagList = ExtendedTagList;

export const pluralSingularTags = [
  'analysis'
];
