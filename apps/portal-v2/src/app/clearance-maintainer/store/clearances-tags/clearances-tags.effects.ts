import { Injectable } from '@angular/core';
import { GeneralService } from '@api/public/services';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ClearancesTagsActions } from './clearances-tags.actions';
import { fetch } from '@ngrx/router-store/data-persistence';
import { State } from './clearances-tags.reducer';
import { map } from 'rxjs';
import { ClearancesActions } from '../clearances/clearances.actions';

@Injectable()
export class ClearancesTagsEffects {
  loadTagsOnClearancesLoad$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClearancesActions.loadAllClearances),
      map(() => ClearancesTagsActions.loadAllClearancesTags())
    );
  });

  loadAllClearancesTags$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClearancesTagsActions.loadAllClearancesTags),
      fetch({
        run: (
          _action: ReturnType<
            typeof ClearancesTagsActions.loadAllClearancesTags
          >,
          _state: State
        ) => {
          return this.generalService
            .tagsGet()
            .pipe(
              map((tags) =>
                ClearancesTagsActions.loadAllClearancesTagsSuccess({ tags })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ClearancesTagsActions.loadAllClearancesTags
          >,
          error
        ) => {
          return ClearancesTagsActions.loadAllClearancesTagsFailure({ error });
        },
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly generalService: GeneralService
  ) {}
}
