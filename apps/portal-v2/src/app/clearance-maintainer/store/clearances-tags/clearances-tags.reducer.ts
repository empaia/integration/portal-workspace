import { createReducer, on } from '@ngrx/store';
import { ClearancesTagsActions } from './clearances-tags.actions';
import { ClearancesTagList } from './clearances-tags.models';
import { HttpErrorResponse } from '@angular/common/http';

export const CLEARANCES_TAGS_FEATURE_KEY = 'clearancesTags';

export interface State {
  tags: ClearancesTagList;
  loaded: boolean;
  error?: HttpErrorResponse | undefined;
}

export const initialState: State = {
  tags: {},
  loaded: true,
  error: undefined,
};

export const reducer = createReducer(
  initialState,
  on(ClearancesTagsActions.loadAllClearancesTags, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ClearancesTagsActions.loadAllClearancesTagsSuccess, (state, { tags }): State => ({
    ...state,
    tags,
    loaded: true,
  })),
  on(ClearancesTagsActions.loadAllClearancesTagsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
);

