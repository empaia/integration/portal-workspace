import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { ClearancesTagList } from './clearances-tags.models';
import { HttpErrorResponse } from '@angular/common/http';

export const ClearancesTagsActions = createActionGroup({
  source: 'Clearances Tags',
  events: {
    'Load All Clearances Tags': emptyProps(),
    'Load All Clearances Tags Success': props<{ tags: ClearancesTagList }>(),
    'Load All Clearances Tags Failure': props<{ error: HttpErrorResponse }>(),
  }
});
