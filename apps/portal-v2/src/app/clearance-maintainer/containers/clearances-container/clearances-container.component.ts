import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ClearancesActions, ClearancesSelectors } from '@clearance-maintainer/store/clearances';
import { Store } from '@ngrx/store';
import { ClearanceItemTableEntity } from '@shared/models/clearances.models';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-clearances-container',
  templateUrl: './clearances-container.component.html',
  styleUrls: ['./clearances-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClearancesContainerComponent {
  public clearances$: Observable<ClearanceItemTableEntity[]>;

  constructor(private store: Store) {
    this.store.dispatch(ClearancesActions.loadAllClearances());

    this.clearances$ = this.store.select(ClearancesSelectors.selectAllActiveClearances);
  }

  public onCreateClearance(): void {
    this.store.dispatch(ClearancesActions.openClearanceCreateDialog());
  }

  public onHideClearance(clearanceId: string): void {
    this.store.dispatch(ClearancesActions.hideClearance({ clearanceId }));
  }

  public onUnhideClearance(clearanceId: string): void {
    this.store.dispatch(ClearancesActions.unhideClearance({ clearanceId }));
  }

  public onEditClearanceItem(clearanceId: string): void {
    this.store.dispatch(ClearancesActions.openClearanceItemEditDialog({ clearanceId }));
  }
}
