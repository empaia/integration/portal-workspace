import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditOrganizationOverviewComponent } from './components/edit-organization-overview/edit-organization-overview.component';
import { EditOrganizationDetailsCardComponent } from './components/edit-organization-details-card/edit-organization-details-card.component';
import { EditOrganizationContactCardComponent } from './components/edit-organization-contact-card/edit-organization-contact-card.component';
import { EditOrganizationLogoCardComponent } from './components/edit-organization-logo-card/edit-organization-logo-card.component';
import { EditOrganizationLogoDialogComponent } from './components/edit-organization-logo-dialog/edit-organization-logo-dialog.component';
import { EditOrganizationDetailsDialogComponent } from './components/edit-organization-details-dialog/edit-organization-details-dialog.component';
import { EditOrganizationContactDialogComponent } from './components/edit-organization-contact-dialog/edit-organization-contact-dialog.component';
import { MaterialModule } from '@material/material.module';
import { SharedModule } from '@shared/shared.module';
import { EditOrganizationNameCardComponent } from './components/edit-organization-name-card/edit-organization-name-card.component';
import { EditOrganizationNameDialogComponent } from './components/edit-organization-name-dialog/edit-organization-name-dialog.component';
import { EditOrganizationStatusIndicatorComponent } from './components/edit-organization-status-indicator/edit-organization-status-indicator.component';

const COMPONENTS = [
  EditOrganizationOverviewComponent,
  EditOrganizationDetailsCardComponent,
  EditOrganizationContactCardComponent,
  EditOrganizationLogoCardComponent,
  EditOrganizationLogoDialogComponent,
  EditOrganizationDetailsDialogComponent,
  EditOrganizationContactDialogComponent,
  EditOrganizationNameCardComponent,
  EditOrganizationNameDialogComponent,
  EditOrganizationStatusIndicatorComponent,
];

@NgModule({
  declarations: [
    COMPONENTS,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
  ],
  exports: [
    COMPONENTS
  ],
})
export class EditOrganizationModule { }
