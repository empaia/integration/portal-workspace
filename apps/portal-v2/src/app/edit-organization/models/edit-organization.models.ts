import { ConfidentialOrganizationDetailsEntity, OrganizationCategory, PostConfidentialOrganizationDetailsEntity, PostInitialConfidentialOrganizationDetailsEntity } from '@api/aaa/models';
import { UnapprovedOrganizationsEntity } from '@user/store/unapproved-organizations';

export type EditOrganizationEntity = UnapprovedOrganizationsEntity;

export interface EditOrganizationId {
  id: string;
}

export type EditOrganizationDetailsEntity = ConfidentialOrganizationDetailsEntity;
export type PostOrganizationDetailsEntity = PostInitialConfidentialOrganizationDetailsEntity;
export type PostOrganizationDetailsWithoutCategories = PostConfidentialOrganizationDetailsEntity;

export const SMALL_DIALOG_WIDTH = '400px';
export const BIG_DIALOG_WIDTH = '800px';

export const ORGANIZATION_CATEGORY = Object.values(OrganizationCategory);

export function convertBooleanToCategoryArray(categories: boolean[]): OrganizationCategory[] {
  return ORGANIZATION_CATEGORY.filter((_c,i) => categories[i]);
}

export function convertCategoryToBooleanArray(categories: OrganizationCategory[] | undefined): boolean[] {
  return ORGANIZATION_CATEGORY.map(c => categories?.includes(c) ?? false);
}

export function checkOrganizationNameStatus(organization: EditOrganizationEntity | undefined): boolean {
  return !!organization?.organization_name;
}

export function checkOrganizationDetailsStatus(organization: EditOrganizationEntity | undefined): boolean {
  return !!organization?.details?.description_de
  && !!organization?.details?.description_en
  && organization?.details?.is_user_count_public !== undefined;
}

export function checkOrganizationContactDataStatus(organization: EditOrganizationEntity | undefined): boolean {
  return !!organization?.contact_data?.country_code
  && !!organization?.contact_data.place_name
  && !!organization?.contact_data.street_name
  && !!organization?.contact_data.street_number
  && !!organization?.contact_data.zip_code
  && !!organization?.contact_data.email_address
  && !!organization?.contact_data.phone_number
  && !!organization?.contact_data.website
  && organization?.contact_data.is_phone_number_public !== undefined
  && organization?.contact_data.is_fax_number_public !== undefined
  && organization?.contact_data.is_email_address_public !== undefined;
}
