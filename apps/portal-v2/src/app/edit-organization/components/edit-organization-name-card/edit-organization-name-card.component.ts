import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-edit-organization-name-card',
  templateUrl: './edit-organization-name-card.component.html',
  styleUrls: ['./edit-organization-name-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditOrganizationNameCardComponent {
  @Input() public organizationName: string | undefined;
  @Input() public isDone: boolean | undefined = false;
  @Input() public isEditable = true;

  @Output() public editOrganizationNameClicked = new EventEmitter<void>();
}
