import { ChangeDetectionStrategy, Component, inject, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MAX_FORM_FIELD_LENGTH } from '@shared/models/form.models';

@Component({
  selector: 'app-edit-organization-name-dialog',
  templateUrl: './edit-organization-name-dialog.component.html',
  styleUrls: ['./edit-organization-name-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditOrganizationNameDialogComponent implements OnInit {
  private fb = inject(FormBuilder);
  public nameForm = this.fb.nonNullable.control(
    '',
    [
      Validators.required,
      Validators.maxLength(MAX_FORM_FIELD_LENGTH)
    ]
  );

  public readonly MAX_FORM_LENGTH = MAX_FORM_FIELD_LENGTH;

  constructor(
    private dialogRef: MatDialogRef<EditOrganizationNameDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private name: string,
  ) {}

  ngOnInit(): void {
    this.setField();
  }

  public onSubmit(): void {
    this.dialogRef.close(this.nameForm.value);
  }

  private setField(): void {
    this.nameForm.patchValue(this.name);
  }
}
