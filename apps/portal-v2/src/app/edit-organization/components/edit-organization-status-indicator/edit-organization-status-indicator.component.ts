import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { OrganizationAccountState } from '@api/aaa/models';

@Component({
  selector: 'app-edit-organization-status-indicator',
  templateUrl: './edit-organization-status-indicator.component.html',
  styleUrls: ['./edit-organization-status-indicator.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditOrganizationStatusIndicatorComponent {
  @Input() public status: OrganizationAccountState | undefined;
}
