import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { EditOrganizationEntity, EditOrganizationId } from '@edit-organization/models/edit-organization.models';

@Component({
  selector: 'app-edit-organization-overview',
  templateUrl: './edit-organization-overview.component.html',
  styleUrls: ['./edit-organization-overview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditOrganizationOverviewComponent {
  @Input() public organization: EditOrganizationEntity | undefined;
  @Input() public isNameDone: boolean | undefined = false;
  @Input() public isDetailsDone: boolean | undefined = false;
  @Input() public isContactDone: boolean | undefined = false;
  @Input() public isEditable = true;

  @Output() public editOrganizationLogoClicked = new EventEmitter<EditOrganizationId>();
  @Output() public editOrganizationNameClicked = new EventEmitter<EditOrganizationId>();
  @Output() public editOrganizationDetailsClicked = new EventEmitter<EditOrganizationId>();
  @Output() public editOrganizationContactDataClicked = new EventEmitter<EditOrganizationId>();
}
