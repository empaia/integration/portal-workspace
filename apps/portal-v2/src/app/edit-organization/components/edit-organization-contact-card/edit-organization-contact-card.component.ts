import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { ConfidentialOrganizationContactDataEntity } from '@api/aaa/models';

@Component({
  selector: 'app-edit-organization-contact-card',
  templateUrl: './edit-organization-contact-card.component.html',
  styleUrls: ['./edit-organization-contact-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditOrganizationContactCardComponent {
  @Input() public organizationContactData: ConfidentialOrganizationContactDataEntity | undefined;
  @Input() public isDone: boolean | undefined = false;
  @Input() public isEditable = true;

  @Output() public editOrganizationContactDataClicked = new EventEmitter<void>();
}
