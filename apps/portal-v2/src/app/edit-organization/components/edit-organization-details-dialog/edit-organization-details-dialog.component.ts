/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { ChangeDetectionStrategy, Component, inject, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  convertBooleanToCategoryArray,
  convertCategoryToBooleanArray,
  EditOrganizationDetailsEntity,
  ORGANIZATION_CATEGORY,
  PostOrganizationDetailsEntity,
  PostOrganizationDetailsWithoutCategories
} from '@edit-organization/models/edit-organization.models';
import { MAX_FORM_DESCRIPTION_FIELD_LENGTH } from '@shared/models/form.models';

export interface EditOrganizationDetailsDialogData {
  details: EditOrganizationDetailsEntity,
  editableCategories: boolean
}

@Component({
  selector: 'app-edit-organization-details-dialog',
  templateUrl: './edit-organization-details-dialog.component.html',
  styleUrls: ['./edit-organization-details-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditOrganizationDetailsDialogComponent implements OnInit {
  private fb = inject(FormBuilder);
  public detailsForm = this.fb.nonNullable.group({
    description_de: ['',
      [
        Validators.maxLength(MAX_FORM_DESCRIPTION_FIELD_LENGTH)
      ]
    ],
    description_en: ['',
      [
        Validators.maxLength(MAX_FORM_DESCRIPTION_FIELD_LENGTH)
      ]
    ],
    categories: this.fb.nonNullable.array(
      ORGANIZATION_CATEGORY.map(() => false),
    ),
    is_user_count_public: false,
  });

  protected categoriesEditable = false;

  public readonly MAX_DESCRIPTION_LENGTH = MAX_FORM_DESCRIPTION_FIELD_LENGTH;
  public readonly ORGANIZATION_CATEGORY_SELECTION = ORGANIZATION_CATEGORY;

  constructor(
    private dialogRef: MatDialogRef<EditOrganizationDetailsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: EditOrganizationDetailsDialogData
  ) {
    this.categoriesEditable = data.editableCategories;
  }

  public ngOnInit(): void {
    this.setFields();
    if (!this.categoriesEditable) {
      this.disableCategories();
    }
  }

  public get descriptionGerman(): FormControl<string> {
    return this.detailsForm.controls.description_de;
  }

  public get descriptionEnglish(): FormControl<string> {
    return this.detailsForm.controls.description_en;
  }

  public get categories(): FormArray<FormControl<boolean>> {
    return this.detailsForm.controls.categories;
  }

  public get isUserCountPublic(): FormControl<boolean> {
    return this.detailsForm.controls.is_user_count_public;
  }

  public onSubmit(): void {
    const details = this.categoriesEditable
      ? this.createDetailsWithCategories()
      : this.createBaseDetails();
    this.dialogRef.close(details);
  }

  private setFields(): void {
    this.detailsForm.patchValue({
      description_de: this.data.details.description_de,
      description_en: this.data.details.description_en,
      categories: convertCategoryToBooleanArray(this.data?.details.categories),
      is_user_count_public: this.data.details.is_user_count_public
    });
  }

  private disableCategories(): void {
    this.categories.disable();
  }

  private createDetailsWithCategories(): PostOrganizationDetailsEntity {
    return {
      ...this.createBaseDetails(),
      categories: convertBooleanToCategoryArray(this.categories.value)
    };
  }

  private createBaseDetails(): PostOrganizationDetailsWithoutCategories {
    return {
      description_de: this.descriptionGerman.value.trim(),
      description_en: this.descriptionEnglish.value.trim(),
      is_user_count_public: this.isUserCountPublic.value
    };
  }
}
