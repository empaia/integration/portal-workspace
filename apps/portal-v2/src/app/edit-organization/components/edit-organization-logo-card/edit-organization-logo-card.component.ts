import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { blankOrgImage} from 'src/app/app.models';

@Component({
  selector: 'app-edit-organization-logo-card',
  templateUrl: './edit-organization-logo-card.component.html',
  styleUrls: ['./edit-organization-logo-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditOrganizationLogoCardComponent {
  @Input() public organizationLogo: string | undefined;
  @Input() public isEditable = true;

  @Output() public editOrganizationLogoClicked = new EventEmitter<void>();

  public readonly fallbackImage = blankOrgImage;
}
