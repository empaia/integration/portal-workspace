import { ChangeDetectionStrategy, Component, inject, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfidentialOrganizationContactDataEntity, CountryEntityV2 } from '@api/aaa/models';
import { MAX_FORM_FIELD_LENGTH, PHONE_NUMBER_PATTERN, ZIP_CODE_PATTERN } from '@shared/models/form.models';
import { CustomFormValidatorsService } from '@shared/services/custom-form-validators/custom-form-validators.service';

export interface OrganizationContactCountriesEntity {
  contactData: ConfidentialOrganizationContactDataEntity;
  countries: CountryEntityV2[];
}

@Component({
  selector: 'app-edit-organization-contact-dialog',
  templateUrl: './edit-organization-contact-dialog.component.html',
  styleUrls: ['./edit-organization-contact-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditOrganizationContactDialogComponent implements OnInit {
  private fb = inject(FormBuilder);

  public contactForm = this.fb.nonNullable.group({
    country_code: ['',
      [
        Validators.required,
      ]
    ],
    place_name: ['',
      [
        Validators.required,
        Validators.maxLength(MAX_FORM_FIELD_LENGTH),
      ]
    ],
    street_name: ['',
      [
        Validators.required,
        Validators.maxLength(MAX_FORM_FIELD_LENGTH),
      ]
    ],
    street_number: ['',
      [
        Validators.required,
        Validators.maxLength(MAX_FORM_FIELD_LENGTH),
      ]
    ],
    zip_code: ['',
      [
        Validators.required,
        Validators.maxLength(MAX_FORM_FIELD_LENGTH),
        Validators.pattern(ZIP_CODE_PATTERN),
      ]
    ],
    email_address: ['',
      [
        Validators.required,
        Validators.maxLength(MAX_FORM_FIELD_LENGTH),
        CustomFormValidatorsService.email,
      ]
    ],
    phone_number: ['',
      [
        Validators.required,
        Validators.maxLength(MAX_FORM_FIELD_LENGTH),
        Validators.pattern(PHONE_NUMBER_PATTERN)
      ]
    ],
    department: ['',
      [
        Validators.maxLength(MAX_FORM_FIELD_LENGTH),
      ]
    ],
    fax_number: ['',
      [
        Validators.maxLength(MAX_FORM_FIELD_LENGTH),
        Validators.pattern(PHONE_NUMBER_PATTERN),
      ]
    ],
    website: ['',
      [
        Validators.required,
        CustomFormValidatorsService.url,
        Validators.maxLength(MAX_FORM_FIELD_LENGTH),
      ]
    ],
    is_phone_number_public: false,
    is_fax_number_public: false,
    is_email_address_public: false
  });

  public readonly MAX_FORM_LENGTH = MAX_FORM_FIELD_LENGTH;

  constructor(
    private dialogRef: MatDialogRef<EditOrganizationContactDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: OrganizationContactCountriesEntity,
  ) { }

  public ngOnInit(): void {
    this.setFields();
  }

  public get countryCode(): FormControl<string> {
    return this.contactForm.controls.country_code;
  }

  public get placeName(): FormControl<string> {
    return this.contactForm.controls.place_name;
  }

  public get streetName(): FormControl<string> {
    return this.contactForm.controls.street_name;
  }

  public get streetNumber(): FormControl<string> {
    return this.contactForm.controls.street_number;
  }

  public get zipCode(): FormControl<string> {
    return this.contactForm.controls.zip_code;
  }

  public get email(): FormControl<string> {
    return this.contactForm.controls.email_address;
  }

  public get phoneNumber(): FormControl<string> {
    return this.contactForm.controls.phone_number;
  }

  public get department(): FormControl<string> {
    return this.contactForm.controls.department;
  }

  public get faxNumber(): FormControl<string> {
    return this.contactForm.controls.fax_number;
  }

  public get website(): FormControl<string> {
    return this.contactForm.controls.website;
  }

  public get isPhoneNumberPublic(): FormControl<boolean> {
    return this.contactForm.controls.is_phone_number_public;
  }

  public get isFaxNumberPublic(): FormControl<boolean> {
    return this.contactForm.controls.is_fax_number_public;
  }

  public get isEmailPublic(): FormControl<boolean> {
    return this.contactForm.controls.is_email_address_public;
  }

  public onSubmit(): void {
    const contactData = { ...this.contactForm.value } as ConfidentialOrganizationContactDataEntity;
    this.dialogRef.close(contactData);
  }

  private setFields(): void {
    this.contactForm.patchValue({ ...this.data.contactData });
  }
}
