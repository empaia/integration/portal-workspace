import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { blankOrgImage } from 'src/app/app.models';

@Component({
  selector: 'app-edit-organization-logo-dialog',
  templateUrl: './edit-organization-logo-dialog.component.html',
  styleUrls: ['./edit-organization-logo-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditOrganizationLogoDialogComponent {
  public readonly fallbackImage = blankOrgImage;

  private image: Blob | null = null;

  constructor(
    private dialogRef: MatDialogRef<EditOrganizationLogoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public logoUrl: string | undefined,
  ) {}

  public onImageChanged(event: Blob | null): void {
    this.image = event;
  }

  public saveImage(): void {
    this.dialogRef.close(this.image);
  }
}
