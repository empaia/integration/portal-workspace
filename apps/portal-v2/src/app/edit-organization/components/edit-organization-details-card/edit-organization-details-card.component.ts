import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { ConfidentialOrganizationDetailsEntity } from '@api/aaa/models';

@Component({
  selector: 'app-edit-organization-details-card',
  templateUrl: './edit-organization-details-card.component.html',
  styleUrls: ['./edit-organization-details-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditOrganizationDetailsCardComponent {
  @Input() public organizationDetails: ConfidentialOrganizationDetailsEntity | undefined;
  @Input() public isDone: boolean | undefined = false;
  @Input() public isEditable = true;

  @Output() public editOrganizationDetailsClicked = new EventEmitter<void>();
}
