import { Injectable } from '@angular/core';
import { LanguageCode } from '@api/aaa/models';
import { getBrowserLang, LangDefinition, TranslocoService } from '@ngneat/transloco';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  constructor(
    private langService: TranslocoService,
  ) {
    // check localstorage for lang settings if there are none 
    // check users browser language 
    // if browser is set to 'de' use 'de' else fallback to 'en'
    const lsLang = localStorage.getItem('language');
    const browserLang = getBrowserLang();
    if (lsLang) {
      this.setActiveLanguageId(lsLang);
    } else if (browserLang === 'de') {
      this.setActiveLanguageId('de');
    } else {
      this.setActiveLanguageId('en');
    }
  }

  getLanguage$(): Observable<string> {
    return this.langService.langChanges$.pipe(map(s => s));
  }
  getLanguage(): string {
    return this.langService.getActiveLang();
  }

  setActiveLanguage(lang: LangDefinition) {
    this.setActiveLanguageId(lang.id);
  }

  setActiveLanguageId(langId: string) {
    this.langService.setActiveLang(langId);
    this.setLocalStorageItem(langId);
  }

  setLocalStorageItem(langId: string) {
    localStorage.setItem('language', langId);
  }

  // setRouterParam(langId: string) {
  //   this.router.navigate(
  //     [],
  //     {
  //       relativeTo: this.route,
  //       queryParams: {
  //         lang: langId
  //       },
  //       queryParamsHandling: 'merge',
  //     });
  // }

  getSwitchToLang(): LangDefinition | undefined {
    const l = this.langService.getActiveLang();
    switch (l) {
      case 'de':
        return (this.langService.getAvailableLangs() as LangDefinition[]).find((l: LangDefinition) => l.id === 'en');
      case 'en':
        return (this.langService.getAvailableLangs() as LangDefinition[]).find((l: LangDefinition) => l.id === 'de');
      default:
        return undefined;
    }
  }


  langDefinitionToCode(langDef: LangDefinition): LanguageCode {
    switch (langDef.id) {
      case 'de': return LanguageCode.DE;
      case 'en': return LanguageCode.EN;
      default: throw new Error('Unknown language set');
    }
  }

  switchLang() {
    const l = this.getSwitchToLang()?.id;
    if (l) {
      this.langService.setActiveLang(l);
    }
  }
}
