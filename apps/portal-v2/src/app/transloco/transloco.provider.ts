// import { Injectable } from '@angular/core';
// import { TranslocoService } from '@ngneat/transloco';
// import { I18nService } from '../services/i18n.service';

// @Injectable()
// export class TranslocoProvider {
//   constructor(
//     private translocoService: TranslocoService,
//     private i18n: I18nService
//   ) {}

//   load() {
//     const persistedLang = this.i18n.getUserLang();
//     if (persistedLang) {
//       this.translocoService.setActiveLang(persistedLang);
//       return this.translocoService.load(persistedLang).toPromise();
//     }
//   }
// }

// export function translocoProviderFactory(provider: TranslocoProvider) {
//   return () => provider.load();
// }
