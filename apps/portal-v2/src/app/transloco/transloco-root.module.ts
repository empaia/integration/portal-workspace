import { Injectable, NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {
  TRANSLOCO_LOADER,
  Translation,
  TranslocoLoader,
  TRANSLOCO_CONFIG,
  translocoConfig,
  TranslocoModule,

} from '@ngneat/transloco';
import { TranslocoMessageFormatModule } from '@ngneat/transloco-messageformat';
import { TranslocoLocaleModule } from '@ngneat/transloco-locale';

import { environment } from '@env/environment';
import { availableLanguages } from '../app.models';



@Injectable({ providedIn: 'root' })
export class TranslocoHttpLoader implements TranslocoLoader {
  constructor(private http: HttpClient) { }

  getTranslation(lang: string) {
    return this.http.get<Translation>(`./assets/i18n/${lang}.json`);
  }
}


@NgModule({
  imports: [
    // TranslocoPreloadLangsModule.forRoot(['lazy-page/es']),
    TranslocoMessageFormatModule.forRoot(),
    TranslocoLocaleModule.forRoot({
      langToLocaleMapping: {
        en: 'en-US',
        de: 'de-DE',
      },
    }),
  ],
  exports: [
    TranslocoModule,
    TranslocoLocaleModule
  ],
  providers: [
    {
      provide: TRANSLOCO_CONFIG,
      useValue: translocoConfig({
        prodMode: environment.production,
        availableLangs: availableLanguages,
        reRenderOnLangChange: true,
        fallbackLang: 'de',
        defaultLang: 'en',
        // missingHandler: {
        //   useFallbackTranslation: false,
        // },
        // interpolation: ['<<<', '>>>']
      }),
    },
    { provide: TRANSLOCO_LOADER, useClass: TranslocoHttpLoader },
  ],
})
export class TranslocoRootModule { }