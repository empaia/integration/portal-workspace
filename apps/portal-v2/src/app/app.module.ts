import { NgModule } from '@angular/core';
import {
  BrowserModule,
  HammerModule,
} from '@angular/platform-browser';


import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { routerReducer, StoreRouterConnectingModule } from '@ngrx/router-store';

import { MaterialModule } from '@material/material.module';
import { CoreModule } from '@core/core.module';
import { EventsModule } from './events/events.module';

import { ApiModule as PublicApiModule } from '@api/public/api.module';
import { ApiModule as AaaApiModule } from '@api/aaa/api.module';
import { ApiModule as EventsApiModule } from '@api/eves/api.module';
import { ApiModule as ProductProviderApiModule } from '@api/product-provider/api.module';
import { ApiModule as ModeratorApiModule } from '@api/moderator/api.module';

import { TranslocoRootModule } from '@transloco/transloco-root.module';
import { AppRoutingModule } from '@router/app.routing.module';
import { environment } from '@env/environment';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { evesApi, mpsApi } from './app.models';
import { RouterEffects } from '@router/router.effects';
import { PushPipe } from '@ngrx/component';
import { HTTP_INTERCEPTOR_PROVIDERS } from './interceptor';
import { UserRootStoreModule } from '@user/store/user-root-store.module';
import { HAMMER_CONFIG_PROVIDER } from '@shared/providers/hammer-config';
import { MatIconRegistry } from '@angular/material/icon';
import { MatNativeDateModule } from '@angular/material/core';
import { MarkdownModule } from 'ngx-markdown';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HammerModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,

    PushPipe,

    AuthModule.forRoot(),
    /* ngrx block start */
    StoreModule.forRoot(
      {
        router: routerReducer,
      },
      {
        metaReducers: !environment.production ? [] : [],
        runtimeChecks: {
          strictActionImmutability: true,
          strictStateImmutability: true,
          // strictStateSerializability: true,
          // strictActionSerializability: true,
          strictActionWithinNgZone: true,
          strictActionTypeUniqueness: true,
        },
      },
    ),
    EffectsModule.forRoot([
      RouterEffects,
    ]),
    StoreRouterConnectingModule.forRoot(),
    !environment.production ? StoreDevtoolsModule.instrument({connectInZone: true}) : [],
    /* ngrx block end */

    AppRoutingModule,
    MaterialModule,
    TranslocoRootModule,

    UserRootStoreModule,
    CoreModule,
    EventsModule,

    MatNativeDateModule,

    PublicApiModule.forRoot({
      rootUrl: `${environment.mpsApiUrl}${mpsApi.version}${mpsApi.public}`,
    }),

    AaaApiModule.forRoot({
      rootUrl: environment.aaaApiUrl,
    }),

    EventsApiModule.forRoot({
      rootUrl: `${environment.evesApiUrl}${evesApi.version}`,
    }),
    ProductProviderApiModule.forRoot({
      rootUrl: `${environment.mpsApiUrl}${mpsApi.version}${mpsApi.productProvider}`
    }),
    ModeratorApiModule.forRoot({
      rootUrl: `${environment.mpsApiUrl}${mpsApi.version}${mpsApi.moderator}`
    }),
    MarkdownModule.forRoot(),
  ],
  providers: [
    HAMMER_CONFIG_PROVIDER,
    HTTP_INTERCEPTOR_PROVIDERS,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  // set default class for <mat-icon> to 'material-symbols-outlined'
  // note: 'material-icons' (old) is not the same as 'material-symbols' (new) !
  constructor(iconRegistry: MatIconRegistry) {
    iconRegistry.setDefaultFontSetClass('material-symbols-outlined');
  }
}
