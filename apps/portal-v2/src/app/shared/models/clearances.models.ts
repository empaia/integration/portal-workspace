import { PublicAggregatedClearance } from '@api/public/models';
import { OrganizationEntity } from '@public/store/organizations/organizations.models';

export interface ClearanceItemTableEntity extends PublicAggregatedClearance {
  organization: OrganizationEntity | undefined;
  isHidden?: boolean;
  clearanceId?: string;
}
