/* eslint-disable no-useless-escape */
const MAX_FORM_FIELD_LENGTH = 255;
const MAX_FORM_DESCRIPTION_FIELD_LENGTH = 750;
const NO_START_END_WHITE_SPACE = /^[^\s]+(\s+[^\s]+)*$/;
const ZIP_CODE_PATTERN = /^$|^[a-zA-Z0-9 \-]+$/;
const PHONE_NUMBER_PATTERN = /^$|^[0-9\- +()]+$/;


export {
  MAX_FORM_FIELD_LENGTH,
  MAX_FORM_DESCRIPTION_FIELD_LENGTH,
  NO_START_END_WHITE_SPACE,
  ZIP_CODE_PATTERN,
  PHONE_NUMBER_PATTERN,
};
