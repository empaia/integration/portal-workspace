import { Pipe, PipeTransform } from '@angular/core';
import { LanguageCode, TextTranslationEntity } from '@api/aaa/models';
import { TextTranslation } from '@api/public/models';
import { TranslocoService } from '@ngneat/transloco';

const TRANSLATION_MISSING = 'missing_translation';
const FALLBACK_LANGUAGE = LanguageCode.EN;

@Pipe({
  name: 'translate',
  pure: false
})
export class TranslatePipe implements PipeTransform {

  // al$ = from(this.translocoService.getActiveLang());

  constructor(
    private translocoService: TranslocoService,
    // private readonly changeDetectorRef: ChangeDetectorRef,
  ) {
    // to make this pipe pure and trigger cd
    // this.al$.subscribe(l => this.changeDetectorRef.markForCheck());
  }

  transform(value: Array<TextTranslation | TextTranslationEntity> | null | undefined, fallbackTranslation?: string): string {
    const activeLang = this.translocoService.getActiveLang();
    let translation = this.getText(activeLang, value);
    if (translation === TRANSLATION_MISSING && fallbackTranslation) {
      translation = fallbackTranslation;
    }
    return translation;
  }

  getText(activeLang: string, translations: Array<TextTranslation | TextTranslationEntity> | null | undefined): string {
    if (translations) {
      for (const translation of translations) {
        if ((this.isTextTranslationEntity(translation) ? translation.language_code : translation.lang)?.toLowerCase() === activeLang.toLowerCase() && translation.text) {
          return translation.text;
        }
      }
      // use english text as a fallback if the current selected language has no text or wasn't stored at all
      const fallback = translations.find(t => (this.isTextTranslationEntity(t) ? t.language_code : t.lang)?.toLowerCase() === FALLBACK_LANGUAGE.toLowerCase() && t.text);
      if (fallback && fallback.text) {
        return fallback.text;
      }
    }
    return TRANSLATION_MISSING;
  }

  private isTextTranslationEntity(object: object): object is TextTranslationEntity {
    return 'language_code' in object;
  }
}
