import { TranslatePipe } from './translate.pipe';
import { createPipeFactory, SpectatorPipe } from '@ngneat/spectator';
import { TranslocoModule } from '@ngneat/transloco';

describe('TranslatePipe', () => {
  let spectator: SpectatorPipe<TranslatePipe>;
  const createPipe = createPipeFactory({
    pipe: TranslatePipe,
    imports: [
      TranslocoModule
    ],
  });

  beforeEach(() => spectator = createPipe());

  it('create an instance', () => {
    expect(spectator.element).toBeTruthy();
  });
});
