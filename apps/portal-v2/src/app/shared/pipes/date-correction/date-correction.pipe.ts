import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateCorrection'
})
export class DateCorrectionPipe implements PipeTransform {

  transform(date: number | undefined): number {
    return date ? date * 1000 : 0;
  }

}
