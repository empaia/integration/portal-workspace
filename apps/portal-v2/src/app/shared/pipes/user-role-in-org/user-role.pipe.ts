import { Pipe, PipeTransform } from '@angular/core';
import { OrganizationUserRoleV2 } from '@api/aaa/models';
import { AuthSelectors } from '@auth/store/auth';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'userRole',
  pure: true
})
export class UserRolePipe implements PipeTransform {
  constructor(
    private store: Store,
  ) {
  }

  transform(value: string): Observable<OrganizationUserRoleV2[] | undefined> {
    const orgs$ = this.store.select(AuthSelectors.selectAuthProfile).pipe(
      // tap(o => console.log('selectOrganizationsArray', o)),
      // eslint-disable-next-line @ngrx/avoid-mapping-selectors
      map(p => p?.organizations[value]?.roles ?? []),
    );

    return orgs$;
  }
}
