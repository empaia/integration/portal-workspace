import { UserRolePipe } from './user-role.pipe';
import { createPipeFactory, SpectatorPipe } from '@ngneat/spectator';
import { TranslocoModule } from '@ngneat/transloco';

describe('TranslatePipe', () => {
  let spectator: SpectatorPipe<UserRolePipe>;
  const createPipe = createPipeFactory({
    pipe: UserRolePipe,
    imports: [
      TranslocoModule
    ],
  });

  beforeEach(() => spectator = createPipe());

  it('create an instance', () => {
    expect(spectator.element).toBeTruthy();
  });
});
