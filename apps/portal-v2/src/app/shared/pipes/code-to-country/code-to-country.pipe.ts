/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { Pipe, PipeTransform } from '@angular/core';
import { TextTranslationEntity } from '@api/aaa/models';
import { TranslocoService } from '@ngneat/transloco';
import { Store } from '@ngrx/store';
import { CountriesSelectors } from '@shared/store/countries';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

@Pipe({
  name: 'codeToCountry'
})
export class CodeToCountryPipe implements PipeTransform {

  constructor(
    private store: Store,
    private translocoService: TranslocoService,
  ) {}

  transform(value: string | undefined): Observable<string> {
    return this.translocoService.langChanges$.pipe(
      switchMap(activeLang =>
        this.store.select(CountriesSelectors.selectAllCountries).pipe(
          map(countries => {
            const translations = countries.find(c => c.country_code === value)?.translated_names;
            return this.getText(activeLang, translations);
          })
        )
      )
    );
  }

  getText(activeLang: string, translations: TextTranslationEntity[] | undefined): string {
    if (translations) {
      for (const translation of translations) {
        if (translation!.language_code!.toLowerCase() === activeLang.toLowerCase()) {
          return translation.text!;
        }
      }
    }
    return 'missing_translation';
  }

}
