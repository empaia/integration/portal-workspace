import { inject, Pipe, PipeTransform } from '@angular/core';
import { Language } from '@api/product-provider/models';
import { DatePrecision } from '@api/public/models';
import { TranslocoService } from '@ngneat/transloco';
import { TranslocoLocaleService } from '@ngneat/transloco-locale';

@Pipe({
  name: 'productReleaseAt',
  pure: false
})
export class ProductReleaseAtPipe implements PipeTransform {

  private locale = inject(TranslocoLocaleService);
  private transloco = inject(TranslocoService);

  transform(value: number | undefined, precision: DatePrecision | undefined, lang?: Language): string {
    if (value !== undefined && value >= 0) {
      const ms = value * 1000;
      switch (precision) {
        case DatePrecision.DATE: {
          return this.locale.localizeDate(ms, lang ? lang.toLowerCase() : this.locale.getLocale(), { day: '2-digit', month: '2-digit', year: 'numeric' });
        }
        case DatePrecision.QUARTER: {
          return this.getQuarter(ms);
        }
        case DatePrecision.MONTH_YEAR: {
          return this.getMonthYear(ms, lang);
        }
        case DatePrecision.YEAR: {
          return this.getYear(ms);
        }
        case DatePrecision.TBA: {
          return this.transloco.translate('clearances.release_tba');
        }
        case DatePrecision.UNKNOWN: {
          return this.transloco.translate('clearances.release_unknown');
        }

        default:
          throw new Error('Unknown DatePrecision');
          return '';
      }
    }
    return 'ERROR: release date undefined';
  }

  getQuarter(ms: number): string {
    const date = new Date(ms);
    const quarter = Math.floor((date.getMonth() + 3) / 3);
    const qs = `Q${quarter}/${date.getFullYear()}`;
    return qs;
  }

  getMonthYear(ms: number, lang?: Language): string {
    const date = new Date(ms);

    const options: Intl.DateTimeFormatOptions = {
      year: "numeric",
      month: "2-digit",
    };
    const str = date.toLocaleDateString(lang ? lang.toLowerCase() : this.locale.getLocale(), options);
    return str;
  }

  getYear(ms: number): string {
    const date = new Date(ms);

    const options: Intl.DateTimeFormatOptions = {
      year: "numeric",
    };
    const str = date.toLocaleDateString(this.locale.getLocale(), options);
    return str;
  }
}
