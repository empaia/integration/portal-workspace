/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { Pipe, PipeTransform } from '@angular/core';
import { TextTranslationEntity } from '@api/aaa/models';
import { TranslocoService } from '@ngneat/transloco';

@Pipe({
  name: 'country'
})
export class CountryPipe implements PipeTransform {

  constructor(private translocoService: TranslocoService) {}

  transform(value: TextTranslationEntity[] | undefined): string {
    const activeLang = this.translocoService.getActiveLang();
    return this.getText(activeLang, value);
  }

  getText(activeLang: string, translations: TextTranslationEntity[] | undefined): string {
    if (translations) {
      for (const translation of translations) {
        if (translation!.language_code!.toLowerCase() === activeLang.toLowerCase()) {
          return translation.text!;
        }
      }
    }
    return 'missing_translation';
  }

}
