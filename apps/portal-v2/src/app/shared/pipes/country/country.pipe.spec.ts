import { createPipeFactory, SpectatorPipe } from '@ngneat/spectator';
import { TranslocoModule } from '@ngneat/transloco';
import { CountryPipe } from './country.pipe';

describe('CountryPipe', () => {
  let spectator: SpectatorPipe<CountryPipe>;
  const createPipe = createPipeFactory({
    pipe: CountryPipe,
    imports: [
      TranslocoModule
    ]
  });

  beforeEach(() => spectator = createPipe());

  it('create an instance', () => {
    expect(spectator.element).toBeTruthy();
  });
});
