import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'trim'
})
export class TrimPipe implements PipeTransform {

  transform(value: string, filterList?: string[]): string {
    let tmpString = value;

    if (filterList) {
      for (const word of filterList) {
        if (tmpString.toLowerCase().includes(word.toLowerCase())) {
          const index = tmpString.toLowerCase().lastIndexOf(word.toLowerCase());
          tmpString = tmpString.slice(index + word.length);
        }
      }
    }

    return tmpString.trim();
  }

}
