import { Pipe, PipeTransform } from '@angular/core';
import { PublicAppView } from '@api/public/models';
import { getAppDetails, PortalAppOrganizationEntity } from '@public/store/portal-apps/portal-apps.models';

@Pipe({
  name: 'publicAppDetails'
})
export class PublicAppDetailsPipe implements PipeTransform {

  transform(value: PortalAppOrganizationEntity): PublicAppView {
    return getAppDetails(value);
  }

}
