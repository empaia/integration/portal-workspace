import { inject, Injectable } from '@angular/core';
import { Route, Router, UrlSegment } from '@angular/router';
import { concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { AppRoutes } from '@router/router.constants';
import { UserOrganizationsSelectors } from '@user/store/user-organizations';
import { Observable } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OrgSelectedGuard 
{
  private store = inject(Store);
  private router = inject(Router);

  canMatch(_route: Route, _segments: UrlSegment[]): Observable<boolean> {
    return this.resolve();
  }

  private resolve(): Observable<boolean> {
    const a = this.store.select(UserOrganizationsSelectors.selectIsOrgSelectionFinished).pipe(
      filter(isDone => isDone),
      concatLatestFrom((_) =>
        this.store.select(
          UserOrganizationsSelectors.selectUserOrganizationsNumberTotal
        )
      ),
      // tap(x => console.log('this guard said ' + x)),
      map(([isOrgSelected, numOrgs]) => {
        if (isOrgSelected && numOrgs === 0) {
          this.router.navigate([AppRoutes.NEED_ORG_MEMBERSHIP]);
          return false;
        }
        return true;
      }),
      // tap(x => console.log('resolved to ' + x)),
      take(1)
    );
    return a;
  }
}
