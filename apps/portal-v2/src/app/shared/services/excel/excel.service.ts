import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { AppTag, DatePrecision, InfoProvider, InfoSourceContent, InfoSourceType, Language } from '@api/public/models';
import { ClearanceItemTableEntity } from '@shared/models/clearances.models';
import { DateCorrectionPipe } from '@shared/pipes/date-correction/date-correction.pipe';
import { ProductReleaseAtPipe } from '@shared/pipes/product-release-at/product-release-at.pipe';
import { Alignment, Font, Workbook } from 'exceljs';
import { saveAs } from 'file-saver';

@Injectable({
  providedIn: 'root',
})
export class ExcelService {
  private readonly DEFAULT_HEADLINE_FONT: Partial<Font> = { size: 16, bold: true, name: 'Opensans, sans-serif' };
  private readonly DEFAULT_FONT: Partial<Font> = { size: 12, name: 'Opensans, sans-serif' };
  private readonly DEFAULT_ALIGNMENT: Partial<Alignment> = { wrapText: true };

  constructor(
    private productReleasePipe: ProductReleaseAtPipe,
    private dateCorrection: DateCorrectionPipe,
    private datePipe: DatePipe,
  ) {}

  public generateMarketSurveySheet(
    data: ClearanceItemTableEntity[],
    headers: string[],
    filename: string,
    headlineFont?: Partial<Font>,
    font?: Partial<Font>,
    alignment?: Partial<Alignment>
  ): void {
    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet('sheet', { properties: { defaultColWidth: 50 } });

    // add headers
    worksheet.addRow(headers);
    // add data
    data.forEach(entry => {
      const row: string[] = [];
      // headline order:
      // org name, product name, product release date, description,
      // references, last updated, provided by,
      // procedure, indication, analysis, clearance,
      // image type, scanner, tissue, stain
      row.push(entry.organization?.organization_name ?? '');
      row.push(entry.product_name);
      switch (entry.product_release_at_precision) {
        case DatePrecision.TBA:
          row.push('TBA');
          break;
        case DatePrecision.UNKNOWN:
          row.push('unknown');
          break;
        default:
          row.push(this.productReleasePipe.transform(entry.product_release_at, entry.product_release_at_precision, Language.DE));
      }
      row.push(entry.description?.filter(d => d.lang === Language.EN)[0].text ?? 'Not available');
      row.push(entry.info_source_type === InfoSourceType.PUBLIC_WEBSITE ? this.infoSourceContentArrayToString(entry.info_source_content) : '');
      row.push(this.datePipe.transform(this.dateCorrection.transform(entry.info_updated_at), 'dd.MM.yyyy') ?? '');
      row.push(entry.info_provider === InfoProvider.ORGANIZATION ? entry.organization?.organization_name ?? entry.info_provider : entry.info_provider);
      row.push(this.tagsToString(entry.tags?.procedures));
      row.push(this.tagsToString(entry.tags?.indications));
      row.push(this.tagsToString(entry.tags?.analysis));
      row.push(this.tagsToString(entry.tags?.clearances));
      row.push(this.tagsToString(entry.tags?.image_types));
      row.push(this.tagsToString(entry.tags?.scanners));
      row.push(this.tagsToString(entry.tags?.tissues));
      row.push(this.tagsToString(entry.tags?.stains));
      worksheet.addRow(row);
    });

    worksheet.eachRow(row => {
      row.alignment = alignment ?? this.DEFAULT_ALIGNMENT;
      row.font = font ?? this.DEFAULT_FONT;
    });
    worksheet.getRow(1).font = headlineFont ?? this.DEFAULT_HEADLINE_FONT;

    // save the workbook
    workbook.xlsx.writeBuffer().then(buffer => {
      const blob = new Blob([buffer], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
      saveAs(blob, `${filename}.xlsx`);
    });
  }

  private tagsToString(tags?: AppTag[]): string {
    return tags?.map(tag => tag.name).join(', ') ?? '';
  }

  private infoSourceContentToString(content: InfoSourceContent): string {
    const label = content.label ? `${content.label.filter(l => l.lang === Language.EN)[0].text}: ` : '';
    return label + content.reference_url;
  }

  private infoSourceContentArrayToString(source?: InfoSourceContent[] | null | undefined): string {
    return source?.map(this.infoSourceContentToString).join(', ') ?? '';
  }
}
