import { Injectable } from '@angular/core';
import { v4 as uuidV4 } from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class StatisticsService {
  private readonly STATISTICS_ID_KEY = 'statisticsId';

  public get statisticsId(): string {
    let id = localStorage.getItem(this.STATISTICS_ID_KEY);

    if (!id) {
      id = uuidV4();
      localStorage.setItem(this.STATISTICS_ID_KEY, id);
    }

    return id;
  }
}
