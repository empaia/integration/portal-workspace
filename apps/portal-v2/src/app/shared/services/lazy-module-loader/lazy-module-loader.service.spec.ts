import { createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { LazyModuleLoaderService } from './lazy-module-loader.service';

describe('LazyModuleLoaderService', () => {
  let spectator: SpectatorService<LazyModuleLoaderService>;
  const createService = createServiceFactory(LazyModuleLoaderService);

  beforeEach(() => spectator = createService());

  it('should...', () => {
    expect(spectator.service).toBeTruthy();
  });
});