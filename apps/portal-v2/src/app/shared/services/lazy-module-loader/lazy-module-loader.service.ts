import { createNgModule, Injectable, Injector, NgModuleRef } from '@angular/core';
import { UserRootStoreModule } from '@user/store/user-root-store.module';

@Injectable({
  providedIn: 'root'
})
export class LazyModuleLoaderService {
  constructor(private injector: Injector) {
    // console.log('Dynamic Module Loader is loaded!');
  }

  loadModule(): NgModuleRef<UserRootStoreModule> {
    const moduleRef = createNgModule(UserRootStoreModule, this.injector);
    return moduleRef;
  }
}
