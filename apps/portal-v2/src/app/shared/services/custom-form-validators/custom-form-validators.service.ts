import { Injectable } from '@angular/core';
import { AbstractControl, FormArray, ValidationErrors } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class CustomFormValidatorsService {

  public static atLeastOne(control: AbstractControl): ValidationErrors | null {
    const valid = (control as FormArray).controls.some(c => c.value);
    return valid ? null : { atLeastOne: { valid: false } };
  }

  public static url(control: AbstractControl): ValidationErrors | null {
    const v: string = control.value;
    const regex = /^(?:http(s)?:\/\/)[\w.-]+(?:\.[\w.-]+)+[\w\-._~:/?#[\]@!$&'()*+,;=.]+/;

    const valid = regex.test(v);
    return valid ? null : { url: { valid: false } };
  }

  public static email(control: AbstractControl): ValidationErrors | null {
    const v: string = control.value;
    // regex from: https://stackoverflow.com/a/29400778
    // seems to catch most cases backend says email is invalid 
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    const valid = regex.test(v);
    return valid ? null : { email: { valid: false } };
  }

  
}
