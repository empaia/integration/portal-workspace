import { Injectable, Provider } from '@angular/core';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { DIRECTION_ALL } from 'hammerjs';

@Injectable()
export class HammerConfig extends HammerGestureConfig {
  override overrides = {
    swipe: { direction: DIRECTION_ALL },
  };
}

export const HAMMER_CONFIG_PROVIDER: Provider[] = [
  {
    provide: HAMMER_GESTURE_CONFIG,
    useClass: HammerConfig
  }
];