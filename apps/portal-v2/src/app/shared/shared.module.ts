import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslocoModule } from '@ngneat/transloco';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { MaterialModule } from '@material/material.module';
import {  PushPipe, LetDirective } from '@ngrx/component';

import { NgLightboxModule } from '@silmar/ng-lightbox';
import { ImageCropperComponent } from 'ngx-image-cropper';

import { TranslatePipe } from './pipes/translate/translate.pipe';
import { UserRolePipe } from './pipes/user-role-in-org/user-role.pipe';
import { CountryPipe } from './pipes/country/country.pipe';

import { ImageDropZoneDirective } from './directives/image-drop-zone/image-drop-zone.directive';

import { SortSelectionComponent } from './components/sort-selection/sort-selection.component';
import { TagDescriptionComponent } from './components/tag-description/tag-description.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';

import { NeedLoginComponent } from './components/need-login/need-login.component';
import { LogoutSuccessComponent } from './components/logout-success/logout-success.component';

import { CountriesEffects } from './store/countries';
import { SHARED_FEATURE_KEY, reducers } from './store/shared.selectors';
import { UploadPictureComponent } from './components/upload-picture/upload-picture.component';

import { CodeToCountryPipe } from './pipes/code-to-country/code-to-country.pipe';
import { NeedOrgMembershipComponent } from './components/need-org-membership/need-org-membership.component';
import { OrganizationUserRolesTableCellComponent } from './components/organization-user-roles-table-cell/organization-user-roles-table-cell.component';
import { CriticalActionDialogComponent } from './components/critical-action-dialog/critical-action-dialog.component';
import { AcceptDialogComponent } from './components/accept-dialog/accept-dialog.component';
import { DenyDialogComponent } from './components/deny-dialog/deny-dialog.component';
import { OrganizationCategoriesTableCellComponentComponent } from './components/organization-categories-table-cell-component/organization-categories-table-cell-component.component';
import { PublicAppDetailsPipe } from './pipes/public-app-details/public-app-details.pipe';
import { TranslatedTagListComponent } from './components/translated-tag-list/translated-tag-list.component';
import { ProductReleaseAtPipe } from './pipes/product-release-at/product-release-at.pipe';
import { DateCorrectionPipe } from './pipes/date-correction/date-correction.pipe';
import { PortalAppFilterBarComponent } from './components/portal-app-filter-bar/portal-app-filter-bar.component';
import { PortalAppFilterCategoryComponent } from './components/portal-app-filter-category/portal-app-filter-category.component';
import { PortalAppFilterChipsComponent } from './components/portal-app-filter-chips/portal-app-filter-chips.component';
import { PortalAppFilterElementComponent } from './components/portal-app-filter-element/portal-app-filter-element.component';
import { PortalAppFilterFormComponent } from './components/portal-app-filter-form/portal-app-filter-form.component';
import { ClearancesTableComponent } from './components/clearances-table/clearances-table.component';
import { ClearancesTableExpandedDetailsComponent } from './components/clearances-table-expanded-details/clearances-table-expanded-details.component';
import { TranslocoRootModule } from '@transloco/transloco-root.module';
import { TrimPipe } from './pipes/trim/trim.pipe';
import { GeneralInfoDialogComponent } from './components/general-info-dialog/general-info-dialog.component';


const modules = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  RouterModule,
  MaterialModule,
  TranslocoModule,
  NgLightboxModule,
  ImageCropperComponent,
  TranslocoRootModule,
  PushPipe,
  LetDirective,
];

const components = [
  SortSelectionComponent,
  TagDescriptionComponent,
  SearchBarComponent,
  NeedLoginComponent,
  LogoutSuccessComponent,
  UploadPictureComponent,
  NeedOrgMembershipComponent,
  OrganizationUserRolesTableCellComponent,
  OrganizationCategoriesTableCellComponentComponent,
  CriticalActionDialogComponent,
  AcceptDialogComponent,
  DenyDialogComponent,
  TranslatedTagListComponent,
  PortalAppFilterBarComponent,
  PortalAppFilterCategoryComponent,
  PortalAppFilterChipsComponent,
  PortalAppFilterElementComponent,
  PortalAppFilterFormComponent,
  ClearancesTableComponent,
  ClearancesTableExpandedDetailsComponent,
  GeneralInfoDialogComponent,
];

const pipes = [
  TranslatePipe,
  UserRolePipe,
  CountryPipe,
  CodeToCountryPipe,
  PublicAppDetailsPipe,
  ProductReleaseAtPipe,
  DateCorrectionPipe,
  TrimPipe,
];

const directives = [
  ImageDropZoneDirective,
];

@NgModule({
  declarations: [
    pipes,
    components,
    directives,
  ],
  imports: [
    modules,
    StoreModule.forFeature(
      SHARED_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      CountriesEffects
    ])
  ],
  exports: [
    modules,
    components,
    pipes,
    directives,
  ],
  providers: [
    TranslatePipe,
    ProductReleaseAtPipe,
    DateCorrectionPipe,
    DatePipe,
  ]
})
export class SharedModule { }
