export interface PaginationParameters {
  page: number;
  page_size: number;
}

export const defaultPaginationParams: PaginationParameters = {
  page: 0,
  page_size: 999,
};
