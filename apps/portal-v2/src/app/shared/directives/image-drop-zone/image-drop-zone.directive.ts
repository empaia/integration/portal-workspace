import { Directive, EventEmitter, HostBinding, HostListener, Input, Output } from '@angular/core';

@Directive({
  selector: '[appImageDropZone]'
})
export class ImageDropZoneDirective {

  @Input() acceptedFormats: string[] = [];

  @Output() public loadedFile = new EventEmitter<File>();
  @Output() public imageToBigError = new EventEmitter<number>();

  @HostBinding('class.highlight-drop-zone') private highlightActive = false;
  @HostBinding('class.deny-drop-zone') private invalidFile = false;


  @HostListener('dragover', ['$event']) private onDragOver(event: DragEvent): void {
    this.preventDefaultPropagation(event);

    const dti = event.dataTransfer?.items[0];
    if (dti) {
      const isValidType = this.acceptedFormats.includes(dti.type);

      if (isValidType) {
        this.highlightActive = true;
      } else {
        this.invalidFile = true;
        if (event.dataTransfer) {
          event.dataTransfer.dropEffect = "none";
        }
      }
    }
  }

  @HostListener('dragleave', ['$event']) private onDragLeave(event: DragEvent): void {
    this.preventDefaultPropagation(event);
    this.highlightActive = false;
    this.invalidFile = false;
  }

  @HostListener('drop', ['$event']) private onDrop(event: DragEvent): void {
    this.preventDefaultPropagation(event);
    this.highlightActive = false;
    this.invalidFile = false;
    const file = event.dataTransfer?.files[0];

    if (file) {
      this.loadedFile.emit(file);
    }
  }

  private preventDefaultPropagation(event: DragEvent): void {
    event.preventDefault();
    event.stopPropagation();
  }
}
