import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { AppTag } from '@api/public/models/app-tag';
import { AppTagList } from '@public/store/portal-apps-filter';

@Component({
  selector: 'app-portal-app-filter-element',
  templateUrl: './portal-app-filter-element.component.html',
  styleUrls: ['./portal-app-filter-element.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PortalAppFilterElementComponent {
  @Input() public name: string | undefined;
  @Input() public tags: AppTag[] | undefined;
  @Input() public activeTags: AppTag[] | undefined;

  @Input() public filterOptionsLabel!: string;

  @Output() public filtersSelected = new EventEmitter<AppTagList>();

  selectFilters(tags: AppTag[], name: string | undefined) {
    if (name) {
      this.filtersSelected.emit({ [name]: tags });
    }
  }
}
