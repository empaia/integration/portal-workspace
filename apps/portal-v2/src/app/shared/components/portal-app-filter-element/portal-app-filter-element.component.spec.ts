import { PortalAppFilterElementComponent } from './portal-app-filter-element.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MockComponents, MockDirectives } from 'ng-mocks';
import { TranslocoDirective } from '@ngneat/transloco';
import { PortalAppFilterFormComponent } from '../portal-app-filter-form/portal-app-filter-form.component';

describe('PortalAppFilterElementComponent', () => {
  let spectator: Spectator<PortalAppFilterElementComponent>;
  const createComponent = createComponentFactory({
    component: PortalAppFilterElementComponent,
    declarations: [
      MockComponents(
        PortalAppFilterFormComponent
      ),
      MockDirectives(
        TranslocoDirective
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
