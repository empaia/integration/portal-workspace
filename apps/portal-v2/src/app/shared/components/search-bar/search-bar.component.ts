import { ChangeDetectionStrategy, Component, EventEmitter, inject, Input, Output } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchBarComponent {
  @Input() public placeholder!: string;
  private _searchValue!: string | undefined;
  @Input() public get searchValue() {
    return this._searchValue;
  }

  public set searchValue(val: string | undefined) {
    this._searchValue = val;
    this.searchForm.patchValue({ search: val });
  }

  @Output() public inputChanged = new EventEmitter<{ search: string }>();
  @Output() public searchSubmit = new EventEmitter<{ search: string }>();

  private fb = inject(FormBuilder);
  protected searchForm = this.fb.nonNullable.group({
    search: '',
  });

  public get searchField(): FormControl<string> {
    return this.searchForm.controls.search;
  }

  public onInputChanged(): void {
    const search = this.searchForm.value?.search;
    if (search) {
      this.inputChanged.emit({ search });
    }
  }

  public onInputDiscard(): void {
    this.searchField.reset();
    this.searchSubmit.emit({ search: '' });
  }

  public onSubmit(): void {
    const search = this.searchForm.value?.search;
    this.searchSubmit.emit({ search: search ?? '' });
  }
}
