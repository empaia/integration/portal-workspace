import { SearchBarComponent } from './search-bar.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';
import { MockDirectives } from 'ng-mocks';
import { TranslocoDirective } from '@ngneat/transloco';
import { ReactiveFormsModule } from '@angular/forms';

describe('SearchBarComponent', () => {
  let spectator: Spectator<SearchBarComponent>;
  const createComponent = createComponentFactory({
    component: SearchBarComponent,
    imports: [
      MaterialModule,
      ReactiveFormsModule,
    ],
    declarations: [
      MockDirectives(
        TranslocoDirective,
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
