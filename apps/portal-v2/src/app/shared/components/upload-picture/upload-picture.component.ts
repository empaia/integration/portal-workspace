import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, HostListener, Input, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TranslocoService } from '@ngneat/transloco';
import { CropperPosition, ImageCroppedEvent } from 'ngx-image-cropper';
import { GeneralInfoDialogComponent } from '../general-info-dialog/general-info-dialog.component';
import { SMALL_DIALOG_WIDTH } from '@edit-organization/models/edit-organization.models';
import { environment } from '@env/environment';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

export type CompatibleImageFormat = 'jpeg' | 'png';

@Component({
  selector: 'app-upload-picture',
  templateUrl: './upload-picture.component.html',
  styleUrls: ['./upload-picture.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UploadPictureComponent {
  @Input() public fallbackImage!: string;
  @Input() public imageUrl: string | undefined;
  @Input() public imageFormat: CompatibleImageFormat = 'jpeg';

  @Output() public imageChanged = new EventEmitter<Blob | null>();

  public imageFile: File | undefined;
  public imageChangedEvent: Event | null | undefined = undefined;
  public croppedImage: Blob | null | undefined;
  public previewImage: SafeUrl | undefined;
  public aspectRatio = 1 / 1;
  public maintainAspectRatio = true;
  public cropperDimension: CropperPosition = {
    x1: 0,
    x2: 0,
    y1: 0,
    y2: 0,
  };

  protected readonly acceptedFormats = ['image/png', 'image/jpeg'];
  public maxMbSize = environment.maxImageMbSize;

  public isProcessing = false;

  @ViewChild('dropZone') private dropZone!: ElementRef;

  constructor(
    private dialog: MatDialog,
    private translocoService: TranslocoService,
    private sanitizer: DomSanitizer,
  ) { }

  public imageCropped(event: ImageCroppedEvent): void {
    this.croppedImage = event.blob;
    this.previewImage = event.objectUrl ? this.sanitizer.bypassSecurityTrustResourceUrl(event.objectUrl) : undefined;
    this.onImageChanged();
  }

  public loadImageFailed(): void {
    this.clearImage();
  }

  public onImageDropped(file: File): void {
    this.imageChangedEvent = undefined;
    const isImage = this.acceptedFormats.includes(file.type);
    if (isImage) {
      this.imageFile = file;
    }
  }

  public onFileSelectChanged(event: Event): void {
    const target = event?.target as HTMLInputElement;
    if (target.files) {
      this.imageChangedEvent = event;
      this.imageFile = target.files[0];
    }
  }

  public clearImage(): void {
    this.imageChangedEvent = undefined;
    this.imageUrl = undefined;
    this.imageFile = undefined;
    this.croppedImage = undefined;
    this.onImageChanged();
  }

  public async onImageChanged(): Promise<void> {
    let image: Blob | null = null;
    if (this.croppedImage) {
      const imageString = this.isImageToBig(this.croppedImage)
        ? await this.resizeBase64Image(this.croppedImage)
        : this.croppedImage;
      image = imageString;
    }
    this.isProcessing = false;
    this.imageChanged.emit(image);
  }

  public startUploadingImage(): void {
    this.isProcessing = true;
  }

  public onImageToBig(): void {
    this.dialog.open(GeneralInfoDialogComponent, {
      width: SMALL_DIALOG_WIDTH,
      data: {
        title: this.translocoService.translate('general_forms.image_to_big_error'),
        content: this.translocoService.translate('general_forms.image_to_big_message', {size: this.maxMbSize}),
        close: this.translocoService.translate('general_forms.close')
      }
    });
  }

  private resizeBase64Image(baseImage: Blob): Promise<Blob> {
    return new Promise((resolve, reject) => {
      const maxSizeInBytes = this.maxMbSize * 1024 * 1024;
      const img = new Image();
      img.src = URL.createObjectURL(baseImage);
      img.onload = () => {
        const canvas = document.createElement('canvas');
        const ctx = canvas.getContext('2d');
        const aspectRatio = img.width / img.height;
        const newWidth = Math.sqrt(maxSizeInBytes * aspectRatio);
        const newHeight = Math.sqrt(maxSizeInBytes / aspectRatio);
        canvas.width = newWidth;
        canvas.height = newHeight;
        ctx?.drawImage(img, 0, 0, newWidth, newHeight);
        URL.revokeObjectURL(img.src);
        canvas.toBlob(blob => {
          if (blob) {
            resolve(blob);
          } else {
            reject('Could not scale!');
          }
        });
      };
    });
  }

  private isImageToBig(img: Blob): boolean {
    const size = img.size / 1024 / 1024;
    return size > this.maxMbSize;
  }

  @HostListener('window:resize')
  private resizeCropper(): void {
    // wait for 100ms after resizing to get the new width and height
    setTimeout(() => {
      const wrapper = this.dropZone.nativeElement.firstChild;
      const width = wrapper.offsetWidth;
      const height = wrapper.offsetHeight;
      const least = Math.min(width, height);
      this.cropperDimension = {
        x1: 0,
        x2: least,
        y1: 0,
        y2: least,
      };
    }, 100);
  }
}
