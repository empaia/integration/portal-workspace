import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppRoutes } from '@router/router.constants';
import * as RouterActions from '@router/router.actions';

@Component({
  selector: 'app-need-org-membership',
  templateUrl: './need-org-membership.component.html',
  styleUrls: ['./need-org-membership.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NeedOrgMembershipComponent {


  private store = inject(Store);

  orgOverview() {
    this.store.dispatch(RouterActions.navigate({
      path: [AppRoutes.PUBLIC_ORG_ROUTE]
    }));
  }

}
