export interface CriticalActionDialogInput {
  title?: string;
  message?: string;

  confirm?: string;
  cancel?: string;
}