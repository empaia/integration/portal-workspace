import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CriticalActionDialogInput } from './critical-action-dialog.model';

@Component({
  selector: 'app-critical-action-dialog',
  templateUrl: './critical-action-dialog.component.html',
  styleUrls: ['./critical-action-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CriticalActionDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<CriticalActionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: CriticalActionDialogInput,) {

  }
}
