import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { Store } from '@ngrx/store';
import * as RouterActions from '@router/router.actions';
import { AppRoutes } from '@router/router.constants';

@Component({
  selector: 'app-logout-success',
  templateUrl: './logout-success.component.html',
  styleUrls: ['./logout-success.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LogoutSuccessComponent {

  private store = inject(Store);

  home() {
    this.store.dispatch(RouterActions.navigate({
      path: [AppRoutes.MARKET_ROUTE]
    }));
  }

}
