import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { FilterCategory } from '@public/store/portal-apps-filter';

@Component({
  selector: 'app-portal-app-filter-category',
  templateUrl: './portal-app-filter-category.component.html',
  styleUrls: ['./portal-app-filter-category.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PortalAppFilterCategoryComponent {
  @Input() public filterCategories: FilterCategory[] = [];
  @Input() public filterCategoriesLabel!: string;

  @Output() public selectedFilterCategory = new EventEmitter<FilterCategory>();
}
