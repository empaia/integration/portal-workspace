import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { TranslocoDirective } from '@ngneat/transloco';
import { MockDirectives } from 'ng-mocks';
import { PortalAppFilterCategoryComponent } from './portal-app-filter-category.component';

describe('PortalAppFilterCategoryComponent', () => {
  let spectator: Spectator<PortalAppFilterCategoryComponent>;
  const createComponent = createComponentFactory({
    component: PortalAppFilterCategoryComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      MockDirectives(
        TranslocoDirective
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
