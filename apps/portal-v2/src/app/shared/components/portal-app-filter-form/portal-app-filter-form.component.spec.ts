import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { TranslocoDirective, TranslocoTestingModule } from '@ngneat/transloco';
import { TranslatePipe } from '@shared/pipes/translate/translate.pipe';
import { MockDeclarations } from 'ng-mocks';
import { PortalAppFilterFormComponent } from './portal-app-filter-form.component';

describe('PortalAppFilterFormComponent', () => {
  let spectator: Spectator<PortalAppFilterFormComponent>;
  const createComponent = createComponentFactory({
    component: PortalAppFilterFormComponent,
    imports: [
      MaterialModule,
      TranslocoTestingModule.forRoot({
        translocoConfig: {
          availableLangs: [
            { id: 'en', label: 'English' },
            { id: 'de', label: 'German' },
          ],
          fallbackLang: 'en',
          defaultLang: 'de',
        },
        preloadLangs: true
      })
    ],
    declarations: [
      MockDeclarations(
        TranslocoDirective
      )
    ],
    providers: [
      TranslatePipe
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
