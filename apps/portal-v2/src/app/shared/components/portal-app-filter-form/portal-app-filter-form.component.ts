import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { AppTag } from '@api/public/models';
import { TranslatePipe } from '@shared/pipes/translate/translate.pipe';
import { merge } from 'lodash';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-portal-app-filter-form',
  templateUrl: './portal-app-filter-form.component.html',
  styleUrls: ['./portal-app-filter-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class PortalAppFilterFormComponent {
  @Input() public tags: AppTag[] | undefined;
  @Input() public set activeTags(val: AppTag[] | undefined) {
    if (val) {
      this.currentTags = new Set<AppTag>(merge(val, this.currentTags));
      this.filterControl.setValue(null);
    }
  }

  @Input() set disabled(val: boolean) {
    if (val) {
      this.filterControl.disable();
    } else {
      this.filterControl.enable();
    }
  }

  @Input() public filterOptionsLabel!: string;

  @Output() public selectedTags = new EventEmitter<AppTag[]>();

  @ViewChild('filterInput') private filterInput!: ElementRef<HTMLInputElement>;

  public filterControl = new FormControl<string | AppTag | null>(null);
  public filteredTags$: Observable<AppTag[] | undefined>;
  public currentTags = new Set<AppTag>();

  constructor(private translate: TranslatePipe) {
    this.filteredTags$ = this.filterControl.valueChanges.pipe(
      startWith(null),
      map((tag: string | AppTag | null) => {
        const value = typeof tag === 'string' ? tag : this.translate.transform(tag?.tag_translations);
        let filtered = tag ? this.filter(value) : this.tags?.slice();
        // remove selected item from auto complete list
        if (this.currentTags && this.currentTags.size > 0) {
          filtered = filtered?.filter(f => !this.currentTags.has(f));
        }
        return filtered;
      })
    );
  }

  public onSelectTag(event: MatAutocompleteSelectedEvent): void {
    const selection = this.tags?.find(t => this.translate.transform(t.tag_translations) === event.option.value);
    if (selection) {
      this.currentTags.add(selection);
      this.filterInput.nativeElement.value = '';
      // trigger valueChanges
      this.filterControl.setValue(null);
      this.selectedTags.emit(Array.from(this.currentTags));
    }
  }

  private filter(value: string): AppTag[] | undefined {
    const filterValue = value.toLowerCase();
    return this.tags?.filter(tag => this.translate.transform(tag?.tag_translations).toLowerCase().includes(filterValue));
  }
}
