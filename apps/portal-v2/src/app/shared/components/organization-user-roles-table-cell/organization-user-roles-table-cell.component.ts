import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { OrganizationUserRoleV2 } from '@api/aaa/models';

@Component({
  selector: 'app-organization-user-roles-table-cell',
  templateUrl: './organization-user-roles-table-cell.component.html',
  styleUrls: ['./organization-user-roles-table-cell.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OrganizationUserRolesTableCellComponent {
  @Input() public roles: OrganizationUserRoleV2[] = [];
}
