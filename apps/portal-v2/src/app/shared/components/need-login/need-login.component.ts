import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthActions } from '@auth/store/auth';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-need-login',
  templateUrl: './need-login.component.html',
  styleUrls: ['./need-login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NeedLoginComponent {

  private route = inject(ActivatedRoute);
  private store = inject(Store);

  login() {
    const r = this.route.snapshot.queryParamMap.get('redirect');
    this.store.dispatch(AuthActions.login({ redirect: r ? r : undefined }));
  }

}
