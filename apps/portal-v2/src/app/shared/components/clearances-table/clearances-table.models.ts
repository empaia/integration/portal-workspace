export const SCANNER_VENDORS = [
  'Leica',
  'Hamamatsu',
  '3DHistech',
  'Olympus',
  'Roche',
  'Huron',
  'Trestle',
  'Zeiss',
  'Nikon',
  'PreciPoint',
  'Grundium',
  'TissueGnostics',
  'Keyence',
  'Inspirata',
  'BioImager',
  'Argos',
  'DMetrix',
  'Syncroscopy',
  'MoticEasyScan',
  'PerkinElmer',
  'OptraScan',
] as const;

export const MAILTO_LINK_MESSAGE = 'mailto:example@example.com?subject=EMPAIA Pathology AI Register&body=Link to EMPAIA Pathology AI Register:';
export const BUTTON_TIMEOUT = 5000;
