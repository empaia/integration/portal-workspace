import { animate, state, style, transition, trigger } from '@angular/animations';
import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AppRoutes } from '@router/router.constants';
import { ClearanceItemTableEntity } from '@shared/models/clearances.models';
import { blankOrgImage } from 'src/app/app.models';
import { BUTTON_TIMEOUT, MAILTO_LINK_MESSAGE, SCANNER_VENDORS } from './clearances-table.models';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-clearances-table',
  templateUrl: './clearances-table.component.html',
  styleUrls: ['./clearances-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClearancesTableComponent implements AfterViewInit, OnInit {
  public readonly PUBLIC_ORG_ROUTE = AppRoutes.PUBLIC_ORG_ROUTE;
  public readonly FALLBACK_IMAGE = blankOrgImage;
  public readonly COMBINED_ROW_COUNT = 3;
  public readonly SCANNER_VENDOR_NAMES = [...SCANNER_VENDORS];

  public isLinkCopied$ = new Subject<boolean>();
  public isEmailCopied$ = new Subject<boolean>();
  public isExcelExported$ = new Subject<boolean>();

  private _clearances!: ClearanceItemTableEntity[];
  @Input() public set clearances(val: ClearanceItemTableEntity[] | undefined) {
    if (val) {
      this._clearances = val;
      this.dataSource.data = val;
      this.dataSource.sort = this.sort;
    }
  }

  public get clearances() {
    return this._clearances;
  }

  @Input() public clearanceCount: number | undefined;
  @Input() public isPublic = false;

  @Output() public hideClearanceClicked = new EventEmitter<string>();
  @Output() public unhideClearanceClicked = new EventEmitter<string>();
  @Output() public editClearanceItemClicked = new EventEmitter<string>();
  @Output() public exportToExcel = new EventEmitter<void>();

  public expandedElement: ClearanceItemTableEntity | null = null;

  @ViewChild(MatSort, { static: false }) private sort!: MatSort;

  public dataSource = new MatTableDataSource<Partial<ClearanceItemTableEntity>>([{
    product_name: 'Loading Data',
  }]);

  public displayedColumns: string[] = [
    'procedure',
    'clearance',
    'tissue',
    'actions',
  ];

  public secondaryColumns: string[] = [
    'indication',
    'image_type',
    'stain',
  ];

  public tertiaryColumns: string[] = [
    'analysis',
    'scanners',
  ];

  public ngOnInit(): void {
    // push organization_name at the start of the array
    // when clearance table is public
    // otherwise add column for product_name
    this.displayedColumns.unshift(this.isPublic ? 'organization_name' : 'product_name');
  }

  public ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;

    if (this.isPublic) {
      // fix: sorting takes word casing into account - we do not want to sort by upper and lower case
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      this.dataSource.sortingDataAccessor = (data: any, sortHeaderId: string): string => {
        // to sort by nested property
        if (sortHeaderId == 'organization_name') {
          return data.organization?.organization_name.toLocaleLowerCase();
        }
        if (typeof data[sortHeaderId] === 'string') {
          return data[sortHeaderId].toLocaleLowerCase();
        }
        return data[sortHeaderId];
      };

      const sortState: Sort = { active: 'organization_name', direction: 'asc' };
      this.sort.active = sortState.active;
      this.sort.direction = sortState.direction;
    }
  }

  public async copyLinkToClipboard(): Promise<void> {
    this.isLinkCopied$.next(true);
    setTimeout(() => this.isLinkCopied$.next(false), BUTTON_TIMEOUT, this);
    await navigator.clipboard.writeText(window.location.href);
  }

  public sendEmailWithLink(): void {
    window.open(`${MAILTO_LINK_MESSAGE} ${window.location.href}`, '_blank');
    this.isEmailCopied$.next(true);
    setTimeout(() => this.isEmailCopied$.next(false), BUTTON_TIMEOUT, this);
  }

  public exportTableToExcel(): void {
    this.isExcelExported$.next(true);
    setTimeout(() => this.isExcelExported$.next(false), BUTTON_TIMEOUT, this);
    this.exportToExcel.emit();
  }
}
