import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { TranslocoDirective } from '@ngneat/transloco';
import { TranslatePipe } from '@shared/pipes/translate/translate.pipe';
import { MockDirectives } from 'ng-mocks';
import { PortalAppFilterChipsComponent } from './portal-app-filter-chips.component';

describe('PortalAppFilterChipsComponent', () => {
  let spectator: Spectator<PortalAppFilterChipsComponent>;
  const createComponent = createComponentFactory({
    component: PortalAppFilterChipsComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      MockDirectives(
        TranslocoDirective
      )
    ],
    providers: [
      TranslatePipe
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
