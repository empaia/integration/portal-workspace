import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { AppTag } from '@api/public/models';
import { AppTagList, FilterCategory } from '@public/store/portal-apps-filter';

@Component({
  selector: 'app-portal-app-filter-chips',
  templateUrl: './portal-app-filter-chips.component.html',
  styleUrls: ['./portal-app-filter-chips.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PortalAppFilterChipsComponent {
  private _activeFilters: AppTagList | undefined;
  @Input() public set activeFilters(val: AppTagList | undefined) {
    this._activeFilters = val;
    if (val) {
      this.tagNames = Object.keys(val) as FilterCategory[];
    }
  }

  public get activeFilters() {
    return this._activeFilters;
  }

  public tagNames: FilterCategory[] = [];


  @Output() public filterSelected = new EventEmitter<AppTagList>();

  public removeTag(category: FilterCategory, tag: AppTag): void {
    const index = this.activeFilters?.[category]?.indexOf(tag);
    if (index !== undefined && index > -1) {
      this.activeFilters?.[category]?.splice(index, 1);
      this.emitChanges();
    }
  }

  hasActive(): boolean {
    for (const tag of this.tagNames) {
      if (this.activeFilters && tag) {
        const a = this.activeFilters?.[tag];
        if (a && a.length > 0) {
          return true;
        }
      }
    }
    return false;
  }

  private emitChanges(): void {
    this.filterSelected.emit(this.activeFilters);
  }


}
