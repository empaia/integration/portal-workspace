import { TagDescriptionComponent } from './tag-description.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MockDirectives } from 'ng-mocks';
import { TranslocoDirective } from '@ngneat/transloco';

describe('TagDescriptionComponent', () => {
  let spectator: Spectator<TagDescriptionComponent>;
  const createComponent = createComponentFactory({
    component: TagDescriptionComponent,
    declarations: [
      MockDirectives(
        TranslocoDirective
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
