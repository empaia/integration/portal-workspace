import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { AppTag } from '@api/public/models/app-tag';

@Component({
  selector: 'app-tag-description',
  templateUrl: './tag-description.component.html',
  styleUrls: ['./tag-description.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TagDescriptionComponent {
  @Input() public name!: string;
  @Input() public tags!: AppTag[] | undefined;
}
