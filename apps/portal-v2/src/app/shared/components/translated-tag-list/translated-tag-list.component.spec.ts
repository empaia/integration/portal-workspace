import { Spectator, createComponentFactory } from '@ngneat/spectator';

import { TranslatedTagListComponent } from './translated-tag-list.component';

describe('TranslatedTagListComponent', () => {
  let spectator: Spectator<TranslatedTagListComponent>;
  const createComponent = createComponentFactory(TranslatedTagListComponent);

  it('should create', () => {
    spectator = createComponent();

    expect(spectator.component).toBeTruthy();
  });
});
