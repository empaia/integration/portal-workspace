import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { AppTag } from '@api/public/models';

@Component({
  selector: 'app-translated-tag-list',
  templateUrl: './translated-tag-list.component.html',
  styleUrls: ['./translated-tag-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class TranslatedTagListComponent {
  @Input() public tags!: Array<AppTag> | undefined;
  @Input() public stringFilterList?: string[];
}
