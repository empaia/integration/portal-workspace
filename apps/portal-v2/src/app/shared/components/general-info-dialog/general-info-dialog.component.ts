import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface InfoDialogEntity {
  title?: string;
  content?: string;
  close?: string;
}

@Component({
  selector: 'app-general-info-dialog',
  templateUrl: './general-info-dialog.component.html',
  styleUrls: ['./general-info-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GeneralInfoDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: InfoDialogEntity,
  ) {}
}
