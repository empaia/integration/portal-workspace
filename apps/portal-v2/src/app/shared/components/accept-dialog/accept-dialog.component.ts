import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface AcceptDialogEntity {
  headlineText: string;
  contentText: string;
  confirmButtonText: string;
  cancelButtonText: string;
}

@Component({
  selector: 'app-accept-dialog',
  templateUrl: './accept-dialog.component.html',
  styleUrls: ['./accept-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AcceptDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: AcceptDialogEntity,
  ) {}
}
