import { PortalAppFilterBarComponent } from './portal-app-filter-bar.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MockComponents, MockDirectives } from 'ng-mocks';
import { TranslocoDirective } from '@ngneat/transloco';
import { PortalAppFilterCategoryComponent } from '../portal-app-filter-category/portal-app-filter-category.component';
import { PortalAppFilterElementComponent } from '../portal-app-filter-element/portal-app-filter-element.component';
import { PortalAppFilterChipsComponent } from '../portal-app-filter-chips/portal-app-filter-chips.component';

describe('PortalAppFilterBarComponent', () => {
  let spectator: Spectator<PortalAppFilterBarComponent>;
  const createComponent = createComponentFactory({
    component: PortalAppFilterBarComponent,
    declarations: [
      MockDirectives(
        TranslocoDirective
      ),
      MockComponents(
        PortalAppFilterCategoryComponent,
        PortalAppFilterElementComponent,
        PortalAppFilterChipsComponent,
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
