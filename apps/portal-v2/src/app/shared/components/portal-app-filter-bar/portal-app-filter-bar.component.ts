import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { AppTagList, DEFAULT_FILTER_CATEGORIES, FilterCategory } from '@public/store/portal-apps-filter';

@Component({
  selector: 'app-portal-app-filter-bar',
  templateUrl: './portal-app-filter-bar.component.html',
  styleUrls: ['./portal-app-filter-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortalAppFilterBarComponent {
  @Input() public filters: AppTagList | undefined;

  @Input() public activeFilters: AppTagList | undefined;

  @Input() public filterCategoriesLabel = 'filters.category';
  @Input() public filterOptionsLabel = 'filters.options';

  @Input() public filterCategories: FilterCategory[] = [...DEFAULT_FILTER_CATEGORIES];
  public selectedFilter: FilterCategory | undefined;

  @Output() public filtersSelected = new EventEmitter<AppTagList>();


  public selectFilters(event: AppTagList): void {
    const selectedFilters = { ...this.activeFilters, ...event };
    this.filtersSelected.emit(selectedFilters);
  }

  public onFilterSelectionChanged(filter: FilterCategory): void {
    this.selectedFilter = filter;
  }

}
