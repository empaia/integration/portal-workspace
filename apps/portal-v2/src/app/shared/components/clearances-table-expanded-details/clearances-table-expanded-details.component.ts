import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { InfoProvider, InfoSourceType, Language } from '@api/public/models';
import { ClearanceItemTableEntity } from '@shared/models/clearances.models';

@Component({
  selector: 'app-clearances-table-expanded-details',
  templateUrl: './clearances-table-expanded-details.component.html',
  styleUrls: ['./clearances-table-expanded-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClearancesTableExpandedDetailsComponent {
  @Input() public clearanceItem: ClearanceItemTableEntity | undefined;

  public readonly INFO_PROVIDER = InfoProvider;
  public readonly INFO_SOURCE_TYPE = InfoSourceType;
  public readonly LANGUAGE = Language;
}
