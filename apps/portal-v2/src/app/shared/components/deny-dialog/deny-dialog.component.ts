import { ChangeDetectionStrategy, Component, inject, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MAX_FORM_DESCRIPTION_FIELD_LENGTH } from '@shared/models/form.models';

export interface DenyDialogEntity {
  headlineText: string;
  reviewLabel: string;
  confirmButtonText: string;
  cancelButtonText: string;
}

@Component({
  selector: 'app-deny-dialog',
  templateUrl: './deny-dialog.component.html',
  styleUrls: ['./deny-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DenyDialogComponent {
  private fb = inject(FormBuilder);
  public reviewForm = this.fb.nonNullable.control(
    '',
    [
      Validators.required,
      Validators.maxLength(MAX_FORM_DESCRIPTION_FIELD_LENGTH)
    ]
  );

  public readonly MAX_FORM_LENGTH = MAX_FORM_DESCRIPTION_FIELD_LENGTH;

  constructor(
    private dialogRef: MatDialogRef<DenyDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DenyDialogEntity,
  ) {}

  public onSubmit(): void {
    const review = this.reviewForm.value;
    this.dialogRef.close(review);
  }
}
