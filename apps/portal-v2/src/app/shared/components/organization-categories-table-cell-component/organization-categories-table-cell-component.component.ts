import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { OrganizationCategory } from '@api/aaa/models';

@Component({
  selector: 'app-organization-categories-table-cell-component',
  templateUrl: './organization-categories-table-cell-component.component.html',
  styleUrls: ['./organization-categories-table-cell-component.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OrganizationCategoriesTableCellComponentComponent {
  @Input() public categories: OrganizationCategory[] = [];
}
