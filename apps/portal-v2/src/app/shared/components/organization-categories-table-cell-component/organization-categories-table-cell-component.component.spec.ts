import { Spectator, createComponentFactory } from '@ngneat/spectator';

import { OrganizationCategoriesTableCellComponentComponent } from './organization-categories-table-cell-component.component';

describe('OrganizationCategoriesTableCellComponentComponent', () => {
  let spectator: Spectator<OrganizationCategoriesTableCellComponentComponent>;
  const createComponent = createComponentFactory(OrganizationCategoriesTableCellComponentComponent);

  it('should create', () => {
    spectator = createComponent();

    expect(spectator.component).toBeTruthy();
  });
});
