import { SortSelectionComponent } from './sort-selection.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';
import { MockDirectives } from 'ng-mocks';
import { TranslocoDirective } from '@ngneat/transloco';

describe('SortSelectionComponent', () => {
  let spectator: Spectator<SortSelectionComponent>;
  const createComponent = createComponentFactory({
    component: SortSelectionComponent,
    imports: [
      MaterialModule
    ],
    declarations: [
      MockDirectives(
        TranslocoDirective
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
