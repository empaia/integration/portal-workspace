import { TextTranslation } from '@api/public/models/text-translation';

export enum SortBy {
  DATE = 'date',
  NAME = 'name',
  VENDOR = 'vendor'
}

export interface SortOption {
  name: SortBy;
  value: TextTranslation[];
}
