import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { SortBy, SortOption } from '@shared/components/sort-selection/sort-selection.models';
import { MatSelectChange } from '@angular/material/select';

@Component({
  selector: 'app-sort-selection',
  templateUrl: './sort-selection.component.html',
  styleUrls: ['./sort-selection.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SortSelectionComponent {
  @Input() public selected!: SortBy | undefined;
  @Input() public sortingOptions!: SortOption[];

  @Output() public optionChanged = new EventEmitter<SortBy>();

  onSelectionChanged(event: MatSelectChange): void {
    this.optionChanged.emit(event.value);
  }
}
