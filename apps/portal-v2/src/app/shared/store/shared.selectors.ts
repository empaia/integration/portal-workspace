import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromCountries from './countries/countries.reducer';

export const SHARED_FEATURE_KEY = 'shared';

export interface State {
  [fromCountries.COUNTRIES_FEATURE_KEY]: fromCountries.State;
}

export const selectSharedFeatureState = createFeatureSelector<State>(
  SHARED_FEATURE_KEY
);

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromCountries.COUNTRIES_FEATURE_KEY]: fromCountries.reducer,
  })(state, action);
}
