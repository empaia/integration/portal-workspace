import { createSelector } from '@ngrx/store';
import { selectSharedFeatureState } from '../shared.selectors';
import { COUNTRIES_FEATURE_KEY, countryAdapter } from './countries.reducer';

const {
  selectAll,
  selectEntities,
} = countryAdapter.getSelectors();

export const selectCountriesState = createSelector(
  selectSharedFeatureState,
  (state) => state[COUNTRIES_FEATURE_KEY]
);

export const selectAllCountries = createSelector(
  selectCountriesState,
  selectAll
);

export const selectCountryEntities = createSelector(
  selectCountriesState,
  selectEntities
);

export const selectCountriesLoaded = createSelector(
  selectCountriesState,
  (state) => state.loaded
);

export const selectCountriesError = createSelector(
  selectCountriesState,
  (state) => state.error
);
