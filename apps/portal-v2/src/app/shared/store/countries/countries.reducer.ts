import { HttpErrorResponse } from '@angular/common/http';
import { CountryEntityV2 } from '@api/aaa/models';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';
import { CountriesActions } from './countries.actions';


export const COUNTRIES_FEATURE_KEY = 'countries';

export interface State extends EntityState<CountryEntityV2> {
  loaded: boolean;
  error?: HttpErrorResponse;
}

export const countryAdapter = createEntityAdapter<CountryEntityV2>({
  selectId: country => country.country_code
});

export const initialState: State = countryAdapter.getInitialState({
  loaded: true,
  error: undefined,
});

export const countriesReducer = createReducer(
  initialState,
  on(CountriesActions.loadAllCountries, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(CountriesActions.loadAllCountriesSuccess, (state, { countries }): State =>
    countryAdapter.setAll(countries, {
      ...state,
      loaded: true,
    })
  ),
  on(CountriesActions.loadAllCountriesFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
);

export function reducer(state: State | undefined, action: Action) {
  return countriesReducer(state, action);
}
