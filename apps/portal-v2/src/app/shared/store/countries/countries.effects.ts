import { Injectable } from '@angular/core';
import { DomainControllerV2Service } from '@api/aaa/services';
import { AuthSelectors } from '@auth/store/auth';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { fetch } from '@ngrx/router-store/data-persistence';
import { filter, map, take } from 'rxjs/operators';
import { CountriesActions } from './countries.actions';
import { State } from './countries.reducer';

@Injectable()
export class CountriesEffects {
  loadAllCountries$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CountriesActions.loadAllCountries),
      fetch({
        run: (
          _action: ReturnType<typeof CountriesActions.loadAllCountries>,
          _state: State
        ) => {
          return this.domainControllerService
            .getCountries()
            .pipe(
              map((countries) =>
                CountriesActions.loadAllCountriesSuccess({ countries })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof CountriesActions.loadAllCountries>,
          error
        ) => {
          return CountriesActions.loadAllCountriesFailure({ error });
        },
      })
    );
  });

  loadAfterLogin$ = createEffect(() => {
    return this.store.select(AuthSelectors.selectIsDoneLoading).pipe(
      filter((loaded) => loaded),
      take(1),
      map(() => CountriesActions.loadAllCountries())
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly domainControllerService: DomainControllerV2Service
  ) {}
}
