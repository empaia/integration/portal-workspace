import { HttpErrorResponse } from '@angular/common/http';
import { CountryEntityV2 } from '@api/aaa/models';
import { createActionGroup, emptyProps, props } from '@ngrx/store';

export const CountriesActions = createActionGroup({
  source: 'Countries',
  events: {
    'Load All Countries': emptyProps(),
    'Load All Countries Success': props<{ countries: CountryEntityV2[] }>(),
    'Load All Countries Failure': props<{ error: HttpErrorResponse }>(),
  }
});
