export type Modify<T, R> = Omit<T, keyof R> & R;


// modify type to make property optional
// example:  make the hometown & nickname optional
// type MakePersonInput = Optional<Person, 'hometown' | 'nickname'>
// https://stackoverflow.com/a/61108377
export type Optional<T, K extends keyof T> = Pick<Partial<T>, K> & Omit<T, K>;
