import { createAction } from '@ngrx/store';
import { UnaryFunction, Observable, pipe, OperatorFunction } from 'rxjs';
import { filter } from 'rxjs/operators';


// filter nullish values and infer type
export function filterNullish<T>(): UnaryFunction<Observable<T | null | undefined>, Observable<T>> {
  return pipe(
    filter(x => x != null) as OperatorFunction<T | null | undefined, T>
  );
}

export const NOOP = createAction(
  // eslint-disable-next-line @ngrx/good-action-hygiene
  'NOOP',
);
