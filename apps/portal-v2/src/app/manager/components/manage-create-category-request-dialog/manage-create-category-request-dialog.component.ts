import { ChangeDetectionStrategy, Component, Inject, inject } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { OrganizationCategory, PostOrganizationCategoryRequestEntity } from '@api/aaa/models';
export interface CategoryRequestDialogInputData {
  organizationCategories: OrganizationCategory[]
}

@Component({
  selector: 'app-manage-create-category-request-dialog',
  templateUrl: './manage-create-category-request-dialog.component.html',
  styleUrls: ['./manage-create-category-request-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManageCreateCategoryRequestDialogComponent {
  private fb = inject(FormBuilder);
  public readonly ORGANIZATION_CATEGORIES = Object.keys(OrganizationCategory);
  public requestForm = this.buildFromEnum();

  protected currentCategories: OrganizationCategory[];

  constructor(
    private dialogRef: MatDialogRef<ManageCreateCategoryRequestDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: CategoryRequestDialogInputData,
  ) { 
    this.currentCategories = data.organizationCategories;
    console.log('cat',this.currentCategories);

    this.currentCategories.forEach(cat => {
      const checkbox = this.requestForm.get(cat);
      if (checkbox) {
        // check checkboxes with categories org already has
        checkbox.setValue(true);
      }
    });
  }
  private buildFromEnum(): FormGroup {
    const form = this.fb.nonNullable.group({});
    this.ORGANIZATION_CATEGORIES.forEach(key =>
      form.addControl(key, this.fb.nonNullable.control(false))
    );
    return form;
  }
  private getControl(field: OrganizationCategory): AbstractControl {
    return this.requestForm.controls[field];
  }

  public onSubmit(): void {
    const request: PostOrganizationCategoryRequestEntity = {
      requested_organization_categories: this.getSubmitData()
    };
    this.dialogRef.close(request);
  }

  getSubmitData(): OrganizationCategory[] {
    const array: Array<OrganizationCategory> = [];

    for (const key of Object.values(OrganizationCategory)) {
      if (this.getControl(key).value) {
        array.push(key);
      }
    }
    return array;
  }
}

