import { Spectator, createComponentFactory } from '@ngneat/spectator';

import { ManageOrganizationMembersTableComponent } from './manage-organization-members-table.component';

describe('ManageOrganizationMembersTableComponent', () => {
  let spectator: Spectator<ManageOrganizationMembersTableComponent>;
  const createComponent = createComponentFactory(ManageOrganizationMembersTableComponent);

  it('should create', () => {
    spectator = createComponent();

    expect(spectator.component).toBeTruthy();
  });
});
