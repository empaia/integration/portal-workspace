import { SelectionModel } from '@angular/cdk/collections';
import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { OrgMemberEntity } from '@manager/store/manage-org-members';

@Component({
  selector: 'app-manage-organization-members-table',
  templateUrl: './manage-organization-members-table.component.html',
  styleUrls: ['./manage-organization-members-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManageOrganizationMembersTableComponent implements AfterViewInit {


  @ViewChild(MatSort) sort!: MatSort;

  private _members!: OrgMemberEntity[];
  @Input() public get members() {
    return this._members;
  }
  public set members(val: OrgMemberEntity[] | undefined) {
    if (val) {
      this._members = val;
      this.dataSource.data = val;
      this.dataSource.sort = this.sort;
    }
    // console.log('dataSource', this.dataSource);
  }

  selection = new SelectionModel<Partial<OrgMemberEntity>>(true, []);
  displayedColumns: string[] = [
    /* 'checkbox', */
    'email_address',
    'last_name',
    'first_name',
    'organization_user_roles',
    'actions'
  ];
  dataSource = new MatTableDataSource<Partial<OrgMemberEntity>>([{
    email_address: 'Loading Data...', organization_user_roles: []
  }]);

  @Output() public editRoles = new EventEmitter<{ userId: string }>();
  @Output() public removeUser = new EventEmitter<{ userId: string }>();

  constructor() {
    // Assign the data to the data source for the table to render
    // this.dataSource = new MatTableDataSource(undefined);
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  toggleAllRows() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }
    this.selection.select(...this.dataSource.data);
  }
}
