import { Spectator, createComponentFactory } from '@ngneat/spectator';

import { ManageMembershipRequestsTableComponent } from './manage-membership-requests-table.component';

describe('ManageMembershipRequestsTableComponent', () => {
  let spectator: Spectator<ManageMembershipRequestsTableComponent>;
  const createComponent = createComponentFactory(ManageMembershipRequestsTableComponent);

  it('should create', () => {
    spectator = createComponent();

    expect(spectator.component).toBeTruthy();
  });
});
