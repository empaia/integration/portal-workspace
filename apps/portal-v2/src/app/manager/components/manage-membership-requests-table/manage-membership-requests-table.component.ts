import { SelectionModel } from '@angular/cdk/collections';
import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MembershipRequestEntity } from '@user/store/membership-request';

@Component({
  selector: 'app-manage-membership-requests-table',
  templateUrl: './manage-membership-requests-table.component.html',
  styleUrls: ['./manage-membership-requests-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManageMembershipRequestsTableComponent implements AfterViewInit {

  @ViewChild(MatSort) sort!: MatSort;

  private _requests!: MembershipRequestEntity[];
  @Input() public get membershipRequests() {
    return this._requests;
  }
  public set membershipRequests(val: MembershipRequestEntity[] | null) {
    if (val) {
      this._requests = val;
      this.dataSource.data = val;
      this.dataSource.sort = this.sort;
    }
    // console.log('dataSource', this.dataSource);
  }

  selection = new SelectionModel<Partial<MembershipRequestEntity>>(true, []);
  displayedColumns: string[] = [
    /* 'checkbox', */
    'last_name',
    'first_name',
    'email_address',
    // 'organization_name',
    'actions'
  ];
  dataSource = new MatTableDataSource<Partial<MembershipRequestEntity>>([
    {
      user_details: {
        user_id: '',
        email_address: '',
        first_name: '',
        last_name: ''
      },
      organization_name: '',
      organization_id: '',
    }]);

  @Output() public acceptRequest = new EventEmitter<{ id: number }>();
  @Output() public rejectRequest = new EventEmitter<{ id: number }>();

  constructor() {
    // Assign the data to the data source for the table to render
    // this.dataSource = new MatTableDataSource(undefined);
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  toggleAllRows() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }
    this.selection.select(...this.dataSource.data);
  }

}
