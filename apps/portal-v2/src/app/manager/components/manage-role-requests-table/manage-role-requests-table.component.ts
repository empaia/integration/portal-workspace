import { SelectionModel } from '@angular/cdk/collections';
import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ManageRoleRequestEntity } from '@manager/store/manage-role-request';

@Component({
  selector: 'app-manage-role-requests-table',
  templateUrl: './manage-role-requests-table.component.html',
  styleUrls: ['./manage-role-requests-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManageRoleRequestsTableComponent implements AfterViewInit {

  @ViewChild(MatSort) sort!: MatSort;

  private _requests!: ManageRoleRequestEntity[];
  @Input() public get roleRequests() {
    return this._requests;
  }
  public set roleRequests(val: ManageRoleRequestEntity[] | undefined) {
    if (val) {
      this._requests = val;
      this.dataSource.data = val;
      this.dataSource.sort = this.sort;
    }
    // console.log('dataSource', this.dataSource);
  }

  selection = new SelectionModel<Partial<ManageRoleRequestEntity>>(true, []);
  displayedColumns: string[] = [/* 'checkbox', */ 'user_id', 'organization_user_role', 'actions'];
  dataSource = new MatTableDataSource<Partial<ManageRoleRequestEntity>>([{
    user_details: {
      user_id: '',
      email_address: '',
      first_name: '',
      last_name: ''
    },
    // organization_user_role: undefined,
  }]);

  @Output() public acceptRequest = new EventEmitter<{ id: number }>();
  @Output() public rejectRequest = new EventEmitter<{ id: number }>();

  constructor() {
    // Assign the data to the data source for the table to render
    // this.dataSource = new MatTableDataSource(undefined);
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  toggleAllRows() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }
    this.selection.select(...this.dataSource.data);
  }
}
