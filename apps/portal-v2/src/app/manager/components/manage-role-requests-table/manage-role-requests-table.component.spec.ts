import { Spectator, createComponentFactory } from '@ngneat/spectator';

import { ManageRoleRequestsTableComponent } from './manage-role-requests-table.component';

describe('ManageRoleRequestsTableComponent', () => {
  let spectator: Spectator<ManageRoleRequestsTableComponent>;
  const createComponent = createComponentFactory(ManageRoleRequestsTableComponent);

  it('should create', () => {
    spectator = createComponent();

    expect(spectator.component).toBeTruthy();
  });
});
