import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-category-request-button',
  templateUrl: './category-request-button.component.html',
  styleUrls: ['./category-request-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoryRequestButtonComponent {
  @Input() public disabled: boolean | undefined = false;

  @Output() public createRequestClicked = new EventEmitter<void>();
}
