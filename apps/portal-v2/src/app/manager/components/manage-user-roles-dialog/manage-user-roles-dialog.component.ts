import { ChangeDetectionStrategy, Component, inject, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfidentialMemberProfileEntity, OrganizationCategory, OrganizationUserRoleV2 } from '@api/aaa/models';
import { convertBooleanToOrganizationRolesArray, convertOrganizationRolesToBooleanArray, ORGANIZATION_USER_ROLES } from './manage-user-roles-dialog.models';

@Component({
  selector: 'app-manage-user-roles-dialog',
  templateUrl: './manage-user-roles-dialog.component.html',
  styleUrls: ['./manage-user-roles-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManageUserRolesDialogComponent implements OnInit {
  private fb = inject(FormBuilder);
  public rolesForm = this.fb.nonNullable.group({
    roles: this.fb.nonNullable.array(
      ORGANIZATION_USER_ROLES.map(() => false)
    )
  });

  public readonly ORGANIZATION_USER_ROLES_SELECTION = ORGANIZATION_USER_ROLES;

  constructor(
    private dialogRef: MatDialogRef<ManageUserRolesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: {
      user: ConfidentialMemberProfileEntity | undefined;
      organizationCategories: OrganizationCategory[]; 
    }
  ) {}

  public ngOnInit(): void {
    this.setFields();
  }

  public get roles(): FormArray<FormControl<boolean>> {
    return this.rolesForm.controls.roles;
  }

  public onSubmit(): void {
    const roleValues = this.roles.controls.map(ctrl => ctrl.value);
    const roles = convertBooleanToOrganizationRolesArray(roleValues);
    this.dialogRef.close(roles);
  }

  public setFields(): void {
    this.rolesForm.patchValue({
      roles: convertOrganizationRolesToBooleanArray(this.data.user?.organization_user_roles)
    });
    if (!this.data.organizationCategories.includes(OrganizationCategory.APP_CUSTOMER)) {
      this.disableCheckbox(OrganizationUserRoleV2.PATHOLOGIST);
      this.disableCheckbox(OrganizationUserRoleV2.DATA_MANAGER);
    }
    if (!this.data.organizationCategories.includes(OrganizationCategory.APP_VENDOR)) {
      this.disableCheckbox(OrganizationUserRoleV2.APP_MAINTAINER);
    }
    if (!this.data.organizationCategories.includes(OrganizationCategory.PRODUCT_PROVIDER)) {
      this.disableCheckbox(OrganizationUserRoleV2.CLEARANCE_MAINTAINER);
    }
  }

  public disableCheckbox(type: OrganizationUserRoleV2): void {
    const roleIndex = Object.keys(OrganizationUserRoleV2).indexOf(type); 
    const checkbox = this.roles.at(roleIndex);
    if (checkbox) {
      checkbox?.setValue(false);
      checkbox?.disable();
    }
  }
}
