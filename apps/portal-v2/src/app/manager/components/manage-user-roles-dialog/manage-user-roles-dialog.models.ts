import { OrganizationUserRoleV2 } from '@api/aaa/models';

export const ORGANIZATION_USER_ROLES = Object.values(OrganizationUserRoleV2);

export function convertBooleanToOrganizationRolesArray(roles: boolean[]): OrganizationUserRoleV2[] {
  return ORGANIZATION_USER_ROLES.filter((_r, i) => roles[i]);
}

export function convertOrganizationRolesToBooleanArray(roles: OrganizationUserRoleV2[] | undefined): boolean[] {
  return ORGANIZATION_USER_ROLES.map(r => roles?.includes(r) ?? false);
}
