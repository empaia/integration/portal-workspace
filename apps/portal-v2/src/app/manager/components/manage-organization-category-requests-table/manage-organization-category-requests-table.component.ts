import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { RequestState } from '@api/aaa/models';
import { ManageOrganizationCategoryRequestEntity } from '@manager/store/manage-organization-category-requests';
import { AppRoutes } from '@router/router.constants';

@Component({
  selector: 'app-manage-organization-category-requests-table',
  templateUrl: './manage-organization-category-requests-table.component.html',
  styleUrls: ['./manage-organization-category-requests-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManageOrganizationCategoryRequestsTableComponent implements AfterViewInit {
  public readonly PUBLIC_ORG_ROUTE = AppRoutes.PUBLIC_ORG_ROUTE;

  private _requests!: ManageOrganizationCategoryRequestEntity[];
  @Input() public set organizationCategoryRequests(val: ManageOrganizationCategoryRequestEntity[] | undefined) {
    if (val) {
      this._requests = val;
      this.dataSource.data = val;
      this.dataSource.sort = this.sort;
    }
  }

  public get organizationCategoryRequests() {
    return this._requests;
  }

  @Output() public revokeRequestClicked = new EventEmitter<{ id: number }>();

  @ViewChild(MatSort) private sort!: MatSort;
  public dataSource = new MatTableDataSource<Partial<ManageOrganizationCategoryRequestEntity>>(
    [{ organization_id: '', organization_category_request_id: -1 }]
  );
  public displayColumns: string[] = [
    'organization_name',
    'email_address',
    'last_name',
    'first_name',
    'requested',
    'status',
    'actions'
  ];
  public readonly REQUEST_STATES = RequestState;

  public ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }
}
