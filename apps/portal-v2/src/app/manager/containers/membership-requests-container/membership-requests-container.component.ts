import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { ManagerMembershipRequestActions, ManagerMembershipRequestSelectors } from '@manager/store/manage-membership-request';
import { Store } from '@ngrx/store';
import { MembershipRequestEntity } from '@user/store/membership-request';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-membership-requests-container',
  templateUrl: './membership-requests-container.component.html',
  styleUrls: ['./membership-requests-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MembershipRequestsContainerComponent {
  private store = inject(Store);

  protected membershipRequests$: Observable<MembershipRequestEntity[]>;

  constructor() {

    this.store.dispatch(ManagerMembershipRequestActions.initializeManagerMembershipRequestPage());

    this.membershipRequests$ = this.store.select(ManagerMembershipRequestSelectors.selectManagerMembershipRequests);
  }


  acceptMembershipRequest(event: { id: number }) {
    this.store.dispatch(ManagerMembershipRequestActions.openApproveMemberDialog({ membershipRequestId: event.id }));
  }

  rejectMembershipRequest(event: { id: number }) {
    this.store.dispatch(ManagerMembershipRequestActions.openRejectMemberDialog({ membershipRequestId: event.id }));
  }
}
