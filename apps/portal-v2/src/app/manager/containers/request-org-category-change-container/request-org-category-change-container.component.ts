import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ManageOrganizationCategoryRequestEntity, ManageOrganizationCategoryRequestsActions, ManageOrganizationCategoryRequestsSelectors } from '@manager/store/manage-organization-category-requests';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-request-org-category-change-container',
  templateUrl: './request-org-category-change-container.component.html',
  styleUrls: ['./request-org-category-change-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequestOrgCategoryChangeContainerComponent {
  public categoryRequests$: Observable<ManageOrganizationCategoryRequestEntity[]>;
  public hasOpenRequests$: Observable<boolean>;

  constructor(private store: Store) {
    this.categoryRequests$ = this.store.select(ManageOrganizationCategoryRequestsSelectors.selectAllCategoryRequests);
    this.hasOpenRequests$ = this.store.select(ManageOrganizationCategoryRequestsSelectors.selectHasOpenRequests);

    this.store.dispatch(ManageOrganizationCategoryRequestsActions.initialLoadCategoryRequests());
  }

  public openCategoryRequestDialog(): void {
    this.store.dispatch(ManageOrganizationCategoryRequestsActions.openCategoryRequestDialog());
  }

  public openRevokeRequestDialog(event: { id: number }): void {
    this.store.dispatch(ManageOrganizationCategoryRequestsActions.openRevokeRequestDialog({ categoryRequestId: event.id }));
  }
}
