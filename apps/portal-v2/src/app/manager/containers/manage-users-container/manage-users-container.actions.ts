import { createActionGroup, emptyProps } from '@ngrx/store';

export const ManageUsersContainerActions = createActionGroup({
  source: 'ManageUserContainer',
  events: {
    'Initialize Mange User Containers': emptyProps(),
  }
});
