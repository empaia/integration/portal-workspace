import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { ManageOrgMembersSelectors, OrgMemberEntity } from '@manager/store/manage-org-members';
import { ManageOrgMembersActions } from '@manager/store/manage-org-members/manage-org-members.actions';
import { ManageRoleRequestActions, ManageRoleRequestEntity, ManageRoleRequestSelectors } from '@manager/store/manage-role-request';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ManageUsersContainerActions } from './manage-users-container.actions';

@Component({
  selector: 'app-manage-users-container',
  templateUrl: './manage-users-container.component.html',
  styleUrls: ['./manage-users-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManageUsersContainerComponent {

  private store = inject(Store);

  protected members$: Observable<OrgMemberEntity[]>;
  protected roleRequests$: Observable<ManageRoleRequestEntity[]>;


  constructor() {
    this.store.dispatch(ManageUsersContainerActions.initializeMangeUserContainers());

    this.members$ = this.store.select(ManageOrgMembersSelectors.selectManageOrgMembers);

    this.roleRequests$ = this.store.select(ManageRoleRequestSelectors.selectManagerRoleRequests);
  }

  public acceptRoleRequest(event: { id: number }): void {
    this.store.dispatch(ManageRoleRequestActions.openApproveRoleRequestDialog({ roleRequestId: event.id }));
  }

  public rejectRoleRequest(event: { id: number }): void {
    this.store.dispatch(ManageRoleRequestActions.openDenyRoleRequestDialog({ roleRequestId: event.id }));
  }

  public removeUserFromOrganization(event: { userId: string }): void {
    this.store.dispatch(ManageOrgMembersActions.openRemoveUserDialog({ userId: event.userId }));
  }

  public editUserOrganizationRoles(event: { userId: string }): void {
    this.store.dispatch(ManageOrgMembersActions.openEditRolesDialog({ userId: event.userId }));
  }
}
