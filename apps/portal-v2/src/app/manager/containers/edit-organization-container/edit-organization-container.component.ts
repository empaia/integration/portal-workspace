import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ManageOrganizationProfileActions, ManageOrganizationProfileEntity, ManageOrganizationProfileSelectors } from '@manager/store/manage-org-profile';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-edit-organization-container',
  templateUrl: './edit-organization-container.component.html',
  styleUrls: ['./edit-organization-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditOrganizationContainerComponent {
  public activeOrganization$: Observable<ManageOrganizationProfileEntity | undefined>;
  public isNameReady$: Observable<boolean>;
  public isDetailsReady$: Observable<boolean>;
  public isContactReady$: Observable<boolean>;

  constructor(private store: Store) {
    this.activeOrganization$ = this.store.select(ManageOrganizationProfileSelectors.selectActiveOrganizationProfile);
    this.isNameReady$ = this.store.select(ManageOrganizationProfileSelectors.selectIsNameReady);
    this.isDetailsReady$ = this.store.select(ManageOrganizationProfileSelectors.selectIsDetailsReady);
    this.isContactReady$ = this.store.select(ManageOrganizationProfileSelectors.selectIsContactDataReady);

    this.store.dispatch(ManageOrganizationProfileActions.loadActiveOrganization());
  }

  public onOrganizationLogoEdit(): void {
    this.store.dispatch(ManageOrganizationProfileActions.openEditOrganizationLogoDialog());
  }

  public onOrganizationNameEdit(): void {
    this.store.dispatch(ManageOrganizationProfileActions.openEditOrganizationNameDialog());
  }

  public onOrganizationDetailsEdit(): void {
    this.store.dispatch(ManageOrganizationProfileActions.openEditOrganizationDetailsDialog());
  }

  public onOrganizationContactEdit(): void {
    this.store.dispatch(ManageOrganizationProfileActions.openEditOrganizationContactDialog());
  }
}
