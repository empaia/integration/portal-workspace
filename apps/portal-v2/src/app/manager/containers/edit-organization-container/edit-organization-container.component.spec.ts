import { EditOrganizationOverviewComponent } from '@edit-organization/components/edit-organization-overview/edit-organization-overview.component';
import { EditOrganizationStatusIndicatorComponent } from '@edit-organization/components/edit-organization-status-indicator/edit-organization-status-indicator.component';
import { ManageOrganizationProfileSelectors } from '@manager/store/manage-org-profile';
import { Spectator, createComponentFactory } from '@ngneat/spectator';
import { TranslocoDirective } from '@ngneat/transloco';
import { LetDirective } from '@ngrx/component';
import { provideMockStore } from '@ngrx/store/testing';
import { MockComponents, MockDirectives } from 'ng-mocks';

import { EditOrganizationContainerComponent } from './edit-organization-container.component';

describe('EditOrganizationContainerComponent', () => {
  let spectator: Spectator<EditOrganizationContainerComponent>;
  const createComponent = createComponentFactory({
    component: EditOrganizationContainerComponent,
    declarations: [
      MockDirectives(
        TranslocoDirective,
        LetDirective,
      ),
      MockComponents(
        EditOrganizationStatusIndicatorComponent,
        EditOrganizationOverviewComponent
      )
    ],
    providers: [
      provideMockStore({
        selectors: [
          {
            selector: ManageOrganizationProfileSelectors.selectActiveOrganizationProfile,
            value: {}
          }
        ]
      })
    ]
  });

  it('should create', () => {
    spectator = createComponent();

    expect(spectator.component).toBeTruthy();
  });
});
