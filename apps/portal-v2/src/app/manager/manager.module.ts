import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { ManagerRoutingModule } from './manager-routing.module';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '@material/material.module';

import { ManagerRootStoreModule } from './store/manager-root-store.module';

import { ManageOrganizationMembersTableComponent } from './components/manage-organization-members-table/manage-organization-members-table.component';
import { ManageMembershipRequestsTableComponent } from './components/manage-membership-requests-table/manage-membership-requests-table.component';
import { EditOrganizationContainerComponent } from './containers/edit-organization-container/edit-organization-container.component';
import { MembershipRequestsContainerComponent } from './containers/membership-requests-container/membership-requests-container.component';
import { ManageUsersContainerComponent } from './containers/manage-users-container/manage-users-container.component';
import { RequestOrgCategoryChangeContainerComponent } from './containers/request-org-category-change-container/request-org-category-change-container.component';
import { ManageRoleRequestsTableComponent } from './components/manage-role-requests-table/manage-role-requests-table.component';
import { EditOrganizationModule } from '@edit-organization/edit-organization.module';
import { ManageUserRolesDialogComponent } from './components/manage-user-roles-dialog/manage-user-roles-dialog.component';
import { ManageOrganizationCategoryRequestsTableComponent } from './components/manage-organization-category-requests-table/manage-organization-category-requests-table.component';
import { CategoryRequestButtonComponent } from './components/category-request-button/category-request-button.component';
import { ManageCreateCategoryRequestDialogComponent } from './components/manage-create-category-request-dialog/manage-create-category-request-dialog.component';


@NgModule({
  declarations: [
    ManageMembershipRequestsTableComponent,
    MembershipRequestsContainerComponent,
    ManageUsersContainerComponent,
    ManageOrganizationMembersTableComponent,
    EditOrganizationContainerComponent,
    RequestOrgCategoryChangeContainerComponent,
    ManageRoleRequestsTableComponent,
    ManageUserRolesDialogComponent,
    ManageOrganizationCategoryRequestsTableComponent,
    CategoryRequestButtonComponent,
    ManageCreateCategoryRequestDialogComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    RouterModule,
    ManagerRoutingModule,
    ManagerRootStoreModule,
    EditOrganizationModule,
  ]
})
export class ManagerModule { }
