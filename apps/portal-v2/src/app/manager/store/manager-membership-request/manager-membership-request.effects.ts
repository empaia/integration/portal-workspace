import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';

import { exhaustMap, map } from 'rxjs/operators';
import { ManagerMembershipRequestActions } from './manage-membership-request.actions';
import { ManagerControllerService } from '@api/aaa/services';
import { HttpErrorResponse } from '@angular/common/http';
import { filterNullish } from '@shared/utility/rxjs-utility';
import { State } from './manage-membership-request.reducer';
import { defaultPaginationParams } from '@shared/api/default-params';
import { MatDialog } from '@angular/material/dialog';
import { AcceptDialogComponent } from '@shared/components/accept-dialog/accept-dialog.component';
import { SMALL_DIALOG_WIDTH } from '@edit-organization/models/edit-organization.models';
import {
  DEFAULT_ACCEPT_MEMBERSHIP_REQUESTS_DIALOG_TEXT,
  DEFAULT_DENY_MEMBERSHIP_REQUESTS_DIALOG_TEXT,
} from './manage-membership-request.model';
import { DenyDialogComponent } from '@shared/components/deny-dialog/deny-dialog.component';
import { EventActions } from 'src/app/events/store';

@Injectable()
export class ManagerMembershipRequestEffects {
  loadManagerMembershipRequests$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManagerMembershipRequestActions.load),
      fetch({
        run: (
          _action: ReturnType<typeof ManagerMembershipRequestActions.load>,
          _state: State
        ) => {
          return this.managerControllerService
            .getMembershipRequestsOfOrganization({
              ...defaultPaginationParams,
            })
            .pipe(
              map((response) => response.content),
              filterNullish(),
              map((requests) =>
                ManagerMembershipRequestActions.loadSuccess({
                  requests: requests,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof ManagerMembershipRequestActions.load>,
          error: HttpErrorResponse
        ) => {
          return ManagerMembershipRequestActions.loadFailure({ error });
        },
      })
    );
  });

  initializeMyOrganizationsPage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ManagerMembershipRequestActions.initializeManagerMembershipRequestPage
      ),
      map(() => ManagerMembershipRequestActions.load())
    );
  });

  refreshOnSse$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(EventActions.sseUpdateMembershipRequests),
      map(() => ManagerMembershipRequestActions.load())
    );
  });

  acceptRequest$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManagerMembershipRequestActions.approve),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof ManagerMembershipRequestActions.approve>,
          _state: State
        ) => {
          return this.managerControllerService
            .acceptMembershipRequest({
              membership_request_id: action.membershipRequestId,
            })
            .pipe(
              // remove from state after request was successful
              map(() =>
                ManagerMembershipRequestActions.approveSuccess({ ...action })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof ManagerMembershipRequestActions.approve>,
          error: HttpErrorResponse
        ) => {
          return ManagerMembershipRequestActions.approveFailure({ error });
        },
      })
    );
  });

  rejectRequest$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManagerMembershipRequestActions.reject),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof ManagerMembershipRequestActions.reject>,
          _state: State
        ) => {
          return this.managerControllerService
            .denyMembershipRequest({
              membership_request_id: action.membershipRequestId,
              body: {
                reviewer_comment: action.comment,
              },
            })
            .pipe(
              // remove from state after request was successful
              map(() =>
                ManagerMembershipRequestActions.rejectSuccess({ ...action })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof ManagerMembershipRequestActions.reject>,
          error: HttpErrorResponse
        ) => {
          return ManagerMembershipRequestActions.rejectFailure({ error });
        },
      })
    );
  });

  openAcceptDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManagerMembershipRequestActions.openApproveMemberDialog),
      map((action) => action.membershipRequestId),
      exhaustMap((membershipRequestId) =>
        this.dialog
          .open(AcceptDialogComponent, {
            width: SMALL_DIALOG_WIDTH,
            data: DEFAULT_ACCEPT_MEMBERSHIP_REQUESTS_DIALOG_TEXT,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map(() =>
              ManagerMembershipRequestActions.approve({ membershipRequestId })
            )
          )
      )
    );
  });

  openRejectDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManagerMembershipRequestActions.openRejectMemberDialog),
      map((action) => action.membershipRequestId),
      exhaustMap((membershipRequestId) =>
        this.dialog
          .open(DenyDialogComponent, {
            width: SMALL_DIALOG_WIDTH,
            data: DEFAULT_DENY_MEMBERSHIP_REQUESTS_DIALOG_TEXT,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map((comment) =>
              ManagerMembershipRequestActions.reject({
                membershipRequestId,
                comment,
              })
            )
          )
      )
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly managerControllerService: ManagerControllerService,
    private readonly dialog: MatDialog
  ) {}
}
