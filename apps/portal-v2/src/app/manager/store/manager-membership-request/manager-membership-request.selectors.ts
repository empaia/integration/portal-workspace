import { createSelector } from '@ngrx/store';
import { selectManagerRootState } from '../manager-root.selectors';
import {
  MANAGER_MEMBERSHIP_REQUEST_FEATURE_KEY,
  managerMembershipRequestAdapter
} from './manage-membership-request.reducer';

export const {
  selectAll,
  selectEntities
} = managerMembershipRequestAdapter.getSelectors();

// membership requests
const selectManagerMembershipRequestState = createSelector(
  selectManagerRootState,
  (state) => state[MANAGER_MEMBERSHIP_REQUEST_FEATURE_KEY]
);

export const selectManagerMembershipRequests = createSelector(
  selectManagerMembershipRequestState,
  selectAll
);
