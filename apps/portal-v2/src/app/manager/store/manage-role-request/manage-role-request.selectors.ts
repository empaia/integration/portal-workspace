import { createSelector } from '@ngrx/store';
import { selectManagerRootState } from '../manager-root.selectors';
import { manageRoleRequestAdapter, MANAGE_ROLE_REQUEST_FEATURE_KEY } from './manage-role-request.reducer';

export const {
  selectAll,
  selectEntities
} = manageRoleRequestAdapter.getSelectors();


// org members
const selectManageRoleRequestState = createSelector(
  selectManagerRootState,
  (state) => state[MANAGE_ROLE_REQUEST_FEATURE_KEY]
);

export const selectManagerRoleRequests = createSelector(
  selectManageRoleRequestState,
  selectAll
);
