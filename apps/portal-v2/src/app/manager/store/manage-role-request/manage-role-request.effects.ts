import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';

import { exhaustMap, map } from 'rxjs/operators';
import { ManageRoleRequestActions } from './manage-role-request.actions';
import { ManagerControllerService } from '@api/aaa/services';
import { HttpErrorResponse } from '@angular/common/http';
import { filterNullish } from '@shared/utility/rxjs-utility';
import { State } from './manage-role-request.reducer';
import { defaultPaginationParams } from '@shared/api/default-params';
import { MatDialog } from '@angular/material/dialog';
import { AcceptDialogComponent } from '@shared/components/accept-dialog/accept-dialog.component';
import { SMALL_DIALOG_WIDTH } from '@edit-organization/models/edit-organization.models';
import {
  DEFAULT_ACCEPT_ORGANIZATION_ROLE_REQUESTS_DIALOG_TEXT,
  DEFAULT_DENY_ORGANIZATION_ROLE_CATEGORY_REQUESTS_DIALOG_TEXT,
} from './manage-role-request.model';
import { DenyDialogComponent } from '@shared/components/deny-dialog/deny-dialog.component';
import { EventActions } from 'src/app/events/store';
import { ManageUsersContainerActions } from '@manager/containers/manage-users-container/manage-users-container.actions';

@Injectable()
export class ManageRoleRequestEffects {
  initializeMyOrganizationsPage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ManageRoleRequestActions.initializeManageRoleRequestPage,
        ManageUsersContainerActions.initializeMangeUserContainers,
      ),
      map(() => ManageRoleRequestActions.load())
    );
  });

  sseRefresh$ = createEffect(() => {
    // TODO: filter on event action type
    return this.actions$.pipe(
      ofType(EventActions.sseUpdateRoleRequests),
      map(() => ManageRoleRequestActions.load())
    );
  });

  loadManageRoleRequests$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManageRoleRequestActions.load),
      fetch({
        run: (
          _action: ReturnType<typeof ManageRoleRequestActions.load>,
          _state: State
        ) => {
          return this.managerControllerService
            .getOrganizationUserRoleRequestsOfOrganization({
              ...defaultPaginationParams,
            })
            .pipe(
              map((response) => response.organization_user_role_requests),
              // tap(content => console.log('getManageRoleRequests', content)),
              map((requests) =>
                ManageRoleRequestActions.loadSuccess({ requests: requests })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof ManageRoleRequestActions.load>,
          error: HttpErrorResponse
        ) => {
          return ManageRoleRequestActions.loadFailure({ error });
        },
      })
    );
  });

  acceptRequest$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManageRoleRequestActions.approve),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof ManageRoleRequestActions.approve>,
          _state: State
        ) => {
          return this.managerControllerService
            .acceptOrganizationUserRoleRequest({
              role_request_id: action.roleRequestId,
            })
            .pipe(
              // remove from state when request was successful
              map(() => ManageRoleRequestActions.approveSuccess({ ...action }))
            );
        },
        onError: (
          _action: ReturnType<typeof ManageRoleRequestActions.approve>,
          error: HttpErrorResponse
        ) => {
          return ManageRoleRequestActions.approveFailure({ error });
        },
      })
    );
  });

  rejectRequest$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManageRoleRequestActions.reject),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof ManageRoleRequestActions.reject>,
          _state: State
        ) => {
          return this.managerControllerService
            .denyOrganizationUserRoleRequest({
              role_request_id: action.roleRequestId,
              body: {
                reviewer_comment: action.comment,
              },
            })
            .pipe(
              // remove from state when request was successful
              map(() => ManageRoleRequestActions.rejectSuccess({ ...action }))
            );
        },
        onError: (
          _action: ReturnType<typeof ManageRoleRequestActions.reject>,
          error: HttpErrorResponse
        ) => {
          return ManageRoleRequestActions.rejectFailure({ error });
        },
      })
    );
  });

  openAcceptDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManageRoleRequestActions.openApproveRoleRequestDialog),
      map((action) => action.roleRequestId),
      exhaustMap((roleRequestId) =>
        this.dialog
          .open(AcceptDialogComponent, {
            width: SMALL_DIALOG_WIDTH,
            data: DEFAULT_ACCEPT_ORGANIZATION_ROLE_REQUESTS_DIALOG_TEXT,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map(() => ManageRoleRequestActions.approve({ roleRequestId }))
          )
      )
    );
  });

  openRejectDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManageRoleRequestActions.openDenyRoleRequestDialog),
      map((action) => action.roleRequestId),
      exhaustMap((roleRequestId) =>
        this.dialog
          .open(DenyDialogComponent, {
            width: SMALL_DIALOG_WIDTH,
            data: DEFAULT_DENY_ORGANIZATION_ROLE_CATEGORY_REQUESTS_DIALOG_TEXT,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map((comment) =>
              ManageRoleRequestActions.reject({
                roleRequestId,
                comment,
              })
            )
          )
      )
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly managerControllerService: ManagerControllerService,
    private readonly dialog: MatDialog
  ) {}
}
