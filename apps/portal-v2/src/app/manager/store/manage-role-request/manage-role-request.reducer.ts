import { HttpErrorResponse } from '@angular/common/http';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { ManageRoleRequestActions } from './manage-role-request.actions';
import { ManageRoleRequestEntity } from './manage-role-request.model';

export const MANAGE_ROLE_REQUEST_FEATURE_KEY = 'manageRoleRequest';

export interface State extends EntityState<ManageRoleRequestEntity> {
  loaded: boolean;
  error?: HttpErrorResponse | undefined;
}

export const manageRoleRequestAdapter = createEntityAdapter<ManageRoleRequestEntity>({
  selectId: request => request.organization_user_role_request_id
});

export const initialState: State = manageRoleRequestAdapter.getInitialState({
  loaded: false,
  error: undefined,
});

export const reducer = createReducer(
  initialState,

  on(ManageRoleRequestActions.load, (state): State => ({
    ...state,
    loaded: false,
  })),

  on(ManageRoleRequestActions.loadSuccess, (state, { requests }): State =>
    manageRoleRequestAdapter.setAll(requests, {
      ...state,
      loaded: true,
    })
  ),

  on(ManageRoleRequestActions.approve, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ManageRoleRequestActions.approveSuccess, (state, { roleRequestId }): State =>
    manageRoleRequestAdapter.removeOne(roleRequestId, {
      ...state,
      loaded: true,
    })
  ),
  on(ManageRoleRequestActions.approveFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ManageRoleRequestActions.reject, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ManageRoleRequestActions.rejectSuccess, (state, { roleRequestId }): State =>
    manageRoleRequestAdapter.removeOne(roleRequestId, {
      ...state,
      loaded: true,
    })
  ),
  on(ManageRoleRequestActions.rejectFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  }))
);
