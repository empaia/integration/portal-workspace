import { HttpErrorResponse } from '@angular/common/http';
import {  createActionGroup, emptyProps, props } from '@ngrx/store';
import { ManageRoleRequestEntity } from './manage-role-request.model';


export const ManageRoleRequestActions = createActionGroup({
  source: 'ManagerRoleRequest',
  events: {
    'Load': emptyProps(),
    'Load Success': props<{ requests: ManageRoleRequestEntity[] }>(),
    'Load Failure': props<{ error: HttpErrorResponse }>(),

    'Initialize Manage Role Request Page': emptyProps(),

    'Approve': props<{ roleRequestId: number }>(),
    'Approve Success': props<{ roleRequestId: number }>(),
    'Approve Failure': props<{ error: HttpErrorResponse }>(),
    'Reject': props<{
      roleRequestId: number,
      comment: string,
    }>(),
    'Reject Success': props<{ roleRequestId: number }>(),
    'Reject Failure': props<{ error: HttpErrorResponse }>(),

    'Open Approve Role Request Dialog': props<{ roleRequestId: number }>(),
    'Open Deny Role Request Dialog': props<{ roleRequestId: number }>(),
  },
});
