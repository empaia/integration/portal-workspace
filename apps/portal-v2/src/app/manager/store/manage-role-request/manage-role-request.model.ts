import { OrganizationUserRoleRequestEntity as ApiManageRoleRequestEntity} from '@api/aaa/models';
import { AcceptDialogEntity } from '@shared/components/accept-dialog/accept-dialog.component';
import { DenyDialogEntity } from '@shared/components/deny-dialog/deny-dialog.component';
export type ManageRoleRequestEntity = ApiManageRoleRequestEntity;


export const DEFAULT_ACCEPT_ORGANIZATION_ROLE_REQUESTS_DIALOG_TEXT: AcceptDialogEntity = {
  headlineText: 'manage_forms.accept_organization_role_request',
  contentText: 'manage_forms.accept_organization_role_request_confirmation',
  confirmButtonText: 'general_forms.confirm',
  cancelButtonText: 'general_forms.cancel',
};

export const DEFAULT_DENY_ORGANIZATION_ROLE_CATEGORY_REQUESTS_DIALOG_TEXT: DenyDialogEntity = {
  headlineText: 'manage_forms.deny_organization_role_request',
  reviewLabel: 'manage_forms.review_organization_role_request',
  confirmButtonText: 'general_forms.confirm',
  cancelButtonText: 'general_forms.cancel'
};
