import { HttpErrorResponse } from '@angular/common/http';
import { ConfidentialOrganizationContactDataEntity } from '@api/aaa/models';
import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { ManageOrganizationProfileEntity } from './manage-org-profile.models';
import { PostOrganizationDetailsWithoutCategories } from '@edit-organization/models/edit-organization.models';

export const ManageOrganizationProfileActions = createActionGroup({
  source: 'ManageOrganizationProfile',
  events: {
    'Load Active Organization': emptyProps(),
    'Load Active Organization Success': props<{ organization: ManageOrganizationProfileEntity }>(),
    'Load Active Organization Failure': props<{ error: HttpErrorResponse }>(),

    'Set Organization Contact Data': props<{ contactData: ConfidentialOrganizationContactDataEntity }>(),
    'Set Organization Contact Data Success': props<{ organization: ManageOrganizationProfileEntity }>(),
    'Set Organization Contact Data Failure': props<{ error: HttpErrorResponse }>(),
    'Set Organization Details': props<{ details: PostOrganizationDetailsWithoutCategories }>(),
    'Set Organization Details Success': props<{ organization: ManageOrganizationProfileEntity }>(),
    'Set Organization Details Failure': props<{ error: HttpErrorResponse }>(),
    'Set Organization Logo': props<{ logo: Blob }>(),
    'Set Organization Logo Success': props<{ organization: ManageOrganizationProfileEntity }>(),
    'Set Organization Logo Failure': props<{ error: HttpErrorResponse }>(),
    'Set Organization Name': props<{ organizationName: string }>(),
    'Set Organization Name Success': props<{ organization: ManageOrganizationProfileEntity }>(),
    'Set Organization Name Failure': props<{ error: HttpErrorResponse }>(),

    'Open Edit Organization Logo Dialog': emptyProps(),
    'Open Edit Organization Name Dialog': emptyProps(),
    'Open Edit Organization Details Dialog': emptyProps(),
    'Open Edit Organization Contact Dialog': emptyProps(),
  }
});
