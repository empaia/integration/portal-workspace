import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import { ManageOrganizationProfileActions } from './manage-org-profile.actions';
import { ManageOrganizationProfileEntity } from './manage-org-profile.models';


export const MANAGE_ORG_PROFILE_FEATURE_KEY = 'manageOrgProfile';

export interface State {
  organization: ManageOrganizationProfileEntity | undefined;
  loaded: boolean;
  error?: HttpErrorResponse | undefined;
}

export const initialState: State = {
  organization: undefined,
  loaded: true,
  error: undefined,
};

export const reducer = createReducer(
  initialState,
  on(ManageOrganizationProfileActions.loadActiveOrganization, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ManageOrganizationProfileActions.loadActiveOrganizationSuccess, (state, { organization }): State => ({
    ...state,
    organization,
    loaded: true,
  })),
  on(ManageOrganizationProfileActions.loadActiveOrganizationFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ManageOrganizationProfileActions.setOrganizationContactData, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ManageOrganizationProfileActions.setOrganizationContactDataSuccess, (state, { organization }): State => ({
    ...state,
    organization,
    loaded: true,
  })),
  on(ManageOrganizationProfileActions.setOrganizationContactDataFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ManageOrganizationProfileActions.setOrganizationDetails, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ManageOrganizationProfileActions.setOrganizationDetailsSuccess, (state, { organization }): State => ({
    ...state,
    organization,
    loaded: true,
  })),
  on(ManageOrganizationProfileActions.setOrganizationDetailsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ManageOrganizationProfileActions.setOrganizationLogo, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ManageOrganizationProfileActions.setOrganizationLogoSuccess, (state, { organization }): State => ({
    ...state,
    organization,
    loaded: true,
  })),
  on(ManageOrganizationProfileActions.setOrganizationLogoFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ManageOrganizationProfileActions.setOrganizationName, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ManageOrganizationProfileActions.setOrganizationNameSuccess, (state, { organization }): State => ({
    ...state,
    loaded: true,
    organization,
  })),
  on(ManageOrganizationProfileActions.setOrganizationNameFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  }))
);
