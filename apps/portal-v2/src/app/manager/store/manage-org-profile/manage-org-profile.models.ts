import { ConfidentialOrganizationProfileEntity } from "@api/aaa/models";

export type ManageOrganizationProfileEntity = ConfidentialOrganizationProfileEntity;
