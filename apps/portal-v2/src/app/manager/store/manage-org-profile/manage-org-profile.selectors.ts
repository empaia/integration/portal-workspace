import { checkOrganizationContactDataStatus, checkOrganizationDetailsStatus, checkOrganizationNameStatus } from '@edit-organization/models/edit-organization.models';
import { createSelector } from '@ngrx/store';
import { selectManagerRootState } from '../manager-root.selectors';
import { MANAGE_ORG_PROFILE_FEATURE_KEY } from './manage-org-profile.reducer';

export const selectManageOrgProfileState = createSelector(
  selectManagerRootState,
  (state) => state[MANAGE_ORG_PROFILE_FEATURE_KEY]
);

export const selectActiveOrganizationProfile = createSelector(
  selectManageOrgProfileState,
  (state) => state.organization
);

export const selectActiveOrganizationProfileLoaded = createSelector(
  selectManageOrgProfileState,
  (state) => state.loaded
);

export const selectActiveOrganizationProfileError = createSelector(
  selectManageOrgProfileState,
  (state) => state.error
);

export const selectIsContactDataReady = createSelector(
  selectActiveOrganizationProfile,
  (organization) => checkOrganizationContactDataStatus(organization)
);

export const selectIsDetailsReady = createSelector(
  selectActiveOrganizationProfile,
  (organization) => checkOrganizationDetailsStatus(organization)
);

export const selectIsNameReady = createSelector(
  selectActiveOrganizationProfile,
  (organization) => checkOrganizationNameStatus(organization)
);
