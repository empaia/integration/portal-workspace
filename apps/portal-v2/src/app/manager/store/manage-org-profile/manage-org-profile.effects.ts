import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ManagerControllerService } from '@api/aaa/services';
import { EditOrganizationContactDialogComponent } from '@edit-organization/components/edit-organization-contact-dialog/edit-organization-contact-dialog.component';
import {
  EditOrganizationDetailsDialogComponent,
  EditOrganizationDetailsDialogData,
} from '@edit-organization/components/edit-organization-details-dialog/edit-organization-details-dialog.component';
import { EditOrganizationLogoDialogComponent } from '@edit-organization/components/edit-organization-logo-dialog/edit-organization-logo-dialog.component';
import { EditOrganizationNameDialogComponent } from '@edit-organization/components/edit-organization-name-dialog/edit-organization-name-dialog.component';
import {
  BIG_DIALOG_WIDTH,
  SMALL_DIALOG_WIDTH,
} from '@edit-organization/models/edit-organization.models';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';
import { CountriesSelectors } from '@shared/store/countries';
import { filterNullish } from '@shared/utility/rxjs-utility';
import { exhaustMap, filter, map } from 'rxjs/operators';
import { ManageOrganizationProfileActions } from './manage-org-profile.actions';
import { State } from './manage-org-profile.reducer';
import * as ManageOrganizationProfileSelectors from './manage-org-profile.selectors';

@Injectable()
export class ManageOrgProfileEffects {
  loadActiveOrganization$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManageOrganizationProfileActions.loadActiveOrganization),
      fetch({
        run: (
          _action: ReturnType<
            typeof ManageOrganizationProfileActions.loadActiveOrganization
          >,
          _state: State
        ) => {
          return this.managerControllerService.getOrganizationProfile().pipe(
            map((organization) =>
              ManageOrganizationProfileActions.loadActiveOrganizationSuccess({
                organization,
              })
            )
          );
        },
        onError: (
          _action: ReturnType<
            typeof ManageOrganizationProfileActions.loadActiveOrganization
          >,
          error: HttpErrorResponse
        ) => {
          return ManageOrganizationProfileActions.loadActiveOrganizationFailure(
            { error }
          );
        },
      })
    );
  });

  setOrganizationContactData$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManageOrganizationProfileActions.setOrganizationContactData),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof ManageOrganizationProfileActions.setOrganizationContactData
          >,
          _state: State
        ) => {
          return this.managerControllerService
            .setOrganizationContactData({
              body: action.contactData,
            })
            .pipe(
              map((organization) =>
                ManageOrganizationProfileActions.setOrganizationContactDataSuccess(
                  { organization }
                )
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ManageOrganizationProfileActions.setOrganizationContactData
          >,
          error: HttpErrorResponse
        ) => {
          return ManageOrganizationProfileActions.setOrganizationContactDataFailure(
            { error }
          );
        },
      })
    );
  });

  setOrganizationDetails$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManageOrganizationProfileActions.setOrganizationDetails),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof ManageOrganizationProfileActions.setOrganizationDetails
          >,
          _state: State
        ) => {
          return this.managerControllerService
            .setOrganizationDetails({
              body: action.details,
            })
            .pipe(
              map((organization) =>
                ManageOrganizationProfileActions.setOrganizationDetailsSuccess({
                  organization,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ManageOrganizationProfileActions.setOrganizationDetails
          >,
          error: HttpErrorResponse
        ) => {
          return ManageOrganizationProfileActions.setOrganizationDetailsFailure(
            { error }
          );
        },
      })
    );
  });

  setOrganizationLogo$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManageOrganizationProfileActions.setOrganizationLogo),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof ManageOrganizationProfileActions.setOrganizationLogo
          >,
          _state: State
        ) => {
          return this.managerControllerService
            .setOrganizationLogo({
              body: {
                logo: action.logo,
              },
            })
            .pipe(
              map((organization) =>
                ManageOrganizationProfileActions.setOrganizationLogoSuccess({
                  organization,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ManageOrganizationProfileActions.setOrganizationLogo
          >,
          error: HttpErrorResponse
        ) => {
          return ManageOrganizationProfileActions.setOrganizationLogoFailure({
            error,
          });
        },
      })
    );
  });

  setOrganizationName$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManageOrganizationProfileActions.setOrganizationName),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof ManageOrganizationProfileActions.setOrganizationName
          >,
          _state: State
        ) => {
          return this.managerControllerService
            .setOrganizationName({
              body: {
                organization_name: action.organizationName,
              },
            })
            .pipe(
              map((organization) =>
                ManageOrganizationProfileActions.setOrganizationNameSuccess({
                  organization,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ManageOrganizationProfileActions.setOrganizationName
          >,
          error
        ) => {
          return ManageOrganizationProfileActions.setOrganizationNameFailure({
            error,
          });
        },
      })
    );
  });

  openActiveOrganizationLogoDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManageOrganizationProfileActions.openEditOrganizationLogoDialog),
      concatLatestFrom(() =>
        this.store.select(
          ManageOrganizationProfileSelectors.selectActiveOrganizationProfile
        )
      ),
      map(([, organization]) => organization),
      filterNullish(),
      exhaustMap((organization) =>
        this.dialog
          .open(EditOrganizationLogoDialogComponent, {
            data: organization.logo_url,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map((logo) =>
              ManageOrganizationProfileActions.setOrganizationLogo({
                logo,
              })
            )
          )
      )
    );
  });

  openActiveOrganizationNameDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManageOrganizationProfileActions.openEditOrganizationNameDialog),
      concatLatestFrom(() =>
        this.store.select(
          ManageOrganizationProfileSelectors.selectActiveOrganizationProfile
        )
      ),
      map(([, organization]) => organization),
      filterNullish(),
      exhaustMap((organization) =>
        this.dialog
          .open(EditOrganizationNameDialogComponent, {
            data: organization.organization_name,
            width: SMALL_DIALOG_WIDTH,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map((organizationName) =>
              ManageOrganizationProfileActions.setOrganizationName({
                organizationName,
              })
            )
          )
      )
    );
  });

  openActiveOrganizationDetailsDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ManageOrganizationProfileActions.openEditOrganizationDetailsDialog
      ),
      concatLatestFrom(() =>
        this.store.select(
          ManageOrganizationProfileSelectors.selectActiveOrganizationProfile
        )
      ),
      map(([, organization]) => organization),
      filterNullish(),
      exhaustMap((organization) =>
        this.dialog
          .open(EditOrganizationDetailsDialogComponent, {
            data: <EditOrganizationDetailsDialogData>{
              details: organization.details,
              editableCategories: false,
            },
            width: BIG_DIALOG_WIDTH,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map((details) =>
              ManageOrganizationProfileActions.setOrganizationDetails({
                details,
              })
            )
          )
      )
    );
  });

  openActiveOrganizationContactDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ManageOrganizationProfileActions.openEditOrganizationContactDialog
      ),
      concatLatestFrom(() => [
        this.store.select(
          ManageOrganizationProfileSelectors.selectActiveOrganizationProfile
        ),
        this.store.select(CountriesSelectors.selectAllCountries),
      ]),
      map(([, organization, countries]) => ({ organization, countries })),
      filter((data) => !!data.organization),
      exhaustMap((data) =>
        this.dialog
          .open(EditOrganizationContactDialogComponent, {
            data: {
              contactData: data.organization?.contact_data,
              countries: data.countries,
            },
            width: BIG_DIALOG_WIDTH,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map((contactData) =>
              ManageOrganizationProfileActions.setOrganizationContactData({
                contactData,
              })
            )
          )
      )
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly dialog: MatDialog,
    private readonly managerControllerService: ManagerControllerService
  ) {}
}
