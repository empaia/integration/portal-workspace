import { HttpErrorResponse } from '@angular/common/http';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { defaultPaginationParams, PaginationParameters } from '@shared/api/default-params';
import { ManageOrganizationCategoryRequestsActions } from './manage-organization-category-requests.actions';
import { ManageOrganizationCategoryRequestEntity } from './manage-organization-category-requests.models';


export const MANAGE_ORGANIZATION_CATEGORY_REQUESTS_FEATURE_KEY = 'manageOrganizationCategoryRequests';

export interface State extends EntityState<ManageOrganizationCategoryRequestEntity> {
  loaded: boolean;
  page: PaginationParameters;
  error?: HttpErrorResponse | undefined;
}

export const manageCategoryRequestsAdapter = createEntityAdapter<ManageOrganizationCategoryRequestEntity>({
  selectId: request => request.organization_category_request_id
});

export const initialState: State = manageCategoryRequestsAdapter.getInitialState({
  loaded: true,
  page: defaultPaginationParams,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(ManageOrganizationCategoryRequestsActions.loadAllCategoryRequests, (state, { page }): State => ({
    ...state,
    loaded: false,
    page,
  })),
  on(ManageOrganizationCategoryRequestsActions.loadAllCategoryRequestsSuccess, (state, { requests }): State =>
    manageCategoryRequestsAdapter.setAll(requests, {
      ...state,
      loaded: true,
    })
  ),
  on(ManageOrganizationCategoryRequestsActions.loadAllCategoryRequestsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ManageOrganizationCategoryRequestsActions.sendCategoryRequest, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ManageOrganizationCategoryRequestsActions.sendCategoryRequestSuccess, (state, { request }): State =>
    manageCategoryRequestsAdapter.upsertOne(request, {
      ...state,
      loaded: true,
    })
  ),
  on(ManageOrganizationCategoryRequestsActions.sendCategoryRequestFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ManageOrganizationCategoryRequestsActions.revokeCategoryRequest, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ManageOrganizationCategoryRequestsActions.revokeCategoryRequestSuccess, (state, { request }): State =>
    manageCategoryRequestsAdapter.upsertOne(request, {
      ...state,
      loaded: true,
    })
  ),
  on(ManageOrganizationCategoryRequestsActions.revokeCategoryRequestFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
);
