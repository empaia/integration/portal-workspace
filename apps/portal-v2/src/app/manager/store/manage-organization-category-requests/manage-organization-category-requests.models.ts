import { OrganizationCategoryRequestEntity } from '@api/aaa/models';

export type ManageOrganizationCategoryRequestEntity = OrganizationCategoryRequestEntity;
