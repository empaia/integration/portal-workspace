import { RequestState } from '@api/aaa/models';
import { createSelector } from '@ngrx/store';
import { selectManagerRootState } from '../manager-root.selectors';
import {
  manageCategoryRequestsAdapter,
  MANAGE_ORGANIZATION_CATEGORY_REQUESTS_FEATURE_KEY
} from './manage-organization-category-requests.reducer';

const {
  selectAll,
  selectEntities,
} = manageCategoryRequestsAdapter.getSelectors();

export const selectManageOrganizationCategoryRequestsState = createSelector(
  selectManagerRootState,
  (state) => state[MANAGE_ORGANIZATION_CATEGORY_REQUESTS_FEATURE_KEY]
);

export const selectAllCategoryRequests = createSelector(
  selectManageOrganizationCategoryRequestsState,
  selectAll,
);

export const selectCategoryRequestEntities = createSelector(
  selectManageOrganizationCategoryRequestsState,
  selectEntities,
);

export const selectHasOpenRequests = createSelector(
  selectAllCategoryRequests,
  (requests) => requests.some(r => r.request_state === RequestState.REQUESTED)
);

export const selectCategoryRequestsLoaded = createSelector(
  selectManageOrganizationCategoryRequestsState,
  (state) => state.loaded
);

export const selectCategoryRequestsError = createSelector(
  selectManageOrganizationCategoryRequestsState,
  (state) => state.error
);
