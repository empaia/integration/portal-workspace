import { HttpErrorResponse } from '@angular/common/http';
import { PostOrganizationCategoryRequestEntity } from '@api/aaa/models';
import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { PaginationParameters } from '@shared/api/default-params';
import { ManageOrganizationCategoryRequestEntity } from './manage-organization-category-requests.models';

export const ManageOrganizationCategoryRequestsActions = createActionGroup({
  source: 'ManageOrganizationCategoryRequests',
  events: {
    'Load All Category Requests': props<{ page: PaginationParameters }>(),
    'Load All Category Requests Success': props<{ requests: ManageOrganizationCategoryRequestEntity[] }>(),
    'Load All Category Requests Failure': props<{ error: HttpErrorResponse }>(),
    'Send Category Request': props<{ postRequest: PostOrganizationCategoryRequestEntity }>(),
    'Send Category Request Success': props<{ request: ManageOrganizationCategoryRequestEntity }>(),
    'Send Category Request Failure': props<{ error: HttpErrorResponse }>(),
    'Revoke Category Request': props<{ categoryRequestId: number }>(),
    'Revoke Category Request Success': props<{ request: ManageOrganizationCategoryRequestEntity }>(),
    'Revoke Category Request Failure': props<{ error: HttpErrorResponse }>(),

    'Initial Load Category Requests': emptyProps(),

    'Open Category Request Dialog': emptyProps(),
    'Open Revoke Request Dialog': props<{ categoryRequestId: number }>(),
  }
});
