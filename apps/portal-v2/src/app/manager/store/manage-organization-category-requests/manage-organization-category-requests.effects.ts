import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ManagerControllerService } from '@api/aaa/services';
import { SMALL_DIALOG_WIDTH } from '@edit-organization/models/edit-organization.models';
import {
  CategoryRequestDialogInputData,
  ManageCreateCategoryRequestDialogComponent,
} from '@manager/components/manage-create-category-request-dialog/manage-create-category-request-dialog.component';
import { TranslocoService } from '@ngneat/transloco';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';
import { defaultPaginationParams } from '@shared/api/default-params';
import { CriticalActionDialogComponent } from '@shared/components/critical-action-dialog/critical-action-dialog.component';
import { filterNullish } from '@shared/utility/rxjs-utility';
import { UserOrganizationsSelectors } from '@user/store/user-organizations';
import { exhaustMap, filter, map } from 'rxjs/operators';
import { EventActions } from 'src/app/events/store';
import { ManageOrganizationCategoryRequestsActions } from './manage-organization-category-requests.actions';
import { State } from './manage-organization-category-requests.reducer';

@Injectable()
export class ManageOrganizationCategoryRequestsEffects {
  initialRequestLoad$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ManageOrganizationCategoryRequestsActions.initialLoadCategoryRequests
      ),
      map(() =>
        ManageOrganizationCategoryRequestsActions.loadAllCategoryRequests({
          page: defaultPaginationParams,
        })
      )
    );
  });

  sseRefresh$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(EventActions.sseUpdateCategoryChange),
      // TODO: filter - update on SSEventActions.APPROVED & REJECTED
      map(() =>
        ManageOrganizationCategoryRequestsActions.loadAllCategoryRequests({
          page: defaultPaginationParams,
        })
      )
    );
  });

  loadAllRequests$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManageOrganizationCategoryRequestsActions.loadAllCategoryRequests),
      fetch({
        run: (
          action: ReturnType<
            typeof ManageOrganizationCategoryRequestsActions.loadAllCategoryRequests
          >,
          _state: State
        ) => {
          return this.managerControllerService
            .getOrganizationCategoryRequests(action.page)
            .pipe(
              map((response) => response.organization_category_requests),
              // tap(requests => console.log('getAllCategoryRequests', requests)),
              map((requests) =>
                ManageOrganizationCategoryRequestsActions.loadAllCategoryRequestsSuccess(
                  { requests }
                )
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ManageOrganizationCategoryRequestsActions.loadAllCategoryRequests
          >,
          error: HttpErrorResponse
        ) => {
          return ManageOrganizationCategoryRequestsActions.loadAllCategoryRequestsFailure(
            { error }
          );
        },
      })
    );
  });

  sendRequest$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManageOrganizationCategoryRequestsActions.sendCategoryRequest),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof ManageOrganizationCategoryRequestsActions.sendCategoryRequest
          >,
          _state: State
        ) => {
          return this.managerControllerService
            .requestOrganizationCategory({
              body: action.postRequest,
            })
            .pipe(
              map((request) =>
                ManageOrganizationCategoryRequestsActions.sendCategoryRequestSuccess(
                  { request }
                )
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ManageOrganizationCategoryRequestsActions.sendCategoryRequest
          >,
          error: HttpErrorResponse
        ) => {
          return ManageOrganizationCategoryRequestsActions.sendCategoryRequestFailure(
            { error }
          );
        },
      })
    );
  });

  revokeRequest$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManageOrganizationCategoryRequestsActions.revokeCategoryRequest),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof ManageOrganizationCategoryRequestsActions.revokeCategoryRequest
          >,
          _state: State
        ) => {
          return this.managerControllerService
            .revokeOrganizationCategoryRequest({
              category_request_id: action.categoryRequestId,
            })
            .pipe(
              map((request) =>
                ManageOrganizationCategoryRequestsActions.revokeCategoryRequestSuccess(
                  { request }
                )
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ManageOrganizationCategoryRequestsActions.revokeCategoryRequest
          >,
          error: HttpErrorResponse
        ) => {
          return ManageOrganizationCategoryRequestsActions.revokeCategoryRequestFailure(
            { error }
          );
        },
      })
    );
  });

  openCategoryRequestDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ManageOrganizationCategoryRequestsActions.openCategoryRequestDialog
      ),
      concatLatestFrom(() =>
        this.store.select(
          UserOrganizationsSelectors.selectSelectedUserOrganizationCategories
        )
      ),
      map(([_action, cat]) => cat),
      exhaustMap((cat) =>
        this.dialog
          .open(ManageCreateCategoryRequestDialogComponent, {
            width: SMALL_DIALOG_WIDTH,
            data: <CategoryRequestDialogInputData>{
              organizationCategories: cat,
            },
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map((postRequest) =>
              ManageOrganizationCategoryRequestsActions.sendCategoryRequest({
                postRequest,
              })
            )
          )
      )
    );
  });

  openRevokeRequestDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManageOrganizationCategoryRequestsActions.openRevokeRequestDialog),
      map((action) => action.categoryRequestId),
      exhaustMap((categoryRequestId) =>
        this.dialog
          .open(CriticalActionDialogComponent, {
            data: {
              message: this.translocoService.translate(
                'manage_organization.revoke_confirm'
              ),
            },
          })
          .afterClosed()
          .pipe(
            filter((result) => !!result),
            map(() =>
              ManageOrganizationCategoryRequestsActions.revokeCategoryRequest({
                categoryRequestId,
              })
            )
          )
      )
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly managerControllerService: ManagerControllerService,
    private readonly dialog: MatDialog,
    private readonly translocoService: TranslocoService
  ) {}
}
