import { ConfidentialMemberProfileEntity } from '@api/aaa/models';
export type OrgMemberEntity = ConfidentialMemberProfileEntity;
