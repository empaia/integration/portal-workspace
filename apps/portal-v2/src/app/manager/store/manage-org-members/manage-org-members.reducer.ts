import { HttpErrorResponse } from '@angular/common/http';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { ManageOrgMembersActions } from './manage-org-members.actions';
import { OrgMemberEntity } from './manage-org-members.model';

export const MANAGE_ORG_MEMBERS_FEATURE_KEY = 'manageOrgMembers';

export interface State extends EntityState<OrgMemberEntity> {
  loaded: boolean;
  error?: HttpErrorResponse | undefined;
}

export const manageOrgMembersAdapter = createEntityAdapter<OrgMemberEntity>({
  selectId: member => member.user_id
});

export const initialState: State = manageOrgMembersAdapter.getInitialState({
  loaded: false,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(ManageOrgMembersActions.load, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ManageOrgMembersActions.loadSuccess, (state, { members }): State =>
    manageOrgMembersAdapter.setAll(members, {
      ...state,
      loaded: true,
    })
  ),
  on(ManageOrgMembersActions.loadFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ManageOrgMembersActions.removeUserFromOrganization, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ManageOrgMembersActions.removeUserFromOrganizationSuccess, (state, { userId }): State =>
    manageOrgMembersAdapter.removeOne(userId, {
      ...state,
      loaded: true,
    })
  ),
  on(ManageOrgMembersActions.removeUserFromOrganizationFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ManageOrgMembersActions.setOrganizationUserRoles, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ManageOrgMembersActions.setOrganizationUserRolesSuccess, (state, { userId, roles }): State =>
    manageOrgMembersAdapter.updateOne(
      {
        id: userId,
        changes: {
          organization_user_roles: roles
        }
      },
      {
        ...state,
        loaded: true,
      }
    )
  ),
  on(ManageOrgMembersActions.setOrganizationUserRolesFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
);
