import { createSelector } from '@ngrx/store';
import { selectManagerRootState } from '../manager-root.selectors';
import {
  manageOrgMembersAdapter,
  MANAGE_ORG_MEMBERS_FEATURE_KEY
} from './manage-org-members.reducer';

export const {
  selectAll,
  selectEntities
} = manageOrgMembersAdapter.getSelectors();


// org members
const selectManageOrgMemberState = createSelector(
  selectManagerRootState,
  (state) => state[MANAGE_ORG_MEMBERS_FEATURE_KEY]
);

export const selectManageOrgMembers = createSelector(
  selectManageOrgMemberState,
  selectAll
);

export const selectManageOrgMemberEntities = createSelector(
  selectManageOrgMemberState,
  selectEntities
);
