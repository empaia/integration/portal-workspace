import { HttpErrorResponse } from '@angular/common/http';
import { OrganizationUserRoleV2 } from '@api/aaa/models';
import {createActionGroup, emptyProps, props } from '@ngrx/store';
import { OrgMemberEntity } from '.';


export const ManageOrgMembersActions = createActionGroup({
  source: 'ManagerOrgMembers',
  events: {
    'Load': emptyProps(),
    'Load Success': props<{ members: OrgMemberEntity[] }>(),
    'Load Failure': props<{ error: HttpErrorResponse }>(),

    'Remove User From Organization': props<{ userId: string }>(),
    'Remove User From Organization Success': props<{ userId: string }>(),
    'Remove User From Organization Failure': props<{ error: HttpErrorResponse }>(),
    'Set Organization User Roles': props<{ userId: string, roles: OrganizationUserRoleV2[] }>(),
    'Set Organization User Roles Success': props<{ userId: string, roles: OrganizationUserRoleV2[] }>(),
    'Set Organization User Roles Failure': props<{ error: HttpErrorResponse }>(),

    'Open Remove User Dialog': props<{ userId: string }>(),
    'Open Edit Roles Dialog': props<{ userId: string }>(),

    'Initialize Manage Org Members Page': emptyProps(),
  },
});
