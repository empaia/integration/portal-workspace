import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';

import { exhaustMap, filter, map, tap } from 'rxjs/operators';
import { ManageOrgMembersActions } from './manage-org-members.actions';
import * as ManagerOrgMembersSelectors from './manage-org-members.selectors';
import { ManagerControllerService } from '@api/aaa/services';
import { HttpErrorResponse } from '@angular/common/http';
import { filterNullish } from '@shared/utility/rxjs-utility';
import { State } from './manage-org-members.reducer';
import { MatDialog } from '@angular/material/dialog';
import { CriticalActionDialogComponent } from '@shared/components/critical-action-dialog/critical-action-dialog.component';
import { SMALL_DIALOG_WIDTH } from '@edit-organization/models/edit-organization.models';
import { CriticalActionDialogInput } from '@shared/components/critical-action-dialog/critical-action-dialog.model';
import { TranslocoService } from '@ngneat/transloco';
import { Store } from '@ngrx/store';
import { ManageUserRolesDialogComponent } from '@manager/components/manage-user-roles-dialog/manage-user-roles-dialog.component';
import { UserOrganizationsSelectors } from '@user/store/user-organizations';
import { ManageUsersContainerActions } from '@manager/containers/manage-users-container/manage-users-container.actions';

@Injectable()
export class ManageOrgMembersEffects {
  loadManageOrgMembers$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManageOrgMembersActions.load),
      fetch({
        run: (
          _action: ReturnType<typeof ManageOrgMembersActions.load>,
          _state: State
        ) => {
          return this.managerControllerService
            .getConfidentialMemberProfiles()
            .pipe(
              map((response) => response.users),
              tap((response) => console.log('getManageOrgMembers', response)),
              filterNullish(),
              map((response) =>
                ManageOrgMembersActions.loadSuccess({ members: response })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof ManageOrgMembersActions.load>,
          error: HttpErrorResponse
        ) => {
          return ManageOrgMembersActions.loadFailure({ error });
        },
      })
    );
  });

  removeUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManageOrgMembersActions.removeUserFromOrganization),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof ManageOrgMembersActions.removeUserFromOrganization
          >,
          _state: State
        ) => {
          return this.managerControllerService
            .removeUserFromOrganization({
              target_user_id: action.userId,
            })
            .pipe(
              map(() =>
                ManageOrgMembersActions.removeUserFromOrganizationSuccess({
                  userId: action.userId,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ManageOrgMembersActions.removeUserFromOrganization
          >,
          error: HttpErrorResponse
        ) => {
          return ManageOrgMembersActions.removeUserFromOrganizationFailure({
            error,
          });
        },
      })
    );
  });

  setOrganizationUserRoles$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManageOrgMembersActions.setOrganizationUserRoles),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof ManageOrgMembersActions.setOrganizationUserRoles
          >,
          _state: State
        ) => {
          return this.managerControllerService
            .setOrganizationUserRoles({
              target_user_id: action.userId,
              body: {
                roles: action.roles,
              },
            })
            .pipe(
              map((response) => response.owned_roles),
              map((roles) =>
                ManageOrgMembersActions.setOrganizationUserRolesSuccess({
                  userId: action.userId,
                  roles,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof ManageOrgMembersActions.setOrganizationUserRoles
          >,
          error: HttpErrorResponse
        ) => {
          return ManageOrgMembersActions.setOrganizationUserRolesFailure({
            error,
          });
        },
      })
    );
  });

  openRemoveUserDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManageOrgMembersActions.openRemoveUserDialog),
      map((action) => action.userId),
      concatLatestFrom(() =>
        this.store.select(
          ManagerOrgMembersSelectors.selectManageOrgMemberEntities
        )
      ),
      map(([id, users]) => users[id]),
      filterNullish(),
      exhaustMap((user) =>
        this.dialog
          .open(CriticalActionDialogComponent, {
            data: {
              title: this.translocoService.translate(
                'manage_organization.remove_user_title'
              ),
              message: this.translocoService.translate(
                'manage_organization.remove_user_confirm',
                {
                  username: user.title
                    ? user.title + ' '
                    : '' + user.first_name + ' ' + user.last_name,
                }
              ),
            } as CriticalActionDialogInput,
            width: SMALL_DIALOG_WIDTH,
          })
          .afterClosed()
          .pipe(
            filter((response) => !!response),
            map(() =>
              ManageOrgMembersActions.removeUserFromOrganization({
                userId: user.user_id,
              })
            )
          )
      )
    );
  });

  openSetUserOrganizationRolesDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ManageOrgMembersActions.openEditRolesDialog),
      map((action) => action.userId),
      concatLatestFrom(() => [
        this.store.select(
          ManagerOrgMembersSelectors.selectManageOrgMemberEntities
        ),
        this.store.select(
          UserOrganizationsSelectors.selectSelectedUserOrganizationCategories
        ),
      ]),
      map(([id, users, organizationCategories]) => ({
        user: users[id],
        organizationCategories,
      })),
      filter((data) => !!data.user),
      exhaustMap((data) =>
        this.dialog
          .open(ManageUserRolesDialogComponent, {
            data: data,
            width: SMALL_DIALOG_WIDTH,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map((roles) =>
              ManageOrgMembersActions.setOrganizationUserRoles({
                userId: data.user?.user_id as string,
                roles,
              })
            )
          )
      )
    );
  });

  initializeMyOrganizationsPage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ManageOrgMembersActions.initializeManageOrgMembersPage,
        ManageUsersContainerActions.initializeMangeUserContainers,
      ),
      map(() => ManageOrgMembersActions.load())
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly managerControllerService: ManagerControllerService,
    private readonly dialog: MatDialog,
    private readonly translocoService: TranslocoService
  ) {}
}
