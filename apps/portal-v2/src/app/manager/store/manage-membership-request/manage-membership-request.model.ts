import { MembershipRequestEntity as ApiManagerMembershipRequestEntity} from '@api/aaa/models';
import { AcceptDialogEntity } from '@shared/components/accept-dialog/accept-dialog.component';
import { DenyDialogEntity } from '@shared/components/deny-dialog/deny-dialog.component';
export type ManagerMembershipRequestEntity = ApiManagerMembershipRequestEntity;

export const DEFAULT_ACCEPT_MEMBERSHIP_REQUESTS_DIALOG_TEXT: AcceptDialogEntity = {
  headlineText: 'manage_forms.accept_membership_request',
  contentText: 'manage_forms.insuring_membership_acceptance',
  confirmButtonText: 'general_forms.confirm',
  cancelButtonText: 'general_forms.cancel',
};

export const DEFAULT_DENY_MEMBERSHIP_REQUESTS_DIALOG_TEXT: DenyDialogEntity = {
  headlineText: 'manage_forms.deny_membership_request',
  reviewLabel: 'manage_forms.review_membership',
  confirmButtonText: 'general_forms.confirm',
  cancelButtonText: 'general_forms.cancel'
};
