import { HttpErrorResponse } from '@angular/common/http';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { ManagerMembershipRequestActions } from './manage-membership-request.actions';
import { ManagerMembershipRequestEntity } from './manage-membership-request.model';

export const MANAGER_MEMBERSHIP_REQUEST_FEATURE_KEY = 'managerMembershipRequest';


export interface State extends EntityState<ManagerMembershipRequestEntity> {
  loaded: boolean;
  error?: HttpErrorResponse | undefined;
}


export const managerMembershipRequestAdapter = createEntityAdapter<ManagerMembershipRequestEntity>({
  selectId: request => request.membership_request_id
});

export const initialState: State = managerMembershipRequestAdapter.getInitialState({
  loaded: false,
  error: undefined,
});



export const reducer = createReducer(
  initialState,

  on(ManagerMembershipRequestActions.load, (state): State => ({
    ...state,
    loaded: false,
  })),

  on(ManagerMembershipRequestActions.loadSuccess, (state, { requests }): State =>
    managerMembershipRequestAdapter.setAll(requests, {
      ...state,
      loaded: true,
    })
  ),

  on(ManagerMembershipRequestActions.approve, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ManagerMembershipRequestActions.approveSuccess, (state, { membershipRequestId }): State =>
    managerMembershipRequestAdapter.removeOne(membershipRequestId, {
      ...state,
      loaded: true,
    })
  ),
  on(ManagerMembershipRequestActions.approveFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ManagerMembershipRequestActions.reject, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ManagerMembershipRequestActions.rejectSuccess, (state, { membershipRequestId }): State =>
    managerMembershipRequestAdapter.removeOne(membershipRequestId, {
      ...state,
      loaded: true,
    })
  ),
  on(ManagerMembershipRequestActions.rejectFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  }))
);
