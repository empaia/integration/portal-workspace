import { HttpErrorResponse } from '@angular/common/http';
import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { ManagerMembershipRequestEntity } from './manage-membership-request.model';



export const ManagerMembershipRequestActions = createActionGroup({
  source: 'ManagerMembershipRequest',
  events: {
    'Load': emptyProps(),
    'Load Success': props<{ requests: ManagerMembershipRequestEntity[] }>(),
    'Load Failure': props<{ error: HttpErrorResponse }>(),

    'Initialize Manager Membership Request Page': emptyProps(),

    'Approve': props<{ membershipRequestId: number }>(),
    'Approve Success': props<{ membershipRequestId: number }>(),
    'Approve Failure': props<{ error: HttpErrorResponse }>(),
    'Reject': props<{
      membershipRequestId: number,
      comment: string,
    }>(),
    'Reject Success': props<{ membershipRequestId: number }>(),
    'Reject Failure': props<{ error: HttpErrorResponse }>(),

    'Open Approve Member Dialog': props<{ membershipRequestId: number }>(),
    'Open Reject Member Dialog': props<{ membershipRequestId: number }>(),
  },
});
