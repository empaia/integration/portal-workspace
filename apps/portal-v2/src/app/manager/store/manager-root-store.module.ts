import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslocoModule } from '@ngneat/transloco';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import * as fromManagerRoot from './manager-root.selectors';

import { ManagerMembershipRequestEffects } from './manage-membership-request/manage-membership-request.effects';
import { ManageRoleRequestEffects } from './manage-role-request/manage-role-request.effects';
import { ManageOrgMembersEffects } from './manage-org-members/manage-org-members.effects';
import { ManageOrgProfileEffects } from './manage-org-profile';
import { ManageOrganizationCategoryRequestsEffects } from './manage-organization-category-requests';

@NgModule({
  declarations: [

  ],
  imports: [
    CommonModule,
    TranslocoModule,
    StoreModule.forFeature(
      fromManagerRoot.MANAGER_ROOT_FEATURE_KEY,
      fromManagerRoot.reducers
    ),
    EffectsModule.forFeature([
      ManagerMembershipRequestEffects,
      ManageOrgMembersEffects,
      ManageRoleRequestEffects,
      ManageOrgProfileEffects,
      ManageOrganizationCategoryRequestsEffects,
    ]),
  ],
  exports: [

  ],
})
export class ManagerRootStoreModule { }
