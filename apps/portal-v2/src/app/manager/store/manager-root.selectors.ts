import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';


import * as fromMembershipRequest from './manage-membership-request/manage-membership-request.reducer';
import * as fromManageOrgMembers from './manage-org-members/manage-org-members.reducer';
import * as fromManageRoleRequest from './manage-role-request/manage-role-request.reducer';
import * as fromManageOrgProfile from './manage-org-profile/manage-org-profile.reducer';
import * as fromManageOrganizationCategoryRequests from './manage-organization-category-requests/manage-organization-category-requests.reducer';


export const MANAGER_ROOT_FEATURE_KEY = 'managerRoot';

export interface ManagerRootState {
  [fromMembershipRequest.MANAGER_MEMBERSHIP_REQUEST_FEATURE_KEY]: fromMembershipRequest.State;
  [fromManageOrgMembers.MANAGE_ORG_MEMBERS_FEATURE_KEY]: fromManageOrgMembers.State;
  [fromManageRoleRequest.MANAGE_ROLE_REQUEST_FEATURE_KEY]: fromManageRoleRequest.State;
  [fromManageOrgProfile.MANAGE_ORG_PROFILE_FEATURE_KEY]: fromManageOrgProfile.State;
  [fromManageOrganizationCategoryRequests.MANAGE_ORGANIZATION_CATEGORY_REQUESTS_FEATURE_KEY]: fromManageOrganizationCategoryRequests.State;
}

export interface State {
  [MANAGER_ROOT_FEATURE_KEY]: ManagerRootState;
}

export function reducers(state: ManagerRootState | undefined, action: Action) {
  return combineReducers({
    [fromMembershipRequest.MANAGER_MEMBERSHIP_REQUEST_FEATURE_KEY]: fromMembershipRequest.reducer,
    [fromManageOrgMembers.MANAGE_ORG_MEMBERS_FEATURE_KEY]: fromManageOrgMembers.reducer,
    [fromManageRoleRequest.MANAGE_ROLE_REQUEST_FEATURE_KEY]: fromManageRoleRequest.reducer,
    [fromManageOrgProfile.MANAGE_ORG_PROFILE_FEATURE_KEY]: fromManageOrgProfile.reducer,
    [fromManageOrganizationCategoryRequests.MANAGE_ORGANIZATION_CATEGORY_REQUESTS_FEATURE_KEY]: fromManageOrganizationCategoryRequests.reducer,
  })(state, action);
}

export const selectManagerRootState = createFeatureSelector<ManagerRootState>(
  MANAGER_ROOT_FEATURE_KEY
);
