import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutes } from '@router/router.constants';
import { EditOrganizationContainerComponent } from './containers/edit-organization-container/edit-organization-container.component';
import { MembershipRequestsContainerComponent } from './containers/membership-requests-container/membership-requests-container.component';
import { ManageUsersContainerComponent } from './containers/manage-users-container/manage-users-container.component';
import { RequestOrgCategoryChangeContainerComponent } from './containers/request-org-category-change-container/request-org-category-change-container.component';

const routes: Routes = [
  // {
  // path: AppRoutes.MANAGE_ACTIVE_ORG_ROUTE,
  // children: [
  {
    path: AppRoutes.MANAGE_USERS,
    component: ManageUsersContainerComponent,
  },
  {
    path: AppRoutes.MANAGE_MEMBERSHIP_REQUESTS,
    component: MembershipRequestsContainerComponent
  },
  {
    path: AppRoutes.MANAGE_REQUEST_ORGANIZATION_CATEGORY_CHANGE,
    component: RequestOrgCategoryChangeContainerComponent
  },
  {
    path: AppRoutes.MANAGE_EDIT_ORGANIZATION,
    component: EditOrganizationContainerComponent
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: AppRoutes.MANAGE_MEMBERSHIP_REQUESTS,
  },
  // ]
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagerRoutingModule { }
