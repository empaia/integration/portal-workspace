import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { Store } from '@ngrx/store';
import * as RouterActions from '@router/router.actions';
import { ADMIN_SIDE_NAV_ITEMS, ElevatedRoles, NavLink, NavLinkId } from 'src/app/app.models';
import { Observable } from 'rxjs';
import { selectUserRoles } from './sidenav-container.selectors';
import { UserOrganizationsSelectors } from '@user/store/user-organizations';
import { SidenavService } from '@core/services/sidenav/sidenav.service';

@Component({
  selector: 'app-sidenav-container',
  templateUrl: './sidenav-container.component.html',
  styleUrls: ['./sidenav-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidenavContainerComponent {
  private store = inject(Store);
  private sidenavService = inject(SidenavService);

  protected userRoles$: Observable<ElevatedRoles>;
  protected activeOrgName$: Observable<string | undefined>;

  protected navOptions = ADMIN_SIDE_NAV_ITEMS;

  constructor() {
    this.userRoles$ = this.store.select(selectUserRoles);
    this.activeOrgName$ = this.store.select(UserOrganizationsSelectors.selectSelectedUserOrganizationName);
  }

  navigateTo(event: { link: NavLink }) {
    this.store.dispatch(RouterActions.navigate({
      path: [event.link.ref],
    }));
  }

  getActiveLink(): Observable<NavLinkId> {
    return this.sidenavService.getActiveLinkId();
  }

}
