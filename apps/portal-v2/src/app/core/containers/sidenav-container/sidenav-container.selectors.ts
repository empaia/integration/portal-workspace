import { selectIsManager } from '@auth/store/auth/auth.selectors';
import { createSelector } from '@ngrx/store';
import { selectIsModerator } from '@user/store/user/user.selectors';
import { ElevatedRoles } from 'src/app/app.models';

export const selectUserRoles = createSelector(
  selectIsManager,
  selectIsModerator,
  (manager, moderator): ElevatedRoles => ({
    manager,
    moderator,
    maintainer: false
  })
);