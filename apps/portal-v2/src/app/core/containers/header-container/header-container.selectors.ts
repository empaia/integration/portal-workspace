import { createSelector } from '@ngrx/store';

import { selectIsManager, selectUserProfileEmail, selectUserProfileName } from '@auth/store/auth/auth.selectors';
import { selectIsModerator, selectProfilePicture60 } from '@user/store/user/user.selectors';
import { UserProfileInfo } from 'src/app/app.models';
import { selectSelectedUserOrganizationName, selectSelectedUserOrganizationImage60, selectUserOrganizationsDefaultOrgId } from '@user/store/user-organizations/user-organizations.selectors';
import { AppRoutes } from '@router/router.constants';

export const selectHasElevatedRole = createSelector(
  selectIsManager,
  selectIsModerator,
  (manager, moderator) => manager || moderator
);


export const selectUserInfo = createSelector(
  selectUserProfileName,
  selectUserProfileEmail,
  selectProfilePicture60,
  selectSelectedUserOrganizationName,
  selectSelectedUserOrganizationImage60,
  selectUserOrganizationsDefaultOrgId,
  (userName, email, profilePicture, orgName, orgPicture, orgId): UserProfileInfo => (
    { email, userName, orgName, profilePicture, orgPicture, orgId }
  )
);

export const selectInitialAdminRoute = createSelector(
  selectIsManager,
  selectIsModerator,
  (manager, moderator) => {
    if (manager) { return AppRoutes.MANAGE_ROUTE + '/' + AppRoutes.MANAGE_USERS; }
    if (moderator) { return AppRoutes.MODERATE_ROUTE + '/' + AppRoutes.MODERATE_USERS; }
    return '';
  }
);
