import { ChangeDetectionStrategy, Component } from '@angular/core';
import { LanguageService } from '@transloco/language.service';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AuthSelectors, AuthActions } from '@auth/store/auth';

import { RouterSelectors } from '@router/router.selectors';
import {  HeaderNavLink } from '@router/router.constants';
import { LangDefinition } from '@ngneat/transloco';
import { AuthService } from '@auth/services/auth.service';
import * as RouterActions from '@router/router.actions';
import { selectHasElevatedRole, selectInitialAdminRoute, selectUserInfo } from './header-container.selectors';
import { UserProfileInfo } from 'src/app/app.models';
import { NotificationSelectors } from '@user/store/notification';
import { UserActions } from '@user/store/user';
import { environment } from '@env/environment';
import { UserOrganizationsActions, UserOrganizationsSelectors } from '@user/store/user-organizations';
import { UserOrganizationEntity } from '@user/store/user-organizations/user-organizations.models';

@Component({
  selector: 'app-header-container',
  templateUrl: './header-container.component.html',
  styleUrls: ['./header-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderContainerComponent {

  protected loggedIn$!: Observable<boolean>;
  protected activeNavLink$!: Observable<HeaderNavLink>;

  protected userInfo$: Observable<UserProfileInfo>;
  protected showAdminButton$: Observable<boolean>;
  protected currentLang$: Observable<string>;
  protected adminRoute$: Observable<string>;
  protected numNotifications$: Observable<number>;
  public isClearanceMaintainer$: Observable<boolean>;
  public userOrganizations$: Observable<UserOrganizationEntity[]>;

  protected showClearances = environment.showClearances;

  constructor(
    private store: Store,
    private languageService: LanguageService,
    private authService: AuthService,
  ) {
    this.activeNavLink$ = this.store.select(RouterSelectors.selectHeaderNavActive);
    this.loggedIn$ = this.store.select(AuthSelectors.selectLoggedIn);
    this.currentLang$ = this.languageService.getLanguage$();

    this.userInfo$ = this.store.select(selectUserInfo);

    this.showAdminButton$ = this.store.select(selectHasElevatedRole);

    this.adminRoute$ = this.store.select(selectInitialAdminRoute);

    this.numNotifications$ = this.store.select(NotificationSelectors.selectNotificationCount);

    this.isClearanceMaintainer$ = this.store.select(AuthSelectors.selectIsClearanceMaintainer);
    this.userOrganizations$ = this.store.select(UserOrganizationsSelectors.selectAllUserOrganizations);
  }

  public register(): void {
    const url = this.authService.registrationLink;
    // console.log('register url', url);
    if (url) {
      window.location.href = url;
    }
  }

  public login(): void {
    this.store.dispatch(AuthActions.login({}));
  }

  public logout(): void {
    this.store.dispatch(AuthActions.logout());
  }

  public setLanguage($event: { langDef: LangDefinition, persist: boolean }): void {
    this.languageService.setActiveLanguage($event.langDef);
    if ($event.persist) {
      this.store.dispatch(UserActions.setUserLanguage({
        code: this.languageService.langDefinitionToCode($event.langDef)
      }));
    }
  }

  public openSelectOrganization(event: { organizationId?: string }): void {
    this.store.dispatch(UserOrganizationsActions.openSelectOrganizationDialog(event));
  }

  public navigateTo($event: { route: string }): void {
    this.store.dispatch(RouterActions.navigate({ path: [$event.route] }));
  }
}
