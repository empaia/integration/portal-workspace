import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';
import { ThemeService } from '@core/services/theme/theme.service';
import { AppRoutes } from '@router/router.constants';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header-logo',
  templateUrl: './header-logo.component.html',
  styleUrls: ['./header-logo.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderLogoComponent {

  public theme$: Observable<string>;

  public readonly ROOT_ROUTE = AppRoutes.ROOT_ROUTE;

  @Output() public logoClicked = new EventEmitter<{ route: string }>();

  constructor(
    public theme: ThemeService,
  ) {
    this.theme$ = this.theme.getTheme$();
  }

}
