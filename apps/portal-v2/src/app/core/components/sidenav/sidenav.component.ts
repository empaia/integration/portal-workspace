import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { ElevatedRoles, NavLink, NavLinkId, UserRoleUI } from 'src/app/app.models';


@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidenavComponent {

  @Input() public links: NavLink[] = [];
  @Input() public activeLink: NavLinkId | undefined = undefined;
  @Input() public activeOrgName!: string | undefined;// = { maintainer: true, 
  @Input() public userRoles!: ElevatedRoles | undefined;// = { maintainer: true, manager: true, moderator: true };
  // @Input() public isManager: boolean | undefined = false;
  // @Input() public isModerator: boolean | undefined = false;
  // @Input() public isMaintainer: boolean | undefined = false;

  @Output() public navigate = new EventEmitter<{ link: NavLink }>();

  get manageLinks(): NavLink[] {
    return this.links.filter(n => n.role === UserRoleUI.MANAGER);
  }

  get moderateLinks(): NavLink[] {
    return this.links.filter(n => n.role === UserRoleUI.MODERATOR);
  }

  get maintainLinks(): NavLink[] {
    return this.links.filter(n => n.role === UserRoleUI.MAINTAINER);
  }

}
