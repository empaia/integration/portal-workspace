import { Spectator, createComponentFactory } from '@ngneat/spectator';

import { SidenavComponent } from './sidenav.component';

describe('SidenavComponent', () => {
  let spectator: Spectator<SidenavComponent>;
  const createComponent = createComponentFactory(SidenavComponent);

  it('should create', () => {
    spectator = createComponent();

    expect(spectator.component).toBeTruthy();
  });
});
