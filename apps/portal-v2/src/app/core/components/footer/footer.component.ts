import { Component, Input } from '@angular/core';
import { environment } from '@env/environment';
import { FooterLinkLibrary, FooterLinkType } from '@footer/footer.models';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
  protected year: number = new Date().getFullYear();
  @Input() public links!: FooterLinkLibrary;
  @Input() public appVersion = environment.appVersion;

  public LINK_TYPES = FooterLinkType;
}
