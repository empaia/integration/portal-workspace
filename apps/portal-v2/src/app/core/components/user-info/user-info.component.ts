import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { blankProfileImage, UserProfileInfo } from 'src/app/app.models';


@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserInfoComponent {

  protected readonly fallbackProfile = blankProfileImage;

  @Input() public userInfo: UserProfileInfo | undefined;


}
