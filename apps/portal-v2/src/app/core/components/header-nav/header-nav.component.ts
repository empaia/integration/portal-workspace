import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from '@angular/core';
import { AppRoutes, HeaderNavLink } from '@router/router.constants';

@Component({
  selector: 'app-header-nav',
  templateUrl: './header-nav.component.html',
  styleUrls: ['./header-nav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderNavComponent {


  protected ORG_ROUTE = AppRoutes.PUBLIC_ORG_ROUTE;
  protected MARKET_ROUTE = AppRoutes.MARKET_ROUTE;
  protected CLEARANCES_ROUTE = AppRoutes.CLEARANCES_ROUTE;

  @Input() public linkActive: HeaderNavLink | undefined;

  @Input() public showClearances: boolean | undefined;
}
