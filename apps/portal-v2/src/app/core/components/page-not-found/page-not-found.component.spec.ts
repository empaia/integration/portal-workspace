import { PageNotFoundComponent } from './page-not-found.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';

describe('PageNotFoundComponentComponent', () => {
  let spectator: Spectator<PageNotFoundComponent>;
  const createComponent = createComponentFactory({
    component: PageNotFoundComponent,
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
