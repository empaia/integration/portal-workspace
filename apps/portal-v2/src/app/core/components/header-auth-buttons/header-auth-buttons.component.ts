import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output
} from '@angular/core';

@Component({
  selector: 'app-header-auth-buttons',
  templateUrl: './header-auth-buttons.component.html',
  styleUrls: ['./header-auth-buttons.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderAuthButtonsComponent   {

  @Output() public registerClick = new EventEmitter<unknown>();
  @Output() public loginClick = new EventEmitter<unknown>();
}
