import { Component, EventEmitter, Input, Output } from '@angular/core';
import { LangDefinition } from '@ngneat/transloco';
import { availableLanguages } from 'src/app/app.models';

@Component({
  selector: 'app-language-button',
  templateUrl: './language-button.component.html',
  styleUrls: ['./language-button.component.scss']
})
export class LanguageButtonComponent {

  availLang = availableLanguages;

  @Input() public currentLang!: string;
  @Output() public langSelected = new EventEmitter<LangDefinition>();

  languageSelected(lang: LangDefinition) {
    this.langSelected.emit(lang);
  }

}
