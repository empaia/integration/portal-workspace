import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { UserProfileInfo, blankOrgImage } from 'src/app/app.models';

@Component({
  selector: 'app-profile-organization-button',
  templateUrl: './profile-organization-button.component.html',
  styleUrls: ['./profile-organization-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileOrganizationButtonComponent {
  @Input() public userInfo: UserProfileInfo | undefined;

  @Output() public organizationButtonClicked = new EventEmitter<{ organizationId?: string }>();

  public readonly FALLBACK_ORG_LOGO = blankOrgImage;
}
