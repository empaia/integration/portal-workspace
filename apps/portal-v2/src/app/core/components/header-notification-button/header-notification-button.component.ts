import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { AppRoutes } from '@router/router.constants';

@Component({
  selector: 'app-header-notification-button',
  templateUrl: './header-notification-button.component.html',
  styleUrls: ['./header-notification-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderNotificationButtonComponent {
  
  protected NOTIFICATIONS_ROUTE = AppRoutes.MY_ROUTE + '/' + AppRoutes.NOTIFICATIONS_ROUTE;
  
  @Input() public numNotifications: number | undefined;
  @Output() public iconClicked = new EventEmitter<{ route: string }>();
}
