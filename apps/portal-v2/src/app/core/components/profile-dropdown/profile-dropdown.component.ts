import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';
import { AppRoutes } from '@router/router.constants';
import { UserProfileInfo } from 'src/app/app.models';

@Component({
  selector: 'app-profile-dropdown',
  templateUrl: './profile-dropdown.component.html',
  styleUrls: ['./profile-dropdown.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileDropdownComponent {

  protected readonly MY_PROFILE_ROUTE = AppRoutes.MY_ROUTE + '/' + AppRoutes.PROFILE_ROUTE;
  protected readonly MY_ORG_ROUTE = AppRoutes.MY_ROUTE + '/' + AppRoutes.MY_ORGANIZATIONS_ROUTE;

  protected readonly MY_ACTIVE_ORG_ROUTE = AppRoutes.MY_ROUTE + '/' + AppRoutes.MY_ACTIVE_ORGANIZATION_ROUTE;

  public readonly CLEARANCE_MAINTAINER_ROUTE = AppRoutes.CLEARANCE_MAINTAINER_ROUTE;

  @Output() public itemClicked = new EventEmitter<{ route: string }>();

  @Output() public logoutClicked = new EventEmitter<unknown>();

  @Input() public userInfo: UserProfileInfo | undefined;
  @Input() public showAdminItem: boolean | undefined = true;
  @Input() public initialAdminRoute: string | undefined = undefined;
  @Input() public showClearances: boolean | undefined = false;
}
