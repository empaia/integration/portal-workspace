import { ProfileDropdownComponent } from './profile-dropdown.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';
import { MockDirectives } from 'ng-mocks';
import { TranslocoDirective } from '@ngneat/transloco';

describe('ProfileDropdownComponent', () => {
  let spectator: Spectator<ProfileDropdownComponent>;
  const createComponent = createComponentFactory({
    component: ProfileDropdownComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      MockDirectives(
        TranslocoDirective
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
