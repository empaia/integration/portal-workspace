import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { PushPipe, LetDirective } from '@ngrx/component';
import { TranslocoModule } from '@ngneat/transloco';

import { MaterialModule } from '@material/material.module';
import { SharedModule } from '@shared/shared.module';

import { ProfileDropdownComponent } from './components/profile-dropdown/profile-dropdown.component';
import { HeaderContainerComponent } from './containers/header-container/header-container.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { HeaderNavComponent } from './components/header-nav/header-nav.component';
import { HeaderMenuComponent } from './components/header-menu/header-menu.component';
import { HeaderAuthButtonsComponent } from './components/header-auth-buttons/header-auth-buttons.component';
import { HeaderLogoComponent } from './components/header-logo/header-logo.component';
import { LanguageButtonComponent } from './components/language-button/language-button.component';
import { FooterComponent } from './components/footer/footer.component';

import { UserInfoComponent } from './components/user-info/user-info.component';

import { SidenavContainerComponent } from './containers/sidenav-container/sidenav-container.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { HeaderNotificationButtonComponent } from './components/header-notification-button/header-notification-button.component';
import { ProfileOrganizationButtonComponent } from './components/profile-organization-button/profile-organization-button.component';


@NgModule({
  declarations: [
    ProfileDropdownComponent,
    HeaderContainerComponent,
    PageNotFoundComponent,
    HeaderNavComponent,
    HeaderMenuComponent,
    HeaderAuthButtonsComponent,
    HeaderLogoComponent,
    LanguageButtonComponent,
    FooterComponent,
    UserInfoComponent,
    SidenavContainerComponent,
    SidenavComponent,
    HeaderNotificationButtonComponent,
    ProfileOrganizationButtonComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    LetDirective,
    PushPipe,
    TranslocoModule,
    SharedModule,
  ],
  exports: [
    HeaderContainerComponent,
    FooterComponent,
    PageNotFoundComponent,
    SidenavContainerComponent,
    SidenavComponent,
  ],
})
export class CoreModule {
  //prevent dev errors
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import it in the AppModule only');
    }
  }
}
