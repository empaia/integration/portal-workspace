import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { themes } from 'src/app/app.models';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private themeName: BehaviorSubject<string> = new BehaviorSubject('');
  private prevThemeName: BehaviorSubject<string> = new BehaviorSubject('');

  // index of themes array
  private current = 0;

  getTheme$(): Observable<string> {
    return this.themeName.asObservable();
  }

  getPrevTheme$(): Observable<string> {
    return this.prevThemeName.asObservable();
  }


  setTheme(name: string) {
    console.log(name);
    this.prevThemeName.next(this.themeName.getValue());
    this.themeName.next(name);
  }

  nextTheme() {
    this.current = this.current + 1 <  themes.length ? this.current + 1 : 0;
    this.setTheme(themes[this.current]);
  }
}
