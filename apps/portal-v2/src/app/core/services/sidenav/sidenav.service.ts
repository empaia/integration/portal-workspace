import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { NavLinkId } from 'src/app/app.models';

@Injectable({
  providedIn: 'root'
})
export class SidenavService {
  private _isOpen: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private _activeLinkId: ReplaySubject<NavLinkId> = new ReplaySubject(1);
  
  public set activeLinkId(id: NavLinkId) {
    this._activeLinkId.next(id);
  }
  public getActiveLinkId(): Observable<NavLinkId> {
    return this._activeLinkId.asObservable();
  }


  isOpen(): boolean {
    return this._isOpen.value;
  }

  toggle() {
    if (this._isOpen.value) {
      this.close();
    } else {
      this.open();
    }
  }

  open() {
    this._isOpen.next(true);
  }

  close() {
    this._isOpen.next(false);
  }
}
