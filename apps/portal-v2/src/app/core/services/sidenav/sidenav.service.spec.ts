import { createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { SidenavService } from './sidenav.service';

describe('SidenavService', () => {
  let spectator: SpectatorService<SidenavService>;
  const createService = createServiceFactory(SidenavService);

  beforeEach(() => spectator = createService());

  it('should...', () => {
    expect(spectator.service).toBeTruthy();
  });
});