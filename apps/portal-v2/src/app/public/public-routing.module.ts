import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutes } from '@router/router.constants';
import { MarketplaceContainerComponent } from './containers/marketplace-container/marketplace-container.component';
import { OrganizationsContainerComponent } from './containers/organizations-container/organizations-container.component';
import { PortalAppDetailContainerComponent } from './containers/portal-app-detail-container/portal-app-detail-container.component';
import {
  OrganizationDetailContainerComponent
} from '@public/containers/organization-detail-container/organization-detail-container.component';
import { ClearancesContainerComponent } from './containers/clearances-container/clearances-container.component';


const routes: Routes = [
  {
    path: AppRoutes.PUBLIC_ORG_ROUTE,
    component: OrganizationsContainerComponent
  },
  {
    path: AppRoutes.PUBLIC_ORG_ROUTE + '/:' + AppRoutes.PUBLIC_ORG_ID,
    component: OrganizationDetailContainerComponent
  },
  {
    path: AppRoutes.MARKET_ROUTE,
    component: MarketplaceContainerComponent,
  },
  {
    path: AppRoutes.MARKET_ROUTE + '/' + AppRoutes.PUBLIC_APP_ROUTE + '/:' + AppRoutes.PUBLIC_APP_ID,
    component: PortalAppDetailContainerComponent,
  },
  {
    path: AppRoutes.MARKET_ROUTE + '/' + AppRoutes.PUBLIC_APP_ROUTE + '/:' + AppRoutes.PUBLIC_APP_ID + '/:' +  AppRoutes.PUBLIC_APP_API_VERSION,
    component: PortalAppDetailContainerComponent,
  },
  {
    path: AppRoutes.CLEARANCES_ROUTE,
    component: ClearancesContainerComponent,
  },
  {
    path: '',
    pathMatch:'full',
    redirectTo: AppRoutes.MARKET_ROUTE,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
