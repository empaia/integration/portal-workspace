import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-portal-app-count',
  templateUrl: './portal-app-count.component.html',
  styleUrls: ['./portal-app-count.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortalAppCountComponent {

  @Input() numApps!: number | undefined;
  @Input() numTotalApps!: number | undefined;

}
