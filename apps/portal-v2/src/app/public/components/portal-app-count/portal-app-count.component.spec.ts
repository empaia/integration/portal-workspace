import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PortalAppCountComponent } from './portal-app-count.component';

describe('PortalAppCountComponent', () => {
  let component: PortalAppCountComponent;
  let fixture: ComponentFixture<PortalAppCountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PortalAppCountComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PortalAppCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
