import { OrganizationDetailContactDataItemComponent } from './organization-detail-contact-data-item.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MockDirectives } from 'ng-mocks';
import { TranslocoDirective } from '@ngneat/transloco';

describe('OrganizationDetailContactDataItemComponent', () => {
  let spectator: Spectator<OrganizationDetailContactDataItemComponent>;
  const createComponent = createComponentFactory({
    component: OrganizationDetailContactDataItemComponent,
    declarations: [
      MockDirectives(
        TranslocoDirective
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
