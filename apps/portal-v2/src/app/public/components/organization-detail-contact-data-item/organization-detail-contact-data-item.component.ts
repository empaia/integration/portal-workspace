import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-organization-detail-contact-data-item',
  templateUrl: './organization-detail-contact-data-item.component.html',
  styleUrls: ['./organization-detail-contact-data-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OrganizationDetailContactDataItemComponent {
  @Input() public type!: string;
  @Input() public isPublic: boolean | undefined = true;
  @Input() public isEmail = false;
  @Input() public isLink = false;
  @Input() public data!: (string | undefined)[];

  public entryClicked(entry: string | undefined): void {
    this.isEmail
      ? window.open('mailto:'+ entry, '_parent')
      : window.open(entry, '_blank');
  }
}
