import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { fallbackImage } from 'src/app/app.models';
import { PortalAppOrganizationEntity } from '../../store/portal-apps/portal-apps.models';

@Component({
  selector: 'app-portal-app-card',
  templateUrl: './portal-app-card.component.html',
  styleUrls: ['./portal-app-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortalAppCardComponent  {

  protected fallbackImage = fallbackImage;
  protected over = false;

  @Input() app!: PortalAppOrganizationEntity;

  @Output() public appClicked = new EventEmitter<{ id: string }>();
}
