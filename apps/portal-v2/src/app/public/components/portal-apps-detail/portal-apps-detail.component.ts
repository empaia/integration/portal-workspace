import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { ApiVersion, PublicAppView } from '@api/public/models';
import { OrganizationEntity } from '@public/store/organizations/organizations.models';
import { getSelectedApiAppView, hasAppApiVersion, PortalAppEntity, PortalAppOrganizationEntity } from '@public/store/portal-apps/portal-apps.models';
import { AppRoutes } from '@router/router.constants';
import { fallbackImage } from 'src/app/app.models';
import { ToggleButton } from '../toggle-buttons/toggle-buttons.component';

@Component({
  selector: 'app-portal-apps-detail',
  templateUrl: './portal-apps-detail.component.html',
  styleUrls: ['./portal-apps-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortalAppsDetailComponent {
  protected PUBLIC_ORG_ROUTE = AppRoutes.PUBLIC_ORG_ROUTE;

  protected fallbackImage = fallbackImage;

  protected apiVersion = ApiVersion;

  private _appEntity: PortalAppOrganizationEntity | undefined;
  @Input() public get appEntity(): PortalAppOrganizationEntity | undefined {
    return this._appEntity;
  }

  public set appEntity(val: PortalAppOrganizationEntity | undefined) {
    this._appEntity = val;
    if (val && this.selectedAppApiVersion) {
      this.setAppView(val, this.selectedAppApiVersion);
    }
  }
  private _selectedAppApiVersion: ApiVersion | undefined;
  @Input() public get selectedAppApiVersion() {
    return this._selectedAppApiVersion;
  }

  public set selectedAppApiVersion(val: ApiVersion | undefined) {
    this._selectedAppApiVersion = val;
    if (this.appEntity && val) {
      this.setAppView(this.appEntity, val);
    }
  }

  protected app: PublicAppView | undefined | null;
  protected org: OrganizationEntity | undefined;

  protected apiVersionToggle: ToggleButton<ApiVersion>[] = [];

  @Output() public apiVersionSelected = new EventEmitter<{ appId: string, version: ApiVersion }>();

  public apiVersionChange($event: { value: ApiVersion }): void {
    if (this.appEntity && $event.value) {
      this.apiVersionSelected.emit({ appId: this.appEntity?.id, version: $event.value });
    }
  }

  private setAppView(entity: PortalAppOrganizationEntity, version: ApiVersion): void {
    this.app = getSelectedApiAppView(entity, version);
    this.org = entity.organization;
    this.initToggles(entity, version);
  }

  private initToggles(entity: PortalAppEntity, version: ApiVersion): void {
    this.apiVersionToggle = [];
    Object.values(ApiVersion).forEach(v => this.apiVersionToggle.push({
      display: v,
      value: v,
      enabled: hasAppApiVersion(entity, v),
      selected: (version === v)
    }));
  }
}
