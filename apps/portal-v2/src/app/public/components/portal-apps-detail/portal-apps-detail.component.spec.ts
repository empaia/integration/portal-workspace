import { PortalAppsDetailComponent } from './portal-apps-detail.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MockComponents, MockDirectives } from 'ng-mocks';
import { PortalAppDetailsWorkflowComponent } from '@public/components/portal-app-details-workflow/portal-app-details-workflow.component';
import { TranslocoDirective } from '@ngneat/transloco';

describe('PortalAppsDetailComponent', () => {
  let spectator: Spectator<PortalAppsDetailComponent>;
  const createComponent = createComponentFactory({
    component: PortalAppsDetailComponent,
    declarations: [
      MockComponents(
        PortalAppDetailsWorkflowComponent,
      ),
      MockDirectives(
        TranslocoDirective
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
