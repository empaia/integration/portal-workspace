import { Spectator, createComponentFactory } from '@ngneat/spectator';

import { ToggleButtonsComponent } from './toggle-buttons.component';

describe('ToggleButtonsComponent', () => {
  let spectator: Spectator<ToggleButtonsComponent>;
  const createComponent = createComponentFactory(ToggleButtonsComponent);

  it('should create', () => {
    spectator = createComponent();

    expect(spectator.component).toBeTruthy();
  });
});
