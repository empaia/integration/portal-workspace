import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { ApiVersion } from '@api/public/models';

export interface ToggleButton<T> {
  enabled: boolean,
  selected: boolean,
  value: T,
  display: string,
}


@Component({
  selector: 'app-toggle-buttons',
  templateUrl: './toggle-buttons.component.html',
  styleUrls: ['./toggle-buttons.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToggleButtonsComponent {

  @Input() model: ToggleButton<ApiVersion>[] = [];
  // @Input() selected!: ApiVersion;
  private _selected!: ApiVersion | undefined;
  @Input() public get selected() {
    return this._selected;
  }
  public set selected(val: ApiVersion | undefined) {
    this._selected = val;
    this.model.forEach(v => {
      if (v.value === val) {
        v.selected = true;
      } else {
        v.selected = false;
      }
    });
  }

  @Output() public selectionChanged = new EventEmitter<{ value: ApiVersion }>();
  // constructor() {
  //   this.initToggles();
  // }

  // initToggles(): ToggleButton<ApiVersion>[] {
  //   const b: ToggleButton<ApiVersion>[] = [];
  //   Object.entries(ApiVersion).map(([v, k]) => b.push({
  //     display: v,
  //     value: k,
  //     enabled: false,
  //     selected: false
  //   }));
  //   return b;
  // }

}
