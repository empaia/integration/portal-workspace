import { OrganizationDetailComponent } from './organization-detail.component';
import { MockComponents, MockDirectives } from 'ng-mocks';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';
import { OrganizationDetailContactDataItemComponent } from '@public/components/organization-detail-contact-data-item/organization-detail-contact-data-item.component';
import { TranslocoDirective } from '@ngneat/transloco';

describe('OrganizationDetailComponent', () => {
  let spectator: Spectator<OrganizationDetailComponent>;
  const createComponent = createComponentFactory({
    component: OrganizationDetailComponent,
    imports: [
      MaterialModule
    ],
    declarations: [
      MockComponents(
        OrganizationDetailContactDataItemComponent,
      ),
      MockDirectives(
        TranslocoDirective
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
