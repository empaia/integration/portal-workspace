import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { RequestState } from '@api/aaa/models';
import { OrganizationEntity } from '@public/store/organizations/organizations.models';
import { MembershipRequestEntity } from '@user/store/membership-request';
import { blankOrgImage } from 'src/app/app.models';

@Component({
  selector: 'app-organization-detail',
  templateUrl: './organization-detail.component.html',
  styleUrls: ['./organization-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OrganizationDetailComponent {

  protected readonly RequestState = RequestState;

  protected readonly fallbackImg = blankOrgImage;

  @Input() public organization!: OrganizationEntity | undefined;
  @Input() public request!: MembershipRequestEntity | undefined;

  @Input() public loggedIn: boolean | undefined = false;
  @Input() public isMemberOfOrganization: boolean | undefined = false;

  @Output() public joinButtonClicked = new EventEmitter<{ organizationId?: string }>();


}
