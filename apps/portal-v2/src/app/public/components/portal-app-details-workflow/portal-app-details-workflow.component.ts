import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { MediaObject } from '@api/public/models/media-object';

@Component({
  selector: 'app-portal-app-details-workflow',
  templateUrl: './portal-app-details-workflow.component.html',
  styleUrls: ['./portal-app-details-workflow.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class PortalAppDetailsWorkflowComponent {
  @Input() public mediaObjects: MediaObject[] | undefined;
}
