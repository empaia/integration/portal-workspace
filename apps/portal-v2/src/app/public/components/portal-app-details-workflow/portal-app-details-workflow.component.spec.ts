import { PortalAppDetailsWorkflowComponent } from './portal-app-details-workflow.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { NgLightboxModule } from '@silmar/ng-lightbox';

describe('PortalAppDetailsWorkflowComponent', () => {
  let spectator: Spectator<PortalAppDetailsWorkflowComponent>;
  const createComponent = createComponentFactory({
    component: PortalAppDetailsWorkflowComponent,
    imports: [
      NgLightboxModule,
    ]
  });

  beforeEach(() => spectator = createComponent({
    props: {
      mediaObjects: []
    }
  }));

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
