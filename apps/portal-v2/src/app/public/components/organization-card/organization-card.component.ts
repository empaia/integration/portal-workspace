import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { OrganizationEntity } from '@public/store/organizations/organizations.models';
import { blankOrgImage } from 'src/app/app.models';

@Component({
  selector: 'app-organization-card',
  templateUrl: './organization-card.component.html',
  styleUrls: ['./organization-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OrganizationCardComponent {
  
  readonly fallbackImage = blankOrgImage;

  @Input() public organization!: OrganizationEntity;
}
