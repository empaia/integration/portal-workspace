import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { APP_SORTING_OPTIONS } from '@public/components/portal-apps-sort/portal-apps-sort.models';
import { SortBy } from '@shared/components/sort-selection/sort-selection.models';

@Component({
  selector: 'app-portal-apps-sort',
  templateUrl: './portal-apps-sort.component.html',
  styleUrls: ['./portal-apps-sort.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortalAppsSortComponent {
  @Input() public appSortSelected!: SortBy | undefined;
  @Input() public appSortOptions = APP_SORTING_OPTIONS;

  @Output() appSortChanged = new EventEmitter<SortBy>();
}
