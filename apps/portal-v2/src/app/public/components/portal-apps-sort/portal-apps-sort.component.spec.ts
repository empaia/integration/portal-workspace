import { PortalAppsSortComponent } from './portal-apps-sort.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MockComponents } from 'ng-mocks';
import { SortSelectionComponent } from '@shared/components/sort-selection/sort-selection.component';

describe('PortalAppsSortComponent', () => {
  let spectator: Spectator<PortalAppsSortComponent>;
  const createComponent = createComponentFactory({
    component: PortalAppsSortComponent,
    declarations: [
      MockComponents(
        SortSelectionComponent,
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
