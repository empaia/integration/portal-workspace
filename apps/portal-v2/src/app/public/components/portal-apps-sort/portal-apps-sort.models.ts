import { SortBy, SortOption } from '@shared/components/sort-selection/sort-selection.models';
import { Language } from '@api/public/models/language';

export const APP_SORTING_OPTIONS: SortOption[] = [
  {
    name: SortBy.DATE,
    value: [
      {
        lang: Language.DE,
        text: 'Neueste'
      },
      {
        lang: Language.EN,
        text: 'Newest'
      }
    ]
  },
  // feature removed
  // lets keep the code for now...
  // {
  //   name: SortBy.NAME,
  //   value: [
  //     {
  //       lang: Language.De,
  //       text: 'App Name'
  //     },
  //     {
  //       lang: Language.En,
  //       text: 'App Name'
  //     }
  //   ]
  // },
  {
    name: SortBy.VENDOR,
    value: [
      {
        lang: Language.DE,
        text: 'Herstellername'
      },
      {
        lang: Language.EN,
        text: 'Vendor name'
      }
    ]
  }
];
