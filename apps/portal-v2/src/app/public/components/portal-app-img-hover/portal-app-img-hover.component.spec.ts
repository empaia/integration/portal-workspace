import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PortalAppImgHoverComponent } from './portal-app-img-hover.component';

describe('PortalAppImgHoverComponent', () => {
  let component: PortalAppImgHoverComponent;
  let fixture: ComponentFixture<PortalAppImgHoverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PortalAppImgHoverComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PortalAppImgHoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
