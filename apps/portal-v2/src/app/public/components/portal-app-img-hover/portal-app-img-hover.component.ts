import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { MediaObject } from '@api/public/models';
import { fallbackImage } from 'src/app/app.models';

@Component({
  selector: 'app-portal-app-img-hover',
  templateUrl: './portal-app-img-hover.component.html',
  styleUrls: ['./portal-app-img-hover.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortalAppImgHoverComponent {

  protected fallbackImage = fallbackImage;

  @Input() public peekImages!: MediaObject[] | undefined;
  @Input() public over = false;

}
