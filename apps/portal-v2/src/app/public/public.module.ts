import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import 'img-comparison-slider';



import { PublicRoutingModule } from './public-routing.module';
import { PortalAppCardComponent } from './components/portal-app-card/portal-app-card.component';
import { SharedModule } from '../shared/shared.module';
import { PortalAppImgHoverComponent } from './components/portal-app-img-hover/portal-app-img-hover.component';
import { PortalAppsSortComponent } from './components/portal-apps-sort/portal-apps-sort.component';
import { PortalAppsDetailComponent } from './components/portal-apps-detail/portal-apps-detail.component';
import { PortalAppDetailContainerComponent } from './containers/portal-app-detail-container/portal-app-detail-container.component';
import { OrganizationsContainerComponent } from './containers/organizations-container/organizations-container.component';
import { MarketplaceContainerComponent } from './containers/marketplace-container/marketplace-container.component';
import { PortalAppCountComponent } from './components/portal-app-count/portal-app-count.component';
import { OrganizationCardComponent } from './components/organization-card/organization-card.component';
import { OrganizationDetailComponent } from './components/organization-detail/organization-detail.component';
import { OrganizationDetailContainerComponent } from './containers/organization-detail-container/organization-detail-container.component';
import { PortalAppDetailsWorkflowComponent } from './components/portal-app-details-workflow/portal-app-details-workflow.component';
import { OrganizationDetailContactDataItemComponent } from './components/organization-detail-contact-data-item/organization-detail-contact-data-item.component';
import { ToggleButtonsComponent } from './components/toggle-buttons/toggle-buttons.component';
import { TranslatePipe } from '@shared/pipes/translate/translate.pipe';
import { PublicRootStoreModule } from './store/public-root-store.module';
import { ClearancesContainerComponent } from './containers/clearances-container/clearances-container.component';
import { TranslocoRootModule } from '@transloco/transloco-root.module';
import { MarkdownModule } from 'ngx-markdown';

const components = [
  PortalAppCardComponent,
  PortalAppImgHoverComponent,
  PortalAppsSortComponent,
  PortalAppsDetailComponent,
  PortalAppDetailContainerComponent,
  OrganizationsContainerComponent,
  MarketplaceContainerComponent,
  PortalAppCountComponent,
  PortalAppDetailsWorkflowComponent,
  OrganizationCardComponent,
  OrganizationDetailComponent,
  OrganizationDetailContainerComponent,
  OrganizationDetailContactDataItemComponent,
  ToggleButtonsComponent,
  ClearancesContainerComponent,
];

@NgModule({
  declarations: [
    components,
  ],
  imports: [
    CommonModule,
    SharedModule,
    TranslocoRootModule,
    PublicRoutingModule,
    PublicRootStoreModule,
    MarkdownModule,
  ],
  providers: [
    TranslatePipe
  ],
  // for 'img-comparison-slider';
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class PublicModule { }
