import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { PortalAppsSelectors } from '@public/store/portal-apps';
import { PortalAppOrganizationEntity } from '@public/store/portal-apps/portal-apps.models';
import { AppRoutes } from '@router/router.constants';
import { ApiVersion } from '@api/public/models';
import * as RouterActions from '@router/router.actions';
import { RouterSelectors } from '@router/router.selectors';

@Component({
  selector: 'app-portal-app-detail-container',
  templateUrl: './portal-app-detail-container.component.html',
  styleUrls: ['./portal-app-detail-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortalAppDetailContainerComponent {

  protected app$!: Observable<PortalAppOrganizationEntity | undefined>;
  protected selectedAppApiVersion$!: Observable<ApiVersion | undefined>;

  constructor(
    public store: Store,
  ) {
    this.app$ = this.store.select(PortalAppsSelectors.selectSelectedAppWithOrg);
    this.selectedAppApiVersion$ = this.store.select(RouterSelectors.selectRouteParamAppApiVersionId);
  }

  apiVersionSelected($event: { appId: string, version: ApiVersion }): void {
    const route = [AppRoutes.MARKET_ROUTE, AppRoutes.PUBLIC_APP_ROUTE, $event.appId, $event.version];
    this.store.dispatch(RouterActions.navigate({ path: route }));
  }
}
