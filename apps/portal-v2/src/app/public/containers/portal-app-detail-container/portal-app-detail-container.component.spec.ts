import { PortalAppDetailContainerComponent } from './portal-app-detail-container.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MockComponents, MockPipe } from 'ng-mocks';
import { PortalAppsDetailComponent } from '@public/components/portal-apps-detail/portal-apps-detail.component';
import { PushPipe } from '@ngrx/component';
import { provideMockStore } from '@ngrx/store/testing';
import { LanguageService } from '@transloco/language.service';
import { TranslocoModule } from '@ngneat/transloco';

describe('PortalAppDetailContainerComponent', () => {
  let spectator: Spectator<PortalAppDetailContainerComponent>;
  const createComponent = createComponentFactory({
    component: PortalAppDetailContainerComponent,
    imports: [
      TranslocoModule
    ],
    declarations: [
      MockComponents(
        PortalAppsDetailComponent,
      ),
      MockPipe(
        PushPipe
      )
    ],
    providers: [
      provideMockStore(),
      LanguageService,
    ],
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
