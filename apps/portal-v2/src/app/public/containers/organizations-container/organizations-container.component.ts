import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { OrganizationEntity } from '@public/store/organizations/organizations.models';
import { OrganizationsSelectors } from '@public/store/organizations';
import { NavigationExtras } from '@angular/router';
import * as RouterActions from '@router/router.actions';
import { AppRoutes } from '@router/router.constants';
import { RouterSelectors } from '@router/router.selectors';
import { UnapprovedOrganizationsActions } from '@user/store/unapproved-organizations';
import { AuthSelectors } from '@auth/store/auth';

@Component({
  selector: 'app-organizations-container',
  templateUrl: './organizations-container.component.html',
  styleUrls: ['./organizations-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrganizationsContainerComponent {
  private store = inject(Store);
  public organizations$: Observable<OrganizationEntity[]>;
  public search$: Observable<string | undefined>;
  public loggedIn$: Observable<boolean>;

  constructor() {
    this.organizations$ = this.store.select(OrganizationsSelectors.selectSearchedOrgs);
    this.search$ = this.store.select(RouterSelectors.selectRouterQueryOrgSearch);
    this.loggedIn$ = this.store.select(AuthSelectors.selectLoggedIn);
  }

  cardClickedNavigate(id?: string): void {
    this.store.dispatch(RouterActions.navigate({
      path: [AppRoutes.PUBLIC_ORG_ROUTE, id]
    }));
  }

  searchSubmit($event: { search: string }): void {
    const search = $event.search.toLocaleLowerCase();
    const extras: NavigationExtras | undefined = search.length > 0 ? { queryParams: { search: search } } : undefined;

    this.store.dispatch(RouterActions.navigate({
      path: [AppRoutes.PUBLIC_ORG_ROUTE],
      extras,
    }));
  }

  createOrganization(): void {
    this.store.dispatch(UnapprovedOrganizationsActions.createOrganization());
  }

  identify(_index: number, item: OrganizationEntity): string {
    return item.organization_id;
  }
}
