import { OrganizationsContainerComponent } from './organizations-container.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';
import { MockComponents, MockDirectives } from 'ng-mocks';
import { TranslocoDirective } from '@ngneat/transloco';
import { SearchBarComponent } from '@shared/components/search-bar/search-bar.component';
import { OrganizationCardComponent } from '@public/components/organization-card/organization-card.component';
import { provideMockStore } from '@ngrx/store/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('OrganizationsContainerComponent', () => {
  let spectator: Spectator<OrganizationsContainerComponent>;
  const createComponent = createComponentFactory({
    component: OrganizationsContainerComponent,
    imports: [
      MaterialModule,
      RouterTestingModule
    ],
    declarations: [
      MockComponents(
        SearchBarComponent,
        OrganizationCardComponent,
      ),
      MockDirectives(
        TranslocoDirective
      ),
    ],
    providers: [
      provideMockStore()
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
