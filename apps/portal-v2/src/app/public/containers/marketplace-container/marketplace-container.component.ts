import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { PortalAppsActions, PortalAppsSelectors } from '@public/store/portal-apps';
import { AppTagList, MARKETPLACE_FILTER_CATEGORIES, PortalAppsFilterActions, PortalAppsFilterSelectors } from '@public/store/portal-apps-filter';
import { getLatestAppApiVersion, PortalAppEntity, PortalAppOrganizationEntity } from '@public/store/portal-apps/portal-apps.models';
import { AppRoutes } from '@router/router.constants';
import { Observable } from 'rxjs';
import { RouterSelectors } from '@router/router.selectors';
import { SortBy } from '@shared/components/sort-selection/sort-selection.models';

@Component({
  selector: 'app-marketplace-container',
  templateUrl: './marketplace-container.component.html',
  styleUrls: ['./marketplace-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MarketplaceContainerComponent {
  protected apps$: Observable<PortalAppOrganizationEntity[]>;
  protected appCount$: Observable<number>;
  protected appTotalCount$: Observable<number>;
  protected tags$: Observable<AppTagList>;
  protected activeTags$: Observable<AppTagList>;
  protected appSort$: Observable<SortBy>;

  public readonly MARKETPLACE_FILTERS = [...MARKETPLACE_FILTER_CATEGORIES];

  constructor(
    private readonly store: Store,
    private readonly router: Router,
  ) {
    this.store.dispatch(PortalAppsFilterActions.initPortalAppsFilters());

    this.apps$ = this.store.select(PortalAppsSelectors.selectAllSortedApps);
    this.appCount$ = this.store.select(PortalAppsSelectors.selectAppCount);
    this.appTotalCount$ = this.store.select(PortalAppsSelectors.selectAppTotalCount);
    this.tags$ = this.store.select(PortalAppsFilterSelectors.selectTags);
    this.activeTags$ = this.store.select(PortalAppsFilterSelectors.selectActiveFilters);
    this.appSort$ = this.store.select(RouterSelectors.selectRouterQuerySortBy);
  }

  id(_index: number, item: PortalAppEntity) {
    return item.id;
  }

  cardClickedNavigate($event: { app: PortalAppEntity }): void {
    const apiVersion = getLatestAppApiVersion($event.app);
    const id = $event.app.id;
    this.router.navigate([AppRoutes.MARKET_ROUTE, AppRoutes.PUBLIC_APP_ROUTE, id, apiVersion]);
  }

  onSortBySelection(sortBy: SortBy): void {
    this.store.dispatch(PortalAppsActions.sortAppsBy({ sortBy }));
  }

  selectedFilters(filters: AppTagList): void {
    this.store.dispatch(PortalAppsFilterActions.setPortalAppsFilters({
      tags: filters
    }));
  }

}
