import { MarketplaceContainerComponent } from './marketplace-container.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MockComponents, MockDirectives, MockPipes } from 'ng-mocks';
import { PortalAppFilterBarComponent } from '@shared/components/portal-app-filter-bar/portal-app-filter-bar.component';
import { PortalAppCountComponent } from '@public/components/portal-app-count/portal-app-count.component';
import { PortalAppCardComponent } from '@public/components/portal-app-card/portal-app-card.component';
import { provideMockStore } from '@ngrx/store/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { LetDirective, PushPipe } from '@ngrx/component';
import { PortalAppsSortComponent } from '@public/components/portal-apps-sort/portal-apps-sort.component';
import { TranslocoDirective, TranslocoPipe } from '@ngneat/transloco';

describe('MarketplaceContainerComponent', () => {
  let spectator: Spectator<MarketplaceContainerComponent>;
  const createComponent = createComponentFactory({
    component: MarketplaceContainerComponent,
    imports: [
      RouterTestingModule,
    ],
    declarations: [
      MockComponents(
        PortalAppFilterBarComponent,
        PortalAppCountComponent,
        PortalAppCardComponent,
        PortalAppsSortComponent,
      ),
      MockDirectives(
        LetDirective,
        TranslocoDirective
      ),
      MockPipes(
        PushPipe,
        TranslocoPipe
      )
    ],
    providers: [
      provideMockStore(),
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
