import { OrganizationDetailContainerComponent } from './organization-detail-container.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MockComponents, MockPipes } from 'ng-mocks';
import { OrganizationDetailComponent } from '@public/components/organization-detail/organization-detail.component';
import { provideMockStore } from '@ngrx/store/testing';
import { PushPipe } from '@ngrx/component';

describe('OrganizationDetailContainerComponent', () => {
  let spectator: Spectator<OrganizationDetailContainerComponent>;
  const createComponent = createComponentFactory({
    component: OrganizationDetailContainerComponent,
    declarations: [
      MockComponents(
        OrganizationDetailComponent
      ),
      MockPipes(
        PushPipe
      )
    ],
    providers: [
      provideMockStore()
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
