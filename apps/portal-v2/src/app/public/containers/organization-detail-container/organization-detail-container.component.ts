import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { OrganizationEntity } from '@public/store/organizations/organizations.models';
import { Store } from '@ngrx/store';
import { OrganizationsSelectors } from '@public/store/organizations';
import { AuthSelectors } from '@auth/store/auth';
import { MembershipRequestActions, MembershipRequestEntity, MembershipRequestSelectors } from '@user/store/membership-request';
import { UserOrganizationsSelectors } from '@user/store/user-organizations';

@Component({
  selector: 'app-organization-detail-container',
  templateUrl: './organization-detail-container.component.html',
  styleUrls: ['./organization-detail-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OrganizationDetailContainerComponent {
  public organization$: Observable<OrganizationEntity | undefined>;
  public request$: Observable<MembershipRequestEntity | undefined>;

  public loggedIn$: Observable<boolean>;
  public memberOfOrganization$: Observable<boolean>;

  constructor(
    private store: Store
  ) {
    this.organization$ = this.store.select(OrganizationsSelectors.selectSelectedOrganization);
    this.request$ = this.store.select(MembershipRequestSelectors.selectLastMembershipRequestsOfSelectedOrg);

    this.loggedIn$ = this.store.select(AuthSelectors.selectLoggedIn);
    this.memberOfOrganization$ = this.store.select(UserOrganizationsSelectors.selectIsMemberOfSelectedPublicOrganization);
  }

  joinOrgClick($event: { organizationId?: string }) {
    if ($event.organizationId) {
      this.store.dispatch(MembershipRequestActions.requestOrganizationMembership({ organizationId: $event.organizationId }));
    }
  }
}
