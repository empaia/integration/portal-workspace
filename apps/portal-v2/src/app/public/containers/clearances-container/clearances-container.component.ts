import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { ClearancesActions, ClearancesSelectors } from '@public/store/clearances';
import { ClearancesFilterActions, ClearancesFilterSelectors, ClearancesTagList } from '@public/store/clearances-filter';
import { RouterSelectors } from '@router/router.selectors';
import { ClearanceItemTableEntity } from '@shared/models/clearances.models';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-clearances-container',
  templateUrl: './clearances-container.component.html',
  styleUrls: ['./clearances-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClearancesContainerComponent {

  private store = inject(Store);
  protected clearances$: Observable<ClearanceItemTableEntity[]>;

  protected tags$: Observable<ClearancesTagList>;
  protected activeTags$: Observable<ClearancesTagList>;

  // number of filtered clearance entries
  public filteredClearanceCount$: Observable<number>;
  public search$: Observable<string | undefined>;

  constructor(){
    this.store.dispatch(ClearancesFilterActions.loadClearancesFiltered());

    this.clearances$ = this.store.select(ClearancesSelectors.selectAllClearancesWithOrg);

    this.tags$ = this.store.select(ClearancesFilterSelectors.selectTags);
    this.activeTags$ = this.store.select(ClearancesFilterSelectors.selectActiveFilters);
    this.filteredClearanceCount$ = this.store.select(ClearancesSelectors.selectFilteredClearanceCount);
    this.search$ = this.store.select(RouterSelectors.selectRouterQueryOrgSearch);
  }

  public selectedFilters(filters: ClearancesTagList): void {
    this.store.dispatch(ClearancesFilterActions.setClearancesFilters({
      tags: filters
    }));
  }

  public onOrganizationSearch(event: { search: string }): void {
    const search = event.search?.length ? event.search : undefined;
    this.store.dispatch(ClearancesFilterActions.setSearchFilter({
      search
    }));
  }

  public onExcelExport(): void {
    this.store.dispatch(ClearancesActions.exportClearancesToExcel());
  }
}
