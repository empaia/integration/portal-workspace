import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { PUBLIC_ROOT_FEATURE_KEY, reducers } from './public-root.selectors';
import { EffectsModule } from '@ngrx/effects';
import { PortalAppsFilterEffects } from './portal-apps-filter';
import { PortalAppsEffects } from './portal-apps';
import { OrganizationsEffects } from './organizations';
import { ClearancesFilterEffects } from './clearances-filter';
import { ClearancesEffects } from './clearances';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(
      PUBLIC_ROOT_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      OrganizationsEffects,
      PortalAppsEffects,
      PortalAppsFilterEffects,
      ClearancesEffects,
      ClearancesFilterEffects
    ])
  ]
})
export class PublicRootStoreModule { }
