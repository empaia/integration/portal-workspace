import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { fetch } from '@ngrx/router-store/data-persistence';
import { PublicControllerService } from '@api/aaa/services/public-controller.service';
import { State } from './organizations.reducer';
import * as OrganizationsActions from './organizations.actions';
import { ROUTER_NAVIGATED } from '@ngrx/router-store';
import { RouterSelectors } from '@router/router.selectors';
import { distinctUntilChanged, filter, map, take } from 'rxjs/operators';
import { filterNullish } from '@shared/utility/rxjs-utility';
import { HttpErrorResponse } from '@angular/common/http';
import { DEFAULT_PAGE_SIZE } from 'src/app/app.models';
import { AuthSelectors } from '@auth/store/auth';
import { ModerateUnapprovedOrganizationsActions } from '@moderator/store/moderate-unapproved-organizations';

@Injectable()
export class OrganizationsEffects {
  loadOrganizations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(OrganizationsActions.loadAllOrganizations),
      fetch({
        run: (
          _action: ReturnType<typeof OrganizationsActions.loadAllOrganizations>,
          _state: State
        ) => {
          return this.publicControllerService
            .getActiveOrganizations({
              page_size: DEFAULT_PAGE_SIZE,
            })
            .pipe(
              map((response) => response.organizations),
              map((organizations) =>
                OrganizationsActions.loadAllOrganizationsSuccess({
                  organizations: organizations ?? [],
                })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof OrganizationsActions.loadAllOrganizations>,
          error: HttpErrorResponse
        ) => {
          return OrganizationsActions.loadAllOrganizationsFailure({ error });
        },
      })
    );
  });

  loadOrganization$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(OrganizationsActions.loadOrganization),
      fetch({
        run: (
          action: ReturnType<typeof OrganizationsActions.loadOrganization>,
          _state: State
        ) => {
          return this.publicControllerService
            .getActiveOrganization({ organization_id: action.organizationId })
            .pipe(
              map((organization) => organization),
              map((organization) =>
                OrganizationsActions.loadOrganizationSuccess({ organization })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof OrganizationsActions.loadOrganization>,
          error: HttpErrorResponse
        ) => {
          return OrganizationsActions.loadOrganizationFailure({ error });
        },
      })
    );
  });

  initDetailsPage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ROUTER_NAVIGATED),
      concatLatestFrom(() =>
        this.store.select(RouterSelectors.selectRouterParamOrganizationId)
      ),
      map(([, id]) => id),
      distinctUntilChanged(),
      filterNullish(),
      map((organizationId) =>
        OrganizationsActions.loadOrganization({ organizationId })
      )
    );
  });

  // just load all organizations one time close to the start of the page
  initialOrganizationLoad$ = createEffect(() => {
    return this.store.select(AuthSelectors.selectIsDoneLoading).pipe(
      filter((loaded) => loaded),
      take(1),
      map(() => OrganizationsActions.loadAllOrganizations())
    );
  });

  // add organization on approval
  addOrganizationOnApproval$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ModerateUnapprovedOrganizationsActions.acceptUnapprovedOrganizationSuccess
      ),
      map((action) => action.organizationId),
      map((organizationId) =>
        OrganizationsActions.loadOrganization({ organizationId })
      )
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly publicControllerService: PublicControllerService
  ) {}
}
