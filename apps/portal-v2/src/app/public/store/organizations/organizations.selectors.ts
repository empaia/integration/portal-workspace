import { createSelector } from '@ngrx/store';
import { ORGANIZATIONS_FEATURE_KEY, publicOrganizationsAdapter } from './organizations.reducer';
import { selectRouterParamOrganizationId, selectRouterQueryOrgSearch } from '@router/router.selectors';
import { selectPublicRootState } from '../public-root.selectors';


const {
  selectAll,
  selectEntities
} = publicOrganizationsAdapter.getSelectors();

export const selectPublicOrganizationsState = createSelector(
  selectPublicRootState,
  (state) => state[ORGANIZATIONS_FEATURE_KEY]
);

export const selectOrganizationsLoaded = createSelector(
  selectPublicOrganizationsState,
  (state) => state.loaded
);

export const selectOrganizationsError = createSelector(
  selectPublicOrganizationsState,
  (state) => state.error
);

export const selectAllOrganizations = createSelector(
  selectPublicOrganizationsState,
  selectAll
);

export const selectOrganizationsEntities = createSelector(
  selectPublicOrganizationsState,
  selectEntities
);

export const selectSelectedOrganization = createSelector(
  selectOrganizationsEntities,
  selectRouterParamOrganizationId,
  (organizations, id) => id ? organizations[id] : undefined
);

export const selectSearchedOrgs = createSelector(
  selectAllOrganizations,
  selectRouterQueryOrgSearch,
  (orgs, search) => {
    if (search) {
      const f = orgs.filter(org => {
        const r = org.organization_name.toLocaleLowerCase().includes(search);
        return r ? org : undefined;
      });
      return f;
    }
    else { return orgs; }
  }
);
