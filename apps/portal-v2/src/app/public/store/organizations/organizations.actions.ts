import { createAction, props } from '@ngrx/store';
import { OrganizationEntity } from '@public/store/organizations/organizations.models';
import { HttpErrorResponse } from '@angular/common/http';

export const loadAllOrganizations = createAction(
  '[Organizations] Load All Organizations',
);

export const loadAllOrganizationsSuccess = createAction(
  '[Organizations] Load All Organizations Success',
  props<{ organizations: OrganizationEntity[] }>()
);

export const loadAllOrganizationsFailure = createAction(
  '[Organizations] Load All Organizations Failure',
  props<{ error: HttpErrorResponse }>()
);

export const loadOrganization = createAction(
  '[Organization] Load Organization',
  props<{ organizationId: string }>()
);

export const loadOrganizationSuccess = createAction(
  '[Organizations] Load Organization Success',
  props<{ organization: OrganizationEntity }>()
);

export const loadOrganizationFailure = createAction(
  '[Organizations] Load Organization Failure',
  props<{ error: HttpErrorResponse }>()
);
