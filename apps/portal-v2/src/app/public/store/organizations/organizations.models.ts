import { PublicOrganizationEntity, PublicOrganizationProfileEntity } from '@api/aaa/models';
import { Optional } from '@shared/utility/ts-utility';

// TODO: Remove Optional categories later
export type OrganizationEntity = Optional<PublicOrganizationEntity, 'categories'> & Optional<PublicOrganizationProfileEntity, 'contact_data' | 'details'>;

