import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { OrganizationEntity } from '@public/store/organizations/organizations.models';
import { HttpErrorResponse } from '@angular/common/http';
import * as OrganizationsActions from './organizations.actions';


export const ORGANIZATIONS_FEATURE_KEY = 'organizations';

export interface State extends EntityState<OrganizationEntity> {
  loaded: boolean;
  error?: HttpErrorResponse;
}

export function sortByName(a: OrganizationEntity, b: OrganizationEntity): number {
  // sort descending by creation date (newest first)
  return a.organization_name.localeCompare(b.organization_name);
}

export const publicOrganizationsAdapter = createEntityAdapter<OrganizationEntity>({
  selectId: organization => organization.organization_id,
  sortComparer: sortByName
});

export const initialState: State = publicOrganizationsAdapter.getInitialState({
  loaded: true,
});

export const reducer = createReducer(
  initialState,
  on(OrganizationsActions.loadAllOrganizations, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(OrganizationsActions.loadAllOrganizationsSuccess, (state, { organizations }): State =>
    publicOrganizationsAdapter.upsertMany(organizations, {
      ...state,
      loaded: true,
    })
  ),
  on(OrganizationsActions.loadAllOrganizationsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(OrganizationsActions.loadOrganization, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(OrganizationsActions.loadOrganizationSuccess, (state, { organization }): State =>
    publicOrganizationsAdapter.upsertOne(organization, {
      ...state,
      loaded: true,
    })
  ),
  on(OrganizationsActions.loadOrganizationFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  }))
);
