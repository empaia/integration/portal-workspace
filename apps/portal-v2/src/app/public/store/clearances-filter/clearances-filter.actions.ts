import { HttpValidationError } from '@api/public/models';
import { createAction, props } from '@ngrx/store';
import { ClearancesTagList } from './clearances-filter.models';


export const loadClearancesFiltered = createAction(
  '[ClearancesFilter] Initialize Clearances Filtered'
);

export const initClearancesFiltersSuccess = createAction(
  '[ClearancesFilter] Initialize ClearancesFilters Success',
  props<{ tags: ClearancesTagList }>()
);

export const initClearancesFiltersFailure = createAction(
  '[ClearancesFilter] Initialize ClearancesFilters Failure',
  props<{ error: HttpValidationError }>()
);

export const setClearancesFilters = createAction(
  '[ClearancesFilter] Set ClearancesFilters',
  props<{ tags: ClearancesTagList }>()
);

export const setClearancesFiltersSuccess = createAction(
  '[ClearancesFilter] Set ClearancesFilters success',
);

export const setClearancesFiltersFailure = createAction(
  '[ClearancesFilter] Set ClearancesFilters failure',
);

export const setSearchFilter = createAction(
  '[ClearancesFilter] Set Organization Search Filter',
  props<{ search?: string }>(),
);

export const setSearchFilterSuccess = createAction(
  '[ClearancesFilter] Set Organization Search Filter Success',
);

export const setSearchFilterFailure = createAction(
  '[ClearancesFilter] Set Organization Search Filter Failure',
);
