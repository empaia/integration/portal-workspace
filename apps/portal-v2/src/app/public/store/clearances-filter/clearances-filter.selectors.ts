import { createSelector } from '@ngrx/store';
import { CLEARANCES_FILTER_FEATURE_KEY, State } from './clearances-filter.reducer';
import { selectPublicRootState } from '../public-root.selectors';
import { selectRouteClearancesQueryFilters } from '@router/router.selectors';
import { ExtendedTagList } from '@api/public/models';



export const selectClearancesFilterState = createSelector(
  selectPublicRootState,
  (state) => state[CLEARANCES_FILTER_FEATURE_KEY]
);

export const selectTags = createSelector(
  selectClearancesFilterState,
  (state: State) => state.tags
);

export const selectActiveFilters = createSelector(
  selectTags,
  selectRouteClearancesQueryFilters,
  (tags, { analysis, indications, stains, tissues, clearances, image_types, procedures, scanners }) => {
    const foundTags: ExtendedTagList = {
      tissues: tags.tissues?.filter(t => tissues?.includes(t.name)),
      stains: tags.stains?.filter(t => stains?.includes(t.name)),
      indications: tags.indications?.filter(t => indications?.includes(t.name)),
      analysis: tags.analysis?.filter(t => analysis?.includes(t.name)),
      clearances: tags.clearances?.filter(t => clearances?.includes(t.name)),
      image_types: tags.image_types?.filter(t => image_types?.includes(t.name)),
      procedures: tags.procedures?.filter(t => procedures?.includes(t.name)),
      scanners: tags.scanners?.filter(t => scanners?.includes(t.name))
    };
    return foundTags;
  }
);
