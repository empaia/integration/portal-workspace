import { HttpValidationError } from '@api/public/models';
import { createReducer, on } from '@ngrx/store';
import * as ClearancesFilterActions from './clearances-filter.actions';
import { ClearancesTagList } from './clearances-filter.models';

export const CLEARANCES_FILTER_FEATURE_KEY = 'clearancesFilter';


export interface State {
  tags: ClearancesTagList;

  loaded: boolean;
  error?: HttpValidationError;
}

export const initialState: State = {
  tags: {},
  loaded: false,
};

export const reducer = createReducer(
  initialState,

  on(ClearancesFilterActions.loadClearancesFiltered, (state): State => state),
  on(ClearancesFilterActions.initClearancesFiltersSuccess,
    (state, { tags }): State => ({
      ...state,
      loaded: true,
      tags,
    })
  ),
  on(ClearancesFilterActions.initClearancesFiltersFailure,
    (state, { error }): State => ({
      ...state,
      loaded: false,
      error,
    })
  ),

);
