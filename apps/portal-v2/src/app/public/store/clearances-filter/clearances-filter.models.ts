import { Modify } from '@shared/utility/ts-utility';
import {  AppTag as AppTagEntity, ExtendedTagList } from '@api/public/models';

export type ClearancesTagList = ExtendedTagList;
export type AppTag = AppTagEntity;


export type TagListStringified = Modify<ClearancesTagList, {
  tissues?: string;
  stains?: string;
  indications?: string;
  analysis?: string;
  clearances?: string;
  image_types?: string;
  procedures?: string
}>;


export const DEFAULT_FILTER_CATEGORIES = [
  'tissues',
  'stains',
  'indications',
  'analysis',
  'clearances',
  'image_types',
  'procedures'
] as const;

// creates a type from a string array - array must be declared 'as const'
export type FilterCategory = typeof DEFAULT_FILTER_CATEGORIES[number];