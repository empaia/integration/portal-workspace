import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';

import { concatMap, debounceTime, map } from 'rxjs/operators';

import * as ClearancesFilterActions from './clearances-filter.actions';
import { Store } from '@ngrx/store';
import { State } from './clearances-filter.reducer';
import { fetch } from '@ngrx/router-store/data-persistence';
import { GeneralService } from '@api/public/services';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterSelectors } from '@router/router.selectors';
import {
  AppTag,
  ExtendedTagList,
  HttpValidationError,
} from '@api/public/models';
import { TagListStringified } from './clearances-filter.models';
import { from } from 'rxjs';
import { ClearancesActions } from '../clearances';

@Injectable()
export class ClearancesFilterEffects {
  loadTags$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClearancesFilterActions.loadClearancesFiltered),
      fetch({
        run: (
          _action: ReturnType<
            typeof ClearancesFilterActions.loadClearancesFiltered
          >,
          _state: State
        ) => {
          return this.generalService.tagsGet().pipe(
            map((tagList) =>
              ClearancesFilterActions.initClearancesFiltersSuccess({
                tags: tagList,
              })
            )
          );
        },
        onError: (
          _action: ReturnType<
            typeof ClearancesFilterActions.loadClearancesFiltered
          >,
          error: HttpValidationError
        ) => {
          console.error('Error', error);
          return ClearancesFilterActions.initClearancesFiltersFailure({
            error,
          });
        },
      })
    );
  });

  // on load of container component
  loadApps$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClearancesFilterActions.initClearancesFiltersSuccess),
      concatLatestFrom(() => [
        this.store.select(RouterSelectors.selectRouteClearancesQueryFilters),
      ]),
      map(([, tags]) => tags),
      map((tags) => ClearancesActions.loadClearances({ tags }))
    );
  });

  setSearchFilter$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClearancesFilterActions.setSearchFilter),
      map((action) => action.search),
      debounceTime(10),
      concatMap((search) =>
        from(
          this.router.navigate([], {
            relativeTo: this.activatedRoute,
            queryParams: { search },
            queryParamsHandling: 'merge',
          })
        )
      ),
      map((routeSuccess) =>
        routeSuccess
          ? ClearancesFilterActions.setSearchFilterSuccess()
          : ClearancesFilterActions.setSearchFilterFailure()
      )
    );
  });

  filterSelectionRouting$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClearancesFilterActions.setClearancesFilters),
      map((action) => action.tags),
      map((tags) => this.stringifyTagList(tags)),
      // wait for router to return success
      concatMap((stringTags) =>
        from(
          this.router.navigate([], {
            relativeTo: this.activatedRoute,
            queryParams: stringTags,
            queryParamsHandling: 'merge',
          })
        )
      ),
      map((routeSuccess) =>
        routeSuccess
          ? ClearancesFilterActions.setClearancesFiltersSuccess()
          : ClearancesFilterActions.setClearancesFiltersFailure()
      )
    );
  });

  // on tag filter change
  routeFiltersRefresh$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClearancesFilterActions.setClearancesFiltersSuccess),
      concatLatestFrom(() => [
        this.store.select(RouterSelectors.selectRouteClearancesQueryFilters),
      ]),
      map(([, tags]) => tags),
      map((tags) => ClearancesActions.loadClearances({ tags }))
    );
  });

  /**
   * Some browsers are using brackets for arrays in the URI like Firefox and others like
   * Chrome are using %5B and %5D for array brackets because brackets are reserved characters
   * defined in RFC 3986
   * https://stackoverflow.com/questions/11490326/is-array-syntax-using-square-brackets-in-url-query-strings-valid
   */
  private stringifyAppTags = (
    tags: AppTag[] | undefined
  ): string | undefined => {
    return tags?.length ? JSON.stringify(tags.map((t) => t.name)) : undefined;
  };

  private stringifyTagList = (
    tagList: ExtendedTagList | undefined
  ): TagListStringified => {
    return <TagListStringified>{
      tissues: this.stringifyAppTags(tagList?.tissues),
      stains: this.stringifyAppTags(tagList?.stains),
      indications: this.stringifyAppTags(tagList?.indications),
      analysis: this.stringifyAppTags(tagList?.analysis),
      clearances: this.stringifyAppTags(tagList?.clearances),
      procedures: this.stringifyAppTags(tagList?.procedures),
      image_types: this.stringifyAppTags(tagList?.image_types),
      scanners: this.stringifyAppTags(tagList?.scanners),
    };
  };

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly generalService: GeneralService
  ) {}
}
