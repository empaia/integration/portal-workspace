import { Modify } from '@shared/utility/ts-utility';
import { AppTag as AppTagEntity, ExtendedTagList } from '@api/public/models';

export type AppTagList = ExtendedTagList;
export type AppTag = AppTagEntity;


export type TagListStringified = Modify<AppTagList, {
  tissues?: string;
  stains?: string;
  indications?: string;
  analysis?: string;
  clearances?: string;
  // image_types?: string;
  // procedures?: string
}>;


export const DEFAULT_FILTER_CATEGORIES = [
  'tissues',
  'stains',
  'indications',
  'analysis',
  'clearances',
  // only for clearances
  'image_types',
  'procedures',
  'scanners'
] as const;

export const MARKETPLACE_FILTER_CATEGORIES = [
  'tissues',
  'stains',
  'indications',
  'analysis',
  'clearances',
] as const;

// creates a type from a string array - array must be declared 'as const'
export type FilterCategory = typeof DEFAULT_FILTER_CATEGORIES[number];
