import { createSelector } from '@ngrx/store';
import { PORTAL_APPS_FILTER_FEATURE_KEY, State } from './portal-apps-filter.reducer';
import { selectRouteQueryFilters } from '@router/router.selectors';
import { TagList } from '@api/public/models/tag-list';
import { selectPublicRootState } from '../public-root.selectors';


export const selectPortalAppsFilterState = createSelector(
  selectPublicRootState,
  (state) => state[PORTAL_APPS_FILTER_FEATURE_KEY]
);

export const selectTags = createSelector(
  selectPortalAppsFilterState,
  (state: State) => state.tags
);

// For marketplace filters
export const selectActiveFilters = createSelector(
  selectTags,
  selectRouteQueryFilters,
  (tags, { analysis, indications, stains, tissues, clearances }) => {
    const foundTags: TagList = {
      tissues: tags.tissues?.filter(t => tissues?.includes(t.name)),
      stains: tags.stains?.filter(t => stains?.includes(t.name)),
      indications: tags.indications?.filter(t => indications?.includes(t.name)),
      analysis: tags.analysis?.filter(t => analysis?.includes(t.name)),
      clearances: tags.clearances?.filter(t => clearances?.includes(t.name)),
    };
    return foundTags;
  }
);

