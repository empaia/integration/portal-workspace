import { HttpValidationError } from '@api/public/models';
import { createAction, props } from '@ngrx/store';
import { AppTagList } from './portal-apps-filter.models';


export const initPortalAppsFilters = createAction(
  '[PortalAppsFilter] Initialize PortalAppsFilters'
);

export const initPortalAppsFiltersSuccess = createAction(
  '[PortalAppsFilter] Initialize PortalAppsFilters Success',
  props<{ tags: AppTagList }>()
);

export const initPortalAppsFiltersFailure = createAction(
  '[PortalAppsFilter] Initialize PortalAppsFilters Failure',
  props<{ error: HttpValidationError }>()
);

export const setPortalAppsFilters = createAction(
  '[PortalAppsFilter] Set PortalAppsFilters',
  props<{ tags: AppTagList }>()
);

export const setPortalAppsFiltersSuccess = createAction(
  '[PortalAppsFilter] Set PortalAppsFilters success',
);

export const setPortalAppsFiltersFailure = createAction(
  '[PortalAppsFilter] Set PortalAppsFilters failure',
);




