import { HttpValidationError } from '@api/public/models';
import { createReducer, on } from '@ngrx/store';
import * as PortalAppsFilterActions from './portal-apps-filter.actions';
import { AppTagList } from './portal-apps-filter.models';

export const PORTAL_APPS_FILTER_FEATURE_KEY = 'portalAppsFilter';


export interface State {
  tags: AppTagList;

  loaded: boolean;
  error?: HttpValidationError;
}

export const initialState: State = {
  tags: {},
  loaded: false,
};

export const reducer = createReducer(
  initialState,

  on(PortalAppsFilterActions.initPortalAppsFilters, (state): State => state),
  on(PortalAppsFilterActions.initPortalAppsFiltersSuccess,
    (state, { tags }): State => ({
      ...state,
      loaded: true,
      tags,
    })
  ),
  on(PortalAppsFilterActions.initPortalAppsFiltersFailure,
    (state, { error }): State => ({
      ...state,
      loaded: false,
      error,
    })
  ),

);
