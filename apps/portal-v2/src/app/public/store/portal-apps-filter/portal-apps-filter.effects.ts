import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';

import { concatMap, map } from 'rxjs/operators';

import * as PortalAppsFilterActions from './portal-apps-filter.actions';
import { Store } from '@ngrx/store';
import { State } from './portal-apps-filter.reducer';
import { fetch } from '@ngrx/router-store/data-persistence';
import { GeneralService } from '@api/public/services';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterSelectors } from '@router/router.selectors';
import { PortalAppsActions } from '@public/store/portal-apps';
import { AppTag, HttpValidationError, TagList } from '@api/public/models';
import { TagListStringified } from './portal-apps-filter.models';
import { from } from 'rxjs';

@Injectable()
export class PortalAppsFilterEffects {
  loadTags$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PortalAppsFilterActions.initPortalAppsFilters),
      fetch({
        run: (
          _action: ReturnType<
            typeof PortalAppsFilterActions.initPortalAppsFilters
          >,
          _state: State
        ) => {
          return this.generalService.tagsGet().pipe(
            map((tagList) =>
              PortalAppsFilterActions.initPortalAppsFiltersSuccess({
                tags: tagList,
              })
            )
          );
        },
        onError: (
          _action: ReturnType<
            typeof PortalAppsFilterActions.initPortalAppsFilters
          >,
          error: HttpValidationError
        ) => {
          console.error('Error', error);
          return PortalAppsFilterActions.initPortalAppsFiltersFailure({
            error,
          });
        },
      })
    );
  });

  // on load of container component
  loadApps$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PortalAppsFilterActions.initPortalAppsFiltersSuccess),
      concatLatestFrom(() =>
        this.store.select(RouterSelectors.selectRouteQueryFilters)
      ),
      map(([_action, tags]) => tags),
      map((tags) => PortalAppsActions.loadPortalApps({ tags }))
    );
  });

  filterSelectionRouting$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PortalAppsFilterActions.setPortalAppsFilters),
      map((action) => action.tags),
      map((tags) => this.stringifyTagList(tags)),
      // wait for router to return success
      concatMap((stringTags) =>
        from(
          this.router.navigate([], {
            relativeTo: this.activatedRoute,
            queryParams: stringTags,
            queryParamsHandling: 'merge',
          })
        )
      ),
      map((routeSuccess) =>
        routeSuccess
          ? PortalAppsFilterActions.setPortalAppsFiltersSuccess()
          : PortalAppsFilterActions.setPortalAppsFiltersFailure()
      )
    );
  });

  // on tag filter change
  routeFiltersRefresh$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PortalAppsFilterActions.setPortalAppsFiltersSuccess),
      concatLatestFrom(() =>
        this.store.select(RouterSelectors.selectRouteQueryFilters)
      ),
      map(([_action, tags]) => tags),
      // tap((tags) => console.log('filters changes', tags)),
      map((tags) => PortalAppsActions.loadPortalApps({ tags }))
    );
  });

  /**
   * Some browsers are using brackets for arrays in the URI like Firefox and others like
   * Chrome are using %5B and %5D for array brackets because brackets are reserved characters
   * defined in RFC 3986
   * https://stackoverflow.com/questions/11490326/is-array-syntax-using-square-brackets-in-url-query-strings-valid
   */
  private stringifyAppTags = (
    tags: AppTag[] | undefined
  ): string | undefined => {
    return tags?.length ? JSON.stringify(tags.map((t) => t.name)) : undefined;
  };

  private stringifyTagList = (
    tagList: TagList | undefined
  ): TagListStringified => {
    return {
      tissues: this.stringifyAppTags(tagList?.tissues),
      stains: this.stringifyAppTags(tagList?.stains),
      indications: this.stringifyAppTags(tagList?.indications),
      analysis: this.stringifyAppTags(tagList?.analysis),
      clearances: this.stringifyAppTags(tagList?.clearances),
    };
  };

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly generalService: GeneralService
  ) {}
}
