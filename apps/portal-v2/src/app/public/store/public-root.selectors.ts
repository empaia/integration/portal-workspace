import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';

import * as fromOrganization from './organizations/organizations.reducer';
import * as fromPortalApps from './portal-apps/portal-apps.reducer';
import * as fromPortalAppsFilters from './portal-apps-filter/portal-apps-filter.reducer';
import * as fromClearances from './clearances/clearances.reducer';
import * as fromClearancesFilters from './clearances-filter/clearances-filter.reducer';

export const PUBLIC_ROOT_FEATURE_KEY = 'publicRoot';

export const selectPublicRootState = createFeatureSelector<PublicRootState>(
  PUBLIC_ROOT_FEATURE_KEY
);

export interface PublicRootState {
  [fromOrganization.ORGANIZATIONS_FEATURE_KEY]: fromOrganization.State;
  [fromPortalApps.PORTAL_APPS_FEATURE_KEY]: fromPortalApps.State;
  [fromPortalAppsFilters.PORTAL_APPS_FILTER_FEATURE_KEY]: fromPortalAppsFilters.State;
  [fromClearances.CLEARANCES_FEATURE_KEY]: fromClearances.State;
  [fromClearancesFilters.CLEARANCES_FILTER_FEATURE_KEY]: fromClearancesFilters.State;
}

export interface State {
  [PUBLIC_ROOT_FEATURE_KEY]: PublicRootState;
}

export function reducers(state: PublicRootState | undefined, action: Action) {
  return combineReducers({
    [fromOrganization.ORGANIZATIONS_FEATURE_KEY]: fromOrganization.reducer,
    [fromPortalApps.PORTAL_APPS_FEATURE_KEY]: fromPortalApps.reducer,
    [fromPortalAppsFilters.PORTAL_APPS_FILTER_FEATURE_KEY]: fromPortalAppsFilters.reducer,
    [fromClearances.CLEARANCES_FEATURE_KEY]: fromClearances.reducer,
    [fromClearancesFilters.CLEARANCES_FILTER_FEATURE_KEY]: fromClearancesFilters.reducer,
  })(state, action);
}
