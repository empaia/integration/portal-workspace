import { createSelector } from '@ngrx/store';
import { portalAppsAdapter, PORTAL_APPS_FEATURE_KEY, State } from './portal-apps.reducer';
import { RouterSelectors, selectRouterQuerySortBy } from '@router/router.selectors';
import { appSortFunctionFactory, PortalAppOrganizationEntity } from '@public/store/portal-apps/portal-apps.models';
import { selectAllOrganizations } from '@public/store/organizations/organizations.selectors';
import { selectPublicRootState } from '../public-root.selectors';

const {
  selectAll,
  selectEntities,
  selectTotal,
} = portalAppsAdapter.getSelectors();

export const selectPortalAppsState = createSelector(
  selectPublicRootState,
  (state) => state[PORTAL_APPS_FEATURE_KEY]
);

export const selectAppsLoaded = createSelector(
  selectPortalAppsState,
  (state: State) => state.loaded
);

export const selectAppsError = createSelector(
  selectPortalAppsState,
  (state: State) => state.error
);

export const selectAppsEntities = createSelector(
  selectPortalAppsState,
  selectEntities
);

export const selectAllApps = createSelector(
  selectPortalAppsState,
  selectAll
);

export const selectAllAppsWithOrg = createSelector(
  selectAllOrganizations,
  selectAllApps,
  (orgs, apps): PortalAppOrganizationEntity[] => {
    const ao = apps.map(app => {
      const org = orgs.find(org => org.organization_id === app.organization_id);
      const appOrg: PortalAppOrganizationEntity = { ...app, organization: org };
      return appOrg;
    });
    // console.log('selectAllAppsWithOrg', ao);
    return ao;
  }
);

export const selectAllSortedApps = createSelector(
  selectAllAppsWithOrg,
  selectRouterQuerySortBy,
  (apps, sortBy) => sortBy ? apps.sort(appSortFunctionFactory(sortBy)) : apps
);

export const selectAppCount = createSelector(
  selectPortalAppsState,
  (state: State) => selectTotal(state)
);

export const selectAppTotalCount = createSelector(
  selectPortalAppsState,
  (state: State) => state.count
);


export const selectSelectedApp = createSelector(
  selectAppsEntities,
  RouterSelectors.selectRouteParamAppId,
  (apps, appId) => appId ? apps[appId] : undefined
);

export const selectSelectedAppWithOrg = createSelector(
  selectSelectedApp,
  selectAllOrganizations,
  (app, orgs): PortalAppOrganizationEntity | undefined => {
    const organization = app ? orgs.find(org => org.organization_id === app.organization_id) : undefined;
    return (app && organization) ? { ...app, organization } : undefined;
  }
);

// TODO: implement filter in frontend.
// all apps are loaded so we could filter without further network requests
/* export const selectApps = createSelector(
  selectAllApps,
  selectActiveFilters,
  (apps: PublicPortalApp[], filters: TagGroup[]) => {

    console.log('active filters', filters);

    if (!filters) {
      return apps;
    }

    const hasFilters = filters.map(
      (item) => item.tags.length ? true : false
    ).some(i => i);

    if (!hasFilters) {
      return apps;
    }

    const appResults = new Map<string, PublicPortalApp>();

    // naive filter implementation - not giving desired results!
    filters.forEach((filterTagGroup) => {
      const filterGroupName = filterTagGroup.name;
      const filterTags = filterTagGroup.tags;
      apps.forEach((app) => {
        app.tags?.groups.forEach((appTagGroup) => {
          const appTags = appTagGroup.tags;
          const appGroupName = appTagGroup.name;
          if (appGroupName === filterGroupName) {
            if (appTags.length > 0 && filterTags.length > 0) {

              const appTagNames = appTags.map(at => at.name);
              const filterTagNames = filterTags.map(ft => ft.name);
              console.log('filterTagNames', filterTagNames);
              const matches = filterTagNames.every(v => appTagNames.includes(v));
              if (matches) {
                appResults.set(app.id, app);
              }

            }
          }
        });
      });
    });
    console.log('appResults.size', appResults.size);
    return Array.from(appResults.values());
  }
); */


