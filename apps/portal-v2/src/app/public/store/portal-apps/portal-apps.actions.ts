import { HttpValidationError, PortalAppQuery, PublicPortalAppList } from '@api/public/models';
import { createAction, props } from '@ngrx/store';
import { PortalAppEntity, PortalAppOrganizationEntity } from './portal-apps.models';
import { SortBy } from '@shared/components/sort-selection/sort-selection.models';

export const loadPortalApps = createAction(
  '[PortalApps] Load PortalApps',
  props<{ tags?: PortalAppQuery }>()
);

export const loadPortalAppsSuccess = createAction(
  '[PortalApps] Load PortalApps Success',
  props<{ apps: PublicPortalAppList }>()
);

export const loadPortalAppsFailure = createAction(
  '[PortalApps] Load PortalApps Failure',
  props<{ error: HttpValidationError }>()
);


export const initializeAppDetailPage = createAction(
  '[PortalApps] initialize App Detail Page',
  props<{ appId: string }>()
);

export const loadPortalApp = createAction(
  '[PortalApps] Load PortalApp',
  props<{ appId: string }>()
);

export const loadPortalAppSuccess = createAction(
  '[PortalApps] Load PortalApp Success',
  props<{ app: PortalAppEntity }>()
);

export const loadPortalAppFailure = createAction(
  '[PortalApps] Load PortalApp Failure',
  props<{ error: HttpValidationError }>()
);

export const sortAppsBy = createAction(
  '[PortalApps] Sort Apps By',
  props<{ sortBy: SortBy }>()
);

export const setOrganizationApps = createAction(
  '[PortalApps] Set Organization Apps',
  props<{ apps: PortalAppOrganizationEntity[] }>()
);
