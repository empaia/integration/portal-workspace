import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { map, distinctUntilChanged } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { ROUTER_NAVIGATED } from '@ngrx/router-store';
import { fetch } from '@ngrx/router-store/data-persistence';

import * as PortalAppsActions from './portal-apps.actions';

import { PortalAppService } from '@api/public/services';
import { RouterSelectors } from '@router/router.selectors';
import { State } from './portal-apps.reducer';
import { filterNullish } from '@shared/utility/rxjs-utility';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpValidationError } from '@api/public/models';

@Injectable()
export class PortalAppsEffects {
  loadApps$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PortalAppsActions.loadPortalApps),
      fetch({
        run: (
          action: ReturnType<typeof PortalAppsActions.loadPortalApps>,
          _state: State
        ) => {
          return this.portalAppService
            .portalAppsQueryPut({ body: action.tags ?? {} })
            .pipe(
              map((apps) => apps),
              map((apps) => PortalAppsActions.loadPortalAppsSuccess({ apps }))
            );
        },
        onError: (
          _action: ReturnType<typeof PortalAppsActions.loadPortalApps>,
          error: HttpValidationError
        ) => {
          console.error('Error', error);
          return PortalAppsActions.loadPortalAppsFailure({ error });
        },
      })
    );
  });

  loadApp$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PortalAppsActions.loadPortalApp),
      fetch({
        run: (
          action: ReturnType<typeof PortalAppsActions.loadPortalApp>,
          _state: State
        ) => {
          return this.portalAppService
            .portalAppsPortalAppIdGet({
              portal_app_id: action.appId,
            })
            .pipe(
              map((app) => PortalAppsActions.loadPortalAppSuccess({ app }))
            );
        },
        onError: (
          _action: ReturnType<typeof PortalAppsActions.loadPortalApp>,
          error: HttpValidationError
        ) => {
          console.error('Error', error);
          return PortalAppsActions.loadPortalAppFailure({ error });
        },
      })
    );
  });

  initDetailsPageLoadApp$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PortalAppsActions.initializeAppDetailPage),
      map((action) => PortalAppsActions.loadPortalApp({ appId: action.appId }))
    );
  });

  initDetailsPageOnRoute$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ROUTER_NAVIGATED),
      concatLatestFrom(() =>
        this.store.select(RouterSelectors.selectRouteParamAppId)
      ),
      map(([_, id]) => id),
      distinctUntilChanged(),
      filterNullish(),
      map((id) => PortalAppsActions.initializeAppDetailPage({ appId: id }))
    );
  });

  sortAppsBy$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(PortalAppsActions.sortAppsBy),
        map((action) => action.sortBy),
        // filterNullish(),
        distinctUntilChanged(),
        map((sortBy) =>
          this.router.navigate([], {
            relativeTo: this.activatedRoute,
            queryParams: { sort: sortBy },
            queryParamsHandling: 'merge',
          })
        )
      );
    },
    { dispatch: false }
  );

  constructor(
    private readonly actions$: Actions,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly store: Store,
    private readonly portalAppService: PortalAppService
  ) {}
}
