
import { HttpValidationError } from '@api/public/models';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import * as PortalAppsActions from './portal-apps.actions';
import { PortalAppEntity } from './portal-apps.models';

export const PORTAL_APPS_FEATURE_KEY = 'portalApps';

export interface State extends EntityState<PortalAppEntity> {

  count: number;
  loaded: boolean; // has the app list has been loaded
  error?: HttpValidationError | undefined; // last known error (if any)
}


export const portalAppsAdapter: EntityAdapter<PortalAppEntity> = createEntityAdapter<PortalAppEntity>();


export const initialState: State = portalAppsAdapter.getInitialState({
  loaded: true,
  count: 0,
});

export const reducer = createReducer(
  initialState,

  on(PortalAppsActions.loadPortalApps, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(PortalAppsActions.loadPortalAppsSuccess, (state, { apps }): State =>
    portalAppsAdapter.setAll(apps.items, {
      ...state,
      loaded: true,
      count: apps.item_count,
    })),
  on(PortalAppsActions.loadPortalAppsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error
  })),

  on(PortalAppsActions.loadPortalApp, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(PortalAppsActions.loadPortalAppSuccess, (state, { app }): State =>
    portalAppsAdapter.setOne(app, {
      ...state,
      loaded: true
    })),
  on(PortalAppsActions.loadPortalAppFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error
  })),
  on(PortalAppsActions.setOrganizationApps, (state, { apps }): State =>
    portalAppsAdapter.upsertMany(apps, {
      ...state,
      loaded: true,
    })
  ),
);
