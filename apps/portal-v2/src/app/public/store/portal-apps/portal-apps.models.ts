import { ApiVersion, PublicAppView, PublicPortalApp } from '@api/public/models';
import { SortBy } from '@shared/components/sort-selection/sort-selection.models';
import { OrganizationEntity } from '@public/store/organizations/organizations.models';



export type PortalAppEntity = PublicPortalApp;

export interface PortalAppOrganizationEntity extends PortalAppEntity {
  organization: OrganizationEntity | undefined;
}

function sortByDate(a: PortalAppEntity, b: PortalAppEntity): number {
  return Number(b.created_at) - Number(a.created_at);
}

function sortByAppName(a: PortalAppOrganizationEntity, b: PortalAppOrganizationEntity): number {
  const appViewA = getAppDetails(a);
  const appViewB = getAppDetails(b);

  if (!appViewA.details?.name && !appViewB.details?.name) {
    return 0;
  } else if (!appViewA.details?.name) {
    return 1;
  } else if (!appViewB.details?.name) {
    return -1;
  } else {
    return appViewA.details.name.toLowerCase().localeCompare(appViewB.details.name.toLowerCase());
  }
}


export function getAppDetails(app: PortalAppOrganizationEntity): PublicAppView {
  if (app.active_app_views) {
    if (app.active_app_views.v3) {
      return app.active_app_views.v3;
    } else if (app.active_app_views.v2) {
      return app.active_app_views.v2;
    } else if (app.active_app_views.v1) {
      return app.active_app_views.v1;
    }
    throw new Error(`app ${app.id} has no PublicAppView`);
  }
  throw new Error(`app ${app.id} has no PublicActiveAppViews`);
}

export function getLatestAppApiVersion(app: PortalAppEntity): ApiVersion {
  if (app.active_app_views) {
    if (app.active_app_views.v3) {
      return ApiVersion.V_3;
    } else if (app.active_app_views.v2) {
      return ApiVersion.V_2;
    } else if (app.active_app_views.v1) {
      return ApiVersion.V_1;
    }
    throw new Error(`app ${app.id} has no PublicAppView with valid Version`);
  }
  throw new Error(`app ${app.id} has no PublicActiveAppViews`);
}

export function hasAppApiVersion(app: PortalAppEntity, version: ApiVersion): boolean {
  if (app.active_app_views) {
    switch (version) {
      case ApiVersion.V_3:
        return app.active_app_views.v3 ? true : false;
      case ApiVersion.V_2:
        return app.active_app_views.v2 ? true : false;
      case ApiVersion.V_1:
        return app.active_app_views.v1 ? true : false;
      default:
        return false;
    }
  }
  throw new Error(`app ${app.id} has no PublicActiveAppViews`);
}

export function getSelectedApiAppView(app: PortalAppEntity, version: ApiVersion) {
  if (version === ApiVersion.V_1) {
    return app.active_app_views?.v1;
  } else if (version === ApiVersion.V_2) {
    return app.active_app_views?.v2;
  } else if (version === ApiVersion.V_3) {
    return app.active_app_views?.v3;
  }
  throw new Error(`Active app view Api version unknown`);
}

function sortByVendorName(a: PortalAppOrganizationEntity, b: PortalAppOrganizationEntity): number {
  if (!a.organization && !b.organization) {
    return 0;
  } else if (!a.organization) {
    return 1;
  } else if (!b.organization) {
    return -1;
  } else {
    if (a.organization?.organization_name && b.organization?.organization_name) {
      return a.organization?.organization_name?.toLowerCase().localeCompare(b.organization.organization_name?.toLowerCase());
    } else {
      return 0;
    }
  }
}

export function appSortFunctionFactory(sortBy: SortBy): (a: PortalAppOrganizationEntity, b: PortalAppOrganizationEntity) => number {
  switch (sortBy) {
    case SortBy.DATE:
      return sortByDate;
    case SortBy.NAME:
      return sortByAppName;
    case SortBy.VENDOR:
      return sortByVendorName;
    default:
      throw Error('Sort by type is not supported');
  }
}
