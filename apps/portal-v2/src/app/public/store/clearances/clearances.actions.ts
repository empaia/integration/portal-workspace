import { HttpValidationError, ClearanceQuery, PublicAggregatedClearanceList } from '@api/public/models';
import { createAction, props } from '@ngrx/store';

export const loadClearances = createAction(
  '[Clearances] Load Clearances',
  props<{ tags?: ClearanceQuery }>()
);

export const loadClearancesSuccess = createAction(
  '[Clearances] Load Clearances Success',
  props<{ apps: PublicAggregatedClearanceList }>()
);

export const loadClearancesFailure = createAction(
  '[Clearances] Load Clearances Failure',
  props<{ error: HttpValidationError }>()
);

export const exportClearancesToExcel = createAction(
  '[Clearances] Export Clearances To Excel',
);
