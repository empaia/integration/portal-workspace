import { PublicAggregatedClearance, TextTranslation } from '@api/public/models';
import { SortBy } from '@shared/components/sort-selection/sort-selection.models';
import { ClearanceItemTableEntity } from '@shared/models/clearances.models';



export type ClearanceEntity = PublicAggregatedClearance;


// below will be reused later when orgids are returned in clearances

function sortByDate(a: ClearanceEntity, b: ClearanceEntity): number {
  return Number(b.created_at) - Number(a.created_at);
}

// function sortByAppName(a: ClearanceOrganizationEntity, b: ClearanceOrganizationEntity): number {

//   if (!appViewA.details?.name && !appViewB.details?.name) {
//     return 0;
//   } else if (!appViewA.details?.name) {
//     return 1;
//   } else if (!appViewB.details?.name) {
//     return -1;
//   } else {
//     return appViewA.details.name.toLowerCase().localeCompare(appViewB.details.name.toLowerCase());
//   }
// }


function sortByVendorName(a: ClearanceItemTableEntity, b: ClearanceItemTableEntity): number {
  if (!a.organization && !b.organization) {
    return 0;
  } else if (!a.organization) {
    return 1;
  } else if (!b.organization) {
    return -1;
  } else {
    if (a.organization?.organization_name && b.organization?.organization_name) {
      return a.organization?.organization_name?.toLowerCase().localeCompare(b.organization.organization_name?.toLowerCase());
    } else {
      return 0;
    }
  }
}

export function appSortFunctionFactory(sortBy: SortBy): (a: ClearanceItemTableEntity, b: ClearanceItemTableEntity) => number {
  switch (sortBy) {
    case SortBy.DATE:
      return sortByDate;
    // case SortBy.NAME:
    //   return sortByAppName;
    case SortBy.VENDOR:
      return sortByVendorName;
    default:
      throw Error('Sort by type is not supported');
  }
}

export function searchInClearances(clearance: ClearanceItemTableEntity, search?: string): boolean | undefined {
  return search ? selectSearchProcedure(clearance, search) : true;
}

function searchInArray(array: Array<unknown>, search: string): boolean {
  return array.some(entry => selectSearchProcedure(entry, search));
}

function searchInObject(object: object, search: string): boolean {
  return Object.values(object).some(entry => selectSearchProcedure(entry, search));
}

function selectSearchProcedure(entry: unknown, search: string): boolean | undefined {
  if (entry instanceof Array) {
    return searchInArray(entry, search);
  } else if (entry instanceof Object && isTextTranslation(entry)) {
    return searchInTextTranslation(entry, search);
  } else if (entry instanceof Object) {
    return searchInObject(entry, search);
  } else {
    return entry?.toString().toLowerCase().includes(search);
  }
}

function searchInTextTranslation(translation: TextTranslation, search: string): boolean {
  return translation.text.toLowerCase().includes(search);
}

function isTextTranslation(object: object): object is TextTranslation {
  return 'lang' in object && 'text' in object;
}

export const PUBLIC_CLEARANCES_EXCEL_HEADLINES = [
  'Organization',
  'Product name',
  'Product release date',
  'Description',
  'References',
  'Last updated',
  'Provided by',
  'Procedure',
  'Indication',
  'Analysis',
  'Clearance',
  'Image type',
  'Scanner',
  'Tissue',
  'Stain'
];
export const DEFAULT_EXCEL_FILENAME = 'Pathology AI Register';
