import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { fetch } from '@ngrx/router-store/data-persistence';

import * as ClearancesActions from './clearances.actions';
import * as ClearancesSelectors from './clearances.selectors';

import { ClearancesService } from '@api/public/services';
import { State } from './clearances.reducer';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpValidationError } from '@api/public/models';
import { ExcelService } from '@shared/services/excel/excel.service';
import { DEFAULT_EXCEL_FILENAME, PUBLIC_CLEARANCES_EXCEL_HEADLINES, appSortFunctionFactory } from './clearances.models';
import { SortBy } from '@shared/components/sort-selection/sort-selection.models';

@Injectable()
export class ClearancesEffects {
  loadClearances$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClearancesActions.loadClearances),
      fetch({
        run: (
          action: ReturnType<typeof ClearancesActions.loadClearances>,
          _state: State
        ) => {
          return this.clearancesService
            .clearancesQueryPut({ body: action.tags ?? {} })
            .pipe(
              map((apps) => apps),
              map((apps) => ClearancesActions.loadClearancesSuccess({ apps }))
            );
        },
        onError: (
          _action: ReturnType<typeof ClearancesActions.loadClearances>,
          error: HttpValidationError
        ) => {
          console.error('Error', error);
          return ClearancesActions.loadClearancesFailure({ error });
        },
      })
    );
  });

  exportToExcel$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClearancesActions.exportClearancesToExcel),
      concatLatestFrom(() =>
        this.store.select(ClearancesSelectors.selectAllClearancesWithOrg)
      ),
      map(([, clearances]) => clearances.sort(appSortFunctionFactory(SortBy.VENDOR))),
      map(clearances => this.excelService.generateMarketSurveySheet(clearances, PUBLIC_CLEARANCES_EXCEL_HEADLINES, DEFAULT_EXCEL_FILENAME))
    );
  }, { dispatch: false });

  constructor(
    private readonly actions$: Actions,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly store: Store,
    private readonly clearancesService: ClearancesService,
    private readonly excelService: ExcelService,
  ) {}
}
