
import { HttpValidationError } from '@api/public/models';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import * as ClearancesActions from './clearances.actions';
import { ClearanceEntity } from './clearances.models';

export const CLEARANCES_FEATURE_KEY = 'clearances';

export interface State extends EntityState<ClearanceEntity> {

  count: number;
  loaded: boolean; // has the Clearances list has been loaded
  error?: HttpValidationError | undefined; // last known error (if any)
}


export const portalClearancesAdapter: EntityAdapter<ClearanceEntity> = createEntityAdapter<ClearanceEntity>({
  sortComparer: sortByName,
});

export function sortByName(a: ClearanceEntity, b: ClearanceEntity): number {
  return a.organization_id.localeCompare(b.organization_id);
}

export const initialState: State = portalClearancesAdapter.getInitialState({
  loaded: true,
  count: 0,
});

export const reducer = createReducer(
  initialState,

  on(ClearancesActions.loadClearances, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ClearancesActions.loadClearancesSuccess, (state, { apps }): State =>
    portalClearancesAdapter.setAll(apps.items, {
      ...state,
      loaded: true,
      count: apps.item_count,
    })),
  on(ClearancesActions.loadClearancesFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error
  })),

);
