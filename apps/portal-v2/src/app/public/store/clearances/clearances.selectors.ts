import { createSelector } from '@ngrx/store';
import { CLEARANCES_FEATURE_KEY, State, portalClearancesAdapter } from './clearances.reducer';
import { selectRouterQueryOrgSearch, selectRouterQuerySortBy } from '@router/router.selectors';
import { appSortFunctionFactory, searchInClearances } from '@public/store/clearances/clearances.models';
import { selectAllOrganizations } from '@public/store/organizations/organizations.selectors';
import { selectPublicRootState } from '../public-root.selectors';
import { ClearanceItemTableEntity } from '@shared/models/clearances.models';

const {
  selectAll,
  selectEntities,
  selectTotal,
} = portalClearancesAdapter.getSelectors();

export const selectClearancesState = createSelector(
  selectPublicRootState,
  (state) => state[CLEARANCES_FEATURE_KEY]
);

export const selectClearancesLoaded = createSelector(
  selectClearancesState,
  (state: State) => state.loaded
);

export const selectClearancesError = createSelector(
  selectClearancesState,
  (state: State) => state.error
);

export const selectClearancesEntities = createSelector(
  selectClearancesState,
  selectEntities
);

export const selectAllClearances = createSelector(
  selectClearancesState,
  selectAll
);

export const selectAllClearancesWithOrg = createSelector(
  selectAllOrganizations,
  selectAllClearances,
  selectRouterQueryOrgSearch,
  (orgs, clearances, search): ClearanceItemTableEntity[] => {
    const ao = clearances.map(app => {
      const org = orgs.find(org => org.organization_id === app.organization_id);
      const appOrg: ClearanceItemTableEntity = { ...app, organization: org };
      return appOrg;
    }).filter(c => searchInClearances(c, search?.toLowerCase()));
    // console.log('selectAllClearancesWithOrg', ao);
    // ao.sort((a, b) => {
    //   if (a.organization?.organization_name && b.organization?.organization_name) {
    //     return b.organization?.organization_name?.localeCompare(a.organization?.organization_name);
    //   } else {
    //     return 0;
    //   }
    // });

    return ao;
  }
);

export const selectAllSortedClearances = createSelector(
  selectAllClearancesWithOrg,
  selectRouterQuerySortBy,
  (clearances, sortBy) => sortBy ? clearances.sort(appSortFunctionFactory(sortBy)) : clearances
);

export const selectAppCount = createSelector(
  selectClearancesState,
  selectTotal
);

export const selectAppTotalCount = createSelector(
  selectClearancesState,
  (state: State) => state.count
);

export const selectFilteredClearanceCount = createSelector(
  selectAllClearancesWithOrg,
  (clearances) => clearances.length
);
