"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var os = require("os");
var get_base_image_files_1 = require("./get-base-image-files");
var get_lockfile_hash_1 = require("./get-lockfile-hash");
/**
 * main function
 * patch all files in file list with new version hash or
 * check if all files match specified version hash or
 * calculate current package-lock.json hash and do check / patch
 * script searches for baseImageName: and replaces docker version
 * new version must be passed as argument to this script
 * run this script from project root with
 * to patch new version:
 * $ ts-node ./tools/ci/scripts/base-image-hash.ts patch <sha256_hex_value>
 * to check version:
 * $ ts-node ./tools/ci/scripts/base-image-hash.ts check <sha256_hex_value>
 * to calculate workspace package-lock.json hash and patch it
 * $ ts-node ./tools/ci/scripts/base-image-hash.ts patch-current
 * to calculate workspace package-lock.json hash and check it
 * $ ts-node ./tools/ci/scripts/base-image-hash.ts check-current
 * @arg {string} hash - package-lock.json hash value
 */
(function () { return __awaiter(void 0, void 0, void 0, function () {
    var _a, args, mode, modeArg, versionHashArg, entries, errorCount, baseImageName, _i, entries_1, fileName, fileContent, lines, indexOfImageName, line, lineContent, indexOfVersion, imageLineString, currentVersion, logString, newLine, newFile, error_1, error_2;
    var _b;
    return __generator(this, function (_c) {
        switch (_c.label) {
            case 0:
                _a = process.argv, args = _a.slice(2);
                // default exit with error
                process.exitCode = 1;
                // check args
                if (!args || !args[0]) {
                    console.error("\n      ERROR expected first argument mode:\n      check / patch / check-current / patch-current");
                    if (args[0] === 'patch' || args[0] === 'check' || !args[1]) {
                        console.error("\n      ERROR expected second argument:\n      NEW-LOCKFILE-HASH");
                    }
                    console.error("\n      Got:", args);
                    return [2 /*return*/];
                }
                modeArg = args[0];
                if (modeArg === 'patch' ||
                    modeArg === 'check' ||
                    modeArg === 'patch-current' ||
                    modeArg === 'check-current') {
                    mode = modeArg;
                }
                else {
                    return [2 /*return*/];
                }
                versionHashArg = "";
                if (mode === 'check-current' || mode === 'patch-current') {
                    // const scriptResult = cp.execSync(`node ${__dirname}/base-image-get-lockfile-hash.js`).toString();
                    // versionHashArg = scriptResult.split(':').at(1)?.trim();
                    console.info("Using current lockfile hash!");
                    versionHashArg = (0, get_lockfile_hash_1.getLockfileHash)();
                }
                else if (args[1]) {
                    // version to patch -or- version to check
                    versionHashArg = args[1].trim();
                }
                else {
                    console.error("\n    ERROR expected second argument:\n    NEW-LOCKFILE-HASH");
                }
                entries = (0, get_base_image_files_1.getBaseImageFiles)();
                errorCount = 0;
                return [4 /*yield*/, (0, get_base_image_files_1.getBaseImageName)()];
            case 1:
                baseImageName = _c.sent();
                console.log('baseImageName', baseImageName);
                _i = 0, entries_1 = entries;
                _c.label = 2;
            case 2:
                if (!(_i < entries_1.length)) return [3 /*break*/, 12];
                fileName = entries_1[_i];
                _c.label = 3;
            case 3:
                _c.trys.push([3, 10, , 11]);
                console.info("".concat(os.EOL, "________________"));
                console.info("processing file: ".concat(fileName));
                return [4 /*yield*/, fs.promises.readFile("".concat(fileName), 'utf-8')];
            case 4:
                fileContent = _c.sent();
                lines = fileContent.toString().replace(/\r\n/g, '\n').split('\n');
                indexOfImageName = lines.map(function (line, index) {
                    if (line.search(baseImageName) > -1)
                        return index;
                }).filter(isNotNullish).pop();
                // string not found abort file
                if (indexOfImageName === undefined || indexOfImageName < 0) {
                    console.error('❌ ERROR: ci-base-image version string not found');
                    errorCount += 1;
                    return [3 /*break*/, 11];
                }
                ;
                line = lines[indexOfImageName];
                if (!line) return [3 /*break*/, 9];
                lineContent = line.split(' ');
                indexOfVersion = lineContent.findIndex(function (i) { return i.split(':').find(function (is) { return is === baseImageName; }); });
                imageLineString = lineContent[indexOfVersion];
                currentVersion = (_b = imageLineString === null || imageLineString === void 0 ? void 0 : imageLineString.split(':')[1]) === null || _b === void 0 ? void 0 : _b.trim();
                if (!(mode === 'check' || mode === 'check-current')) return [3 /*break*/, 5];
                logString = "Comparing hashes:\n            Version in file === Version to check\n            ".concat(currentVersion, " === ").concat(versionHashArg, "\n          ");
                if (currentVersion === versionHashArg) {
                    console.info(logString + '✅ Success: matches');
                }
                else {
                    console.info(logString + '❌ ERROR: no match');
                    errorCount += 1;
                }
                return [3 /*break*/, 9];
            case 5:
                if (!(mode === 'patch' || mode === 'patch-current')) return [3 /*break*/, 9];
                if (currentVersion === versionHashArg) {
                    console.info("\u2705 skipping file - hashes up to date");
                    return [3 /*break*/, 11];
                }
                else {
                    console.info("old version hash");
                    console.info("".concat(currentVersion));
                    console.info("new version hash:");
                    console.info("".concat(versionHashArg));
                }
                // console.info(`current base image:`)
                // console.info(line);
                // replace old version string with new one
                lineContent.splice(indexOfVersion, 1, "".concat(baseImageName, ":").concat(versionHashArg));
                newLine = lineContent.join(' ').trimEnd();
                // console.info(`new base image:`)
                // console.info(newLine);
                // replace old line with new one in line array
                lines.splice(indexOfImageName, 1, newLine);
                newFile = lines.join(os.EOL);
                _c.label = 6;
            case 6:
                _c.trys.push([6, 8, , 9]);
                // write new file to disk
                return [4 /*yield*/, fs.promises.writeFile(fileName, newFile, 'utf-8')];
            case 7:
                // write new file to disk
                _c.sent();
                return [3 /*break*/, 9];
            case 8:
                error_1 = _c.sent();
                console.error("ERROR: writing file ".concat(fileName));
                return [3 /*break*/, 9];
            case 9: return [3 /*break*/, 11];
            case 10:
                error_2 = _c.sent();
                console.error("ERROR: reading file at ".concat(fileName));
                return [3 /*break*/, 11];
            case 11:
                _i++;
                return [3 /*break*/, 2];
            case 12:
                process.exitCode = errorCount;
                return [2 /*return*/];
        }
    });
}); })();
function isNotNullish(value) {
    return value !== null && value !== undefined;
}
