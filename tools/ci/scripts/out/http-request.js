"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.httpRequest = void 0;
const https = require("https");
const http = require("http");
;
;
/**
 * Do a request with options provided.
 * uses https to make request
 * @param {Object} options - http req options
 * @return {Promise} a promise of request
 */
function makeGetRequest(options) {
    // console.debug('DEBUG: https req options ' + JSON.stringify(options));
    return new Promise((resolve, reject) => {
        const req = https.get(options, res => reqFuncBody(res, resolve));
        onReqError(req, reject);
    });
}
// uses http to make proxied request
function makeProxyGetRequest(options) {
    // console.debug('DEBUG: http req options ' + JSON.stringify(options));
    return new Promise((resolve, reject) => {
        const req = http.get(options, res => reqFuncBody(res, resolve));
        onReqError(req, reject);
    });
}
function reqFuncBody(res, resolve) {
    res.setEncoding('utf8');
    let responseBody = '';
    res.on('data', (chunk) => {
        responseBody += chunk;
    });
    res.on('end', () => {
        // console.debug(`DEBUG: responseBody: ${JSON.stringify(responseBody)}`);
        resolve(JSON.parse(responseBody));
    });
}
;
const onReqError = (req, reject) => {
    req.on('error', (err) => {
        reject(err);
    });
    req.end();
};
// for debugging on non public gitlab instances
function getAuthHeader({ token, headerType }) {
    //if (process.env.GITLAB_CI) {
    if (headerType === 'JOB-TOKEN') {
        // header for ci - token created by pipeline
        return { 'JOB-TOKEN': token };
    }
    else if (headerType === 'gitlab-ci-token') {
        // header for ci - created by pipeline
        return { 'gitlab-ci-token': token };
    }
    else if (headerType === 'AuthBearer') {
        // create via gitlab jwt api
        return { 'Authorization': 'Bearer ' + token };
    }
    else if (headerType === 'PRIVATE-TOKEN') {
        // header for local tests - create token via webui
        return { 'PRIVATE-TOKEN': token };
    }
    else if (headerType === 'BasicAuth') {
        return { 'Authorization': token };
    }
    return {};
}
async function httpRequest(args) {
    let authHeader;
    if (args.header && args.header?.token) {
        authHeader = getAuthHeader({ token: args.header.token, headerType: args.header.type });
    }
    console.debug(`request path: ${args.instanceUrl}${args.path}`);
    // console.debug(`request header:`, args.header);
    if (args.proxy) {
        const proxyUrl = new URL(args.proxy);
        const httpProxyOptions = {
            hostname: proxyUrl.hostname,
            port: proxyUrl.port,
            path: args.instanceUrl + args.path,
            headers: authHeader
        };
        return await makeProxyGetRequest(httpProxyOptions);
    }
    else {
        const iUrl = new URL(args.instanceUrl);
        const httpOptions = {
            hostname: iUrl.hostname,
            path: args.path,
            headers: authHeader
        };
        return await makeGetRequest(httpOptions);
    }
}
exports.httpRequest = httpRequest;
