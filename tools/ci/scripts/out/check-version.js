#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getTagVersion = exports.parseTags = void 0;
const semver = require("semver");
const path = require("path");
const promises_1 = require("fs/promises");
const http_request_1 = require("./http-request");
const color_stdout_1 = require("./color-stdout");
const load_dot_env_1 = require("./load-dot-env");
/**
 * get last tagged version for project
 * * @param {string} projectName name of project in monorepo
 * @param {string} projectId id of gitlab project - CI_PROJECT_ID
 * @param {string} instanceUrl gitlab base url - https://gitlab.com
 * @param {string} token gitlab token to access repo
 * @param {string} proxy
 * @return {Promise} a promise of request
 */
async function getTagVersion(args) {
    // requesting tags: https://docs.gitlab.com/ee/api/tags.html
    let reqPath = `/api/v4/projects/${args.projectId}/repository/tags?per_page=50&search=^${args.projectName}`;
    let token;
    let authHeader;
    // using project token to access api of private project
    token = process.env.CI_PROJECT_ACCESS_TOKEN || '';
    authHeader = { token, type: 'PRIVATE-TOKEN' };
    return (0, http_request_1.httpRequest)({ instanceUrl: args.instanceUrl, path: reqPath, proxy: args.proxy, header: authHeader });
}
exports.getTagVersion = getTagVersion;
/**
 * get version from main branch
 * @param {string} response response of get tags http req
 * @param {string} projectName as defined by path to project
 * @return {string} tag version
 */
function parseTags(response, projectName) {
    // console.debug('response: ' + JSON.stringify(response, null, ' '));
    if (response) {
        if (Array.isArray(response)) {
            const tags = [...response];
            // add 'project' property to objects - remove version from tag name - name: workbench-client-0.3.7 -> project: workbench-client
            tags.forEach((t) => t["project"] = t.name.slice(0, t.name.lastIndexOf('-')));
            // console.debug('tags: ' + JSON.stringify(tags, null, ' '));
            // get first item matching project (array is sorted by update date and time in descending order) see: https://docs.gitlab.com/ee/api/tags.html
            // console.debug('projectName ' + JSON.stringify(tags, null, ' '));
            const lastTag = tags.find((i) => i.project === projectName);
            if (lastTag) {
                // console.debug('lastTag: ' + JSON.stringify(lastTag, null, ' '));
                // parse version from name string - expects {projectName}-{major}.{minor}.{patch}
                const tagVersion = lastTag.name.slice(lastTag.name.lastIndexOf('-') + 1, lastTag.name.length);
                // console.debug('tagVersion: ' + JSON.stringify(tagVersion));
                return tagVersion;
            }
            else {
                return 'no-tag-found';
            }
        }
    }
    (0, color_stdout_1.printError)(`ERROR: No tag response from API`);
    return undefined;
}
exports.parseTags = parseTags;
function checkVersionDiff(thisV, lastV) {
    const semThis = semver.parse(thisV);
    const semLast = semver.parse(lastV);
    if (semThis && semLast) {
        const majDiff = semThis.major - semLast.major;
        const minDiff = semThis.minor - semLast.minor;
        const patchDiff = semThis.patch - semLast.patch;
        // console.log('Diff', majDiff, minDiff, patchDiff);
        if (majDiff > 1 || minDiff > 1 || patchDiff > 1) {
            (0, color_stdout_1.printWarn)(`⚠ WARN: version diff +${majDiff}.+${minDiff}.+${patchDiff}`);
            (0, color_stdout_1.printWarn)(`Version should be bumped by +1`);
        }
    }
}
/**
 * checks if version in current branch is higher than in main branch
 * expects a version.json in this format: { "version": "0.0.0" }
 * locally expects to be called with all params on ci params are set from env vars
 * Script does not support semver with trailing prerelease tags (for example, 1.2.3-alpha.3)
 * tag must end with with version in this format: -1.2.3
 */
(async () => {
    // default: exit with error
    process.exitCode = 1;
    // console.log('starting');
    const [, , ...args] = process.argv;
    // only json file arg is required!
    // other args only used for debugging on local machine
    if (!args || !args[0]) {
        console.error(`
      ERROR expected arguments:
      <path/to/version/file.json> (required)
    `);
        process.exit(1);
    }
    console.log(`args ${JSON.stringify(args)}`);
    // version file with path relative to project root
    const versionFilePath = args[0];
    if (!versionFilePath) {
        return;
    }
    if (!process.env.GITLAB_CI) {
        await (0, load_dot_env_1.loadDotEnv)();
    }
    // gitlab $CI_PROJECT_ID
    const projectId = process.env.CI_PROJECT_ID;
    // expects valid url - i.e. https://gitlab.com
    const gitlabInstanceUrl = process.env.CI_SERVER_URL;
    // gitlab api access token - not needed for public project
    // const token = process.env.CI_JOB_TOKEN || args.at(3);
    // optional proxy settings
    const proxy = process.env.HTTP_PROXY || undefined;
    // console.debug(`proxy ${JSON.stringify(proxy)}`);
    // if we got windows path replace backslash with slash
    // const normPath = versionFilePath.replaceAll('\\', '/');
    const normPath = versionFilePath.split('\\').join('/');
    // console.info(`normPath: ${normPath}`);
    // extract path - replace leading './' 
    //const projectPath = path.dirname(normPath).replaceAll('./', '');
    const projectPath = path.dirname(normPath).split('./').join('');
    console.info(`projectPath: ${projectPath}`);
    // extract filename
    const filename = path.basename(normPath);
    console.info(`filename: ${filename}`);
    // read version.json from this branch
    let fileContent;
    try {
        fileContent = await (0, promises_1.readFile)(`${normPath}`, "utf8");
    }
    catch (error) {
        (0, color_stdout_1.printError)(`ERROR: reading file at ${normPath}`);
        (0, color_stdout_1.printError)(`Does the file exist?`);
        return;
    }
    // let data = JSON.parse(await readFile(`${normPath}`, "utf8"));
    let data = JSON.parse(fileContent);
    const thisV = data.version;
    // extract project name from path to version json file - expects prefix libs or apps
    const projectName = String(projectPath).replace('apps/', '').replace('libs/', '').replace(filename, '').replace('/', '');
    console.debug(`projectName: ${projectName}`);
    if (projectName && projectId && gitlabInstanceUrl) {
        const response = await getTagVersion({
            projectName,
            projectId,
            instanceUrl: gitlabInstanceUrl,
            proxy
        });
        const tagVersion = parseTags(response, projectName);
        const lastV = tagVersion;
        console.debug(`this version ${thisV}`);
        console.debug(`last tagged version: ${lastV}`);
        const thisVs = semver.parse(thisV);
        if (thisVs && thisV && lastV) {
            // this indicates an initial publish attempt 
            // skip version check as there is no previously tagged version in repo
            if (thisVs?.version === '0.0.1' && lastV === 'no-tag-found') {
                (0, color_stdout_1.printInfo)(`Skipping compare with previous version`);
                (0, color_stdout_1.printSuccess)(`Version ${thisV} is valid for initial publish!`);
                process.exitCode = 0;
                return;
            }
            else if (lastV === 'no-tag-found') {
                (0, color_stdout_1.printError)(`ERROR: No previously tagged version found!`);
                (0, color_stdout_1.printInfo)(`INFO: Is this an un-versioned project? Set version to 0.0.1 to publish an initial version!`);
                return;
            }
            checkVersionDiff(thisV, lastV);
            // the default version bump check
            if (!semver.valid(thisV)) {
                (0, color_stdout_1.printError)(`ERROR: ${thisV} this version is invalid`);
            }
            if (!semver.valid(lastV)) {
                (0, color_stdout_1.printError)(`ERROR: ${lastV} LAST version is invalid!`);
            }
            if (semver.lte(thisV, lastV)) {
                (0, color_stdout_1.printError)(`ERROR: version bump in ${normPath} needed!`);
                (0, color_stdout_1.printError)(`this ${thisV} <= last tag ${lastV}`);
            }
            else {
                (0, color_stdout_1.printSuccess)(`Version is valid!`);
                (0, color_stdout_1.printSuccess)(`this ${thisV} > last tag ${lastV}`);
                process.exitCode = 0;
            }
        }
        else {
            (0, color_stdout_1.printError)(`Error parsing versions`);
            (0, color_stdout_1.printError)(`thisVs ${thisVs}`);
            (0, color_stdout_1.printError)(`thisV ${thisV}`);
            (0, color_stdout_1.printError)(`lastV ${lastV}`);
        }
    }
    else {
        (0, color_stdout_1.printError)(`Error parsing params`);
    }
})();
