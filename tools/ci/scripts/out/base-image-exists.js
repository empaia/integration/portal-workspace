"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const http_request_1 = require("./http-request");
const load_dot_env_1 = require("./load-dot-env");
async function getToken(args) {
    // requesting bearer token
    // const reqPath: string = `/jwt/auth?client_id=docker&offline_token=true&service=container_registry&scope=repository%3Aempaia%2Fintegration%2Fci-examples-frontend%2Fci-base-image%3Apull`
    const usr = process.env.CI_REGISTRY_USER;
    const pass = process.env.CI_REGISTRY_PASSWORD;
    const authToken = 'Basic ' + Buffer.from(usr + ':' + pass).toString('base64');
    const authHeader = { token: authToken, type: 'BasicAuth' };
    const reqPath = `/jwt/auth?client_id=docker&offline_token=true&service=container_registry&scope=repository:${args.projectPath}/${args.imageName}:pull`;
    return (0, http_request_1.httpRequest)({ instanceUrl: args.instanceUrl, path: reqPath, proxy: args.proxy, header: authHeader });
}
async function getTags(args) {
    // // requesting docker tags
    // instanceUrl = 'https://registry.gitlab.com';
    const reqPath = `/v2/${args.projectPath}/${args.imageName}/tags/list`;
    const authHeader = { token: args.token, type: 'AuthBearer' };
    return (0, http_request_1.httpRequest)({ instanceUrl: args.instanceUrl, path: reqPath, proxy: args.proxy, header: authHeader });
}
;
;
/**
 * check if a give version exists in gitlab docker registry
 * compile with: npm run tsc -- -p /tools/ci/scripts/tsconfig.ci-scripts.json
 * run with: node ./tools/ci/scripts/out/base-image-check-registry.js
 */
(async () => {
    // default: exit with error
    process.exitCode = 1;
    // console.log('starting');
    const [, , ...args] = process.argv;
    if (!args || !args[0]) {
        console.error(`
      ERROR expected arguments:
      version-tag (required) - check if this image exists
    `);
        return;
    }
    const versionTag = args[0];
    if (!versionTag) {
        return;
    }
    if (!process.env.GITLAB_CI) {
        await (0, load_dot_env_1.loadDotEnv)();
    }
    console.debug('process.env.CI_SERVER_URL', process.env.CI_SERVER_URL);
    // expects valid url - i.e. https://gitlab.com
    const gitlabInstanceUrl = process.env.CI_SERVER_URL;
    // env is registry.gitlab.com - prepend https
    const gitlabRegistryUrl = 'https://' + process.env.CI_REGISTRY;
    // repo path/name
    const projectPath = process.env.CI_PROJECT_PATH;
    const imageName = 'ci-base-image';
    // optional proxy settings
    const proxy = process.env.HTTP_PROXY || undefined;
    if (gitlabInstanceUrl && gitlabRegistryUrl && projectPath) {
        const tokenResp = await getToken({
            instanceUrl: gitlabInstanceUrl, projectPath, imageName, proxy
        });
        // console.debug('token:', tokenResp);
        if (tokenResp.token) {
            const tagsResp = await getTags({
                instanceUrl: gitlabRegistryUrl,
                projectPath,
                imageName,
                proxy,
                token: tokenResp.token
            });
            if (tagsResp.tags) {
                const set = new Set(tagsResp.tags);
                console.debug(set);
                const hasTag = set.has(versionTag);
                // console.debug(hasTag);
                const resultString = `${(hasTag ? '✅' : '❌')} Version tag${hasTag ? '' : ' not'} found in registry`;
                console.info(resultString);
                process.exitCode = hasTag ? 0 : 1;
                return;
            }
        }
    }
})();
