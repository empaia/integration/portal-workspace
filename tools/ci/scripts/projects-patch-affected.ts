import * as cp from 'child_process';
import * as path from 'path';
import * as fs from 'fs';
import * as semver from 'semver';
import * as os from 'os';
import { loadDotEnv } from './load-dot-env';
import { getTagVersion, parseTags } from './check-version';

/**
 * use with care! 
 * Highly experimental!
 * only run once per branch!
 * only use if you know what you are doing!
 * Always check git changes before committing after using this script!
 */

/**
 * Usage - run from project root
 * check mode - are all affected projects version bumped?
 * ts-node ./tools/ci/scripts/patch-affected.ts check
 * write mode - writes next versions to all affected files
 * ts-node ./tools/ci/scripts/patch-affected.ts write
 */

/**
 * Script auto patches versions for all affected projects in workspace.
 * Bump version by hand if any other semver bump is needed than patch.
 */

interface project {
  name: string;
  path: string;
  type: 'app' | 'lib';
}

type mode = 'write' | 'check' | 'patch-apps';

const getDirectories = (srcPath: string) => fs.readdirSync(srcPath).filter(file => fs.statSync(path.join(srcPath, file)).isDirectory());

(async () => {

  // console.log('starting');
  const [, , ...args] = process.argv;

  // only json file arg is required!
  // other args only used for debugging on local machine
  if (!args || !args[0]) {
    console.error(`
      ERROR expected argument:
      check
      ---or---
      write
    `);
    return;
  }

  if (!process.env.GITLAB_CI) {
    await loadDotEnv();
  }

  const mode: mode = args[0] as mode;

  let affectedArray: string[] = []

  // console.log(affected);
  if (mode === 'patch-apps') {
    const rootDir = process.cwd();
    const appsDir = rootDir + '/apps';
    affectedArray = getDirectories(appsDir).filter(p => !p.endsWith('-e2e'));
  }
  else {
    // get affected projects names
    const affected = cp.execSync(`nx print-affected base=origin/master --select=projects`).toString();
    // format to array and remove e2e test projects
    affectedArray = affected.trim().split(', ').filter(e => !e.endsWith('-e2e'));
  }

  const affectedProjects = readAffectedProjects(affectedArray);

  // console.log(affectedArray);

  // patch version / package file version field
  for (const project of affectedProjects) {
    // 'security-test' is not versioned
    if (project.name === 'security-test') continue;

    let fileContent;
    let newFile;
    try {
      console.log(project.path);
      fileContent = await fs.promises.readFile(`${project.path}`, 'utf-8');
    } catch (error) {
      console.error(`ERROR: reading file at ${project.path}`);
      continue;
    }

    const vJson = JSON.parse(fileContent);
    const version = vJson.version;
    // console.log(version);
    const semV = semver.coerce(version);
    if (semV) {
      const newV = semver.inc(semV, 'patch');
      if (newV == null) {
        throw new Error("newV is null");
      }
      vJson.version = newV;
      newFile = JSON.stringify(vJson, null, '  ').concat(os.EOL);

      const tagInfo = await getTagInfoFromApi(project);
      if (tagInfo) {
        let infoString = versionBumpWarn(tagInfo, newV);

        console.log(`${project.type} | ${project.name}:`);
        console.log(`    | this ${version} - next ${newV}`);
        console.log(`    | ${tagInfo} ${infoString}`);
        console.log(os.EOL);

        if (mode === 'write' || mode === 'patch-apps') {
          try {
            // const testPath = path.parse(app.path).dir + '/version-test.json';
            await fs.promises.writeFile(project.path, newFile, 'utf-8');
          } catch (error) {
            console.error(`ERROR: writing file for ${project.name}`);
          }
        }
      }
    }
  }
})();

function readAffectedProjects(affectedArray: string[]): project[] {
  // get project root
  // const rootDir = path.join(__dirname, '../../..');

  const rootDir = process.cwd();
  console.log(`project root: \n`, rootDir);

  const appsDir = rootDir + '/apps';
  const libsDir = rootDir + '/libs';

  // get all folder in sub dirs - used to determine project type (app or lib)
  const appsDirs = getDirectories(appsDir);
  const libsDirs = getDirectories(libsDir);
  // console.log(appsDirs);

  // filter only affected by project type
  const affectedApps = appsDirs.filter(v => affectedArray.includes(v));
  const affectedLibs = libsDirs.filter(v => affectedArray.includes(v));

  console.log(`affected apps: \n`, affectedApps);
  console.log(`affected libs: \n`, affectedLibs);

  // create array with interface containing type and path
  const affectedProjects: project[] = [];
  affectedApps.map(appName => affectedProjects.push({
    name: appName,
    path: appsDir + '/' + appName + '/version.json',
    type: 'app',
  }));
  affectedLibs.map(libName => affectedProjects.push({
    name: libName,
    path: libsDir + '/' + libName + '/package.json',
    type: 'lib',
  }));

  affectedProjects.forEach(i => i.path = i.path.split('\\').join('/'));

  return affectedProjects;
}

function versionBumpWarn(taggedVersion: string, currentVersion: string): string {
  // const tagVS = tagInfo.slice(tagInfo.search(' last tag '), tagInfo.length - 1);
  const tagVersion = semver.coerce(taggedVersion)
  // console.log('tagVersion', tagVersion?.version);

  const thisVersion = semver.parse(currentVersion);
  // console.log('thisVersion', thisVersion?.version);
  let infoString = ''
  if (thisVersion && tagVersion) {
    if (tagVersion?.patch + 1 === thisVersion?.patch) {
      infoString = '✅';
    }
    if (thisVersion?.patch !== tagVersion?.patch + 1) {
      infoString = `❌ ${os.EOL}    | --> Wanted ${tagVersion.major}.${tagVersion.minor}.${tagVersion.patch + 1} ?`;
    }
  }
  return infoString;
}

async function getTagInfoFromApi(project: project): Promise<string | undefined> {
  // request api
  const instanceUrl = process.env.CI_SERVER_URL;
  const projectId = process.env.CI_PROJECT_ID;
  const proxy = process.env.HTTP_PROXY;
  const projectName = project.name;
  // const scriptCall = `node ./tools/ci/scripts/out/check-version.js`

  // const pathParam = project.type === 'app' ? `./apps/${project.name}/version.json` : `./libs/${project.name}/package.json`;

  // const scriptExec = `${scriptCall} ${pathParam} ${projectId} ${baseUrl}`

  // const versionResp = cp.execSync(scriptExec).toString();
  // const tagInfo = versionResp.slice(versionResp.search('this '), versionResp.length).trim();
  // // console.info(tagInfo);

  if (instanceUrl && projectId && projectName) {

    const response = await getTagVersion({
      projectName,
      projectId,
      instanceUrl,
      proxy
    });
    const tagVersion = parseTags(response, projectName);

    return tagVersion;
  }
  return undefined;
}