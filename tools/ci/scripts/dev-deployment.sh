#!/bin/bash

git clone --recurse-submodules https://pw-baseimage-test:${GITLAB_TOKEN}@gitlab.com/empaia/integration/global-deployment.git
cd global-deployment

# just for testing - use main later
git checkout portal-v2-dev

python3 -m venv .venv
source .venv/bin/activate
export PYTHONPATH=$(pwd)
poetry install
cd deployments/dev/
cp sample.initialize.env .initialize.env
cp sample.env .env
./recreate_all_v2_api.sh 