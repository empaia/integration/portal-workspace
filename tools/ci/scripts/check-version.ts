#!/usr/bin/env node
import * as  semver from 'semver';
import * as path from 'path';
import { readFile } from 'fs/promises';
import { httpRequest, RequestHeader } from './http-request';
import { printError, printInfo, printSuccess, printWarn } from './color-stdout';
import { loadDotEnv } from './load-dot-env';


/**
 * get last tagged version for project
 * * @param {string} projectName name of project in monorepo
 * @param {string} projectId id of gitlab project - CI_PROJECT_ID
 * @param {string} instanceUrl gitlab base url - https://gitlab.com 
 * @param {string} token gitlab token to access repo 
 * @param {string} proxy
 * @return {Promise} a promise of request
 */
async function getTagVersion(
  args: {
    projectName: string,
    projectId: string,
    instanceUrl: string,
    proxy?: string
  }
): Promise<unknown> {
  // requesting tags: https://docs.gitlab.com/ee/api/tags.html
  let reqPath = `/api/v4/projects/${args.projectId}/repository/tags?per_page=50&search=^${args.projectName}`

  let token: string;
  let authHeader: RequestHeader;

  // using project token to access api of private project
  token = process.env.CI_PROJECT_ACCESS_TOKEN || '';
  authHeader = { token, type: 'PRIVATE-TOKEN' };

  return httpRequest({ instanceUrl: args.instanceUrl, path: reqPath, proxy: args.proxy, header: authHeader });
}

/**
 * get version from main branch
 * @param {string} response response of get tags http req
 * @param {string} projectName as defined by path to project
 * @return {string} tag version
 */
function parseTags(response: any, projectName: string): string | undefined {

  // console.debug('response: ' + JSON.stringify(response, null, ' '));

  if (response) {
    if (Array.isArray(response)) {
      const tags: any = [...response];
      // add 'project' property to objects - remove version from tag name - name: workbench-client-0.3.7 -> project: workbench-client
      tags.forEach((t: any) =>
        t["project"] = t.name.slice(0, t.name.lastIndexOf('-'))
      );
      // console.debug('tags: ' + JSON.stringify(tags, null, ' '));
      // get first item matching project (array is sorted by update date and time in descending order) see: https://docs.gitlab.com/ee/api/tags.html
      // console.debug('projectName ' + JSON.stringify(tags, null, ' '));
      const lastTag: any = tags.find((i: any) => i.project === projectName);
      if (lastTag) {
        // console.debug('lastTag: ' + JSON.stringify(lastTag, null, ' '));
        // parse version from name string - expects {projectName}-{major}.{minor}.{patch}
        const tagVersion = lastTag.name.slice(lastTag.name.lastIndexOf('-') + 1, lastTag.name.length);
        // console.debug('tagVersion: ' + JSON.stringify(tagVersion));

        return tagVersion;
      } else {
        return 'no-tag-found';
      }
    }
  }
  printError(`ERROR: No tag response from API`);
  return undefined;
}

function checkVersionDiff(thisV: string, lastV: string) {
  const semThis = semver.parse(thisV);
  const semLast = semver.parse(lastV);

  if (semThis && semLast) {

    const majDiff = semThis.major - semLast.major;
    const minDiff = semThis.minor - semLast.minor;
    const patchDiff = semThis.patch - semLast.patch;

    // console.log('Diff', majDiff, minDiff, patchDiff);

    if (majDiff > 1 || minDiff > 1 || patchDiff > 1) {
      printWarn(`⚠ WARN: version diff +${majDiff}.+${minDiff}.+${patchDiff}`);
      printWarn(`Version should be bumped by +1`);
    }
  }
}


/**
 * checks if version in current branch is higher than in main branch
 * expects a version.json in this format: { "version": "0.0.0" }
 * locally expects to be called with all params on ci params are set from env vars
 * Script does not support semver with trailing prerelease tags (for example, 1.2.3-alpha.3)
 * tag must end with with version in this format: -1.2.3
 */
(async () => {

  // default: exit with error
  process.exitCode = 1;

  // console.log('starting');
  const [, , ...args] = process.argv;

  // only json file arg is required!
  // other args only used for debugging on local machine
  if (!args || !args[0]) {
    console.error(`
      ERROR expected arguments:
      <path/to/version/file.json> (required)
    `);
    process.exit(1);
  }

  console.log(`args ${JSON.stringify(args)}`);

  // version file with path relative to project root
  const versionFilePath = args[0];

  if (!versionFilePath) { return; }

  if (!process.env.GITLAB_CI) {
    await loadDotEnv();
  }

  // gitlab $CI_PROJECT_ID
  const projectId = process.env.CI_PROJECT_ID;

  // expects valid url - i.e. https://gitlab.com
  const gitlabInstanceUrl = process.env.CI_SERVER_URL;

  // gitlab api access token - not needed for public project
  // const token = process.env.CI_JOB_TOKEN || args.at(3);

  // optional proxy settings
  const proxy = process.env.HTTP_PROXY || undefined;
  // console.debug(`proxy ${JSON.stringify(proxy)}`);

  // if we got windows path replace backslash with slash
  // const normPath = versionFilePath.replaceAll('\\', '/');
  const normPath = versionFilePath.split('\\').join('/');
  // console.info(`normPath: ${normPath}`);

  // extract path - replace leading './' 
  //const projectPath = path.dirname(normPath).replaceAll('./', '');
  const projectPath = path.dirname(normPath).split('./').join('');
  console.info(`projectPath: ${projectPath}`);
  // extract filename
  const filename = path.basename(normPath);
  console.info(`filename: ${filename}`);

  // read version.json from this branch
  let fileContent;
  try {
    fileContent = await readFile(`${normPath}`, "utf8");
  } catch (error) {
    printError(`ERROR: reading file at ${normPath}`);
    printError(`Does the file exist?`);
    return;
  }

  // let data = JSON.parse(await readFile(`${normPath}`, "utf8"));
  let data = JSON.parse(fileContent);
  const thisV: string = data.version;

  // extract project name from path to version json file - expects prefix libs or apps
  const projectName = String(projectPath).replace('apps/', '').replace('libs/', '').replace(filename, '').replace('/', '');
  console.debug(`projectName: ${projectName}`);

  if (projectName && projectId && gitlabInstanceUrl) {
    const response = await getTagVersion({
      projectName,
      projectId,
      instanceUrl: gitlabInstanceUrl,
      proxy
    });
    const tagVersion = parseTags(response, projectName);
    const lastV = tagVersion;

    console.debug(`this version ${thisV}`);
    console.debug(`last tagged version: ${lastV}`);

    const thisVs = semver.parse(thisV);

    if (thisVs && thisV && lastV) {
      // this indicates an initial publish attempt 
      // skip version check as there is no previously tagged version in repo
      if (thisVs?.version === '0.0.1' && lastV === 'no-tag-found') {
        printInfo(`Skipping compare with previous version`)
        printSuccess(`Version ${thisV} is valid for initial publish!`);
        process.exitCode = 0;
        return;
      } else if (lastV === 'no-tag-found') {
        printError(`ERROR: No previously tagged version found!`);
        printInfo(`INFO: Is this an un-versioned project? Set version to 0.0.1 to publish an initial version!`);
        return;
      }

      checkVersionDiff(thisV, lastV);
      // the default version bump check
      if (!semver.valid(thisV)) {
        printError(`ERROR: ${thisV} this version is invalid`);
      }
      if (!semver.valid(lastV)) {
        printError(`ERROR: ${lastV} LAST version is invalid!`);
      }
      if (semver.lte(thisV, lastV)) {
        printError(`ERROR: version bump in ${normPath} needed!`);
        printError(`this ${thisV} <= last tag ${lastV}`);
      } else {
        printSuccess(`Version is valid!`);
        printSuccess(`this ${thisV} > last tag ${lastV}`);
        process.exitCode = 0;
      }
    } else {
      printError(`Error parsing versions`);
      printError(`thisVs ${thisVs}`);
      printError(`thisV ${thisV}`);
      printError(`lastV ${lastV}`);
    }
  } else {
    printError(`Error parsing params`);
  }

})();

export { parseTags, getTagVersion };