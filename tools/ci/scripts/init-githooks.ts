import * as os from 'os';
import * as cp from 'child_process';


/**
 * script is called as postinstall script after running 'npm install'
 * setting up git pre commit hook in dev environment
 */
(async () => {

  // skip if we are in CI
  if (process.env.CI) {
    console.info(`CI detected - skipping git hook init`);
    return;
  }

  const commitHooksPath = './tools/ci/git-hooks/';
  const preCommitHook = 'pre-commit';
  const isWindows = os.platform() === 'win32';

  if (!isWindows) {
    // on linux set script executable
    console.info(`Setting git pre commit hook executable`);
    cp.execSync(`chmod +x  ${commitHooksPath}${preCommitHook}`);
  }

  // set git config
  console.info(`Setting git config hooks path to ${commitHooksPath}`);
  cp.execSync(`git config core.hooksPath ${commitHooksPath}`);

})();
