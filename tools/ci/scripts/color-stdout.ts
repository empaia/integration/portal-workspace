
import * as os from 'os';

const RED_COLOR = '\x1b[31m';
const GREEN_COLOR = '\x1b[32m';
const YELLOW_COLOR = '\x1b[33m';
const RESET_COLOR = '\x1b[0m';

function printError(msg: unknown) {
  process.stderr.write(`${RED_COLOR}${msg}${RESET_COLOR}${os.EOL}`);
}

function printWarn(msg: unknown) {
  process.stdout.write(`${YELLOW_COLOR}${msg}${RESET_COLOR}${os.EOL}`);
}

function printSuccess(msg: unknown) {
  process.stdout.write(`${GREEN_COLOR}${msg}${RESET_COLOR}${os.EOL}`);
}

function printInfo(msg: unknown) {
  process.stdout.write(`${YELLOW_COLOR}${msg}${RESET_COLOR}${os.EOL}`);
}

export { printError, printInfo, printSuccess, printWarn };