# Portal Workspace

This project uses [Nx](https://nx.dev).

[Nx Documentation](https://nx.dev/angular).

**This project uses Submodules!** Clone with `git clone --recurse-submodules`.

Update existing repo with `git submodule update --init --recursive`.

## Apps

### Portal v2

Run with:

`nx serve portal-v2`

Use [transloco](https://ngneat.github.io/transloco/docs/) for translatable text content.

### Keycloak-theme

Build a Keycloak theme from an Angular App. Uses keycloakify library included as submodule in this workspace.

Development & Merge request review:

1. start angular build in watch mode
   - `nx build keycloak-theme --configuration=dev-watch --watch`
2. setup dev deployment in global development repo
   - see: [Global Deployment](https://gitlab.com/empaia/integration/global-deployment/)
   - set variables for `aaa` in `.env`
     ```
     AAA_ENABLE_V2_API=true
     AAA_SWAGGER_SERVER_URL=http://localhost:39001
     ```
3. setup keycloak docker container in global development repo
   - set `KEYCLOAK_THEME_PATH` env var to point to output of keycloak theme project
   - add `http://portal-v2.localdev:4200` to `PORTAL_CLIENT_REDIRECT_URIS` in `.initialize.env`
   - Example `KEYCLOAK_THEME_PATH=/path/to/portal-workspace/dist/keycloak-theme/themes/empaia-material`
4. (optional) setup file watcher browser extension for hot reload
5. run portal-v2
   - `nx serve portal-v2 --host=portal-v2.localdev`
   - add `127.0.0.1 portal-v2.localdev` to hosts file

##### Prod Build:

Build with: `nx build keycloak-theme`. Output is found at `./dist/keycloak-theme`.

**Build Config**

The keycloak-theme project uses a custom webpack build config. The config can be found in the `/apps/keycloak-theme/build` directory and consists of two files:

1. `extra-webpack.config.ts`
   - copies assets and resources in the directory structure keycloak expects for its themes
   - copies theme.properties file
2. `index-html-transform.ts`
   - Transforms `index-template.html` to `*.ftl` files
   - **IMPORTANT** Keycloak pages that should be build need to be added to `pagesToBuild` array at the top.

#### Production

##### keycloak

Dockerfile `tools\docker\keycloak-theme\Dockerfile` is used for production builds.

Test build locally with:
`docker build -t keycloak -f .\tools\docker\keycloak-theme\Dockerfile . --build-arg PROJECT_NAME=keycloak-theme`

##### keycloak - mapper-scripts

Mapper scripts are automatically copied over to the docker image, also the required script engine via these lines:

```
COPY apps/keycloak-theme/mapper_scrips/script_engine/* /opt/keycloak/providers/
COPY apps/keycloak-theme/mapper_scrips/script_src/script_src.jar /opt/keycloak/providers/script_src.jar
```

if changes are necessary, re-zip `script_src.jar`. Just a zip container containeing the contents of `apps/keycloak-theme/mapper_scrips/script_src/` with a `.jar` extension.
make sure the jar contains everything from within `apps/keycloak-theme/mapper_scripts/script_src/` in the top level.

#### adding dynamic translated error messages

if a not yet translated info/warn/error message is encountered:

1. find the keycloak message id by searching for the message in  
   `apps\keycloak-theme\src\assets\i18n\login\en.json` or `apps\keycloak-theme\src\assets\i18n\login\de.json` respectively.
2. copy the message id as new translation into `apps\keycloak-theme\src\assets\messages\messages_de.properties` **and** `apps\keycloak-theme\src\assets\messages\messages_en.properties`. Follow the scheme already used (example: `loginTimeout=loginTimeout`)
3. add a desired message text into the transloco translation json files in the `kcMessages` section using the key just used. (example for de.json: "loginTimeout":"Login abgelaufen")

### email-template-generator

Build with: `nx build email-template-generator:compiler`

Server templates with `nx serve email-template-generator`

To mount templates to local docker container build them once then build the keycloak theme with `nx build keycloak-theme` to copy them to correct folder. Make sure to set `KEYCLOAK_THEME_PATH=/your/local/absolute/path/portal-workspace/dist/keycloak-theme/themes/empaia-material` in global deployment / dev deployment repo.

In Keycloak Admin Ui: Realm Setting -> Themes -> Login & Email Theme should have an option to select 'empaia-material' as Theme.

#### run compiler directly

`ts-node --project .\apps\email-template-generator\tsconfig.app.json .\apps\email-template-generator\src\compiler.ts `

## OS hosts file entries:

```
127.0.0.1 portal-v2.localdev
::1 portal-v2.localdev
```

- [Linux Hostfile](https://manpages.ubuntu.com/manpages/trusty/man5/hosts.5.html)

- [Windows Hostfile](https://www.groovypost.com/howto/edit-hosts-file-windows-10/)

# Notes

keycloak config json:

http://localhost:39000/auth/realms/empaia/.well-known/openid-configuration

Keycloak admin console fo master realm:

http://localhost:39000/auth/realms/master/protocol/openid-connect/auth

keycloak portal login:

http://localhost:39000/auth/realms/empaia/protocol/openid-connect/auth

swagger docs of aaa backend:

http://localhost:39001/docs/swagger-ui/index.html?configUrl=%2Fv3%2Fapi-docs%2Fswagger-config&urls.primaryName=v2#

mps swagger:

http://localhost:39005/v1/public/docs#/

http://localhost:39005/v1/customer/docs#/

http://localhost:39005/v1/vendor/docs#/

# maildev config:

```json
   "smtpServer": {
    "starttls": "",
    "auth": "",
    "port": "1025",
    "host": "maildev",
    "from": "admin@keycloak.dev",
    "ssl": ""
  },
```

# Insomnia

### Auth server:

Import schema from: http://localhost:39001/v3/api-docs/v2

env config:

```
{
   "scheme": "http",
   "base_path": "",
   "host": "localhost:39001",
   "oauth2ClientId": "swagger",
   "oauth2RedirectUrl": "http://localhost:39001",
   "oauth2ClientSecret": "clientSecret"
}
```

## playwright e2e test

one time setup (done):

- remove cypress projects from workspace `npx nx generate @nx/workspace:rm --project my-app-e2e`
- remove dependencies from `package.json`
- install helper package: `npm install -D @nxkit/playwright`
- add new e2e project with `nx generate @nxkit/playwright:project my-app-e2e --frontendProject my-app`
- add extra e2e runner configs

### commands

#### if already serving

- run `nx e2e portal-v2-e2e --skipServe`
- debug `nx debug portal-v2-e2e --skipServe`
- ui `nx run portal-v2-e2e:e2e --ui --skip-serve`

### specific test

`-g` for glob as last argument

e.g. only run login test and show browser to view test run
`nx e2e portal-v2-e2e -g login `

debug
`nx debug portal-v2-e2e -g login`

### general info

All tests depend on the language of the app being set to english! The app reads the browser language (should be english by default in the playwright browsers) ans sets the language accordingly.

#### files

`00-auth-setup.ts` file is run once before all other tests. this is configured via project dependencies in `playwright.config.ts`. Docs: https://playwright.dev/docs/test-global-setup-teardown#setup

The setup logs in all user defined in userList and saves the tokens returned on successful login (which are normally persisted to session storage) to files located at `.auth/${username}-ss.json`. These files with the valid tokens can then be reused in all other tests to authenticated as the previously logged in users. Please note that these tokens will be valid only for a limited time and that the `.auth/` folder must be gitignored!

`001-auth-setup-test.spec.ts` uses the saved tokens and tests if both users can login. To ease this process for further tests a fixture containing pages using the users auth data are created inside `fixtures/fixtures.ts`. These can be injected in the test function to skip repeating the same setup steps in every test.

`user-register-and-delete.spec.ts` does not use the tokens but tests registering a new users, confirming the verification email and deleting the user again. note that due to the configuration used the `00-auth-setup.ts` is still run in this test even if the data is not used.
Please note that if this test fails the created user must be deleted manually to re run the test!

This test currently fails because of an open issue with maildev: https://github.com/maildev/maildev/issues/457 . there is an open mr to resolve this issue if this mr does not get merged change the development mail server to a maintained one.

#### possible issues

- reusing the tokens could cause issues if the tests run longer than the tokens are valid. In this case using a test.beforeAll hook to log in the users could be considered. This would also eliminate the unnecessary run of the setup step for tests tha do not need it.

### ci e2e

the e2e test are currently not integrated into the ci pipeline

missing work that needs to be done:

- the current tests need to be run on changes to both the portal-v2 and the keycloak-theme project as the tests involve both projects

  - this would require changes on how ci pipeline triggers work
  - or run e2e test on every commit independent of changes to projects

- the test need to setup the global deployment in the ci pipeline

  - some work is done here: `/tools/ci/scripts/dev-deployment.sh`

- a new baseimage containing the playwright dependencies is needed

  - a new test image is here: `/tools/ci/test-baseimage/Dockerfile`
  - this should replace the current baseimage in `tools/ci/base-image/Dockerfile`

- the e2e test job must include (no work done here)
  1. build keycloak-theme and portal-v2 docker container with current changes
  2. replace image names in global-deployment `deployments/dev/docker-compose.yml` file with image just build in step 1
  3. start global dev deployment (see: `/tools/ci/scripts/dev-deployment.sh`)
  4. run e2e tests
