export type {
  KcContextBase,
  Attribute,
  Validators
} from "./KcContextBase";

export type {
  ExtendsKcContextBase,
} from "./getKcContextFromWindow";

export {
  getKcContextFromWindow
} from "./getKcContextFromWindow";


export { getKcContext } from "./getKcContext";
