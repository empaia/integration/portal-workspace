import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'common-ui-data-privacy-de',
  templateUrl: './data-privacy-de.component.html',
  styleUrls: ['./data-privacy-de.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DataPrivacyDeComponent {}
