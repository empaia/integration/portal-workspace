import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataPrivacyDeComponent } from './data-privacy-de.component';

describe('DataPrivacyDeComponent', () => {
  let component: DataPrivacyDeComponent;
  let fixture: ComponentFixture<DataPrivacyDeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DataPrivacyDeComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(DataPrivacyDeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
