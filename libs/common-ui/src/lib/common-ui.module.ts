import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataPrivacyComponent } from './data-privacy/data-privacy.component';
import { TermsOfUseComponent } from './terms-of-use/terms-of-use.component';
import { DataPrivacyDeComponent } from './data-privacy-de/data-privacy-de.component';
import { DataPrivacyEnComponent } from './data-privacy-en/data-privacy-en.component';
import { TermsOfUseDeComponent } from './terms-of-use-de/terms-of-use-de.component';
import { TermsOfUseEnComponent } from './terms-of-use-en/terms-of-use-en.component';
import { ImprintComponent } from './imprint/imprint.component';
import { ImprintDeComponent } from './imprint-de/imprint-de.component';
import { ImprintEnComponent } from './imprint-en/imprint-en.component';

@NgModule({
  imports: [CommonModule],
  declarations: [
    DataPrivacyComponent,
    TermsOfUseComponent,
    DataPrivacyDeComponent,
    DataPrivacyEnComponent,
    TermsOfUseDeComponent,
    TermsOfUseEnComponent,
    ImprintComponent,
    ImprintDeComponent,
    ImprintEnComponent,
  ],
  exports: [
    DataPrivacyComponent,
    TermsOfUseComponent,
    DataPrivacyDeComponent,
    DataPrivacyEnComponent,
    TermsOfUseDeComponent,
    TermsOfUseEnComponent,
    ImprintComponent,
    ImprintDeComponent,
    ImprintEnComponent,
  ],
})
export class CommonUiModule {}
