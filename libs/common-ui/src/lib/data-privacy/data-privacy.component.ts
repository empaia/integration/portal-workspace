import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'common-ui-data-privacy',
  templateUrl: './data-privacy.component.html',
  styleUrls: ['./data-privacy.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DataPrivacyComponent {
  @Input() public lang: string | undefined = undefined;
}
