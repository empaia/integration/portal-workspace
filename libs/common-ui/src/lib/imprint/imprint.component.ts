import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'common-ui-imprint',
  templateUrl: './imprint.component.html',
  styleUrls: ['./imprint.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImprintComponent {
  @Input() public lang: string | undefined = undefined;
}
