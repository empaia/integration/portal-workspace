import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsOfUseDeComponent } from './terms-of-use-de.component';

describe('TermsOfUseDeComponent', () => {
  let component: TermsOfUseDeComponent;
  let fixture: ComponentFixture<TermsOfUseDeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TermsOfUseDeComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TermsOfUseDeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
