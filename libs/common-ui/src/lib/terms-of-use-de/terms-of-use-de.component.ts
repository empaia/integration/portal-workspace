import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'common-ui-terms-of-use-de',
  templateUrl: './terms-of-use-de.component.html',
  styleUrls: ['./terms-of-use-de.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TermsOfUseDeComponent {}
