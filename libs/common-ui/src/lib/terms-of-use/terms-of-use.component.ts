import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'common-ui-terms-of-use',
  templateUrl: './terms-of-use.component.html',
  styleUrls: ['./terms-of-use.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TermsOfUseComponent {
  @Input() public lang: string | undefined = undefined;
}
