import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'common-ui-terms-of-use-en',
  templateUrl: './terms-of-use-en.component.html',
  styleUrls: ['./terms-of-use-en.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TermsOfUseEnComponent {}
