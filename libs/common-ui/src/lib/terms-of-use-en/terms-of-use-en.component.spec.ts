import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsOfUseEnComponent } from './terms-of-use-en.component';

describe('TermsOfUseEnComponent', () => {
  let component: TermsOfUseEnComponent;
  let fixture: ComponentFixture<TermsOfUseEnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TermsOfUseEnComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TermsOfUseEnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
