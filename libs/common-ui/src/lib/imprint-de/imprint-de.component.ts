import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'common-ui-imprint-de',
  templateUrl: './imprint-de.component.html',
  styleUrls: ['./imprint-de.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImprintDeComponent {}
