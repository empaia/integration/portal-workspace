import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'common-ui-imprint-en',
  templateUrl: './imprint-en.component.html',
  styleUrls: ['./imprint-en.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImprintEnComponent {}
