import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'common-ui-data-privacy-en',
  templateUrl: './data-privacy-en.component.html',
  styleUrls: ['./data-privacy-en.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DataPrivacyEnComponent {}
