import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataPrivacyEnComponent } from './data-privacy-en.component';

describe('DataPrivacyEnComponent', () => {
  let component: DataPrivacyEnComponent;
  let fixture: ComponentFixture<DataPrivacyEnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DataPrivacyEnComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(DataPrivacyEnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
